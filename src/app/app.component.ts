import { Component, ChangeDetectionStrategy } from '@angular/core';
import { ThemeService } from './core/services/theme.service';
import { AuthFacade } from './features/auth/+store/auth.facade';
import * as fromActions from './features/auth/+store/auth.actions';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  constructor(
    private themeService: ThemeService,
    private authFacade: AuthFacade,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    this.themeService.setTheme();
    this.authFacade.dispatch(fromActions.checkAuth());
    iconRegistry.addSvgIcon(
      'admin_panel_settings',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/svg/icon/admin_panel_settings.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'bubble_chart',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/svg/icon/bubble_chart.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'corporate_fare',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/svg/icon/corporate_fare.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'directions_car',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/svg/icon/directions_car.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'money',
      sanitizer.bypassSecurityTrustResourceUrl('assets/svg/icon/money.svg')
    );
    iconRegistry.addSvgIcon(
      'redeem',
      sanitizer.bypassSecurityTrustResourceUrl('assets/svg/icon/redeem.svg')
    );
    iconRegistry.addSvgIcon(
      'store',
      sanitizer.bypassSecurityTrustResourceUrl('assets/svg/icon/store.svg')
    );
    iconRegistry.addSvgIcon(
      'storefront',
      sanitizer.bypassSecurityTrustResourceUrl('assets/svg/icon/storefront.svg')
    );
    iconRegistry.addSvgIcon(
      'style',
      sanitizer.bypassSecurityTrustResourceUrl('assets/svg/icon/style.svg')
    );
    iconRegistry.addSvgIcon(
      'delete_forever',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/svg/icon/delete_forever.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'edit',
      sanitizer.bypassSecurityTrustResourceUrl('assets/svg/icon/edit.svg')
    );
    iconRegistry.addSvgIcon(
      'add',
      sanitizer.bypassSecurityTrustResourceUrl('assets/svg/icon/add.svg')
    );
    iconRegistry.addSvgIcon(
      'keyboard_arrow_down',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/svg/icon/keyboard_arrow_down.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'payment',
      sanitizer.bypassSecurityTrustResourceUrl('assets/svg/icon/payment.svg')
    );
  }
}

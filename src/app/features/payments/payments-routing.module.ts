import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreatePaymentComponent } from './create-payment/create-payment.component';
import { PaymentsListComponent } from './payments-list/payments-list.component';

import { PaymentsComponent } from './payments.component';

const routes: Routes = [
  {
    path: '',
    component: PaymentsComponent,
    children: [
      { path: '', redirectTo: 'payments-list' },
      { path: 'payments-list', component: PaymentsListComponent },
      { path: 'create-payment', component: CreatePaymentComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsRoutingModule {}

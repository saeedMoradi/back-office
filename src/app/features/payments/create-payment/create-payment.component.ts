import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormGroupDirective
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PaymentsFacade } from '../+store/payments.facade';
import { PaymentsService } from '../services/payments.service';
import * as fromActions from '../+store/payments.actions';

@Component({
  selector: 'app-create-payment',
  templateUrl: './create-payment.component.html',
  styleUrls: ['./create-payment.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreatePaymentComponent implements OnInit {
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  paymentForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public paymentsFacade: PaymentsFacade,
    private paymentsService: PaymentsService
  ) {}

  ngOnInit() {
    this.initializepaymentForm();
    this.listenFormReset();
  }

  initializepaymentForm() {
    this.paymentForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]]
    });
  }

  listenFormReset() {
    this.paymentsService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetPaymentForm();
      });
  }

  resetPaymentForm() {
    this.formDirective.resetForm();
    this.paymentForm.reset();
  }

  onPayment() {
    this.paymentForm.markAllAsTouched();
    if (this.paymentForm.invalid) {
      return;
    }
    this.paymentsFacade.dispatch(
      fromActions.createPayment({ request: this.paymentForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

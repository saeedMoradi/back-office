import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-edit-payment',
  templateUrl: './edit-payment.component.html',
  styleUrls: ['./edit-payment.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditPaymentComponent implements OnInit {
  paymentForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditPaymentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.initializePaymentForm();
  }

  initializePaymentForm() {
    this.paymentForm = this.formBuilder.group({
      title: [this.data.title, [Validators.required]],
      description: [this.data.description, [Validators.required]]
    });
  }

  onPayment() {
    this.paymentForm.markAllAsTouched();
    if (this.paymentForm.invalid) {
      return;
    }
    this.dialogRef.close(this.paymentForm.value);
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

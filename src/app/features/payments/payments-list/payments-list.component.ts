import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { PaymentsFacade } from '../+store/payments.facade';
import {
  getPayments,
  deletePayment,
  putPayment
} from '../+store/payments.actions';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PaginationData } from 'src/app/shared/interfaces';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { EditPaymentComponent } from '../edit-payment/edit-payment.component';

@Component({
  selector: 'app-payments-list',
  templateUrl: './payments-list.component.html',
  styleUrls: ['./payments-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentsListComponent implements OnInit {
  payments: any[];
  originalpayments: any;
  uniqueKey: any;
  destroy$ = new Subject();

  constructor(
    public paymentsFacade: PaymentsFacade,
    private cd: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.paymentsFacade.dispatch(getPayments({ page: 'page=1' }));
    this.paymentsFacade.payments$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any[]) => {
        if (data) {
          this.payments = data;
          this.originalpayments = data;
          data.forEach(element => {
            this.uniqueKey = {
              ...this.uniqueKey,
              [element.id]: false
            };
          });
          this.cd.detectChanges();
        }
      });
  }

  search(term = '') {
    this.payments = this.originalpayments.filter((element: any) => {
      return (
        element.title.toLowerCase().includes(term) ||
        element.description.toLowerCase().includes(term)
      );
    });
  }

  onRemove(data: any) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.paymentsFacade.dispatch(deletePayment({ id: data.id }));
      }
    });
  }

  onEdit(data: any) {
    const dialogRef = this.dialog.open(EditPaymentComponent, {
      autoFocus: false,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.paymentsFacade.dispatch(
          putPayment({ id: data.id, request: result })
        );
      }
    });
  }

  onChangePage(event: PaginationData) {
    this.paymentsFacade.dispatch(
      getPayments({ page: `page=${event.pageIndex + 1}` })
    );
  }

  trackByFn(index: number, item: any) {
    return index;
  }

  trackByFnDetails(index: number, item: any) {
    return index;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

import { createSelector, createFeatureSelector } from '@ngrx/store';
import { PaymentsState } from './payments.interface';
import { featureKey } from './payments.reducer';

export const selectFeature = createFeatureSelector<PaymentsState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: PaymentsState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: PaymentsState) => state.isLoading
);
export const selectPayments = createSelector(
  selectFeature,
  (state: PaymentsState) =>
    state.payments ? state.payments['hydra:member'] : null
);
export const selectPaymentsTotalLength = createSelector(
  selectFeature,
  (state: PaymentsState) =>
    state.payments ? state.payments['hydra:totalItems'] : null
);

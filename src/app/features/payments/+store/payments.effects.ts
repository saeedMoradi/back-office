import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './payments.actions';
import { PaymentsService } from '../services/payments.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Injectable()
export class PaymentsEffects {
  constructor(
    private actions$: Actions,
    private paymentsService: PaymentsService,
    private matSnackBar: MatSnackBar,
    private router: Router
  ) {}

  getPayments$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getPayments),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.paymentsService.getPayments(page).pipe(
          map((data: any) => {
            return fromActions.getPaymentsSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getPaymentsFail({ error })))
        );
      })
    )
  );

  createPayment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createPayment),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.paymentsService.createPayment(request).pipe(
          map((data: any) => {
            this.paymentsService.clearForm();
            this.matSnackBar.open('Payment Created');
            this.router.navigate(['/home/payments/payments-list']);
            return fromActions.createPaymentsuccess({ response: data });
          }),
          catchError(error => of(fromActions.createPaymentFail({ error })))
        );
      })
    )
  );

  deletePayment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deletePayment),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.paymentsService.deletePayment(id).pipe(
          map((data: any) => {
            return fromActions.deletePaymentsuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deletePaymentFail({ error }));
          })
        );
      })
    )
  );

  putPayment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putPayment),
      switchMap(({ id, request }) => {
        return this.paymentsService.putPayment(id, request).pipe(
          map((data: any) => {
            return fromActions.putPaymentsuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putPaymentFail({ error }));
          })
        );
      })
    )
  );
}

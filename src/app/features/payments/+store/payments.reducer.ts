import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './payments.actions';
import { PaymentsState } from './payments.interface';

export const initialState: PaymentsState = {
  error: null,
  isLoading: false,
  payments: null
};

export const featureKey = 'payments';

const paymentsReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): PaymentsState => ({ ...state, error: false })
  ),
  on(
    fromActions.getPaymentsSuccess,
    (state, action): PaymentsState => ({
      ...state,
      error: null,
      payments: action.response
    })
  ),
  on(
    fromActions.getPaymentsFail,
    (state, action): PaymentsState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createPayment,
    (state, action): PaymentsState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createPaymentsuccess,
    (state, action): PaymentsState => ({
      ...state,
      error: null,
      isLoading: false,
      payments: {
        ...state.payments,
        'hydra:member': [action.response, ...state.payments['hydra:member']],
        'hydra:totalItems': state.payments['hydra:totalItems'] + 1
      }
    })
  ),
  on(
    fromActions.createPaymentFail,
    (state, action): PaymentsState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deletePaymentsuccess,
    (state, action): PaymentsState => {
      const newpayments = state.payments['hydra:member'].filter((item: any) => {
        return item.id !== action.id;
      });
      return {
        ...state,
        error: null,
        payments: {
          ...state.payments,
          'hydra:member': newpayments,
          'hydra:totalItems': state.payments['hydra:totalItems'] - 1
        }
      };
    }
  ),
  on(
    fromActions.putPaymentsuccess,
    (state, action): PaymentsState => {
      const newpayments = state.payments['hydra:member'].map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        payments: { ...state.payments, 'hydra:member': newpayments }
      };
    }
  )
);

export function reducer(state: PaymentsState, action: Action) {
  return paymentsReducer(state, action);
}

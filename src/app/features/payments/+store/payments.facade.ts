import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './payments.selectors';

@Injectable({
  providedIn: 'root'
})
export class PaymentsFacade implements Facade {
  isloading$: Observable<boolean>;
  error$: Observable<string>;
  payments$: Observable<any[]>;
  paymentsTotalLength$: Observable<number>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.payments$ = store.pipe(select(fromSelector.selectPayments));
    this.paymentsTotalLength$ = store.pipe(
      select(fromSelector.selectPaymentsTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

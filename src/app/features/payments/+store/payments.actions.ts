import { createAction, props } from '@ngrx/store';

export const getPayments = createAction(
  '[Payments] Get Payments',
  props<{ page: string }>()
);
export const getPaymentsSuccess = createAction(
  '[Payments] Get Payments Success',
  props<{ response: any }>()
);
export const getPaymentsFail = createAction(
  '[Payments] Get Payments Fail',
  props<{ error: any }>()
);

export const createPayment = createAction(
  '[Payments] Create Payment',
  props<{ request: any }>()
);
export const createPaymentsuccess = createAction(
  '[Payments] Create Payment Success',
  props<{ response: any }>()
);
export const createPaymentFail = createAction(
  '[Payments] Create Payment Fail',
  props<{ error: any }>()
);

export const getPayment = createAction(
  '[Payments] Get Payment',
  props<{ id: any }>()
);
export const getPaymentsuccess = createAction(
  '[Payments] Get Payment Success',
  props<{ response: any }>()
);
export const getPaymentFail = createAction(
  '[Payments] Get Payment Fail',
  props<{ error: any }>()
);

export const deletePayment = createAction(
  '[Payments] Delete Payment',
  props<{ id: any }>()
);
export const deletePaymentsuccess = createAction(
  '[Payments] Delete Payment Success',
  props<{ id: any }>()
);
export const deletePaymentFail = createAction(
  '[Payments] Delete Payment Fail',
  props<{ error: any }>()
);

export const putPayment = createAction(
  '[Payments] Put Payment',
  props<{ id: any; request: any }>()
);
export const putPaymentsuccess = createAction(
  '[Payments] Put Payment Success',
  props<{ response: any }>()
);
export const putPaymentFail = createAction(
  '[Payments] Put Payment Fail',
  props<{ error: any }>()
);

export const patchPayment = createAction(
  '[Payments] Patch Payment',
  props<{ id: any; request: any }>()
);
export const patchPaymentsuccess = createAction(
  '[Payments] Patch Payment Success',
  props<{ response: any }>()
);
export const patchPaymentFail = createAction(
  '[Payments] Patch Payment Fail',
  props<{ error: any }>()
);

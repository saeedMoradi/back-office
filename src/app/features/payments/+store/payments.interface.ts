export interface PaymentsState {
  error: any;
  isLoading: boolean;
  payments: any;
}

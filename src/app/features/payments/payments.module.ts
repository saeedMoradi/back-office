import { NgModule } from '@angular/core';

import { PaymentsRoutingModule } from './payments-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
// Components
import { PaymentsComponent } from './payments.component';
import { CreatePaymentComponent } from './create-payment/create-payment.component';
import { PaymentsListComponent } from './payments-list/payments-list.component';
import { EditPaymentComponent } from './edit-payment/edit-payment.component';
// NGRX
// import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { PaymentsEffects } from './+store/payments.effects';
// import * as fromReducer from './+store/payments.reducer';

@NgModule({
  declarations: [
    PaymentsComponent,
    CreatePaymentComponent,
    PaymentsListComponent,
    EditPaymentComponent
  ],
  imports: [
    SharedModule,
    PaymentsRoutingModule,
    // StoreModule.forFeature(fromReducer.featureKey, fromReducer.reducer),
    EffectsModule.forFeature([PaymentsEffects])
  ]
})
export class PaymentsModule {}

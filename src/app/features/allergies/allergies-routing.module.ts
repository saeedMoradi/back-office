import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllergiesListComponent } from './allergies-list/allergies-list.component';

import { AllergiesComponent } from './allergies.component';
import { CreateAllergyComponent } from './create-allergy/create-allergy.component';

const routes: Routes = [
  {
    path: '',
    component: AllergiesComponent,
    children: [
      { path: '', redirectTo: 'allergies-list' },
      { path: 'allergies-list', component: AllergiesListComponent },
      { path: 'create-allergy', component: CreateAllergyComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllergiesRoutingModule {}

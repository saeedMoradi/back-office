import * as allergiesReducer from './allergies.reducer';
import { AllergiesState } from './allergies.interface';
import * as allergiesSelectors from './allergies.selectors';

export { allergiesReducer, allergiesSelectors, AllergiesState };

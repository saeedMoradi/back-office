export interface AllergiesState {
  error: any;
  isLoading: boolean;
  allergies: any;
}

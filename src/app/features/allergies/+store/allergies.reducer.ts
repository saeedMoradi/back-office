import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './allergies.actions';
import { AllergiesState } from './allergies.interface';

export const initialState: AllergiesState = {
  error: null,
  isLoading: false,
  allergies: null
};

export const featureKey = 'allergies';

const allergiesReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): AllergiesState => ({ ...state, error: null })
  ),
  on(
    fromActions.getAllergiesSuccess,
    (state, action): AllergiesState => ({
      ...state,
      error: null,
      allergies: action.response
    })
  ),
  on(
    fromActions.getAllergiesFail,
    (state, action): AllergiesState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createAllergy,
    (state, action): AllergiesState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createAllergySuccess,
    (state, action): AllergiesState => {
      if (!state.allergies) {
        return {
          ...state,
          allergies: {
            'hydra:member': [action.response],
            'hydra:totalItems': 1
          }
        };
      }
      return {
        ...state,
        error: null,
        isLoading: false,
        allergies: {
          ...state.allergies,
          'hydra:member': [action.response, ...state.allergies['hydra:member']],
          'hydra:totalItems': state.allergies['hydra:totalItems'] + 1
        }
      };
    }
  ),
  on(
    fromActions.createAllergyFail,
    (state, action): AllergiesState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteAllergySuccess,
    (state, action): AllergiesState => {
      const newallergies = state.allergies['hydra:member'].filter(
        (item: any) => {
          return item.id !== action.id;
        }
      );
      return {
        ...state,
        error: null,
        allergies: {
          ...state.allergies,
          'hydra:member': newallergies,
          'hydra:totalItems': state.allergies['hydra:totalItems'] - 1
        }
      };
    }
  ),
  on(
    fromActions.putAllergySuccess,
    (state, action): AllergiesState => {
      const newallergies = state.allergies['hydra:member'].map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        allergies: { ...state.allergies, 'hydra:member': newallergies }
      };
    }
  )
);

export function reducer(state: AllergiesState, action: Action) {
  return allergiesReducer(state, action);
}

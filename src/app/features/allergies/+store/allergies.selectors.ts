import { createSelector, createFeatureSelector } from '@ngrx/store';
import { AllergiesState } from './allergies.interface';
import { featureKey } from './allergies.reducer';

export const selectFeature = createFeatureSelector<AllergiesState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: AllergiesState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: AllergiesState) => state.isLoading
);
export const selectAllergies = createSelector(
  selectFeature,
  (state: AllergiesState) =>
    state.allergies ? state.allergies['hydra:member'] : null
);
export const selectAllergiesTotalLength = createSelector(
  selectFeature,
  (state: AllergiesState) =>
    state.allergies ? state.allergies['hydra:totalItems'] : null
);

import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { AllergiesService } from '../services/allergies.service';
import * as fromActions from './allergies.actions';

@Injectable()
export class AllergiesEffects {
  constructor(
    private actions$: Actions,
    private allergiesService: AllergiesService,
    private matSnackBar: MatSnackBar,
    private router: Router
  ) {}

  getAllergies$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getAllergies),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.allergiesService.getAllergies(page).pipe(
          map((data: any) => {
            return fromActions.getAllergiesSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.getAllergiesFail({ error }));
          })
        );
      })
    )
  );

  createAllergy$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createAllergy),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.allergiesService.createAllergy(request).pipe(
          map((data: any) => {
            this.allergiesService.clearForm();
            this.matSnackBar.open('Allergy Created');
            this.router.navigate(['/home/allergies/allergies-list']);
            return fromActions.createAllergySuccess({ response: data });
          }),
          catchError(error => of(fromActions.createAllergyFail({ error })))
        );
      })
    )
  );

  getAllergy$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getAllergy),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.allergiesService.getAllergy(id).pipe(
          map((data: any) => {
            return fromActions.getAllergySuccess({ response: data });
          }),
          catchError(error => of(fromActions.getAllergyFail({ error })))
        );
      })
    )
  );

  deleteAllergy$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteAllergy),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.allergiesService.deleteAllergy(id).pipe(
          map((data: any) => {
            return fromActions.deleteAllergySuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteAllergyFail({ error }));
          })
        );
      })
    )
  );

  putAllergy$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putAllergy),
      switchMap(({ id, request }) => {
        return this.allergiesService.putAllergy(id, request).pipe(
          map((data: any) => {
            return fromActions.putAllergySuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putAllergyFail({ error }));
          })
        );
      })
    )
  );

  patchAllergy$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchAllergy),
      switchMap(({ id, request }) => {
        return this.allergiesService.patchAllergy(id, request).pipe(
          map((data: any) => {
            return fromActions.patchAllergySuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.patchAllergyFail({ error }));
          })
        );
      })
    )
  );
}

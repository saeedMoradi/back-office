import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './allergies.selectors';

@Injectable({
  providedIn: 'root'
})
export class AllergiesFacade implements Facade {
  isloading$: Observable<boolean>;
  error$: Observable<any>;
  allergies$: Observable<any>;
  allergiesTotalLength$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.allergies$ = store.pipe(select(fromSelector.selectAllergies));
    this.allergiesTotalLength$ = store.pipe(
      select(fromSelector.selectAllergiesTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

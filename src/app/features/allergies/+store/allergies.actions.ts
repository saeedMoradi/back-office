import { createAction, props } from '@ngrx/store';

export const getAllergies = createAction(
  '[Allergies] Get Allergies',
  props<{ page: any }>()
);
export const getAllergiesSuccess = createAction(
  '[Allergies] Get Allergies Success',
  props<{ response: any }>()
);
export const getAllergiesFail = createAction(
  '[Allergies] Get Allergies Fail',
  props<{ error: any }>()
);

export const createAllergy = createAction(
  '[Allergies] Create Allergy',
  props<{ request: any }>()
);
export const createAllergySuccess = createAction(
  '[Allergies] Create Allergy Success',
  props<{ response: any }>()
);
export const createAllergyFail = createAction(
  '[Allergies] Create Allergy Fail',
  props<{ error: any }>()
);

export const getAllergy = createAction(
  '[Allergies] Get Allergy',
  props<{ id: any }>()
);
export const getAllergySuccess = createAction(
  '[Allergies] Get Allergy Success',
  props<{ response: any }>()
);
export const getAllergyFail = createAction(
  '[Allergies] Get Allergy Fail',
  props<{ error: any }>()
);

export const deleteAllergy = createAction(
  '[Allergies] Delete Allergy',
  props<{ id: any }>()
);
export const deleteAllergySuccess = createAction(
  '[Allergies] Delete Allergy Success',
  props<{ id: any }>()
);
export const deleteAllergyFail = createAction(
  '[Allergies] Delete Allergy Fail',
  props<{ error: any }>()
);

export const putAllergy = createAction(
  '[Allergies] Put Allergy',
  props<{ id: any; request: any }>()
);
export const putAllergySuccess = createAction(
  '[Allergies] Put Allergy Success',
  props<{ response: any }>()
);
export const putAllergyFail = createAction(
  '[Allergies] Put Allergy Fail',
  props<{ error: any }>()
);

export const patchAllergy = createAction(
  '[Allergies] Patch Allergy',
  props<{ id: any; request: any }>()
);
export const patchAllergySuccess = createAction(
  '[Allergies] Patch Allergy Success',
  props<{ response: any }>()
);
export const patchAllergyFail = createAction(
  '[Allergies] Patch Allergy Fail',
  props<{ error: any }>()
);

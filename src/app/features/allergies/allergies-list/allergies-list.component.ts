import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { PaginationData } from 'src/app/shared/interfaces';
import { AllergiesFacade } from '../+store/allergies.facade';
import {
  getAllergies,
  deleteAllergy,
  putAllergy
} from '../+store/allergies.actions';
import { EditAllergyComponent } from '../edit-allergy/edit-allergy.component';

@Component({
  selector: 'app-allergies-list',
  templateUrl: './allergies-list.component.html',
  styleUrls: ['./allergies-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AllergiesListComponent implements OnInit {
  allergies: any;
  originalAllergies: any;
  uniqueKey: any;
  destroy$ = new Subject();

  constructor(
    public allergiesFacade: AllergiesFacade,
    private cd: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.allergiesFacade.dispatch(getAllergies({ page: 'page=1' }));
    this.allergiesFacade.allergies$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        if (data) {
          this.allergies = data;
          this.originalAllergies = data;
          data.forEach(element => {
            this.uniqueKey = {
              ...this.uniqueKey,
              [element.id]: false
            };
          });
          this.cd.detectChanges();
        }
      });
  }

  search(term = '') {
    this.allergies = this.originalAllergies.filter((element: any) => {
      return (
        element.title.toLowerCase().includes(term) ||
        element.description.toLowerCase().includes(term)
      );
    });
  }

  onRemove(data) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.allergiesFacade.dispatch(deleteAllergy({ id: data.id }));
      }
    });
  }

  onEdit(data) {
    const dialogRef = this.dialog.open(EditAllergyComponent, {
      autoFocus: false,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.allergiesFacade.dispatch(
          putAllergy({ id: data.id, request: result })
        );
      }
    });
  }

  onExpand(data) {
    this.uniqueKey = {
      ...this.uniqueKey,
      [data.id]: !this.uniqueKey[data.id]
    };
  }

  onChangePage(event: PaginationData) {
    this.allergiesFacade.dispatch(
      getAllergies({ page: `page=${event.pageIndex + 1}` })
    );
  }

  trackByFn(index: number, item: any) {
    return index;
  }

  trackByFnDetails(index: number, item: any) {
    return index;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

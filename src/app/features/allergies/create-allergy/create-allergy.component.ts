import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormGroupDirective,
  Validators
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AllergiesFacade } from '../+store/allergies.facade';
import * as fromActions from '../+store/allergies.actions';
import { AllergiesService } from '../services/allergies.service';

@Component({
  selector: 'app-create-allergy',
  templateUrl: './create-allergy.component.html',
  styleUrls: ['./create-allergy.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateAllergyComponent implements OnInit {
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  allergyForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public allergiesFacade: AllergiesFacade,
    private allergiesService: AllergiesService
  ) {}

  ngOnInit() {
    this.initializeAllergyForm();
    this.listenFormReset();
  }

  initializeAllergyForm() {
    this.allergyForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]]
    });
  }

  listenFormReset() {
    this.allergiesService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetAllergyForm();
      });
  }

  resetAllergyForm() {
    this.formDirective.resetForm();
    this.allergyForm.reset();
  }

  onAllergy() {
    this.allergyForm.markAllAsTouched();
    if (this.allergyForm.invalid) {
      return;
    }
    this.allergiesFacade.dispatch(
      fromActions.createAllergy({ request: this.allergyForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

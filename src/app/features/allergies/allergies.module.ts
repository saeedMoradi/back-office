import { NgModule } from '@angular/core';

import { AllergiesRoutingModule } from './allergies-routing.module';
import { AllergiesComponent } from './allergies.component';
import { AllergiesListComponent } from './allergies-list/allergies-list.component';
import { CreateAllergyComponent } from './create-allergy/create-allergy.component';
import { SharedModule } from 'src/app/shared/shared.module';
// import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AllergiesEffects } from './+store/allergies.effects';
import { EditAllergyComponent } from './edit-allergy/edit-allergy.component';
// import * as fromReducer from './+store/allergies.reducer';

@NgModule({
  declarations: [
    AllergiesComponent,
    AllergiesListComponent,
    CreateAllergyComponent,
    EditAllergyComponent
  ],
  imports: [
    SharedModule,
    AllergiesRoutingModule,
    // StoreModule.forFeature(fromReducer.featureKey, fromReducer.reducer),
    EffectsModule.forFeature([AllergiesEffects])
  ]
})
export class AllergiesModule {}

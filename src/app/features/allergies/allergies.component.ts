import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-allergies',
  templateUrl: './allergies.component.html',
  styleUrls: ['./allergies.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AllergiesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

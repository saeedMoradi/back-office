import { createAction, props } from '@ngrx/store';

export const getManagers = createAction(
  '[Managers] Get Managers',
  props<{ page: any }>()
);
export const getManagersSuccess = createAction(
  '[Managers] Get Managers Success',
  props<{ response: any }>()
);
export const getManagersFail = createAction(
  '[Managers] Get Managers Fail',
  props<{ error: any }>()
);

export const createManager = createAction(
  '[Managers] Create Manager',
  props<{ request: any }>()
);
export const createManagerSuccess = createAction(
  '[Managers] Create Manager Success',
  props<{ response: any }>()
);
export const createManagerFail = createAction(
  '[Managers] Create Manager Fail',
  props<{ error: any }>()
);

export const getManager = createAction(
  '[Managers] Get Manager',
  props<{ id: any }>()
);
export const getManagerSuccess = createAction(
  '[Managers] Get Manager Success',
  props<{ response: any }>()
);
export const getManagerFail = createAction(
  '[Managers] Get Manager Fail',
  props<{ error: any }>()
);

export const deleteManager = createAction(
  '[Managers] Delete Manager',
  props<{ id: any }>()
);
export const deleteManagerSuccess = createAction(
  '[Managers] Delete Manager Success',
  props<{ id: any }>()
);
export const deleteManagerFail = createAction(
  '[Managers] Delete Manager Fail',
  props<{ error: any }>()
);

export const putManager = createAction(
  '[Managers] Put Manager',
  props<{ id: any; request: any }>()
);
export const putManagerSuccess = createAction(
  '[Managers] Put Manager Success',
  props<{ response: any }>()
);
export const putManagerFail = createAction(
  '[Managers] Put Manager Fail',
  props<{ error: any }>()
);

export const patchManager = createAction(
  '[Managers] Patch Manager',
  props<{ id: any; request: any }>()
);
export const patchManagerSuccess = createAction(
  '[Managers] Patch Manager Success',
  props<{ response: any }>()
);
export const patchManagerFail = createAction(
  '[Managers] Patch Manager Fail',
  props<{ error: any }>()
);

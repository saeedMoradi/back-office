import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './managers.actions';
import { ManagersState } from './managers.interface';

export const initialState: ManagersState = {
  error: null,
  isLoading: false,
  managers: null
};

export const featureKey = 'managers';

const managersReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): ManagersState => ({ ...state, error: null })
  ),
  on(
    fromActions.getManagersSuccess,
    (state, action): ManagersState => ({
      ...state,
      error: null,
      managers: action.response
    })
  ),
  on(
    fromActions.getManagersFail,
    (state, action): ManagersState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createManager,
    (state, action): ManagersState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createManagerSuccess,
    (state, action): ManagersState => {
      return {
        ...state,
        error: null,
        isLoading: false
        // managers: {
        //   ...state.managers,
        //   'hydra:member': [action.response, ...state.managers['hydra:member']],
        //   'hydra:totalItems': state.managers['hydra:totalItems'] + 1
        // }
      };
    }
  ),
  on(
    fromActions.createManagerFail,
    (state, action): ManagersState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteManagerSuccess,
    (state, action): ManagersState => {
      const newmanagers = state.managers['hydra:member'].filter((item: any) => {
        return item.id !== action.id;
      });
      return {
        ...state,
        error: null,
        managers: {
          ...state.managers,
          'hydra:member': newmanagers,
          'hydra:totalItems': state.managers['hydra:totalItems'] - 1
        }
      };
    }
  ),
  on(
    fromActions.putManagerSuccess,
    (state, action): ManagersState => {
      const newmanagers = state.managers['hydra:member'].map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        managers: { ...state.managers, 'hydra:member': newmanagers }
      };
    }
  )
);

export function reducer(state: ManagersState, action: Action) {
  return managersReducer(state, action);
}

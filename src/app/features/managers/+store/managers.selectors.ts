import { createSelector, createFeatureSelector } from '@ngrx/store';
import { ManagersState } from './managers.interface';
import { featureKey } from './managers.reducer';

export const selectFeature = createFeatureSelector<ManagersState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: ManagersState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: ManagersState) => state.isLoading
);
export const selectManagers = createSelector(
  selectFeature,
  (state: ManagersState) =>
    state.managers ? state.managers['hydra:member'] : null
);
export const selectManagersTotalLength = createSelector(
  selectFeature,
  (state: ManagersState) =>
    state.managers ? state.managers['hydra:totalItems'] : null
);

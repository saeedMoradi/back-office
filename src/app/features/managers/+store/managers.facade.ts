import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './managers.selectors';

@Injectable({
  providedIn: 'root'
})
export class ManagersFacade implements Facade {
  isLoading$: Observable<boolean>;
  error$: Observable<any>;
  managers$: Observable<any>;
  managersTotalLength$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.isLoading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.managers$ = store.pipe(select(fromSelector.selectManagers));
    this.managersTotalLength$ = store.pipe(
      select(fromSelector.selectManagersTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

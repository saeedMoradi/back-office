import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './managers.actions';
import { ManagersService } from '../services/managers.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../auth/services/auth.service';

@Injectable()
export class ManagersEffects {
  constructor(
    private actions$: Actions,
    private managersService: ManagersService,
    private authService: AuthService,
    private router: Router,
    private matSnackBar: MatSnackBar
  ) {}

  getManagers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getManagers),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.managersService.getManagers(page).pipe(
          map((data: any) => {
            return fromActions.getManagersSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getManagersFail({ error })))
        );
      })
    )
  );

  createManager$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createManager),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.authService.register(request).pipe(
          switchMap((user: any) => {
            return this.managersService
              .createManager({
                user: '/api/users/' + user.id,
                shops: request.shops
              })
              .pipe(
                map((data: any) => {
                  this.managersService.clearForm();
                  this.matSnackBar.open('Manager Created');
                  this.router.navigate(['/home/managers/managers-list']);
                  return fromActions.createManagerSuccess({ response: data });
                }),
                catchError(error =>
                  of(fromActions.createManagerFail({ error }))
                )
              );
          })
        );
      })
    )
  );

  getManager$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getManager),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.managersService.getManager(id).pipe(
          map((data: any) => {
            return fromActions.getManagerSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getManagerFail({ error })))
        );
      })
    )
  );

  deleteManager$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteManager),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.managersService.deleteManager(id).pipe(
          map((data: any) => {
            return fromActions.deleteManagerSuccess({ id });
          }),
          catchError(error => of(fromActions.deleteManagerFail({ error })))
        );
      })
    )
  );

  putManager$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putManager),
      switchMap(({ id, request }) => {
        return this.managersService.putManager(id, request).pipe(
          map((data: any) => {
            return fromActions.putManagerSuccess({ response: data });
          }),
          catchError(error => of(fromActions.putManagerFail({ error })))
        );
      })
    )
  );

  patchManager$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchManager),
      switchMap(({ id, request }) => {
        return this.managersService.patchManager(id, request).pipe(
          map((data: any) => {
            return fromActions.patchManagerSuccess({ response: data });
          }),
          catchError(error => of(fromActions.patchManagerFail({ error })))
        );
      })
    )
  );
}

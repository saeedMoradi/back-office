import * as managersReducer from './managers.reducer';
import { ManagersState } from './managers.interface';
import * as managersSelectors from './managers.selectors';

export { managersReducer, managersSelectors, ManagersState };

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateManagerComponent } from './create-manager/create-manager.component';
import { ManagersListComponent } from './managers-list/managers-list.component';

import { ManagersComponent } from './managers.component';

const routes: Routes = [
  {
    path: '',
    component: ManagersComponent,
    children: [
      { path: '', redirectTo: 'managers-list' },
      { path: 'managers-list', component: ManagersListComponent },
      { path: 'create-manager', component: CreateManagerComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagersRoutingModule {}

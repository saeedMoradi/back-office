import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-managers',
  templateUrl: './managers.component.html',
  styleUrls: ['./managers.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManagersComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

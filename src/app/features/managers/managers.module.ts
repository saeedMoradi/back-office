import { NgModule } from '@angular/core';

import { ManagersRoutingModule } from './managers-routing.module';
import { ManagersComponent } from './managers.component';
import { ManagersListComponent } from './managers-list/managers-list.component';
import { CreateManagerComponent } from './create-manager/create-manager.component';
import { SharedModule } from 'src/app/shared/shared.module';
// import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ManagersEffects } from './+store/managers.effects';
// import * as fromReducer from './+store/managers.reducer';
import { ShopsEffects } from '../shops/+store/shops.effects';
import { EditManagerComponent } from './edit-manager/edit-manager.component';

@NgModule({
  declarations: [
    ManagersComponent,
    ManagersListComponent,
    CreateManagerComponent,
    EditManagerComponent
  ],
  imports: [
    SharedModule,
    ManagersRoutingModule,
    // StoreModule.forFeature(fromReducer.featureKey, fromReducer.reducer),
    EffectsModule.forFeature([ManagersEffects, ShopsEffects])
  ]
})
export class ManagersModule {}

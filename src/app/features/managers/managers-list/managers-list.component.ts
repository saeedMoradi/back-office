import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { PaginationData } from 'src/app/shared/interfaces';
import { ManagersFacade } from '../+store/managers.facade';
import { getManagers, deleteManager } from '../+store/managers.actions';
import { EditManagerComponent } from '../edit-manager/edit-manager.component';
import { AuthFacade } from '../../auth/+store/auth.facade';
import { putUser } from '../../auth/+store/auth.actions';

@Component({
  selector: 'app-managers-list',
  templateUrl: './managers-list.component.html',
  styleUrls: ['./managers-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManagersListComponent implements OnInit {
  managers: any;
  originalManagers: any;
  uniqueKey: any;
  destroy$ = new Subject();

  constructor(
    public managersFacade: ManagersFacade,
    public authFacade: AuthFacade,
    private cd: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.managersFacade.dispatch(getManagers({ page: 'page=1' }));
    this.managersFacade.managers$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        if (data) {
          this.managers = data;
          this.originalManagers = data;
          data.forEach(element => {
            this.uniqueKey = {
              ...this.uniqueKey,
              [element.id]: false
            };
          });
          this.cd.detectChanges();
        }
      });
  }

  search(term = '') {
    this.managers = this.originalManagers.filter((element: any) => {
      return (
        element.user.firstName.toLowerCase().includes(term) ||
        element.user.lastName.toLowerCase().includes(term)
      );
    });
  }

  onRemove(data) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.managersFacade.dispatch(deleteManager({ id: data.id }));
      }
    });
  }

  onEdit(data) {
    const dialogRef = this.dialog.open(EditManagerComponent, {
      autoFocus: false,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.authFacade.dispatch(
          putUser({ id: data.user.id, request: result })
        );
      }
    });
  }

  onExpand(data) {
    this.uniqueKey = {
      ...this.uniqueKey,
      [data.id]: !this.uniqueKey[data.id]
    };
  }

  onChangePage(event: PaginationData) {
    this.managersFacade.dispatch(
      getManagers({ page: `page=${event.pageIndex + 1}` })
    );
  }

  trackByFn(index: number, item: any) {
    return index;
  }

  trackByFnDetails(index: number, item: any) {
    return index;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

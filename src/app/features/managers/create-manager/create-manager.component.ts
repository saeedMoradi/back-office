import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild
} from '@angular/core';
import { FormGroup, FormBuilder, FormGroupDirective } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ManagersService } from '../services/managers.service';
import { ManagersFacade } from '../+store/managers.facade';
import * as fromActions from '../+store/managers.actions';
import { ShopsFacade } from '../../shops/+store/shops.facade';
import { getShops } from '../../shops/+store/shops.actions';
import { PersistanceService } from 'src/app/core/services/persistance.service';
import { LoginResponse } from 'src/app/shared/interfaces';

const roles = [
  {
    name: 'Products',
    childs: [
      {
        name: 'Accessory',
        roles: [
          { name: 'Create', value: 'ACCESSORY_CREATE' },
          { name: 'Edit', value: 'ACCESSORY_EDIT' },
          { name: 'Delete', value: 'ACCESSORY_DELETE' }
        ]
      },
      {
        name: 'Allergy',
        roles: [
          { name: 'Create', value: 'ALLERGY_CREATE' },
          { name: 'Edit', value: 'ALLERGY_EDIT' },
          { name: 'Delete', value: 'ALLERGY_DELETE' }
        ]
      },
      {
        name: 'Category',
        roles: [
          { name: 'Create', value: 'CATEGORY_CREATE' },
          { name: 'Edit', value: 'CATEGORY_EDIT' },
          { name: 'Delete', value: 'CATEGORY_DELETE' }
        ]
      },
      {
        name: 'Meal Deal',
        roles: [
          { name: 'Create', value: 'MEALDEAL_CREATE' },
          { name: 'Edit', value: 'MEALDEAL_EDIT' },
          { name: 'Delete', value: 'MEALDEAL_DELETE' }
        ]
      },
      {
        name: 'Menu',
        roles: [
          { name: 'Create', value: 'MENU_CREATE' },
          { name: 'Edit', value: 'MENU_EDIT' },
          { name: 'Delete', value: 'MENU_DELETE' }
        ]
      },
      {
        name: 'Product',
        roles: [
          { name: 'Create', value: 'PRODUCT_CREATE' },
          { name: 'Edit', value: 'PRODUCT_EDIT' },
          { name: 'Delete', value: 'PRODUCT_DELETE' }
        ]
      }
      // {
      //   name: 'Sets',
      //   roles: [
      //     { name: 'Create', value: 'SETS_CREATE' },
      //     { name: 'Edit', value: 'SETS_EDIT' }
      //   ]
      // },
      // {
      //   name: 'Step',
      //   roles: [
      //     { name: 'Create', value: 'STEP_CREATE' },
      //     { name: 'Edit', value: 'STEP_EDIT' }
      //   ]
      // }
    ]
  },
  {
    name: 'Other',
    childs: [
      // {
      //   name: 'Address',
      //   roles: [
      //     { name: 'Create', value: 'ADDRESS_CREATE' },
      //     { name: 'Edit', value: 'ADDRESS_EDIT' }
      //   ]
      // },
      // {
      //   name: 'Currency',
      //   roles: [
      //     { name: 'Create', value: 'CURRENCY_CREATE' },
      //     { name: 'Edit', value: 'CURRENCY_EDIT' }
      //   ]
      // },
      {
        name: 'Discount',
        roles: [
          { name: 'Create', value: 'DISCOUNT_CREATE' },
          { name: 'Edit', value: 'DISCOUNT_EDIT' },
          { name: 'Delete', value: 'DISCOUNT_DELETE' }
        ]
      },
      {
        name: 'Payment',
        roles: [
          { name: 'Create', value: 'PAYINFO_CREATE' },
          { name: 'Edit', value: 'PAYINFO_EDIT' },
          { name: 'Delete', value: 'PAYINFO_DELETE' }
        ]
      },
      {
        name: 'Settings',
        roles: [
          { name: 'Create', value: 'SETTING_CREATE' },
          { name: 'Edit', value: 'SETTING_EDIT' },
          { name: 'Delete', value: 'SETTING_DELETE' }
        ]
      },
      // {
      //   name: 'Merchant',
      //   roles: [
      //     { name: 'Create', value: 'MERCHANT_CREATE' },
      //     { name: 'Edit', value: 'MERCHANT_EDIT' },
      //     { name: 'Delete', value: 'MERCHANT_DELETE' }
      //   ]
      // },
      {
        name: 'Store',
        roles: [
          { name: 'Create', value: 'SHOP_CREATE' },
          { name: 'Edit', value: 'SHOP_EDIT' },
          { name: 'Delete', value: 'SHOP_DELETE' }
        ]
      }
      // { name: 'User', roles: [{ name: 'User', value: 'ROLE_USER' }] }
    ]
  }
];

@Component({
  selector: 'app-create-manager',
  templateUrl: './create-manager.component.html',
  styleUrls: ['./create-manager.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateManagerComponent implements OnInit {
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  managerForm: FormGroup;
  roles = roles;
  permissions = [];
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public managersFacade: ManagersFacade,
    private managersService: ManagersService,
    public shopsFacade: ShopsFacade,
    private persistanceService: PersistanceService
  ) {}

  ngOnInit() {
    this.initializeManagerForm();
    this.listenFormReset();
    this.shopsFacade.dispatch(getShops({ page: 'pagination=false' }));
  }

  initializeManagerForm() {
    const profile: LoginResponse = this.persistanceService.get('profile');
    this.managerForm = this.formBuilder.group({
      parent: [`/api/users/${profile.id}`, []],
      firstName: ['', []],
      lastName: ['', []],
      description: ['', []],
      email: ['', []],
      password: ['', []],
      phone: ['', []],
      roles: [[], []],
      shops: [[], []]
    });
  }

  listenFormReset() {
    this.managersService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetManagerForm();
      });
  }

  resetManagerForm() {
    this.formDirective.resetForm();
    this.managerForm.reset();
  }

  addCreatePermission(e: boolean, identifier: string, role: any) {
    const find = role.find((data: any) => {
      return data.name === identifier;
    });
    if (e) {
      if (find) {
        this.permissions = [...this.permissions, find.value];
      }
    } else {
      if (find) {
        this.permissions = this.permissions.filter((el: any) => {
          return el !== find.value;
        });
      }
    }
  }

  addAllPermission(e: boolean, data) {
    if (e) {
      const arr = [];
      data.forEach(element => {
        element.roles.forEach(el => {
          const find = this.permissions.find(e => e === el.value);
          if (!find) arr.push(el.value);
        });
      });
      this.permissions = [...this.permissions, ...arr];
    } else {
      data.forEach(element => {
        element.roles.forEach(el => {
          const idx = this.permissions.findIndex(e => e === el.value);
          if (idx >= 0) this.permissions.splice(idx, 1);
        });
      });
    }
  }

  checkIfExist(val) {
    const find = this.permissions.find((el: any) => {
      return el === val;
    });
    if (find) {
      return true;
    } else {
      return false;
    }
  }

  onManager() {
    this.managerForm.markAllAsTouched();
    if (this.managerForm.invalid) {
      return;
    }
    this.managerForm.patchValue({
      roles: this.permissions
    });
    this.managersFacade.dispatch(
      fromActions.createManager({ request: this.managerForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

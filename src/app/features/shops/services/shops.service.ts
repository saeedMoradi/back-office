import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  City,
  Country,
  LoginResponse,
  State,
  TimeZone
} from 'src/app/shared/interfaces';
import { environment } from 'src/environments/environment';
import { Shop } from 'src/app/shared/interfaces/shop.interface';
import { PersistanceService } from 'src/app/core/services/persistance.service';

@Injectable({
  providedIn: 'root'
})
export class ShopsService {
  private readonly apiUrl = environment.apiUrl;
  private clearFormSource = new Subject<boolean>();
  clearForm$ = this.clearFormSource.asObservable();

  constructor(
    private http: HttpClient,
    private persistanceService: PersistanceService
  ) {}

  clearForm() {
    this.clearFormSource.next(true);
  }

  getShops(page = ''): Observable<Shop> {
    const profile: LoginResponse = this.persistanceService.get('profile');
    return this.http.get<Shop>(
      `${this.apiUrl}/api/shops?owner.id=${profile.id}&order[id]=desc&${page}`
    );
  }
  createShop(data: any): Observable<Shop> {
    return this.http.post<Shop>(`${this.apiUrl}/api/shops`, data);
  }
  getShop(id: any): Observable<Shop> {
    return this.http.get<Shop>(`${this.apiUrl}/api/shops/${id}`);
  }
  deleteShop(id: any): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/api/shops/${id}`);
  }
  putShop(id: any, data: any): Observable<Shop> {
    return this.http.put<Shop>(`${this.apiUrl}/api/shops/${id}`, data);
  }
  patchShop(id: any, data: any): Observable<Shop> {
    return this.http.patch<Shop>(`${this.apiUrl}/api/shops/${id}`, data);
  }
  getCountries(): Observable<Country[]> {
    return this.http.get<Country[]>(`assets/json/countries.json`);
  }
  getStates(id): Observable<State[]> {
    return this.http.get<State[]>(`assets/json/states/${id}.json`);
  }
  getCities(id): Observable<City[]> {
    return this.http.get<City[]>(`assets/json/cities/${id}.json`);
  }
  getTimeZone(id): Observable<TimeZone[]> {
    return this.http.get<TimeZone[]>(`assets/json/timezones/${id}.json`);
  }
  getCompleteTimeZone(country): Observable<TimeZone[]> {
    return this.http.get<TimeZone[]>(`assets/json/timezone.json`).pipe(
      map((data: any) => {
        return data.filter((res: any) => {
          return country === res.name;
        });
      })
    );
  }
}

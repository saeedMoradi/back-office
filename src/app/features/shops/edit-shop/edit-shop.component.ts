import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject,
  ChangeDetectorRef,
  ElementRef,
  ViewChild
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';
import { Observable, Subject } from 'rxjs';
import { MerchantsFacade } from '../../merchants/+store/merchants.facade';
import { getMerchants } from '../../merchants/+store/merchants.actions';
import { Country, TimeZone } from 'src/app/shared/interfaces';
import { TimeDialogComponent } from 'src/app/shared/components/time-dialog/time-dialog.component';
import { ShopsService } from '../services/shops.service';
import { GoogleMap } from '@angular/google-maps';
import {
  Hydramember,
  Worktime
} from 'src/app/shared/interfaces/shop.interface';
import { MatSlideToggle } from '@angular/material/slide-toggle';

interface WorkTime {
  day: string;
  time: { open: string; close: string }[];
}

@Component({
  selector: 'app-edit-shop',
  templateUrl: './edit-shop.component.html',
  styleUrls: ['./edit-shop.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditShopComponent implements OnInit {
  @ViewChild('googleMap', { static: false }) googleMap: GoogleMap;
  @ViewChild('postCode') postCode: ElementRef;
  shopForm: FormGroup;
  country$: Observable<Country[]>;
  timeZone$: Observable<TimeZone[]>;
  center: google.maps.LatLngLiteral;
  zoom = 17;
  options: google.maps.MapOptions = {
    styles: [
      {
        featureType: 'poi',
        elementType: 'labels',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'water',
        elementType: 'labels',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'transit.station',
        elementType: 'labels',
        stylers: [{ visibility: 'off' }]
      }
    ],
    streetViewControl: false,
    mapTypeControl: false,
    gestureHandling: 'cooperative'
  };
  markerOptions: google.maps.MarkerOptions = { draggable: false };
  markerPosition: google.maps.LatLngLiteral;
  workTime: WorkTime[] = [];
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditShopComponent>,
    public merchantsFacade: MerchantsFacade,
    public dialog: MatDialog,
    private cd: ChangeDetectorRef,
    private shopsService: ShopsService,
    @Inject(MAT_DIALOG_DATA) public data: Hydramember
  ) {
    data.worktimes.forEach((item: Worktime) => {
      let time = [];
      item.times.forEach((el: any) => {
        const open = `${new Date(el.open.date).getHours()}:${new Date(
          el.open.date
        ).getMinutes()}`;
        const close = `${new Date(el.close.date).getHours()}:${new Date(
          el.close.date
        ).getMinutes()}`;
        time.push({ open, close });
      });
      this.workTime.push({
        day: item.day,
        time
      });
    });
    this.markerPosition = {
      lat: this.data.location.coordinates[0],
      lng: this.data.location.coordinates[1]
    };
    this.center = {
      lat: this.data.location.coordinates[0],
      lng: this.data.location.coordinates[1]
    };
  }

  ngOnInit() {
    this.initializeCountries();
    this.initializeShopForm();
    this.initializeCompleteTimeZone(this.data.country);
    this.merchantsFacade.dispatch(getMerchants({ page: 'pagination=false' }));
  }

  ngAfterViewInit() {
    let that = this;
    let autocomplete = new google.maps.places.Autocomplete(
      this.postCode.nativeElement
    );
    autocomplete.setFields([
      'address_components',
      'geometry',
      'formatted_address'
    ]);
    autocomplete.addListener('place_changed', getPlace);
    function getPlace() {
      var place = autocomplete.getPlace();
      if (place.geometry.viewport) {
        that.googleMap.googleMap.fitBounds(place.geometry.viewport);
      } else {
        that.googleMap.googleMap.setCenter(place.geometry.location);
        that.googleMap.googleMap.setZoom(17); // Why 17? Because it looks good.
      }
      let filtred_country = place.address_components.filter(function (
        address_component
      ) {
        return address_component.types.includes('country');
      });
      let filtred_postcode = place.address_components.filter(function (
        address_component
      ) {
        return address_component.types.includes('postal_code');
      });
      let country = filtred_country.length ? filtred_country[0].long_name : '';
      let postcode = filtred_postcode.length
        ? filtred_postcode[0].long_name
        : '';
      that.shopForm.patchValue({
        country,
        address: place.formatted_address,
        zipCode: postcode
      });
      that.getTimeZone(filtred_country[0].short_name);
      that.cd.detectChanges();
    }
  }

  initializeCountries() {
    this.country$ = this.shopsService.getCountries();
  }

  initializeCompleteTimeZone(data) {
    this.timeZone$ = this.shopsService.getCompleteTimeZone(data);
  }

  initializeShopForm() {
    const merchant = this.data.merchant.split('/');
    this.shopForm = this.formBuilder.group({
      title: [this.data.title, []],
      zipCode: [this.data.zipCode, []],
      location: [
        {
          latitude: this.data.location.coordinates[0],
          longitude: this.data.location.coordinates[1]
        },
        [Validators.required]
      ],
      merchant: [+merchant[merchant.length - 1], []],
      email: [this.data.email, []],
      timeZone: [this.data.timeZone, []],
      city: [this.data.city, []],
      country: [this.data.country, []],
      state: [this.data.state, []],
      phone: [this.data.phone, []],
      address: [this.data.address, []],
      buildingNumber: [this.data.buildingNumber, []],
      streetNameNumber: [this.data.streetNameNumber, []],
      workTime: [this.workTime, []],
      currency: [this.data.currency, []],
      status: [this.data.status, []],
      url: ['', []]
    });
  }

  getTimeZone(id) {
    this.timeZone$ = this.shopsService.getTimeZone(id);
  }

  addTime() {
    const dialogRef = this.dialog.open(TimeDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe((result: WorkTime[]) => {
      if (result) {
        result.forEach((element: WorkTime) => {
          const find = this.workTime.find((el: WorkTime) => {
            return element.day === el.day;
          });
          if (find) {
            find.time = [...find.time, ...element.time];
          } else {
            this.workTime = [...this.workTime, element];
          }
        });
        this.shopForm.patchValue({
          workTime: this.workTime
        });
        this.cd.detectChanges();
      }
    });
  }

  deleteDay(index) {
    this.workTime.splice(index, 1);
    this.shopForm.patchValue({
      workTime: this.workTime
    });
  }

  addMarker(event: google.maps.MouseEvent) {
    this.markerPosition = event.latLng.toJSON();
    const location = event.latLng.toJSON();
    this.shopForm.patchValue({
      location: {
        latitude: location.lat,
        longitude: location.lng
      }
    });
  }

  onShop() {
    this.shopForm.markAllAsTouched();
    if (this.shopForm.invalid) {
      return;
    }
    this.dialogRef.close(this.shopForm.value);
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

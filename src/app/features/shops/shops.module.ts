import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { GoogleMapsModule } from '@angular/google-maps';
import { ShopsRoutingModule } from './shops-routing.module';
// Components
import { ShopsComponent } from './shops.component';
import { ShopsListComponent } from './shops-list/shops-list.component';
import { CreateShopComponent } from './create-shop/create-shop.component';
// NGRX
import { EffectsModule } from '@ngrx/effects';
// import { StoreModule } from '@ngrx/store';
import { ShopsEffects } from './+store/shops.effects';
// import * as fromReducer from './+store/shops.reducer';
import { MerchantsEffects } from '../merchants/+store/merchants.effects';
import { EditShopComponent } from './edit-shop/edit-shop.component';
import { GeneralEffect } from '../general/+store/general.effects';

@NgModule({
  declarations: [
    ShopsComponent,
    ShopsListComponent,
    CreateShopComponent,
    EditShopComponent
  ],
  imports: [
    SharedModule,
    ShopsRoutingModule,
    GoogleMapsModule,
    // StoreModule.forFeature(fromReducer.featureKey, fromReducer.reducer),
    EffectsModule.forFeature([ShopsEffects, MerchantsEffects, GeneralEffect])
  ]
})
export class ShopsModule {}

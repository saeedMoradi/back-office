import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { ShopsFacade } from '../+store/shops.facade';
import { getShops, deleteShop, putShop } from '../+store/shops.actions';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PaginationData } from 'src/app/shared/interfaces';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { EditShopComponent } from '../edit-shop/edit-shop.component';
import { Hydramember } from 'src/app/shared/interfaces/shop.interface';

@Component({
  selector: 'app-shops-list',
  templateUrl: './shops-list.component.html',
  styleUrls: ['./shops-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShopsListComponent implements OnInit {
  shops: Hydramember[];
  originalShops: Hydramember[];
  uniqueKey: any;
  destroy$ = new Subject();

  constructor(
    public shopsFacade: ShopsFacade,
    private cd: ChangeDetectorRef,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.shopsFacade.dispatch(getShops({ page: 'page=1' }));
    this.shopsFacade.shops$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: Hydramember[]) => {
        if (data) {
          this.shops = data;
          this.originalShops = data;
          // data.forEach(element => {
          //   this.uniqueKey = {
          //     ...this.uniqueKey,
          //     [element.id]: false
          //   };
          // });
          this.cd.detectChanges();
        }
      });
  }

  search(term = '') {
    this.shops = this.originalShops.filter((element: any) => {
      return element.title.toLowerCase().includes(term);
    });
  }

  onRemove(data: Hydramember) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.shopsFacade.dispatch(deleteShop({ id: data.id }));
      }
    });
  }

  onEdit(data: Hydramember) {
    const dialogRef = this.dialog.open(EditShopComponent, {
      autoFocus: false,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.shopsFacade.dispatch(putShop({ id: data.id, request: result }));
      }
    });
  }

  onChangePage(event: PaginationData) {
    this.shopsFacade.dispatch(
      getShops({ page: `page=${event.pageIndex + 1}` })
    );
  }

  trackByFn(index: number, item: any) {
    return index;
  }

  trackByFnDetails(index: number, item: any) {
    return index;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

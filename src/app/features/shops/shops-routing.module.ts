import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShopsComponent } from './shops.component';
import { ShopsListComponent } from './shops-list/shops-list.component';
import { CreateShopComponent } from './create-shop/create-shop.component';

const routes: Routes = [
  {
    path: '',
    component: ShopsComponent,
    children: [
      { path: '', redirectTo: 'shops-list' },
      { path: 'shops-list', component: ShopsListComponent },
      { path: 'create-shop', component: CreateShopComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopsRoutingModule {}

import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormGroupDirective
} from '@angular/forms';
import { GoogleMap } from '@angular/google-maps';
import { ShopsFacade } from '../+store/shops.facade';
import * as fromActions from '../+store/shops.actions';
import { ShopsService } from '../services/shops.service';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MerchantsFacade } from '../../merchants/+store/merchants.facade';
import { getMerchants } from '../../merchants/+store/merchants.actions';
import { City, Country, State, TimeZone } from 'src/app/shared/interfaces';
import { MatDialog } from '@angular/material/dialog';
import { TimeDialogComponent } from 'src/app/shared/components/time-dialog/time-dialog.component';
import { GeneralFacade } from '../../general/+store/general.facade';
import { getCurrencies } from '../../general/+store/general.actions';
import { MatSlideToggle } from '@angular/material/slide-toggle';

interface WorkTime {
  day: string;
  time: { open: string; close: string }[];
}

@Component({
  selector: 'app-create-shop',
  templateUrl: './create-shop.component.html',
  styleUrls: ['./create-shop.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateShopComponent implements OnInit {
  @ViewChild('googleMap', { static: false }) googleMap: GoogleMap;
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  @ViewChild('postCode') postCode: ElementRef;
  shopForm: FormGroup;
  country$: Observable<Country[]>;
  states$: Observable<State[]>;
  cities$: Observable<City[]>;
  timeZone$: Observable<TimeZone[]>;
  center: google.maps.LatLngLiteral = { lat: 50.8225, lng: 0.1372 };
  zoom = 12;
  options: google.maps.MapOptions = {
    styles: [
      {
        featureType: 'poi',
        elementType: 'labels',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'water',
        elementType: 'labels',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'transit.station',
        elementType: 'labels',
        stylers: [{ visibility: 'off' }]
      }
    ],
    streetViewControl: false,
    mapTypeControl: false,
    gestureHandling: 'cooperative'
    // scrollwheel: false
  };
  markerOptions: google.maps.MarkerOptions = { draggable: true };
  markerPosition: google.maps.LatLngLiteral;
  geocoder: google.maps.Geocoder = new google.maps.Geocoder();
  workTime: WorkTime[] = [];
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public shopsFacade: ShopsFacade,
    public generalFacade: GeneralFacade,
    private shopsService: ShopsService,
    public merchantsFacade: MerchantsFacade,
    private cd: ChangeDetectorRef,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.initializeShopForm();
    this.initializeCountries();
    this.listenFormReset();
    this.merchantsFacade.dispatch(getMerchants({ page: 'pagination=false' }));
    this.generalFacade.dispatch(getCurrencies({ page: 'pagination=false' }));
  }

  ngAfterViewInit() {
    let that = this;
    let autocomplete = new google.maps.places.Autocomplete(
      this.postCode.nativeElement
    );
    autocomplete.setFields([
      'address_components',
      'geometry',
      'formatted_address'
    ]);
    autocomplete.addListener('place_changed', getPlace);
    function getPlace() {
      const place = autocomplete.getPlace();
      if (place.geometry.viewport) {
        that.googleMap.googleMap.fitBounds(place.geometry.viewport);
      } else {
        that.googleMap.googleMap.setCenter(place.geometry.location);
        that.googleMap.googleMap.setZoom(17); // Why 17? Because it looks good.
      }
      let filtred_country = place.address_components.filter(function (
        address_component
      ) {
        return address_component.types.includes('country');
      });
      let filtred_postcode = place.address_components.filter(function (
        address_component
      ) {
        return address_component.types.includes('postal_code');
      });
      let country = filtred_country.length ? filtred_country[0].long_name : '';
      let postcode = filtred_postcode.length
        ? filtred_postcode[0].long_name
        : '';
      that.markerPosition = place.geometry.location.toJSON();
      that.shopForm.patchValue({
        location: {
          latitude: that.markerPosition.lat,
          longitude: that.markerPosition.lng
        },
        country,
        address: place.formatted_address,
        zipCode: postcode
      });
      that.getTimeZone(filtred_country[0].short_name);
      that.cd.detectChanges();
    }
  }

  initializeShopForm() {
    const urlPattern = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/;
    this.shopForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      zipCode: ['', [Validators.required]],
      location: ['', [Validators.required]],
      email: ['', [Validators.required]],
      timeZone: ['', []],
      city: ['', []],
      country: ['', []],
      state: ['', []],
      phone: ['', []],
      address: ['', [Validators.required]],
      buildingNumber: ['', []],
      streetNameNumber: ['', []],
      workTime: [[], []],
      currency: ['', []],
      url: ['', [Validators.pattern(urlPattern)]],
      status: [true, []]
    });
  }

  listenFormReset() {
    this.shopsService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetShopForm();
      });
  }

  initializeCountries() {
    this.country$ = this.shopsService.getCountries();
  }

  getTimeZone(id) {
    this.timeZone$ = this.shopsService.getTimeZone(id);
  }

  getStates(e: Country) {
    this.states$ = this.shopsService.getStates(e.id);
    this.timeZone$ = this.shopsService.getTimeZone(e.iso2);
  }

  getCities(e: State) {
    this.runGeoCoder(e.name);
    this.cities$ = this.shopsService.getCities(e.id);
  }

  runGeoCoder(place) {
    let that = this;
    this.geocoder.geocode({ address: place }, function (results, status) {
      if (status == 'OK') {
        that.googleMap.googleMap.setCenter(results[0].geometry.location);
        that.googleMap.googleMap.fitBounds(results[0].geometry.viewport);
        // var marker = new google.maps.Marker({
        //     map: this.googleMap,
        //     position: results[0].geometry.location
        // });
      }
      // else {
      //   console.log(
      //     'Geocode was not successful for the following reason: ' + status
      //   );
      // }
    });
  }

  addTime() {
    const dialogRef = this.dialog.open(TimeDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe((result: WorkTime[]) => {
      if (result) {
        result.forEach((element: WorkTime) => {
          const find = this.workTime.find((el: WorkTime) => {
            return element.day === el.day;
          });
          if (find) {
            find.time = [...find.time, ...element.time];
          } else {
            this.workTime = [...this.workTime, element];
          }
        });
        this.shopForm.patchValue({
          workTime: this.workTime
        });
        this.cd.detectChanges();
      }
    });
  }

  deleteDay(index) {
    this.workTime.splice(index, 1);
    this.shopForm.patchValue({
      workTime: this.workTime
    });
  }

  addMarker(event: google.maps.MouseEvent) {
    this.markerPosition = event.latLng.toJSON();
    const location = event.latLng.toJSON();
    this.shopForm.patchValue({
      location: {
        latitude: location.lat,
        longitude: location.lng
      }
    });
  }

  dragMarker($event) {
    const location = $event.latLng.toJSON();
    this.shopForm.patchValue({
      location: {
        latitude: location.lat,
        longitude: location.lng
      }
    });
  }

  resetShopForm() {
    this.workTime = [];
    this.formDirective.resetForm();
    this.shopForm.reset({
      city: '',
      state: '',
      streetNameNumber: '',
      status: true
    });
  }

  onShop() {
    this.shopForm.markAllAsTouched();
    if (this.shopForm.invalid) {
      return;
    }
    this.shopsFacade.dispatch(
      fromActions.createShop({ request: this.shopForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

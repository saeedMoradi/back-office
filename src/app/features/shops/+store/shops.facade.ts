import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import { ShopsState } from './shops.interface';
import * as fromSelector from './shops.selectors';
import { Shop, Hydramember } from 'src/app/shared/interfaces/shop.interface';

@Injectable({
  providedIn: 'root'
})
export class ShopsFacade implements Facade {
  error$: Observable<string>;
  isLoading$: Observable<boolean>;
  shops$: Observable<Hydramember[]>;
  shopsTotalLength$: Observable<number>;

  constructor(private readonly store: Store<ShopsState>) {
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.isLoading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.shops$ = store.pipe(select(fromSelector.selectShops));
    this.shopsTotalLength$ = store.pipe(
      select(fromSelector.selectShopsTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

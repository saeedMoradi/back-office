import * as shopsReducer from './shops.reducer';
import { ShopsState } from './shops.interface';
import * as shopsSelectors from './shops.selectors';

export { shopsReducer, shopsSelectors, ShopsState };

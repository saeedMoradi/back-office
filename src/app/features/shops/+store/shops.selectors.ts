import { createSelector, createFeatureSelector } from '@ngrx/store';
import { ShopsState } from './shops.interface';
import { featureKey } from './shops.reducer';

export const selectFeature = createFeatureSelector<ShopsState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: ShopsState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: ShopsState) => state.isLoading
);
export const selectShops = createSelector(selectFeature, (state: ShopsState) =>
  state.shops ? state.shops['hydra:member'] : null
);
export const selectShopsTotalLength = createSelector(
  selectFeature,
  (state: ShopsState) => (state.shops ? state.shops['hydra:totalItems'] : null)
);

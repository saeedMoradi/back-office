import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './shops.actions';
import { ShopsService } from '../services/shops.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Shop, Hydramember } from 'src/app/shared/interfaces/shop.interface';
import { Router } from '@angular/router';

@Injectable()
export class ShopsEffects {
  constructor(
    private actions$: Actions,
    private shopsService: ShopsService,
    private matSnackBar: MatSnackBar,
    private router: Router
  ) {}

  getShops$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getShops),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.shopsService.getShops(page).pipe(
          map((data: Shop) => {
            return fromActions.getShopsSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getShopsFail({ error })))
        );
      })
    )
  );

  createShop$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createShop),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.shopsService.createShop(request).pipe(
          map((data: Shop) => {
            this.matSnackBar.open('Shop Created');
            this.shopsService.clearForm();
            this.router.navigate(['/home/store/shops-list']);
            return fromActions.createShopSuccess({ response: data });
          }),
          catchError(error => of(fromActions.createShopFail({ error })))
        );
      })
    )
  );

  getShop$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getShop),
      map(action => {
        return action.id;
      }),
      switchMap((id: number) => {
        return this.shopsService.getShop(id).pipe(
          map((data: Shop) => {
            return fromActions.getShopSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getShopFail({ error })))
        );
      })
    )
  );

  deleteShop$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteShop),
      map(action => {
        return action.id;
      }),
      switchMap((id: number) => {
        return this.shopsService.deleteShop(id).pipe(
          map((data: any) => {
            return fromActions.deleteShopSuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteShopFail({ error }));
          })
        );
      })
    )
  );

  putShop$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putShop),
      switchMap(({ id, request }) => {
        return this.shopsService.putShop(id, request).pipe(
          map((data: Shop) => {
            return fromActions.putShopSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putShopFail({ error }));
          })
        );
      })
    )
  );

  patchShop$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchShop),
      switchMap(({ id, request }) => {
        return this.shopsService.patchShop(id, request).pipe(
          map((data: Shop) => {
            return fromActions.patchShopSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.patchShopFail({ error }));
          })
        );
      })
    )
  );
}

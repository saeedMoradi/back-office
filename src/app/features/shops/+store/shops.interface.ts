import { Shop } from 'src/app/shared/interfaces/shop.interface';

export interface ShopsState {
  error: any;
  isLoading: boolean;
  shops: Shop;
}

import { createAction, props } from '@ngrx/store';
import { Shop, Hydramember } from 'src/app/shared/interfaces/shop.interface';

export const getShops = createAction(
  '[Shops] Get Shops',
  props<{ page: string }>()
);
export const getShopsSuccess = createAction(
  '[Shops] Get Shops Success',
  props<{ response: Shop }>()
);
export const getShopsFail = createAction(
  '[Shops] Get Shops Fail',
  props<{ error: any }>()
);

export const createShop = createAction(
  '[Shops] Create Shop',
  props<{ request: any }>()
);
export const createShopSuccess = createAction(
  '[Shops] Create Shop Success',
  props<{ response: any }>()
);
export const createShopFail = createAction(
  '[Shops] Create Shop Fail',
  props<{ error: any }>()
);

export const getShop = createAction('[Shops] Get Shop', props<{ id: any }>());
export const getShopSuccess = createAction(
  '[Shops] Get Shop Success',
  props<{ response: Shop }>()
);
export const getShopFail = createAction(
  '[Shops] Get Shop Fail',
  props<{ error: any }>()
);

export const deleteShop = createAction(
  '[Shops] Delete Shop',
  props<{ id: any }>()
);
export const deleteShopSuccess = createAction(
  '[Shops] Delete Shop Success',
  props<{ id: any }>()
);
export const deleteShopFail = createAction(
  '[Shops] Delete Shop Fail',
  props<{ error: any }>()
);

export const putShop = createAction(
  '[Shops] Put Shop',
  props<{ id: any; request: any }>()
);
export const putShopSuccess = createAction(
  '[Shops] Put Shop Success',
  props<{ response: any }>()
);
export const putShopFail = createAction(
  '[Shops] Put Shop Fail',
  props<{ error: any }>()
);

export const patchShop = createAction(
  '[Shops] Patch Shop',
  props<{ id: any; request: any }>()
);
export const patchShopSuccess = createAction(
  '[Shops] Patch Shop Success',
  props<{ response: any }>()
);
export const patchShopFail = createAction(
  '[Shops] Patch Shop Fail',
  props<{ error: any }>()
);

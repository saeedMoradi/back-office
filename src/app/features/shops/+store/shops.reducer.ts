import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './shops.actions';
import { ShopsState } from './shops.interface';

export const initialState: ShopsState = {
  error: null,
  isLoading: false,
  shops: null
};

export const featureKey = 'shops';

const shopsReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): ShopsState => ({ ...state, error: null })
  ),
  on(
    fromActions.getShopsSuccess,
    (state, action): ShopsState => ({
      ...state,
      error: null,
      shops: action.response
    })
  ),
  on(
    fromActions.getShopsFail,
    (state, action): ShopsState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createShop,
    (state, action): ShopsState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createShopSuccess,
    (state, action): ShopsState => {
      if (!state.shops) {
        return {
          ...state,
          shops: {
            'hydra:member': [action.response],
            'hydra:totalItems': 1
          }
        };
      }
      return {
        ...state,
        error: null,
        isLoading: false,
        shops: {
          ...state.shops,
          'hydra:member': [action.response, ...state.shops['hydra:member']],
          'hydra:totalItems': state.shops['hydra:totalItems'] + 1
        }
      };
    }
  ),
  on(
    fromActions.createShopFail,
    (state, action): ShopsState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteShopSuccess,
    (state, action): ShopsState => {
      const newshops = state.shops['hydra:member'].filter((item: any) => {
        return item.id !== action.id;
      });
      return {
        ...state,
        error: null,
        shops: {
          ...state.shops,
          'hydra:member': newshops,
          'hydra:totalItems': state.shops['hydra:totalItems'] - 1
        }
      };
    }
  ),
  on(
    fromActions.putShopSuccess,
    (state, action): ShopsState => {
      const newshops = state.shops['hydra:member'].map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        shops: { ...state.shops, 'hydra:member': newshops }
      };
    }
  )
);

export function reducer(state: ShopsState | undefined, action: Action) {
  return shopsReducer(state, action);
}

import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-deliveries',
  templateUrl: './deliveries.component.html',
  styleUrls: ['./deliveries.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeliveriesComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}

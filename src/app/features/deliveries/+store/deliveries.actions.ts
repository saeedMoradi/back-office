import { createAction, props } from '@ngrx/store';

export const getDeliveries = createAction(
  '[Deliveries] Get Deliveries',
  props<{ page: any }>()
);
export const getDeliveriesSuccess = createAction(
  '[Deliveries] Get Deliveries Success',
  props<{ response: any }>()
);
export const getDeliveriesFail = createAction(
  '[Deliveries] Get Deliveries Fail',
  props<{ error: any }>()
);

export const createDelivery = createAction(
  '[Deliveries] Create Delivery',
  props<{ request: any }>()
);
export const createDeliverySuccess = createAction(
  '[Deliveries] Create Delivery Success',
  props<{ response: any }>()
);
export const createDeliveryFail = createAction(
  '[Deliveries] Create Delivery Fail',
  props<{ error: any }>()
);

export const getDelivery = createAction(
  '[Deliveries] Get Delivery',
  props<{ id: any }>()
);
export const getDeliverySuccess = createAction(
  '[Deliveries] Get Delivery Success',
  props<{ response: any }>()
);
export const getDeliveryFail = createAction(
  '[Deliveries] Get Delivery Fail',
  props<{ error: any }>()
);

export const deleteDelivery = createAction(
  '[Deliveries] Delete Delivery',
  props<{ id: any }>()
);
export const deleteDeliverySuccess = createAction(
  '[Deliveries] Delete Delivery Success',
  props<{ id: any }>()
);
export const deleteDeliveryFail = createAction(
  '[Deliveries] Delete Delivery Fail',
  props<{ error: any }>()
);

export const putDelivery = createAction(
  '[Deliveries] Put Delivery',
  props<{ id: any; request: any }>()
);
export const putDeliverySuccess = createAction(
  '[Deliveries] Put Delivery Success',
  props<{ response: any }>()
);
export const putDeliveryFail = createAction(
  '[Deliveries] Put Delivery Fail',
  props<{ error: any }>()
);

export const patchDelivery = createAction(
  '[Deliveries] Patch Delivery',
  props<{ id: any; request: any }>()
);
export const patchDeliverySuccess = createAction(
  '[Deliveries] Patch Delivery Success',
  props<{ response: any }>()
);
export const patchDeliveryFail = createAction(
  '[Deliveries] Patch Delivery Fail',
  props<{ error: any }>()
);

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './deliveries.actions';
import { DeliveriesService } from '../services/deliveries.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DeliveriesFacade } from './deliveries.facade';

@Injectable()
export class DeliveriesEffects {
  constructor(
    private actions$: Actions,
    private deliveriesService: DeliveriesService,
    private matSnackBar: MatSnackBar,
    private deliveriesFacade: DeliveriesFacade
  ) {}

  getdeliveries$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getDeliveries),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.deliveriesService.getDeliveries(page).pipe(
          map((data: any) => {
            return fromActions.getDeliveriesSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getDeliveriesFail({ error })))
        );
      })
    )
  );

  createDelivery$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createDelivery),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.deliveriesService.createDelivery(request).pipe(
          map((data: any) => {
            this.deliveriesService.clearForm();
            this.matSnackBar.open('Delivery Created');
            this.deliveriesFacade.dispatch(
              fromActions.getDeliveries({ page: 'page=1' })
            );
            return fromActions.createDeliverySuccess({ response: data });
          }),
          catchError(error => of(fromActions.createDeliveryFail({ error })))
        );
      })
    )
  );

  getDelivery$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getDelivery),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.deliveriesService.getDelivery(id).pipe(
          map((data: any) => {
            return fromActions.getDeliverySuccess({ response: data });
          }),
          catchError(error => of(fromActions.getDeliveryFail({ error })))
        );
      })
    )
  );

  deleteDelivery$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteDelivery),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.deliveriesService.deleteDelivery(id).pipe(
          map((data: any) => {
            return fromActions.deleteDeliverySuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteDeliveryFail({ error }));
          })
        );
      })
    )
  );

  putDelivery$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putDelivery),
      switchMap(({ id, request }) => {
        return this.deliveriesService.putDelivery(id, request).pipe(
          map((data: any) => {
            this.deliveriesService.clearForm();
            this.matSnackBar.open('Delivery Updated');
            this.deliveriesFacade.dispatch(
              fromActions.getDeliveries({ page: 'pagination=false' })
            );
            return fromActions.putDeliverySuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putDeliveryFail({ error }));
          })
        );
      })
    )
  );

  patchDelivery$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchDelivery),
      switchMap(({ id, request }) => {
        return this.deliveriesService.patchDelivery(id, request).pipe(
          map((data: any) => {
            return fromActions.patchDeliverySuccess({ response: data });
          }),
          catchError(error => of(fromActions.patchDeliveryFail({ error })))
        );
      })
    )
  );
}

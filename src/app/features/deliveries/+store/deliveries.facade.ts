import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './deliveries.selectors';

@Injectable({
  providedIn: 'root'
})
export class DeliveriesFacade implements Facade {
  isloading$: Observable<boolean>;
  error$: Observable<any>;
  deliveries$: Observable<any>;
  deliveriesTotalLength$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.deliveries$ = store.pipe(select(fromSelector.selectDeliveries));
    this.deliveriesTotalLength$ = store.pipe(
      select(fromSelector.selectDeliveriesTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

import { createSelector, createFeatureSelector } from '@ngrx/store';
import { DeliveriesState } from './deliveries.interface';
import { featureKey } from './deliveries.reducer';

export const selectFeature = createFeatureSelector<DeliveriesState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: DeliveriesState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: DeliveriesState) => state.isLoading
);
export const selectDeliveries = createSelector(
  selectFeature,
  (state: DeliveriesState) =>
    state.deliveries ? state.deliveries['hydra:member'] : null
);
export const selectDeliveriesTotalLength = createSelector(
  selectFeature,
  (state: DeliveriesState) =>
    state.deliveries ? state.deliveries['hydra:totalItems'] : null
);

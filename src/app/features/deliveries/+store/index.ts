import * as deliveriesReducer from './deliveries.reducer';
import { DeliveriesState } from './deliveries.interface';
import * as deliveriesSelectors from './deliveries.selectors';

export { deliveriesReducer, deliveriesSelectors, DeliveriesState };

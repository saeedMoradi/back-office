import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './deliveries.actions';
import { DeliveriesState } from './deliveries.interface';

export const initialState: DeliveriesState = {
  error: null,
  isLoading: false,
  deliveries: null
};

export const featureKey = 'deliveries';

const deliveriesReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): DeliveriesState => ({ ...state, error: false })
  ),
  on(
    fromActions.getDeliveriesSuccess,
    (state, action): DeliveriesState => ({
      ...state,
      error: null,
      deliveries: action.response
    })
  ),
  on(
    fromActions.getDeliveriesFail,
    (state, action): DeliveriesState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createDelivery,
    (state, action): DeliveriesState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createDeliverySuccess,
    (state, action): DeliveriesState => {
      if (!state.deliveries) {
        return {
          ...state,
          deliveries: {
            'hydra:member': [action.response],
            'hydra:totalItems': 1
          }
        };
      }
      return {
        ...state,
        error: null,
        isLoading: false,
        deliveries: {
          ...state.deliveries,
          'hydra:member': [
            action.response,
            ...state.deliveries['hydra:member']
          ],
          'hydra:totalItems': state.deliveries['hydra:totalItems'] + 1
        }
      };
    }
  ),
  on(
    fromActions.createDeliveryFail,
    (state, action): DeliveriesState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteDeliverySuccess,
    (state, action): DeliveriesState => {
      const newDeliveries = state.deliveries['hydra:member'].filter(
        (item: any) => {
          return item.id !== action.id;
        }
      );
      return {
        ...state,
        error: null,
        deliveries: {
          ...state.deliveries,
          'hydra:member': newDeliveries,
          'hydra:totalItems': state.deliveries['hydra:totalItems'] - 1
        }
      };
    }
  )
  // on(
  //   fromActions.putDeliverySuccess,
  //   (state, action): DeliveriesState => {
  //     const newDeliveries = state.deliveries['hydra:member'].map(item => {
  //       if (item.id !== action.response.id) {
  //         return item;
  //       }
  //       return action.response;
  //     });
  //     return {
  //       ...state,
  //       deliveries: { ...state.deliveries, 'hydra:member': newDeliveries }
  //     };
  //   }
  // )
);

export function reducer(state: DeliveriesState, action: Action) {
  return deliveriesReducer(state, action);
}

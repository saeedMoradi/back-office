export interface DeliveriesState {
  error: any;
  isLoading: boolean;
  deliveries: any;
}

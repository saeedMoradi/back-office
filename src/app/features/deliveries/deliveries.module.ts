import { NgModule } from '@angular/core';

import { DeliveriesRoutingModule } from './deliveries-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { GoogleMapsModule } from '@angular/google-maps';
// Components
import { DeliveriesComponent } from './deliveries.component';
import { CreateDeliveryComponent } from './create-delivery/create-delivery.component';
import { DeliveriesListComponent } from './deliveries-list/deliveries-list.component';
import { EditDeliveryComponent } from './edit-delivery/edit-delivery.component';
// NGRX
import { EffectsModule } from '@ngrx/effects';
import { DeliveriesEffects } from './+store/deliveries.effects';
import { ShopsEffects } from '../shops/+store/shops.effects';

@NgModule({
  declarations: [
    DeliveriesComponent,
    CreateDeliveryComponent,
    DeliveriesListComponent,
    EditDeliveryComponent
  ],
  imports: [
    SharedModule,
    DeliveriesRoutingModule,
    EffectsModule.forFeature([DeliveriesEffects, ShopsEffects]),
    GoogleMapsModule
  ]
})
export class DeliveriesModule {}

import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ChangeDetectorRef
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormGroupDirective
} from '@angular/forms';
import { GoogleMap } from '@angular/google-maps';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DeliveriesFacade } from '../+store/deliveries.facade';
import { DeliveriesService } from '../services/deliveries.service';
import * as fromActions from '../+store/deliveries.actions';
import { ShopsFacade } from '../../shops/+store/shops.facade';
import { getShops } from '../../shops/+store/shops.actions';
import { MatDialog } from '@angular/material/dialog';
import { TimeDialogComponent } from 'src/app/shared/components/time-dialog/time-dialog.component';
import { Hydramember } from 'src/app/shared/interfaces/shop.interface';
import { deleteDelivery, getDeliveries } from '../+store/deliveries.actions';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-create-delivery',
  templateUrl: './create-delivery.component.html',
  styleUrls: ['./create-delivery.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateDeliveryComponent implements OnInit {
  @ViewChild('googleMap') googleMap: GoogleMap;
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  currency: string;
  editMode: number;
  deliveryForm: FormGroup;
  options: google.maps.MapOptions = {
    styles: [
      // {
      //   featureType: 'administrative',
      //   elementType: 'labels',
      //   stylers: [{ visibility: 'off' }]
      // },
      {
        featureType: 'poi',
        elementType: 'labels',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'water',
        elementType: 'labels',
        stylers: [{ visibility: 'off' }]
      },
      // {
      //   featureType: 'road',
      //   elementType: 'labels',
      //   stylers: [{ visibility: 'off' }]
      // },
      {
        featureType: 'transit.station',
        elementType: 'labels',
        stylers: [{ visibility: 'off' }]
      }
    ],
    streetViewControl: false,
    mapTypeControl: false,
    gestureHandling: 'cooperative'
    // scrollwheel: false
  };
  markerOptions: google.maps.MarkerOptions = { draggable: false };
  markerPosition: google.maps.LatLngLiteral;
  center: google.maps.LatLngLiteral = { lat: 50.736129, lng: -1.988229 };
  zoom = 6;
  circle: google.maps.Circle;
  circleRadius: number = 50;
  polygon: google.maps.Polygon;
  polygonPaths: google.maps.LatLngLiteral[] = [];
  workTime = [];
  shops: Hydramember[];
  mapShapeMode = 'circle';
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public deliveriesFacade: DeliveriesFacade,
    public shopsFacade: ShopsFacade,
    private deliveriesService: DeliveriesService,
    private cd: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.deliveriesFacade.dispatch(getDeliveries({ page: 'pagination=false' }));
    this.shopsFacade.dispatch(getShops({ page: 'pagination=false' }));
    this.initializeDeliveryForm();
    this.listenFormReset();
    this.shopsFacade.shops$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: Hydramember[]) => {
        this.shops = data;
      });
  }

  createCircle() {
    this.circle = new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 1,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      editable: true,
      map: this.googleMap.googleMap,
      center: this.center,
      radius: this.circleRadius
    });
    this.circle.addListener('radius_changed', () => {
      this.circleRadius = this.circle.getRadius();
      this.updateVerticsForm();
    });
  }

  createPolygon() {
    this.polygon = new google.maps.Polygon({
      paths: this.polygonPaths,
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 3,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      editable: true,
      map: this.googleMap.googleMap
    });
  }

  circlePath(center, radius, points) {
    var a = [],
      p = 360 / points,
      d = 0;
    for (var i = 0; i < points; ++i, d += p) {
      a.push(google.maps.geometry.spherical.computeOffset(center, radius, d));
    }
    return [...a, a[0]];
  }

  updateVerticsForm() {
    const point = new google.maps.LatLng(this.center.lat, this.center.lng);
    const vertics = this.circlePath(point, this.circleRadius, 20);
    this.deliveryForm.patchValue({
      area: vertics
    });
  }

  switchShape(data: string) {
    if (this.mapShapeMode === data) {
      return;
    }
    this.mapShapeMode = data;
    if (data === 'polygon') {
      this.circle.setMap(null);
      this.createPolygon();
    } else {
      this.polygonPaths = [];
      this.polygon.setMap(null);
      this.createCircle();
      this.updateVerticsForm();
    }
  }

  initializeDeliveryForm() {
    this.deliveryForm = this.formBuilder.group({
      shop: ['', [Validators.required]],
      area: [[], []],
      // minimumAmountOrder: ['', []],
      deliveryFee: [0, [Validators.required]],
      deliveryTime: [15, [Validators.required]],
      deliveryAvailableTimes: [[], []],
      name: [[], []],
      min: [0, []],
      max: [0, []],
      tax: [0, []]
    });
  }

  listenFormReset() {
    this.deliveriesService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetDeliveryForm();
      });
  }

  resetDeliveryForm() {
    this.currency = null;
    this.editMode = null;
    this.formDirective.resetForm();
    this.workTime = [];
    this.deliveryForm.reset({
      min: 0,
      max: 0,
      tax: 0,
      deliveryTime: 15,
      deliveryAvailableTimes: [],
      area: []
    });
    if (this.mapShapeMode === 'circle') {
      this.circle.setMap(null);
    } else {
      this.polygon.setMap(null);
    }
  }

  addMarker(event: google.maps.MouseEvent) {
    if (this.mapShapeMode === 'polygon') {
      this.polygonPaths.push(event.latLng.toJSON());
      this.polygon.setPaths(this.polygonPaths);
      this.cd.detectChanges();
    }
  }

  addTime() {
    const dialogRef = this.dialog.open(TimeDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe((result: any[]) => {
      if (result) {
        result.forEach((element: any) => {
          const find = this.workTime.find((el: any) => {
            return element.day === el.day;
          });
          if (find) {
            find.time = [...find.time, ...element.time];
          } else {
            this.workTime = [...this.workTime, element];
          }
        });
        this.deliveryForm.patchValue({
          deliveryAvailableTimes: this.workTime
        });
        this.cd.detectChanges();
      }
    });
  }

  deleteDay(index) {
    this.workTime.splice(index, 1);
    this.deliveryForm.patchValue({
      deliveryAvailableTimes: this.workTime
    });
  }

  selectShop(id) {
    const find = this.shops.find((element: Hydramember) => {
      return element.id === id;
    });
    this.currency = find.currency;
    this.center = {
      lat: find.location.coordinates[0],
      lng: find.location.coordinates[1]
    };
    this.zoom = 17;
    this.markerPosition = {
      lat: find.location.coordinates[0],
      lng: find.location.coordinates[1]
    };
    if (this.mapShapeMode === 'circle') {
      this.createCircle();
      this.updateVerticsForm();
    } else {
      this.polygonPaths = [];
      this.createPolygon();
    }
  }

  onRemove(data) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deliveriesFacade.dispatch(deleteDelivery({ id: data.id }));
      }
    });
  }

  editDelivery(delivery) {
    this.editMode = delivery.id;
    this.mapShapeMode = 'polygon';
    const shop = delivery.shop.split('/');
    const shopId = +shop[shop.length - 1];
    const find = this.shops.find((element: Hydramember) => {
      return element.id === shopId;
    });
    this.currency = find.currency;
    this.center = {
      lat: find.location.coordinates[0],
      lng: find.location.coordinates[1]
    };
    this.zoom = 17;
    this.markerPosition = {
      lat: find.location.coordinates[0],
      lng: find.location.coordinates[1]
    };
    if (this.circle) {
      this.circle.setMap(null);
    }
    if (this.polygon) {
      this.polygon.setMap(null);
    }
    this.polygonPaths = delivery.area.coordinates[0].map((data: any) => {
      return { lat: data[0], lng: data[1] };
    });
    this.polygon = new google.maps.Polygon({
      paths: this.polygonPaths,
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      editable: true
    });
    this.polygon.setMap(this.googleMap.googleMap);
    const that = this;
    this.polygon.getPaths().forEach(function (path, index) {
      google.maps.event.addListener(path, 'set_at', function (e) {
        const arr = [];
        path.getArray().forEach(el => {
          arr.push(el.toJSON());
        });
        that.polygonPaths = arr;
      });
    });
    this.workTime = [];
    delivery.deliveryAvailableTimes.forEach((item: any) => {
      let time = [];
      item.times.forEach((el: any) => {
        const open =
          new Date(el.open.date).getHours() +
          ':' +
          this.correctMin(el.open.date);
        const close =
          new Date(el.close.date).getHours() +
          ':' +
          this.correctMin(el.close.date);
        time.push({ open, close });
      });
      this.workTime.push({
        day: item.day,
        time
      });
    });
    this.deliveryForm.patchValue({
      shop: shopId,
      area: delivery.area.coordinates[0],
      deliveryFee: delivery.deliveryFee,
      deliveryTime: delivery.deliveryTime,
      deliveryAvailableTimes: this.workTime,
      name: delivery.name,
      min: delivery.min,
      max: delivery.max,
      tax: delivery.tax || 0
    });
  }

  correctMin(data) {
    let min: any = new Date(data).getMinutes();
    if (min < 10) {
      min = `0${min}`;
    }
    return min;
  }

  onDelivery() {
    this.deliveryForm.markAllAsTouched();
    if (this.deliveryForm.invalid) {
      return;
    }
    if (this.editMode && this.editMode >= 0) {
      if (this.mapShapeMode === 'polygon') {
        this.deliveryForm.patchValue({
          area: this.polygonPaths
        });
      }
      this.deliveriesFacade.dispatch(
        fromActions.putDelivery({
          id: this.editMode,
          request: this.deliveryForm.value
        })
      );
    } else {
      if (this.mapShapeMode === 'polygon') {
        this.deliveryForm.patchValue({
          area: [...this.polygonPaths, this.polygonPaths[0]]
        });
      }
      this.deliveriesFacade.dispatch(
        fromActions.createDelivery({ request: this.deliveryForm.value })
      );
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject,
  ViewChild,
  ChangeDetectorRef
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GoogleMap } from '@angular/google-maps';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { Hydramember } from 'src/app/shared/interfaces/shop.interface';
import { getShops } from '../../shops/+store/shops.actions';
import { ShopsFacade } from '../../shops/+store/shops.facade';
import { TimeDialogComponent } from 'src/app/shared/components/time-dialog/time-dialog.component';

@Component({
  selector: 'app-edit-delivery',
  templateUrl: './edit-delivery.component.html',
  styleUrls: ['./edit-delivery.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditDeliveryComponent implements OnInit {
  @ViewChild('googleMap', { static: true }) googleMap: GoogleMap;
  deliveryForm: FormGroup;
  options: google.maps.MapOptions = {
    styles: [
      // {
      //   featureType: 'administrative',
      //   elementType: 'labels',
      //   stylers: [{ visibility: 'off' }]
      // },
      {
        featureType: 'poi',
        elementType: 'labels',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'water',
        elementType: 'labels',
        stylers: [{ visibility: 'off' }]
      },
      // {
      //   featureType: 'road',
      //   elementType: 'labels',
      //   stylers: [{ visibility: 'off' }]
      // },
      {
        featureType: 'transit.station',
        elementType: 'labels',
        stylers: [{ visibility: 'off' }]
      }
    ],
    streetViewControl: false,
    mapTypeControl: false,
    gestureHandling: 'cooperative'
    // scrollwheel: false
  };
  markerOptions: google.maps.MarkerOptions = { draggable: false };
  markerPosition: google.maps.LatLngLiteral;
  center: google.maps.LatLngLiteral;
  zoom = 17;
  shops: Hydramember[];
  findShop: Hydramember;
  deliveryAvailableTimes = [];
  poly: google.maps.Polygon;
  vertices;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    public shopsFacade: ShopsFacade,
    public dialogRef: MatDialogRef<EditDeliveryComponent>,
    private cd: ChangeDetectorRef,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.initializeDeliveryForm();
    this.shopsFacade.dispatch(getShops({ page: 'pagination=false' }));
    this.shopsFacade.shops$.subscribe((res: Hydramember[]) => {
      this.shops = res;
      const shop = this.data.shop.split('/');
      if (this.shops && this.shops.length) {
        const findShop = this.shops.find((s: Hydramember) => {
          return s.id === +shop[shop.length - 1];
        });
        this.center = {
          lat: findShop.location.coordinates[0],
          lng: findShop.location.coordinates[1]
        };
        this.markerPosition = {
          lat: findShop.location.coordinates[0],
          lng: findShop.location.coordinates[1]
        };
        this.vertices = this.data.area.coordinates[0].map((data: any) => {
          return { lat: data[0], lng: data[1] };
        });
        this.poly = new google.maps.Polygon({
          paths: this.vertices,
          strokeColor: '#FF0000',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.35,
          editable: true
        });
        this.poly.setMap(this.googleMap.googleMap);
      }
      this.cd.detectChanges();
    });
  }

  initializeDeliveryForm() {
    const shop = this.data.shop.split('/');
    const ht =
      new Date(this.data.deliveryTime).getHours() +
      ':' +
      new Date(this.data.deliveryTime).getMinutes();
    this.data.deliveryAvailableTimes.forEach((item: any) => {
      let time = [];
      item.times.forEach((el: any) => {
        const open = `${new Date(el.open.date).getHours()}:${new Date(
          el.open.date
        ).getMinutes()}`;
        const close = `${new Date(el.close.date).getHours()}:${new Date(
          el.close.date
        ).getMinutes()}`;
        time.push({ open, close });
      });
      this.deliveryAvailableTimes.push({
        day: item.day,
        time
      });
    });
    this.deliveryForm = this.formBuilder.group({
      shop: [+shop[shop.length - 1], [Validators.required]],
      area: [this.vertices, []],
      // minimumAmountOrder: [this.data.minimumAmountOrder, []],
      deliveryFee: [this.data.deliveryFee, []],
      deliveryTime: [ht, []],
      deliveryAvailableTimes: [this.deliveryAvailableTimes, []],
      min: [this.data.min, []],
      max: [this.data.max, []]
    });
  }

  addTime() {
    const dialogRef = this.dialog.open(TimeDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe((result: any[]) => {
      if (result) {
        result.forEach((element: any) => {
          const find = this.deliveryAvailableTimes.find((el: any) => {
            return element.day === el.day;
          });
          if (find) {
            find.time = [...find.time, ...element.time];
          } else {
            this.deliveryAvailableTimes = [
              ...this.deliveryAvailableTimes,
              element
            ];
          }
        });
        this.deliveryForm.patchValue({
          deliveryAvailableTimes: this.deliveryAvailableTimes
        });
      }
    });
  }

  deleteDay(index) {
    this.deliveryAvailableTimes.splice(index, 1);
    this.deliveryForm.patchValue({
      deliveryAvailableTimes: this.deliveryAvailableTimes
    });
  }

  selectShop(id) {
    const find = this.shops.find((element: Hydramember) => {
      return element.id === id;
    });
    this.center = {
      lat: find.location.coordinates[0],
      lng: find.location.coordinates[1]
    };
    this.zoom = 17;
    this.markerPosition = {
      lat: find.location.coordinates[0],
      lng: find.location.coordinates[1]
    };
  }

  onDelivery() {
    this.deliveryForm.markAllAsTouched();
    if (this.deliveryForm.invalid) {
      return;
    }
    var polygonBounds: any = this.poly.getPath();
    var bounds = [];
    for (var i = 0; i < polygonBounds.length; i++) {
      var point = {
        lat: polygonBounds.getAt(i).lat(),
        lng: polygonBounds.getAt(i).lng()
      };
      bounds.push(point);
    }
    this.deliveryForm.patchValue({
      area: bounds
    });
    this.dialogRef.close(this.deliveryForm.value);
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

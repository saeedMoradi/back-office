import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateDeliveryComponent } from './create-delivery/create-delivery.component';
import { DeliveriesListComponent } from './deliveries-list/deliveries-list.component';

import { DeliveriesComponent } from './deliveries.component';

const routes: Routes = [
  {
    path: '',
    component: DeliveriesComponent,
    children: [
      { path: '', redirectTo: 'create-delivery' },
      { path: 'deliveries-list', component: DeliveriesListComponent },
      { path: 'create-delivery', component: CreateDeliveryComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveriesRoutingModule {}

import { createAction, props } from '@ngrx/store';

export const getMerchants = createAction(
  '[Merchants] Get Merchants',
  props<{ page: any }>()
);
export const getMerchantsSuccess = createAction(
  '[Merchants] Get Merchants Success',
  props<{ response: any }>()
);
export const getMerchantsFail = createAction(
  '[Merchants] Get Merchants Fail',
  props<{ error: any }>()
);

export const createMerchant = createAction(
  '[Merchants] Create Merchant',
  props<{ request: any }>()
);
export const createMerchantSuccess = createAction(
  '[Merchants] Create Merchant Success',
  props<{ response: any }>()
);
export const createMerchantFail = createAction(
  '[Merchants] Create Merchant Fail',
  props<{ error: any }>()
);

export const getMerchant = createAction(
  '[Merchants] Get Merchant',
  props<{ id: any }>()
);
export const getMerchantSuccess = createAction(
  '[Merchants] Get Merchant Success',
  props<{ response: any }>()
);
export const getMerchantFail = createAction(
  '[Merchants] Get Merchant Fail',
  props<{ error: any }>()
);

export const deleteMerchant = createAction(
  '[Merchants] Delete Merchant',
  props<{ id: any }>()
);
export const deleteMerchantSuccess = createAction(
  '[Merchants] Delete Merchant Success',
  props<{ id: any }>()
);
export const deleteMerchantFail = createAction(
  '[Merchants] Delete Merchant Fail',
  props<{ error: any }>()
);

export const putMerchant = createAction(
  '[Merchants] Put Merchant',
  props<{ id: any; request: any }>()
);
export const putMerchantSuccess = createAction(
  '[Merchants] Put Merchant Success',
  props<{ response: any }>()
);
export const putMerchantFail = createAction(
  '[Merchants] Put Merchant Fail',
  props<{ error: any }>()
);

export const patchMerchant = createAction(
  '[Merchants] Patch Merchant',
  props<{ id: any; request: any }>()
);
export const patchMerchantSuccess = createAction(
  '[Merchants] Patch Merchant Success',
  props<{ response: any }>()
);
export const patchMerchantFail = createAction(
  '[Merchants] Patch Merchant Fail',
  props<{ error: any }>()
);

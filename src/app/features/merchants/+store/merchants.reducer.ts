import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './merchants.actions';
import { MerchantsState } from './merchants.interface';

export const initialState: MerchantsState = {
  isLoading: false,
  merchants: null,
  error: null
};

export const featureKey = 'merchants';

const merchantsReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): MerchantsState => ({ ...state, error: false })
  ),
  on(
    fromActions.getMerchantsSuccess,
    (state, action): MerchantsState => ({
      ...state,
      error: null,
      merchants: action.response
    })
  ),
  on(
    fromActions.getMerchantsFail,
    (state, action): MerchantsState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createMerchant,
    (state, action): MerchantsState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createMerchantSuccess,
    (state, action): MerchantsState => {
      if (!state.merchants) {
        return {
          ...state,
          merchants: {
            'hydra:member': [action.response],
            'hydra:totalItems': 1
          }
        };
      }
      return {
        ...state,
        error: null,
        isLoading: false,
        merchants: {
          ...state.merchants,
          'hydra:member': [action.response, ...state.merchants['hydra:member']],
          'hydra:totalItems': state.merchants['hydra:totalItems'] + 1
        }
      };
    }
  ),
  on(
    fromActions.createMerchantFail,
    (state, action): MerchantsState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteMerchantSuccess,
    (state, action): MerchantsState => {
      const newmerchants = state.merchants['hydra:member'].filter(
        (item: any) => {
          return item.id !== action.id;
        }
      );
      return {
        ...state,
        error: null,
        merchants: {
          ...state.merchants,
          'hydra:member': newmerchants,
          'hydra:totalItems': state.merchants['hydra:totalItems'] - 1
        }
      };
    }
  ),
  on(
    fromActions.putMerchantSuccess,
    (state, action): MerchantsState => {
      const newmerchants = state.merchants['hydra:member'].map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        merchants: { ...state.merchants, 'hydra:member': newmerchants }
      };
    }
  )
);

export function reducer(state: MerchantsState | undefined, action: Action) {
  return merchantsReducer(state, action);
}

import { createSelector, createFeatureSelector } from '@ngrx/store';
import { MerchantsState } from './merchants.interface';
import { featureKey } from './merchants.reducer';

export const selectFeature = createFeatureSelector<MerchantsState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: MerchantsState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: MerchantsState) => state.isLoading
);
export const selectMerchants = createSelector(
  selectFeature,
  (state: MerchantsState) =>
    state.merchants ? state.merchants['hydra:member'] : null
);
export const selectMerchantsTotalLength = createSelector(
  selectFeature,
  (state: MerchantsState) =>
    state.merchants ? state.merchants['hydra:totalItems'] : null
);

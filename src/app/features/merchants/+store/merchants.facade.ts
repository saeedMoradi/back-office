import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './merchants.selectors';

@Injectable({
  providedIn: 'root'
})
export class MerchantsFacade implements Facade {
  error$: Observable<boolean>;
  isLoading$: Observable<boolean>;
  merchants$: Observable<any>;
  merchantsTotalLength$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.isLoading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.merchants$ = store.pipe(select(fromSelector.selectMerchants));
    this.merchantsTotalLength$ = store.pipe(
      select(fromSelector.selectMerchantsTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

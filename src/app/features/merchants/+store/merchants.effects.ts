import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './merchants.actions';
import { MerchantsService } from '../services/merchants.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Injectable()
export class MerchantsEffects {
  constructor(
    private actions$: Actions,
    private merchantsService: MerchantsService,
    private matSnackBar: MatSnackBar,
    private router: Router
  ) {}

  getMerchants$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getMerchants),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.merchantsService.getMerchants(page).pipe(
          map((data: any) => {
            return fromActions.getMerchantsSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getMerchantsFail({ error })))
        );
      })
    )
  );

  createMerchant$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createMerchant),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.merchantsService.createMerchant(request).pipe(
          map((data: any) => {
            this.merchantsService.clearForm();
            this.matSnackBar.open('Company Created');
            this.router.navigate(['/home/company/merchants-list']);
            return fromActions.createMerchantSuccess({ response: data });
          }),
          catchError(error => of(fromActions.createMerchantFail({ error })))
        );
      })
    )
  );

  getMerchant$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getMerchant),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.merchantsService.getMerchant(id).pipe(
          map((data: any) => {
            return fromActions.getMerchantSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getMerchantFail({ error })))
        );
      })
    )
  );

  deleteMerchant$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteMerchant),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.merchantsService.deleteMerchant(id).pipe(
          map((data: any) => {
            return fromActions.deleteMerchantSuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteMerchantFail({ error }));
          })
        );
      })
    )
  );

  putMerchant$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putMerchant),
      switchMap(({ id, request }) => {
        return this.merchantsService.putMerchant(id, request).pipe(
          map((data: any) => {
            return fromActions.putMerchantSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putMerchantFail({ error }));
          })
        );
      })
    )
  );

  patchMerchant$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchMerchant),
      switchMap(({ id, request }) => {
        return this.merchantsService.patchMerchant(id, request).pipe(
          map((data: any) => {
            return fromActions.patchMerchantSuccess({ response: data });
          }),
          catchError(error => of(fromActions.patchMerchantFail({ error })))
        );
      })
    )
  );
}

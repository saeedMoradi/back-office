import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PersistanceService } from 'src/app/core/services/persistance.service';
import { Country, State, City, LoginResponse } from 'src/app/shared/interfaces';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MerchantsService {
  private readonly apiUrl = environment.apiUrl;
  private clearFormSource = new Subject<boolean>();
  clearForm$ = this.clearFormSource.asObservable();

  constructor(
    private http: HttpClient,
    private persistanceService: PersistanceService
  ) {}

  clearForm() {
    this.clearFormSource.next(true);
  }

  getMerchants(page = ''): Observable<any> {
    const profile: LoginResponse = this.persistanceService.get('profile');
    return this.http.get<any>(
      `${this.apiUrl}/api/merchants?owner.id=${profile.id}&order[id]=desc&${page}`
    );
  }
  createMerchant(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/merchants`, data);
  }
  getMerchant(id: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/merchants/${id}`);
  }
  deleteMerchant(id: any): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/api/merchants/${id}`);
  }
  putMerchant(id: any, data: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/api/merchants/${id}`, data);
  }
  patchMerchant(id: any, data: any): Observable<any> {
    return this.http.patch<any>(`${this.apiUrl}/api/merchants/${id}`, data);
  }
  getCountries(): Observable<Country[]> {
    return this.http.get<Country[]>(`assets/json/countries.json`);
  }
  getStates(id): Observable<State[]> {
    return this.http.get<State[]>(`assets/json/states/${id}.json`);
  }
  getCities(id): Observable<City[]> {
    return this.http.get<City[]>(`assets/json/cities/${id}.json`);
  }
}

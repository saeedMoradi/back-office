import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-edit-merchant',
  templateUrl: './edit-merchant.component.html',
  styleUrls: ['./edit-merchant.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditMerchantComponent implements OnInit {
  merchantForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditMerchantComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.initializeMerchantForm();
  }

  initializeMerchantForm() {
    this.merchantForm = this.formBuilder.group({
      companyName: [this.data.companyName, []],
      registrationAddress: [this.data.registrationAddress, []],
      phone: [this.data.phone, []],
      city: [this.data.city, []],
      country: [this.data.country, []],
      zipCode: [this.data.zipCode, []]
    });
  }

  onMerchant() {
    this.merchantForm.markAllAsTouched();
    if (this.merchantForm.invalid) {
      return;
    }
    this.dialogRef.close(this.merchantForm.value);
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

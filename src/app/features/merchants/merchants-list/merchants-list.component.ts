import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { MerchantsFacade } from '../+store/merchants.facade';
import {
  getMerchants,
  deleteMerchant,
  putMerchant
} from '../+store/merchants.actions';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { PaginationData } from 'src/app/shared/interfaces';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { EditMerchantComponent } from '../edit-merchant/edit-merchant.component';

@Component({
  selector: 'app-merchants-list',
  templateUrl: './merchants-list.component.html',
  styleUrls: ['./merchants-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MerchantsListComponent implements OnInit {
  merchants: any;
  originalMerchants: any;
  uniqueKey: any;
  destroy$ = new Subject();

  constructor(
    public merchantsFacade: MerchantsFacade,
    private cd: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.merchantsFacade.dispatch(getMerchants({ page: 'page=1' }));
    this.merchantsFacade.merchants$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        if (data) {
          this.merchants = data;
          this.originalMerchants = data;
          data.forEach(element => {
            this.uniqueKey = {
              ...this.uniqueKey,
              [element.id]: false
            };
          });
          this.cd.detectChanges();
        }
      });
  }

  search(term = '') {
    this.merchants = this.originalMerchants.filter((element: any) => {
      return element.companyName.toLowerCase().includes(term);
    });
  }

  onRemove(data) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.merchantsFacade.dispatch(deleteMerchant({ id: data.id }));
      }
    });
  }

  onEdit(data) {
    const dialogRef = this.dialog.open(EditMerchantComponent, {
      autoFocus: false,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.merchantsFacade.dispatch(
          putMerchant({ id: data.id, request: result })
        );
      }
    });
  }

  onExpand(data) {
    this.uniqueKey = {
      ...this.uniqueKey,
      [data.id]: !this.uniqueKey[data.id]
    };
  }

  onChangePage(event: PaginationData) {
    this.merchantsFacade.dispatch(
      getMerchants({ page: `page=${event.pageIndex + 1}` })
    );
  }

  trackByFn(index: number, item: any) {
    return index;
  }

  trackByFnDetails(index: number, item: any) {
    return index;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

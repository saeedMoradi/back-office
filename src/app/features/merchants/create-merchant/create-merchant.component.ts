import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef
} from '@angular/core';
import { FormGroup, FormBuilder, FormGroupDirective } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { map, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { MerchantsFacade } from '../+store/merchants.facade';
import { MerchantsService } from '../services/merchants.service';
import * as fromActions from '../+store/merchants.actions';
import { City, Country, State } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-create-merchant',
  templateUrl: './create-merchant.component.html',
  styleUrls: ['./create-merchant.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateMerchantComponent implements OnInit {
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  @ViewChild('postCode') postCode: ElementRef;
  merchantForm: FormGroup;
  countries$: Observable<Country[]>;
  cities$: Observable<City[]>;
  states: State[];
  filteredOptions: Observable<any[]>;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public merchantsFacade: MerchantsFacade,
    private merchantsService: MerchantsService,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.initializeMerchantForm();
    this.listenFormReset();
    this.getCountries();
  }

  ngAfterViewInit() {
    let that = this;
    let autocomplete = new google.maps.places.Autocomplete(
      this.postCode.nativeElement
    );
    autocomplete.setFields([
      'address_components',
      'geometry',
      'formatted_address'
    ]);
    autocomplete.addListener('place_changed', getPlace);
    function getPlace() {
      var place = autocomplete.getPlace();
      let filtred_country = place.address_components.filter(function (
        address_component
      ) {
        return address_component.types.includes('country');
      });
      let filtred_postcode = place.address_components.filter(function (
        address_component
      ) {
        return address_component.types.includes('postal_code');
      });
      let country = filtred_country.length ? filtred_country[0].long_name : '';
      let postcode = filtred_postcode.length
        ? filtred_postcode[0].long_name
        : '';
      that.merchantForm.patchValue({
        country,
        registrationAddress: place.formatted_address,
        zipCode: postcode
      });
      that.getStates(country);
      that.cd.detectChanges();
    }
  }

  initializeMerchantForm() {
    this.merchantForm = this.formBuilder.group({
      companyName: ['', []],
      registrationAddress: ['', []],
      phone: ['', []],
      city: ['', []],
      country: ['', []],
      zipCode: ['', []]
    });
    this.filteredOptions = this.merchantForm.get('city').valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  private _filter(value: string) {
    if (this.states) {
      return this.states.filter(option =>
        option.name.toLowerCase().includes(value)
      );
    }
    return;
  }

  getCountries() {
    this.countries$ = this.merchantsService.getCountries();
  }

  getStates(e: string) {
    this.countries$
      .pipe(
        switchMap((data: Country[]) => {
          const find = data.find((el: Country) => {
            return el.name === e;
          });
          return this.merchantsService.getStates(find.id);
        })
      )
      .subscribe((state: any) => {
        this.states = state;
        this.cd.detectChanges();
      });
  }

  getCities(e: number) {
    this.cities$ = this.merchantsService.getCities(e);
  }

  listenFormReset() {
    this.merchantsService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetMerchantForm();
      });
  }

  resetMerchantForm() {
    this.formDirective.resetForm();
    this.merchantForm.reset();
  }

  onMerchant() {
    this.merchantForm.markAllAsTouched();
    if (this.merchantForm.invalid) {
      return;
    }
    this.merchantsFacade.dispatch(
      fromActions.createMerchant({ request: this.merchantForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateMerchantComponent } from './create-merchant/create-merchant.component';
import { MerchantsListComponent } from './merchants-list/merchants-list.component';

import { MerchantsComponent } from './merchants.component';

const routes: Routes = [
  {
    path: '',
    component: MerchantsComponent,
    children: [
      { path: '', redirectTo: 'merchants-list' },
      { path: 'merchants-list', component: MerchantsListComponent },
      { path: 'create-merchant', component: CreateMerchantComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MerchantsRoutingModule {}

import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-merchants',
  templateUrl: './merchants.component.html',
  styleUrls: ['./merchants.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MerchantsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

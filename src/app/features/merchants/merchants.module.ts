import { NgModule } from '@angular/core';

import { MerchantsRoutingModule } from './merchants-routing.module';
import { MerchantsComponent } from './merchants.component';
import { CreateMerchantComponent } from './create-merchant/create-merchant.component';
import { MerchantsListComponent } from './merchants-list/merchants-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { EffectsModule } from '@ngrx/effects';
import { MerchantsEffects } from './+store/merchants.effects';
import { EditMerchantComponent } from './edit-merchant/edit-merchant.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

@NgModule({
  declarations: [
    MerchantsComponent,
    CreateMerchantComponent,
    MerchantsListComponent,
    EditMerchantComponent
  ],
  imports: [
    SharedModule,
    MerchantsRoutingModule,
    EffectsModule.forFeature([MerchantsEffects]),
    MatAutocompleteModule
  ]
})
export class MerchantsModule {}

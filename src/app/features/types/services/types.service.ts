import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PersistanceService } from 'src/app/core/services/persistance.service';
import { LoginResponse } from 'src/app/shared/interfaces';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TypesService {
  private readonly apiUrl = environment.apiUrl;
  private clearFormSource = new Subject<boolean>();
  clearForm$ = this.clearFormSource.asObservable();

  constructor(
    private http: HttpClient,
    private persistanceService: PersistanceService
  ) {}

  clearForm() {
    this.clearFormSource.next(true);
  }

  getTypes(page = ''): Observable<any> {
    const profile: LoginResponse = this.persistanceService.get('profile');
    return this.http.get<any>(
      `${this.apiUrl}/api/product_types?owner.id=${profile.id}&order[id]=desc&${page}`
    );
  }
  createType(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/product_types`, data);
  }
  getType(id: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/product_types/${id}`);
  }
  deleteType(id: any): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/api/product_types/${id}`);
  }
  putType(id: any, data: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/api/product_types/${id}`, data);
  }
  patchType(id: any, data: any): Observable<any> {
    return this.http.patch<any>(`${this.apiUrl}/api/product_types/${id}`, data);
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateTypeComponent } from './create-type/create-type.component';
import { TypesListComponent } from './types-list/types-list.component';

import { TypesComponent } from './types.component';

const routes: Routes = [
  {
    path: '',
    component: TypesComponent,
    children: [
      { path: '', redirectTo: 'types-list' },
      { path: 'types-list', component: TypesListComponent },
      { path: 'create-type', component: CreateTypeComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TypesRoutingModule {}

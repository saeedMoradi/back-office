import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormGroupDirective
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TypesFacade } from '../+store/types.facade';
import { TypesService } from '../services/types.service';
import * as fromActions from '../+store/types.actions';
import { ProductsFacade } from '../../products/+store/products.facade';
import { getProducts } from '../../products/+store/products.actions';

@Component({
  selector: 'app-create-type',
  templateUrl: './create-type.component.html',
  styleUrls: ['./create-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateTypeComponent implements OnInit {
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  typeForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public typesFacade: TypesFacade,
    public productsFacade: ProductsFacade,
    private typesService: TypesService
  ) {}

  ngOnInit() {
    this.initializeTypeForm();
    this.listenFormReset();
    this.productsFacade.dispatch(getProducts({ page: 'pagination=false' }));
  }

  initializeTypeForm() {
    this.typeForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', []]
      // products: [[], []]
    });
  }

  listenFormReset() {
    this.typesService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetTypeForm();
      });
  }

  resetTypeForm() {
    this.formDirective.resetForm();
    this.typeForm.reset();
  }

  onType() {
    this.typeForm.markAllAsTouched();
    if (this.typeForm.invalid) {
      return;
    }
    this.typesFacade.dispatch(
      fromActions.createType({ request: this.typeForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

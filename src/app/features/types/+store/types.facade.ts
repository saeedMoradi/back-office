import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './types.selectors';

@Injectable({
  providedIn: 'root'
})
export class TypesFacade implements Facade {
  isloading$: Observable<boolean>;
  error$: Observable<any>;
  types$: Observable<any>;
  typesTotalLength$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.types$ = store.pipe(select(fromSelector.selectTypes));
    this.typesTotalLength$ = store.pipe(
      select(fromSelector.selectTypesTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

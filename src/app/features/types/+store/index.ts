import * as typesReducer from './types.reducer';
import { TypesState } from './types.interface';
import * as typesSelectors from './types.selectors';

export { typesReducer, typesSelectors, TypesState };

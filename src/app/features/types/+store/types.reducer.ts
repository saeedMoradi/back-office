import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './types.actions';
import { TypesState } from './types.interface';

export const initialState: TypesState = {
  error: null,
  isLoading: false,
  types: null
};

export const featureKey = 'types';

const typesReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): TypesState => ({ ...state, error: false })
  ),
  on(
    fromActions.getTypesSuccess,
    (state, action): TypesState => ({
      ...state,
      error: null,
      types: action.response
    })
  ),
  on(
    fromActions.getTypesFail,
    (state, action): TypesState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createType,
    (state, action): TypesState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createTypesuccess,
    (state, action): TypesState => {
      if (!state.types) {
        return {
          ...state,
          types: {
            'hydra:member': [action.response],
            'hydra:totalItems': 1
          }
        };
      }
      return {
        ...state,
        error: null,
        isLoading: false,
        types: {
          ...state.types,
          'hydra:member': [action.response, ...state.types['hydra:member']],
          'hydra:totalItems': state.types['hydra:totalItems'] + 1
        }
      };
    }
  ),
  on(
    fromActions.createTypeFail,
    (state, action): TypesState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteTypeSuccess,
    (state, action): TypesState => {
      const newtypes = state.types['hydra:member'].filter((item: any) => {
        return item.id !== action.id;
      });
      return {
        ...state,
        error: null,
        types: {
          ...state.types,
          'hydra:member': newtypes,
          'hydra:totalItems': state.types['hydra:totalItems'] - 1
        }
      };
    }
  ),
  on(
    fromActions.putTypeSuccess,
    (state, action): TypesState => {
      const newtypes = state.types['hydra:member'].map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        types: { ...state.types, 'hydra:member': newtypes }
      };
    }
  )
);

export function reducer(state: TypesState, action: Action) {
  return typesReducer(state, action);
}

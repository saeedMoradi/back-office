import { createAction, props } from '@ngrx/store';

export const getTypes = createAction(
  '[Types] Get Types',
  props<{ page: any }>()
);
export const getTypesSuccess = createAction(
  '[Types] Get Types Success',
  props<{ response: any }>()
);
export const getTypesFail = createAction(
  '[Types] Get Types Fail',
  props<{ error: any }>()
);

export const createType = createAction(
  '[Types] Create Type',
  props<{ request: any }>()
);
export const createTypesuccess = createAction(
  '[Types] Create Type Success',
  props<{ response: any }>()
);
export const createTypeFail = createAction(
  '[Types] Create Type Fail',
  props<{ error: any }>()
);

export const getType = createAction('[Types] Get Type', props<{ id: any }>());
export const getTypeSuccess = createAction(
  '[Types] Get Type Success',
  props<{ response: any }>()
);
export const getTypeFail = createAction(
  '[Types] Get Type Fail',
  props<{ error: any }>()
);

export const deleteType = createAction(
  '[Types] Delete Type',
  props<{ id: any }>()
);
export const deleteTypeSuccess = createAction(
  '[Types] Delete Type Success',
  props<{ id: any }>()
);
export const deleteTypeFail = createAction(
  '[Types] Delete Type Fail',
  props<{ error: any }>()
);

export const putType = createAction(
  '[Types] Put Type',
  props<{ id: any; request: any }>()
);
export const putTypeSuccess = createAction(
  '[Types] Put Type Success',
  props<{ response: any }>()
);
export const putTypeFail = createAction(
  '[Types] Put Type Fail',
  props<{ error: any }>()
);

export const patchType = createAction(
  '[Types] Patch Type',
  props<{ id: any; request: any }>()
);
export const patchTypeSuccess = createAction(
  '[Types] Patch Type Success',
  props<{ response: any }>()
);
export const patchTypeFail = createAction(
  '[Types] Patch Type Fail',
  props<{ error: any }>()
);

import { createSelector, createFeatureSelector } from '@ngrx/store';
import { TypesState } from './types.interface';
import { featureKey } from './types.reducer';

export const selectFeature = createFeatureSelector<TypesState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: TypesState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: TypesState) => state.isLoading
);
export const selectTypes = createSelector(selectFeature, (state: TypesState) =>
  state.types ? state.types['hydra:member'] : null
);
export const selectTypesTotalLength = createSelector(
  selectFeature,
  (state: TypesState) => (state.types ? state.types['hydra:totalItems'] : null)
);

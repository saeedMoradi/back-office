import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './types.actions';
import { TypesService } from '../services/types.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Injectable()
export class TypesEffects {
  constructor(
    private actions$: Actions,
    private typesService: TypesService,
    private matSnackBar: MatSnackBar,
    private router: Router
  ) {}

  getTypes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getTypes),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.typesService.getTypes(page).pipe(
          map((data: any) => {
            return fromActions.getTypesSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getTypesFail({ error })))
        );
      })
    )
  );

  createType$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createType),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.typesService.createType(request).pipe(
          map((data: any) => {
            this.typesService.clearForm();
            this.matSnackBar.open('Type Created');
            this.router.navigate(['/home/types/types-list']);
            return fromActions.createTypesuccess({ response: data });
          }),
          catchError(error => of(fromActions.createTypeFail({ error })))
        );
      })
    )
  );

  getType$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getType),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.typesService.getType(id).pipe(
          map((data: any) => {
            return fromActions.getTypeSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getTypeFail({ error })))
        );
      })
    )
  );

  deleteType$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteType),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.typesService.deleteType(id).pipe(
          map((data: any) => {
            return fromActions.deleteTypeSuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteTypeFail({ error }));
          })
        );
      })
    )
  );

  putType$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putType),
      switchMap(({ id, request }) => {
        return this.typesService.putType(id, request).pipe(
          map((data: any) => {
            return fromActions.putTypeSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putTypeFail({ error }));
          })
        );
      })
    )
  );

  patchType$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchType),
      switchMap(({ id, request }) => {
        return this.typesService.patchType(id, request).pipe(
          map((data: any) => {
            return fromActions.patchTypeSuccess({ response: data });
          }),
          catchError(error => of(fromActions.patchTypeFail({ error })))
        );
      })
    )
  );
}

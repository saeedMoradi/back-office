import { NgModule } from '@angular/core';

import { TypesRoutingModule } from './types-routing.module';
import { SharedModule } from '../../shared/shared.module';
// Components
import { TypesComponent } from './types.component';
import { CreateTypeComponent } from './create-type/create-type.component';
import { TypesListComponent } from './types-list/types-list.component';
import { EditTypeComponent } from './edit-type/edit-type.component';
// NGRX
// import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { TypesEffects } from './+store/types.effects';
import { ProductsEffects } from '../products/+store/products.effects';
// import * as fromReducer from './+store/types.reducer';

@NgModule({
  declarations: [
    TypesComponent,
    CreateTypeComponent,
    TypesListComponent,
    EditTypeComponent
  ],
  imports: [
    SharedModule,
    TypesRoutingModule,
    // StoreModule.forFeature(fromReducer.featureKey, fromReducer.reducer),
    EffectsModule.forFeature([TypesEffects, ProductsEffects])
  ]
})
export class TypesModule {}

import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ChangeDetectorRef
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormGroupDirective,
  FormArray
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MenusFacade } from '../+store/menus.facade';
import { MenusService } from '../services/menus.service';
import * as fromActions from '../+store/menus.actions';
import { ProductsFacade } from '../../products/+store/products.facade';
import { ShopsFacade } from '../../shops/+store/shops.facade';
import { getShops } from '../../shops/+store/shops.actions';
import { getProducts } from '../../products/+store/products.actions';
import { TimeDialogComponent } from 'src/app/shared/components/time-dialog/time-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-create-menu',
  templateUrl: './create-menu.component.html',
  styleUrls: ['./create-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateMenuComponent implements OnInit {
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  menuForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public menusFacade: MenusFacade,
    public productsFacade: ProductsFacade,
    public shopsFacade: ShopsFacade,
    private menusService: MenusService,
    private dialog: MatDialog,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.initializeMenuForm();
    this.listenFormReset();
    this.shopsFacade.dispatch(getShops({ page: 'pagination=false' }));
    this.productsFacade.dispatch(getProducts({ page: 'pagination=false' }));
  }

  initializeMenuForm() {
    this.menuForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      shop: ['', [Validators.required]],
      productHasMenu: this.formBuilder.array([]),
      menuAvailableTimes: this.formBuilder.array([])
    });
  }

  get productHasMenuControl() {
    return this.menuForm.get('productHasMenu') as FormArray;
  }

  addProductHasMenu() {
    this.productHasMenuControl.push(
      this.formBuilder.group({
        product: ['', []],
        price: ['', []]
      })
    );
  }

  deleteProductHasMenu(index) {
    this.productHasMenuControl.removeAt(index);
  }

  get menuAvailableTimesControl() {
    return this.menuForm.get('menuAvailableTimes') as FormArray;
  }

  addMenuAvailableTimes() {
    this.menuAvailableTimesControl.push(
      this.formBuilder.group({
        from: ['', []],
        to: ['', []],
        days: [[], []]
      })
    );
  }

  deleteMenuAvailableTimes(index) {
    this.menuAvailableTimesControl.removeAt(index);
  }

  addTimes(index) {
    const dialogRef = this.dialog.open(TimeDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe((result: any[]) => {
      if (result) {
        const control = this.menuAvailableTimesControl.controls[index];
        let days = control.value.days;
        result.forEach((element: any) => {
          const find = days.find((el: any) => {
            return element.day === el.day;
          });
          if (find) {
            find.time = [...find.times, ...element.times];
          } else {
            days = [...days, element];
          }
        });
        control.patchValue({
          days
        });
        this.cd.detectChanges();
      }
    });
  }

  deleteDay(controlIndex, workTimeindex) {
    const control = this.menuAvailableTimesControl.controls[controlIndex];
    let days = control.value.days;
    days.splice(workTimeindex, 1);
    control.patchValue({
      days
    });
  }

  listenFormReset() {
    this.menusService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetMenuForm();
      });
  }

  resetMenuForm() {
    this.formDirective.resetForm();
    this.menuForm.reset();
    this.menuForm.setControl('productHasMenu', this.formBuilder.array([]));
    this.menuForm.setControl('menuAvailableTimes', this.formBuilder.array([]));
  }

  onMenu() {
    this.menuForm.markAllAsTouched();
    if (this.menuForm.invalid) {
      return;
    }
    this.menusFacade.dispatch(
      fromActions.createMenu({ request: this.menuForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

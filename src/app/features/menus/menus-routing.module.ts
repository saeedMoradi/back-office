import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateMenuComponent } from './create-menu/create-menu.component';
import { MenusListComponent } from './menus-list/menus-list.component';

import { MenusComponent } from './menus.component';

const routes: Routes = [
  {
    path: '',
    component: MenusComponent,
    children: [
      { path: '', redirectTo: 'menus-list' },
      { path: 'menus-list', component: MenusListComponent },
      { path: 'create-menu', component: CreateMenuComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenusRoutingModule {}

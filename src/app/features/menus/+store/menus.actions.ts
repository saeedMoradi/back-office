import { createAction, props } from '@ngrx/store';

export const getMenus = createAction(
  '[Menus] Get Menus',
  props<{ page: any }>()
);
export const getMenusSuccess = createAction(
  '[Menus] Get Menus Success',
  props<{ response: any }>()
);
export const getMenusFail = createAction(
  '[Menus] Get Menus Fail',
  props<{ error: any }>()
);

export const createMenu = createAction(
  '[Menus] Create Menu',
  props<{ request: any }>()
);
export const createMenuSuccess = createAction(
  '[Menus] Create Menu Success',
  props<{ response: any }>()
);
export const createMenuFail = createAction(
  '[Menus] Create Menu Fail',
  props<{ error: any }>()
);

export const getMenu = createAction('[Menus] Get Menu', props<{ id: any }>());
export const getMenuSuccess = createAction(
  '[Menus] Get Menu Success',
  props<{ response: any }>()
);
export const getMenuFail = createAction(
  '[Menus] Get Menu Fail',
  props<{ error: any }>()
);

export const deleteMenu = createAction(
  '[Menus] Delete Menu',
  props<{ id: any }>()
);
export const deleteMenuSuccess = createAction(
  '[Menus] Delete Menu Success',
  props<{ id: any }>()
);
export const deleteMenuFail = createAction(
  '[Menus] Delete Menu Fail',
  props<{ error: any }>()
);

export const putMenu = createAction(
  '[Menus] Put Menu',
  props<{ id: any; request: any }>()
);
export const putMenuSuccess = createAction(
  '[Menus] Put Menu Success',
  props<{ response: any }>()
);
export const putMenuFail = createAction(
  '[Menus] Put Menu Fail',
  props<{ error: any }>()
);

export const patchMenu = createAction(
  '[Menus] Patch Menu',
  props<{ id: any; request: any }>()
);
export const patchMenuSuccess = createAction(
  '[Menus] Patch Menu Success',
  props<{ response: any }>()
);
export const patchMenuFail = createAction(
  '[Menus] Patch Menu Fail',
  props<{ error: any }>()
);

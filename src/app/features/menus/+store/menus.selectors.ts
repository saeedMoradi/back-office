import { createSelector, createFeatureSelector } from '@ngrx/store';
import { MenusState } from './menus.interface';
import { featureKey } from './menus.reducer';

export const selectFeature = createFeatureSelector<MenusState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: MenusState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: MenusState) => state.isLoading
);
export const selectMenus = createSelector(selectFeature, (state: MenusState) =>
  state.menus ? state.menus['hydra:member'] : null
);
export const selectMenusTotalLength = createSelector(
  selectFeature,
  (state: MenusState) => (state.menus ? state.menus['hydra:totalItems'] : null)
);

import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './menus.selectors';

@Injectable({
  providedIn: 'root'
})
export class MenusFacade implements Facade {
  isloading$: Observable<boolean>;
  error$: Observable<any>;
  menus$: Observable<any>;
  menusTotalLength$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.menus$ = store.pipe(select(fromSelector.selectMenus));
    this.menusTotalLength$ = store.pipe(
      select(fromSelector.selectMenusTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './menus.actions';
import { MenusState } from './menus.interface';

export const initialState: MenusState = {
  error: null,
  isLoading: false,
  menus: null
};

export const featureKey = 'menus';

const menusReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): MenusState => ({ ...state, error: false })
  ),
  on(
    fromActions.getMenusSuccess,
    (state, action): MenusState => ({
      ...state,
      error: null,
      menus: action.response
    })
  ),
  on(
    fromActions.getMenusFail,
    (state, action): MenusState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createMenu,
    (state, action): MenusState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createMenuSuccess,
    (state, action): MenusState => {
      if (!state.menus) {
        return {
          ...state,
          menus: {
            'hydra:member': [action.response],
            'hydra:totalItems': 1
          }
        };
      }
      return {
        ...state,
        error: null,
        isLoading: false,
        menus: {
          ...state.menus,
          'hydra:member': [action.response, ...state.menus['hydra:member']],
          'hydra:totalItems': state.menus['hydra:totalItems'] + 1
        }
      };
    }
  ),
  on(
    fromActions.createMenuFail,
    (state, action): MenusState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteMenuSuccess,
    (state, action): MenusState => {
      const newmenus = state.menus['hydra:member'].filter((item: any) => {
        return item.id !== action.id;
      });
      return {
        ...state,
        error: null,
        menus: {
          ...state.menus,
          'hydra:member': newmenus,
          'hydra:totalItems': state.menus['hydra:totalItems'] - 1
        }
      };
    }
  ),
  on(
    fromActions.putMenuSuccess,
    (state, action): MenusState => {
      const newmenus = state.menus['hydra:member'].map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        menus: { ...state.menus, 'hydra:member': newmenus }
      };
    }
  )
);

export function reducer(state: MenusState, action: Action) {
  return menusReducer(state, action);
}

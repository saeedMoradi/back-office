import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './menus.actions';
import { MenusService } from '../services/menus.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class MenusEffects {
  constructor(
    private actions$: Actions,
    private menusService: MenusService,
    private matSnackBar: MatSnackBar
  ) {}

  getMenus$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getMenus),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.menusService.getMenus(page).pipe(
          map((data: any) => {
            return fromActions.getMenusSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getMenusFail({ error })))
        );
      })
    )
  );

  createMenu$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createMenu),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.menusService.createMenu(request).pipe(
          map((data: any) => {
            this.menusService.clearForm();
            this.matSnackBar.open('Menu Created');
            return fromActions.createMenuSuccess({ response: data });
          }),
          catchError(error => of(fromActions.createMenuFail({ error })))
        );
      })
    )
  );

  getMenu$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getMenu),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.menusService.getMenu(id).pipe(
          map((data: any) => {
            return fromActions.getMenuSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getMenuFail({ error })))
        );
      })
    )
  );

  deleteMenu$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteMenu),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.menusService.deleteMenu(id).pipe(
          map((data: any) => {
            return fromActions.deleteMenuSuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteMenuFail({ error }));
          })
        );
      })
    )
  );

  putMenu$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putMenu),
      switchMap(({ id, request }) => {
        return this.menusService.putMenu(id, request).pipe(
          map((data: any) => {
            return fromActions.putMenuSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putMenuFail({ error }));
          })
        );
      })
    )
  );

  patchMenu$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchMenu),
      switchMap(({ id, request }) => {
        return this.menusService.patchMenu(id, request).pipe(
          map((data: any) => {
            return fromActions.patchMenuSuccess({ response: data });
          }),
          catchError(error => of(fromActions.patchMenuFail({ error })))
        );
      })
    )
  );
}

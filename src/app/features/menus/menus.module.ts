import { NgModule } from '@angular/core';

import { MenusRoutingModule } from './menus-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
// Components
import { MenusComponent } from './menus.component';
import { CreateMenuComponent } from './create-menu/create-menu.component';
import { MenusListComponent } from './menus-list/menus-list.component';
import { EditMenuComponent } from './edit-menu/edit-menu.component';
// NGRX
// import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MenusEffects } from './+store/menus.effects';
import { ProductsEffects } from '../products/+store/products.effects';
import { ShopsEffects } from '../shops/+store/shops.effects';

@NgModule({
  declarations: [
    MenusComponent,
    CreateMenuComponent,
    MenusListComponent,
    EditMenuComponent
  ],
  imports: [
    SharedModule,
    MenusRoutingModule,
    EffectsModule.forFeature([MenusEffects, ShopsEffects, ProductsEffects])
  ]
})
export class MenusModule {}

import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { getShops } from '../../shops/+store/shops.actions';
import { ShopsFacade } from '../../shops/+store/shops.facade';

@Component({
  selector: 'app-edit-menu',
  templateUrl: './edit-menu.component.html',
  styleUrls: ['./edit-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditMenuComponent implements OnInit {
  menuForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditMenuComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public shopsFacade: ShopsFacade
  ) {}

  ngOnInit() {
    this.initializeMenuForm();
    this.shopsFacade.dispatch(getShops({ page: 'pagination=false' }));
  }

  initializeMenuForm() {
    const shop = this.data.shop.split('/');
    const productHasMenu = this.data.productHasMenu.map((res: any) => {
      return { product: res.id, price: res.price };
    });
    const menuAvailableTimes = this.data.menuAvailableTimes.map((res: any) => {
      const days = res.days.map((day: any) => {
        let times = [];
        day.times.forEach(el => {
          const open = `${new Date(el.open.date).getHours()}:${new Date(
            el.open.date
          ).getMinutes()}`;
          const close = `${new Date(el.close.date).getHours()}:${new Date(
            el.close.date
          ).getMinutes()}`;
          times.push({ open, close });
        });
        return { day: day.day, times };
      });
      return {
        from: res.fromDate,
        to: res.toDate,
        days
      };
    });
    this.menuForm = this.formBuilder.group({
      title: [this.data.title, [Validators.required]],
      description: [this.data.description, []],
      shop: [+shop[shop.length - 1], [Validators.required]],
      productHasMenu: [productHasMenu],
      menuAvailableTimes: [menuAvailableTimes]
    });
  }

  onMenu() {
    this.menuForm.markAllAsTouched();
    if (this.menuForm.invalid) {
      return;
    }
    this.dialogRef.close(this.menuForm.value);
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

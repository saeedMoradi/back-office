import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { MenusFacade } from '../+store/menus.facade';
import { getMenus, deleteMenu, putMenu } from '../+store/menus.actions';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PaginationData } from 'src/app/shared/interfaces';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { EditMenuComponent } from '../edit-menu/edit-menu.component';

@Component({
  selector: 'app-menus-list',
  templateUrl: './menus-list.component.html',
  styleUrls: ['./menus-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenusListComponent implements OnInit {
  menus: any;
  originalMenus: any;
  uniqueKey: any;
  destroy$ = new Subject();

  constructor(
    public menusFacade: MenusFacade,
    private cd: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.menusFacade.dispatch(getMenus({ page: 'page=1' }));
    this.menusFacade.menus$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        if (data) {
          this.menus = data;
          this.originalMenus = data;
          data.forEach(element => {
            this.uniqueKey = {
              ...this.uniqueKey,
              [element.id]: false
            };
          });
          this.cd.detectChanges();
        }
      });
  }

  search(term = '') {
    this.menus = this.originalMenus.filter((element: any) => {
      return (
        element.title.toLowerCase().includes(term) ||
        element.description.toLowerCase().includes(term)
      );
    });
  }

  onRemove(data) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.menusFacade.dispatch(deleteMenu({ id: data.id }));
      }
    });
  }

  onEdit(data) {
    const dialogRef = this.dialog.open(EditMenuComponent, {
      autoFocus: false,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.menusFacade.dispatch(putMenu({ id: data.id, request: result }));
      }
    });
  }

  onExpand(data) {
    this.uniqueKey = {
      ...this.uniqueKey,
      [data.id]: !this.uniqueKey[data.id]
    };
  }

  onChangePage(event: PaginationData) {
    this.menusFacade.dispatch(
      getMenus({ page: `page=${event.pageIndex + 1}` })
    );
  }

  trackByFn(index: number, item: any) {
    return index;
  }

  trackByFnDetails(index: number, item: any) {
    return index;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

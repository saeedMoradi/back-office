import { createServiceFactory, SpectatorService } from '@ngneat/spectator/jest';

import { TaskbarService } from './taskbar.service';

describe('Taskbar Service', () => {
  let spectator: SpectatorService<TaskbarService>;
  const createService = createServiceFactory({
    service: TaskbarService
  });

  beforeEach(() => (spectator = createService()));

  it('should be true', () => {
    expect('123').toEqual('123');
  });
});

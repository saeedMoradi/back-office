import { createServiceFactory, SpectatorService } from '@ngneat/spectator/jest';

import { NavbarService } from './navbar.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('Navbar Service', () => {
  let spectator: SpectatorService<NavbarService>;
  const createService = createServiceFactory({
    service: NavbarService
  });

  beforeEach(() => (spectator = createService()));

  it('should be true', () => {
    expect('123').toEqual('123');
  });
});

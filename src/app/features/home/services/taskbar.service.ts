import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable()
export class TaskbarService {
  elem: any;
  constructor(
    @Inject(DOCUMENT)
    private document: any
  ) {
    this.elem = this.document.documentElement;
  }

  get checkFullScreen() {
    return this.document.fullscreenElement ? true : false;
  }

  openFullscreen() {
    if (
      !this.document.fullscreenElement ||
      !this.document.webkitFullscreenElement ||
      !this.document.mozFullScreenElement
    ) {
      if (this.elem.requestFullscreen) {
        this.elem.requestFullscreen();
      } else if (this.elem.mozRequestFullScreen) {
        this.elem.mozRequestFullScreen();
      } else if (this.elem.webkitRequestFullscreen) {
        this.elem.webkitRequestFullscreen();
      } else if (this.elem.msRequestFullscreen) {
        this.elem.msRequestFullscreen();
      }
    }
  }

  closeFullscreen() {
    if (
      this.document.fullscreenElement ||
      this.document.webkitFullscreenElement ||
      this.document.mozFullScreenElement
    ) {
      if (this.document.exitFullscreen) {
        this.document.exitFullscreen();
      } else if (this.document.mozCancelFullScreen) {
        this.document.mozCancelFullScreen();
      } else if (this.document.webkitExitFullscreen) {
        this.document.webkitExitFullscreen();
      } else if (this.document.msExitFullscreen) {
        this.document.msExitFullscreen();
      }
    }
  }
}

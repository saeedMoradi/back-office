import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromSelector from './home.selectors';
import { HomeState } from './home.interface';
import { Facade } from '../../../shared/interfaces';

@Injectable({
  providedIn: 'root'
})
export class HomeFacade implements Facade {
  isMenuClose$: Observable<boolean>;
  isSettingsOpen$: Observable<boolean>;
  isPalletOpen$: Observable<boolean>;
  isFullScreenMode$: Observable<boolean>;

  constructor(private readonly store: Store<HomeState>) {
    this.isMenuClose$ = store.pipe(select(fromSelector.selectIsMenuClose));
    this.isSettingsOpen$ = store.pipe(
      select(fromSelector.selectIsSettingsOpen)
    );
    this.isPalletOpen$ = store.pipe(select(fromSelector.selectIsPalletOpen));
    this.isFullScreenMode$ = store.pipe(
      select(fromSelector.selectIsFullScreenMode)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

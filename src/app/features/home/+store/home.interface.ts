export interface HomeState {
  isMenuClose: boolean;
  isSettingsOpen: boolean;
  isPalletOpen: boolean;
  isFullScreenMode: boolean;
}

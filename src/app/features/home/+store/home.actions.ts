import { createAction, props } from '@ngrx/store';

export const changeMenu = createAction('[TaskBar] Change Menu');
export const changeSettings = createAction('[TaskBar] Change Settings');
export const changePallet = createAction('[TaskBar] Change Pallet');
export const changeFullScreenMode = createAction(
  '[TaskBar] Change FullScreen Mode'
);

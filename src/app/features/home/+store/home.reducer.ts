import { Action, createReducer, on } from '@ngrx/store';
import * as homeActions from './home.actions';
import { HomeState } from './home.interface';

export const initialState: HomeState = {
  isMenuClose: false,
  isSettingsOpen: false,
  isPalletOpen: false,
  isFullScreenMode: false
};

export const featureKey = 'home';

const homeReducer = createReducer(
  initialState,
  on(
    homeActions.changeMenu,
    (state): HomeState => ({ ...state, isMenuClose: !state.isMenuClose })
  ),
  on(
    homeActions.changeSettings,
    (state): HomeState => ({ ...state, isSettingsOpen: !state.isSettingsOpen })
  ),
  on(
    homeActions.changePallet,
    (state): HomeState => ({ ...state, isPalletOpen: !state.isPalletOpen })
  ),
  on(
    homeActions.changeFullScreenMode,
    (state): HomeState => ({
      ...state,
      isFullScreenMode: !state.isFullScreenMode
    })
  )
);

export function reducer(state: HomeState, action: Action) {
  return homeReducer(state, action);
}

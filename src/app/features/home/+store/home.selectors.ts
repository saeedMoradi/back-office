import { createSelector, createFeatureSelector } from '@ngrx/store';
import { HomeState } from './home.interface';
import { featureKey } from './home.reducer';

export const selectFeature = createFeatureSelector<HomeState>(featureKey);

export const selectIsMenuClose = createSelector(
  selectFeature,
  (state: HomeState) => state.isMenuClose
);

export const selectIsSettingsOpen = createSelector(
  selectFeature,
  (state: HomeState) => state.isSettingsOpen
);

export const selectIsPalletOpen = createSelector(
  selectFeature,
  (state: HomeState) => state.isPalletOpen
);

export const selectIsFullScreenMode = createSelector(
  selectFeature,
  (state: HomeState) => state.isFullScreenMode
);

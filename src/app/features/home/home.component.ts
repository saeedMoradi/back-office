import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject,
  NgZone,
  ChangeDetectorRef,
  ViewChild,
  ElementRef
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { fromEvent, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as homeActions from './+store/home.actions';
import { SettingsMenuComponent } from './settings-menu/settings-menu.component';
import { fadeInAnimation } from 'src/app/core/animations/fade-in.animation';
import { HomeFacade } from './+store/home.facade';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [fadeInAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {
  @ViewChild(SettingsMenuComponent, { read: ElementRef })
  SettingsMenuComponent: ElementRef;
  isSettingsOpen: boolean;
  destroy$ = new Subject();

  constructor(
    private homeFacade: HomeFacade,
    @Inject(DOCUMENT)
    private document: Document,
    private zone: NgZone,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.initializeData();
  }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      var el = this.SettingsMenuComponent.nativeElement;
      fromEvent(this.document, 'click')
        .pipe(takeUntil(this.destroy$))
        .subscribe((event: Event) => {
          if (this.isSettingsOpen) {
            if (!el.contains(event.target as Node)) {
              this.homeFacade.dispatch(homeActions.changeSettings());
              this.cd.detectChanges();
            }
          }
        });
    });
  }

  initializeData() {
    this.homeFacade.isSettingsOpen$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.isSettingsOpen = data;
      });
    this.homeFacade.isMenuClose$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        const element: HTMLElement = this.document.querySelector(
          '.route-container'
        );
        if (data) {
          element.classList.add('expand');
        } else {
          element.classList.remove('expand');
        }
      });
  }

  getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

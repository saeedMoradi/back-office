import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { SettingsMenuComponent } from './settings-menu.component';
describe('Settings Menu Component', () => {
  let spectator: Spectator<SettingsMenuComponent>;
  const createComponent = createComponentFactory({
    component: SettingsMenuComponent
  });

  beforeEach(() => (spectator = createComponent()));

  it('should create', () => {
    expect(spectator).toBeTruthy();
  });
});

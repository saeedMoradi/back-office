import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { HomeComponent } from './home.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NavbarComponent } from './navbar/navbar.component';
import { NavbarLinkComponent } from './navbar-link/navbar-link.component';
import { TaskbarComponent } from './taskbar/taskbar.component';
import { SettingsMenuComponent } from './settings-menu/settings-menu.component';
import { MenuSvgComponent } from './taskbar/svg/menu-svg.component';
import { BellSvgComponent } from './taskbar/svg/bell-svg.component';
import { PowerSvgComponent } from './taskbar/svg/power-svg.component';
import { PalletSvgComponent } from './taskbar/svg/pallet-svg.component';
import { MaximizeSvgComponent } from './taskbar/svg/maximize-svg.component';
import { SettingsSvgComponent } from './taskbar/svg/settings-svg.component';
import { MatIconModule } from '@angular/material/icon';
import { HomeFacade } from './+store/home.facade';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

class FakeHomeFacade {}

describe('Home Component', () => {
  let spectator: Spectator<HomeComponent>;
  let store: MockStore;
  const createComponent = createComponentFactory({
    component: HomeComponent,
    declarations: [
      NavbarComponent,
      TaskbarComponent,
      SettingsMenuComponent,
      NavbarLinkComponent,
      MenuSvgComponent,
      MaximizeSvgComponent,
      PalletSvgComponent,
      PowerSvgComponent,
      BellSvgComponent,
      SettingsSvgComponent
    ],
    // componentProviders: [HomeFacade],
    // providers: [provideMockStore()],
    providers: [
      { provide: HomeFacade, useClass: FakeHomeFacade },
      provideMockStore({})
    ],
    imports: [RouterTestingModule, MatIconModule]
  });

  beforeEach(() => (spectator = createComponent()));

  it('should create', () => {
    expect(spectator).toBeTruthy();
  });
});

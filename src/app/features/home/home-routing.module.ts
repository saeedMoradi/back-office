import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: '', redirectTo: 'company', pathMatch: 'full' },
      {
        path: 'general',
        loadChildren: () =>
          import('../general/general.module').then(m => m.GeneralModule)
      },
      {
        path: 'products',
        loadChildren: () =>
          import('../products/products.module').then(m => m.ProductsModule)
      },
      {
        path: 'accessories',
        loadChildren: () =>
          import('../accessories/accessories.module').then(
            m => m.AccessoriesModule
          )
      },
      {
        path: 'variations',
        loadChildren: () =>
          import('../variations/variations.module').then(
            m => m.VariationsModule
          )
      },
      {
        path: 'mealdeals',
        loadChildren: () =>
          import('../mealdeals/mealdeals.module').then(m => m.MealdealsModule)
      },
      {
        path: 'allergies',
        loadChildren: () =>
          import('../allergies/allergies.module').then(m => m.AllergiesModule)
      },
      {
        path: 'company',
        loadChildren: () =>
          import('../merchants/merchants.module').then(m => m.MerchantsModule)
      },
      {
        path: 'categories',
        loadChildren: () =>
          import('../categories/categories.module').then(
            m => m.CategoriesModule
          )
      },
      {
        path: 'departments',
        loadChildren: () =>
          import('../departments/departments.module').then(
            m => m.DepartmentsModule
          )
      },
      {
        path: 'managers',
        loadChildren: () =>
          import('../managers/managers.module').then(m => m.ManagersModule)
      },
      {
        path: 'store',
        loadChildren: () =>
          import('../shops/shops.module').then(m => m.ShopsModule)
      },
      {
        path: 'pickups',
        loadChildren: () =>
          import('../pickups/pickups.module').then(m => m.PickupsModule)
      },
      {
        path: 'preorders',
        loadChildren: () =>
          import('../preorders/preorders.module').then(m => m.PreOrdersModule)
      },
      {
        path: 'deliveries',
        loadChildren: () =>
          import('../deliveries/deliveries.module').then(
            m => m.DeliveriesModule
          )
      },
      {
        path: 'taxes',
        loadChildren: () => import('../tax/tax.module').then(m => m.TaxModule)
      },
      {
        path: 'discounts',
        loadChildren: () =>
          import('../discounts/discounts.module').then(m => m.DiscountsModule)
      },
      {
        path: 'types',
        loadChildren: () =>
          import('../types/types.module').then(m => m.TypesModule)
      },
      {
        path: 'menus',
        loadChildren: () =>
          import('../menus/menus.module').then(m => m.MenusModule)
      }
      // {
      //   path: 'payments',
      //   loadChildren: () =>
      //     import('../payments/payments.module').then(m => m.PaymentsModule)
      // }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}

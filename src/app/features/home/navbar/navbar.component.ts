import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ElementRef
} from '@angular/core';
import { HomeFacade } from '../+store/home.facade';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Link, Children } from 'src/app/shared/interfaces';

function createLink(
  routeName: string,
  routePath: any[],
  iconName: string,
  children: any[] = []
): Link {
  return {
    routeName,
    routePath,
    iconName,
    children
  };
}
function createChildLink(routeName: string, routePath: any[]): Children {
  return {
    routeName,
    routePath
  };
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements OnInit {
  links: Link[] = [
    createLink('Company', ['/home/company'], 'storefront'),
    createLink('Store', ['/home/store'], 'store'),
    createLink('Managers', ['/home/managers'], 'admin_panel_settings'),
    createLink('Products', null, 'style', [
      createChildLink('Departments', ['/home/departments']),
      createChildLink('Categories', ['/home/categories']),
      createChildLink('Products', ['/home/products']),
      createChildLink('Product Types', ['/home/types']),
      createChildLink('Accessories', ['/home/accessories']),
      createChildLink('Variations', ['/home/variations']),
      createChildLink('Meal Deals', ['/home/mealdeals']),
      createChildLink('Allergies', ['/home/allergies']),
      createChildLink('Menus', ['/home/menus'])
    ]),
    createLink('Service Type', null, 'directions_car', [
      createChildLink('Pickup', ['/home/pickups']),
      createChildLink('Delivery', ['/home/deliveries']),
      createChildLink('Order For Later', ['/home/preorders'])
    ]),
    createLink('Discounts', ['/home/discounts'], 'redeem'),
    createLink('Taxes', ['/home/taxes'], 'money'),
    // createLink('Payments', ['/home/payments'], 'payment'),
    createLink('General Settings', ['/home/general'], 'bubble_chart')
  ];
  destroy$ = new Subject();

  constructor(private homeFacade: HomeFacade, private el: ElementRef) {}

  ngOnInit(): void {
    this.initializeData();
  }

  initializeData() {
    this.homeFacade.isMenuClose$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        const element: HTMLElement = this.el.nativeElement;
        if (data) {
          element.classList.add('nav-closed');
        } else {
          element.classList.remove('nav-closed');
        }
      });
  }

  trackByFn(index, item: Link) {
    return item.routeName;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

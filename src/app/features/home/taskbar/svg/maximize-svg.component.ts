import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  AfterViewInit,
  ChangeDetectorRef
} from '@angular/core';

@Component({
  selector: 'app-maximize-svg',
  template: `
    <svg
      xmlns="http://www.w3.org/2000/svg"
      class="icon icon-tabler icon-tabler-maximize"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      stroke-width="2"
      stroke="currentColor"
      fill="none"
      stroke-linecap="round"
      stroke-linejoin="round"
    >
      <path stroke="none" d="M0 0h24v24H0z" />
      <path class="stroke" d="M4 8v-2a2 2 0 0 1 2 -2h2" />
      <path class="stroke" d="M4 16v2a2 2 0 0 0 2 2h2" />
      <path class="stroke" d="M16 4h2a2 2 0 0 1 2 2v2" />
      <path class="stroke" d="M16 20h2a2 2 0 0 0 2 -2v-2" />
    </svg>
  `,
  styleUrls: ['./svg.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MaximizeSvgComponent implements OnInit, AfterViewInit {
  constructor(private cd: ChangeDetectorRef) {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.cd.detach();
  }
}

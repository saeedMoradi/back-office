import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  AfterViewInit,
  ChangeDetectorRef
} from '@angular/core';

@Component({
  selector: 'app-pallet-svg',
  template: `
    <svg
      xmlns="http://www.w3.org/2000/svg"
      class="icon icon-tabler icon-tabler-loader"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      stroke-width="2"
      stroke="currentColor"
      fill="none"
      stroke-linecap="round"
      stroke-linejoin="round"
    >
      <path stroke="none" d="M0 0h24v24H0z" />
      <line class="stroke" x1="12" y1="6" x2="12" y2="3" />
      <line class="stroke" x1="16.25" y1="7.75" x2="18.4" y2="5.6" />
      <line class="stroke" x1="18" y1="12" x2="21" y2="12" />
      <line class="stroke" x1="16.25" y1="16.25" x2="18.4" y2="18.4" />
      <line class="stroke" x1="12" y1="18" x2="12" y2="21" />
      <line class="stroke" x1="7.75" y1="16.25" x2="5.6" y2="18.4" />
      <line class="stroke" x1="6" y1="12" x2="3" y2="12" />
      <line class="stroke" x1="7.75" y1="7.75" x2="5.6" y2="5.6" />
    </svg>
  `,
  styleUrls: ['./svg.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PalletSvgComponent implements OnInit, AfterViewInit {
  constructor(private cd: ChangeDetectorRef) {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.cd.detach();
  }
}

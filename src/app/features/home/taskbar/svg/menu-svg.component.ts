import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  AfterViewInit,
  ChangeDetectorRef
} from '@angular/core';

@Component({
  selector: 'app-menu-svg',
  template: `
    <svg
      xmlns="http://www.w3.org/2000/svg"
      class="icon icon-tabler icon-tabler-menu-2"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      stroke-width="2"
      stroke="currentColor"
      fill="none"
      stroke-linecap="round"
      stroke-linejoin="round"
    >
      <path stroke="none" d="M0 0h24v24H0z" />
      <line class="stroke" x1="4" y1="6" x2="20" y2="6" />
      <line class="stroke" x1="4" y1="12" x2="20" y2="12" />
      <line class="stroke" x1="4" y1="18" x2="20" y2="18" />
    </svg>
  `,
  styleUrls: ['./svg.component.scss'],
  styles: [
    `
      :host {
        margin-left: 0;
        width: 4rem;
      }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuSvgComponent implements OnInit, AfterViewInit {
  constructor(private cd: ChangeDetectorRef) {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.cd.detach();
  }
}

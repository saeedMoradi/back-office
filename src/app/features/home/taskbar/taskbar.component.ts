import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject,
  NgZone,
  ChangeDetectorRef
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { fromEvent, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HomeFacade } from '../+store/home.facade';
import * as homeActions from '../+store/home.actions';
import * as authActions from '../../auth/+store/auth.actions';
import { ThemeService } from 'src/app/core/services/theme.service';
import { TaskbarService } from '../services/taskbar.service';
import { AuthFacade } from '../../auth/+store/auth.facade';

@Component({
  selector: 'app-taskbar',
  templateUrl: './taskbar.component.html',
  styleUrls: ['./taskbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskbarComponent implements OnInit {
  isPalletOpen: boolean;
  isFullScreen: boolean;
  isSettingsOpen: boolean;
  destroy$ = new Subject();

  constructor(
    @Inject(DOCUMENT)
    private document,
    public authFacade: AuthFacade,
    private homeFacade: HomeFacade,
    private taskbarService: TaskbarService,
    private themeService: ThemeService,
    private zone: NgZone,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.initializeData();
  }

  ngAfterViewInit(): void {
    this.zone.runOutsideAngular(() => {
      var el = this.document.querySelector('.pallet-menu');
      fromEvent(this.document, 'click')
        .pipe(takeUntil(this.destroy$))
        .subscribe((event: Event) => {
          if (this.isPalletOpen) {
            if (!el.contains(event.target as Node)) {
              this.homeFacade.dispatch(homeActions.changePallet());
              this.cd.detectChanges();
            }
          }
        });
    });
    this.isFullScreen = this.taskbarService.checkFullScreen;
  }

  initializeData() {
    this.homeFacade.isPalletOpen$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.isPalletOpen = data;
      });
    this.homeFacade.isSettingsOpen$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.isSettingsOpen = data;
      });
  }

  changeMenu() {
    this.homeFacade.dispatch(homeActions.changeMenu());
  }

  changeSettings() {
    event.stopPropagation();
    if (this.isPalletOpen) {
      this.homeFacade.dispatch(homeActions.changePallet());
    }
    this.homeFacade.dispatch(homeActions.changeSettings());
  }

  changeNotify() {}

  changePallet() {
    event.stopPropagation();
    if (this.isSettingsOpen) {
      this.homeFacade.dispatch(homeActions.changeSettings());
    }
    this.homeFacade.dispatch(homeActions.changePallet());
  }

  selectTheme(data: string) {
    this.themeService.switchTheme(data);
    this.homeFacade.dispatch(homeActions.changePallet());
  }

  changeMaximize() {
    if (!this.isFullScreen) {
      this.taskbarService.openFullscreen();
      this.isFullScreen = true;
    } else {
      this.taskbarService.closeFullscreen();
      this.isFullScreen = false;
    }
  }

  logout() {
    if (this.isSettingsOpen) {
      this.homeFacade.dispatch(homeActions.changeSettings());
    }
    if (this.isPalletOpen) {
      this.homeFacade.dispatch(homeActions.changePallet());
    }
    this.homeFacade.dispatch(authActions.logout());
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

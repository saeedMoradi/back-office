import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { TaskbarComponent } from './taskbar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MenuSvgComponent } from './svg/menu-svg.component';
import { BellSvgComponent } from './svg/bell-svg.component';
import { PowerSvgComponent } from './svg/power-svg.component';
import { PalletSvgComponent } from './svg/pallet-svg.component';
import { MaximizeSvgComponent } from './svg/maximize-svg.component';
import { SettingsSvgComponent } from './svg/settings-svg.component';
import { provideMockStore } from '@ngrx/store/testing';
import { HomeFacade } from '../+store/home.facade';
import { TaskbarService } from '../services/taskbar.service';

describe('Taskbar Component', () => {
  let spectator: Spectator<TaskbarComponent>;
  const createComponent = createComponentFactory({
    component: TaskbarComponent,
    declarations: [
      MenuSvgComponent,
      MaximizeSvgComponent,
      PalletSvgComponent,
      PowerSvgComponent,
      BellSvgComponent,
      SettingsSvgComponent
    ],
    componentProviders: [HomeFacade, TaskbarService],
    providers: [provideMockStore()],
    imports: [RouterTestingModule]
  });

  beforeEach(() => (spectator = createComponent()));

  it('should create', () => {
    expect(spectator).toBeTruthy();
  });
});

import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ChangeDetectorRef
} from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Link, Children } from 'src/app/shared/interfaces';
import { RouterFacade } from '../../../core/+store/router.facade';

@Component({
  selector: 'app-navbar-link',
  templateUrl: './navbar-link.component.html',
  styleUrls: ['./navbar-link.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarLinkComponent implements OnInit {
  _link: Link;
  @Input('link') set link(data: Link) {
    this._link = data;
    if (
      this._link &&
      this._link.routePath &&
      this._link.routePath[0] === this.currentUrl
    ) {
      this.active = true;
    } else {
      if (this._link && !this._link.routePath && this._link.children.length) {
        const find = this._link.children.find((child: Children) => {
          return child.routePath[0] === this.currentUrl;
        });
        if (find) {
          this.active = true;
          this.isChildRoutesOpen = true;
        } else {
          this.active = false;
        }
      } else {
        this.active = false;
      }
    }
  }
  currentUrl: string;
  active: boolean;
  isChildRoutesOpen: boolean = false;
  destroy$ = new Subject();

  constructor(
    private routerFacade: RouterFacade,
    private router: Router,
    private cd: ChangeDetectorRef
  ) {
    this.routerFacade.selectUrl$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: string) => {
        if (this._link && this._link.children && this._link.children.length) {
          this.currentUrl = data;
          if (
            this._link &&
            this._link.routePath &&
            this._link.routePath[0] === this.currentUrl
          ) {
            this.active = true;
            this.cd.detectChanges();
            return;
          }
          if (
            this._link &&
            this._link.routePath &&
            this._link.routePath[0] !== this.currentUrl
          ) {
            this.active = false;
            this.cd.detectChanges();
            return;
          }
          if (
            this._link &&
            !this._link.routePath &&
            this._link.children.length
          ) {
            const find = this._link.children.find((child: Children) => {
              return child.routePath[0] === data;
            });
            if (find) {
              this.active = true;
              this.cd.detectChanges();
            } else {
              this.active = false;
              this.cd.detectChanges();
            }
          }
        }
      });
  }

  ngOnInit(): void {}

  navigate() {
    if (this._link.routePath && !this._link.children.length) {
      this.router.navigate(this._link.routePath);
    } else {
      this.isChildRoutesOpen = !this.isChildRoutesOpen;
    }
  }

  trackByFn(index, item: Children) {
    return item.routeName;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

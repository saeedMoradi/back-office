import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { MatIconModule } from '@angular/material/icon';
// NGRX
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
// import { AuthEffects } from './+store/home.effects';
import * as fromHomeReducer from './+store/home.reducer';
// Components
import { HomeComponent } from './home.component';
import { TaskbarComponent } from './taskbar/taskbar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NavbarLinkComponent } from './navbar-link/navbar-link.component';
import { SettingsMenuComponent } from './settings-menu/settings-menu.component';
// SVG
import { MenuSvgComponent } from './taskbar/svg/menu-svg.component';
import { SettingsSvgComponent } from './taskbar/svg/settings-svg.component';
import { MaximizeSvgComponent } from './taskbar/svg/maximize-svg.component';
import { BellSvgComponent } from './taskbar/svg/bell-svg.component';
import { PowerSvgComponent } from './taskbar/svg/power-svg.component';
import { PalletSvgComponent } from './taskbar/svg/pallet-svg.component';
// Services
import { TaskbarService } from './services/taskbar.service';
import { NavbarService } from './services/navbar.service';

const CONPONENTS = [
  HomeComponent,
  TaskbarComponent,
  NavbarComponent,
  NavbarLinkComponent,
  MenuSvgComponent,
  SettingsSvgComponent,
  MaximizeSvgComponent,
  BellSvgComponent,
  PowerSvgComponent,
  SettingsMenuComponent,
  PalletSvgComponent
];

const MATERIAL_MODULES = [MatIconModule];

@NgModule({
  declarations: CONPONENTS,
  providers: [TaskbarService, NavbarService],
  imports: [
    CommonModule,
    HomeRoutingModule,
    StoreModule.forFeature(fromHomeReducer.featureKey, fromHomeReducer.reducer),
    // EffectsModule.forFeature([AuthEffects]),
    ...MATERIAL_MODULES
  ]
})
export class HomeModule {}

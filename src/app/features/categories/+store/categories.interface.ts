export interface CategoriesState {
  error: any;
  isLoading: boolean;
  categories: any;
  categoriesWithGraphql: any;
}

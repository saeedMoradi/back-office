import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './categories.actions';
import { CategoriesState } from './categories.interface';

export const initialState: CategoriesState = {
  error: null,
  isLoading: false,
  categories: null,
  categoriesWithGraphql: null
};

export const featureKey = 'categories';

const categoriesReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): CategoriesState => ({ ...state, error: null })
  ),
  on(
    fromActions.getCategoriesSuccess,
    (state, action): CategoriesState => ({
      ...state,
      error: null,
      categories: action.response
    })
  ),
  on(
    fromActions.getCategoriesWithGraphqlSuccess,
    (state, action): CategoriesState => ({
      ...state,
      error: null,
      categoriesWithGraphql: action.response
    })
  ),
  on(
    fromActions.getCategoriesFail,
    (state, action): CategoriesState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createCategory,
    (state, action): CategoriesState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createCategorySuccess,
    (state, action): CategoriesState => {
      if (!state.categories) {
        return {
          ...state,
          categories: {
            'hydra:member': [action.response],
            'hydra:totalItems': 1
          }
        };
      }
      return {
        ...state,
        error: null,
        isLoading: false,
        categories: {
          ...state.categories,
          'hydra:member': [
            action.response,
            ...state.categories['hydra:member']
          ],
          'hydra:totalItems': state.categories['hydra:totalItems'] + 1
        }
      };
    }
  ),
  on(
    fromActions.createCategoryFail,
    (state, action): CategoriesState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteCategorySuccess,
    (state, action): CategoriesState => {
      const newcategories = state.categories['hydra:member'].filter(
        (item: any) => {
          return item.id !== action.id;
        }
      );
      return {
        ...state,
        error: null,
        categories: {
          ...state.categories,
          'hydra:member': newcategories,
          'hydra:totalItems': state.categories['hydra:totalItems'] - 1
        }
      };
    }
  ),
  on(
    fromActions.putCategorySuccess,
    (state, action): CategoriesState => {
      const newcategories = state.categories['hydra:member'].map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        categories: { ...state.categories, 'hydra:member': newcategories }
      };
    }
  )
);

export function reducer(state: CategoriesState, action: Action) {
  return categoriesReducer(state, action);
}

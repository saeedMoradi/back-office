import * as categoriesReducer from './categories.reducer';
import { CategoriesState } from './categories.interface';
import * as categoriesSelectors from './categories.selectors';

export { categoriesReducer, categoriesSelectors, CategoriesState };

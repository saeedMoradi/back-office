import { createSelector, createFeatureSelector } from '@ngrx/store';
import { CategoriesState } from './categories.interface';
import { featureKey } from './categories.reducer';

export const selectFeature = createFeatureSelector<CategoriesState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: CategoriesState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: CategoriesState) => state.isLoading
);
export const selectCategoriesWithGraphql = createSelector(
  selectFeature,
  (state: CategoriesState) =>
    state.categoriesWithGraphql
      ? state.categoriesWithGraphql.categories.edges
      : null
);
export const selectCategories = createSelector(
  selectFeature,
  (state: CategoriesState) =>
    state.categories ? state.categories['hydra:member'] : null
);
export const selectCategoriesTotalLength = createSelector(
  selectFeature,
  (state: CategoriesState) =>
    state.categories ? state.categories['hydra:totalItems'] : null
);

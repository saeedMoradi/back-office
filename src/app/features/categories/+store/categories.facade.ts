import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './categories.selectors';

@Injectable({
  providedIn: 'root'
})
export class CategoriesFacade implements Facade {
  isloading$: Observable<boolean>;
  error$: Observable<any>;
  categories$: Observable<any>;
  categoriesTotalLength$: Observable<any>;
  categoriesWithGraphql$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.categoriesWithGraphql$ = store.pipe(
      select(fromSelector.selectCategoriesWithGraphql)
    );
    this.categories$ = store.pipe(select(fromSelector.selectCategories));
    this.categoriesTotalLength$ = store.pipe(
      select(fromSelector.selectCategoriesTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

import { createAction, props } from '@ngrx/store';

export const getCategories = createAction(
  '[Categories] Get Categories',
  props<{ page: any }>()
);
export const getCategoriesSuccess = createAction(
  '[Categories] Get Categories Success',
  props<{ response: any }>()
);
export const getCategoriesFail = createAction(
  '[Categories] Get Categories Fail',
  props<{ error: any }>()
);

export const getCategoriesWithGraphql = createAction(
  '[Categories] Get Categories With Graphql'
);
export const getCategoriesWithGraphqlSuccess = createAction(
  '[Categories] Get Categories With Graphql Success',
  props<{ response: any }>()
);
export const getCategoriesWithGraphqlFail = createAction(
  '[Categories] Get Categories With Graphql Fail',
  props<{ error: any }>()
);

export const createCategory = createAction(
  '[Categories] Create Category',
  props<{ request: any }>()
);
export const createCategorySuccess = createAction(
  '[Categories] Create Category Success',
  props<{ response: any }>()
);
export const createCategoryFail = createAction(
  '[Categories] Create Category Fail',
  props<{ error: any }>()
);

export const getCategory = createAction(
  '[Categories] Get Category',
  props<{ id: any }>()
);
export const getCategorySuccess = createAction(
  '[Categories] Get Category Success',
  props<{ response: any }>()
);
export const getCategoryFail = createAction(
  '[Categories] Get Category Fail',
  props<{ error: any }>()
);

export const deleteCategory = createAction(
  '[Categories] Delete Category',
  props<{ id: any }>()
);
export const deleteCategorySuccess = createAction(
  '[Categories] Delete Category Success',
  props<{ id: any }>()
);
export const deleteCategoryFail = createAction(
  '[Categories] Delete Category Fail',
  props<{ error: any }>()
);

export const putCategory = createAction(
  '[Categories] Put Category',
  props<{ id: any; request: any }>()
);
export const putCategorySuccess = createAction(
  '[Categories] Put Category Success',
  props<{ response: any }>()
);
export const putCategoryFail = createAction(
  '[Categories] Put Category Fail',
  props<{ error: any }>()
);

export const patchCategory = createAction(
  '[Categories] Patch Category',
  props<{ id: any; request: any }>()
);
export const patchCategorySuccess = createAction(
  '[Categories] Patch Category Success',
  props<{ response: any }>()
);
export const patchCategoryFail = createAction(
  '[Categories] Patch Category Fail',
  props<{ error: any }>()
);

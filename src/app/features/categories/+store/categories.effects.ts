import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './categories.actions';
import { CategoriesService } from '../services/categories.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Apollo, gql } from 'apollo-angular';

const GET_CATEGORIES = gql`
  {
    categories(first: 100) {
      totalCount
      edges {
        node {
          id
          _id
          title
          products {
            edges {
              node {
                id
                _id
                title
              }
            }
            totalCount
          }
        }
      }
    }
  }
`;

@Injectable()
export class CategoriesEffects {
  constructor(
    private actions$: Actions,
    private categoriesService: CategoriesService,
    private matSnackBar: MatSnackBar,
    private router: Router,
    private apollo: Apollo
  ) {}

  getCategories$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getCategories),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.categoriesService.getCategories(page).pipe(
          map((data: any) => {
            return fromActions.getCategoriesSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.getCategoriesFail({ error }));
          })
        );
      })
    )
  );

  getCategoriesWithGraphql$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getCategoriesWithGraphql),
      switchMap(() => {
        const queryRef = this.apollo.watchQuery<any>({
          query: GET_CATEGORIES
        });
        queryRef.refetch();
        return queryRef.valueChanges.pipe(
          map(({ data }) => {
            return fromActions.getCategoriesWithGraphqlSuccess({
              response: data
            });
          }),
          catchError(error => {
            return of(fromActions.getCategoriesWithGraphqlFail({ error }));
          })
        );
      })
    )
  );

  createCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createCategory),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.categoriesService.createCategory(request).pipe(
          map((data: any) => {
            this.categoriesService.clearForm();
            this.matSnackBar.open('Category Created');
            this.router.navigate(['/home/categories/categories-list']);
            return fromActions.createCategorySuccess({ response: data });
          }),
          catchError(error => of(fromActions.createCategoryFail({ error })))
        );
      })
    )
  );

  getCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getCategory),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.categoriesService.getCategory(id).pipe(
          map((data: any) => {
            return fromActions.getCategorySuccess({ response: data });
          }),
          catchError(error => of(fromActions.getCategoryFail({ error })))
        );
      })
    )
  );

  deleteCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteCategory),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.categoriesService.deleteCategory(id).pipe(
          map((data: any) => {
            return fromActions.deleteCategorySuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteCategoryFail({ error }));
          })
        );
      })
    )
  );

  putCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putCategory),
      switchMap(({ id, request }) => {
        return this.categoriesService.putCategory(id, request).pipe(
          map((data: any) => {
            return fromActions.putCategorySuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putCategoryFail({ error }));
          })
        );
      })
    )
  );

  patchCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchCategory),
      switchMap(({ id, request }) => {
        return this.categoriesService.patchCategory(id, request).pipe(
          map((data: any) => {
            return fromActions.patchCategorySuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.patchCategoryFail({ error }));
          })
        );
      })
    )
  );
}

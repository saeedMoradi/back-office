import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ChangeDetectorRef,
  ElementRef
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormGroupDirective,
  Validators,
  FormArray
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CategoriesFacade } from '../+store/categories.facade';
import * as fromActions from '../+store/categories.actions';
import { CategoriesService } from '../services/categories.service';
import { HttpEventType } from '@angular/common/http';
import { MediaObjects } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateCategoryComponent implements OnInit {
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  @ViewChild('coverInput', { static: false }) coverInput: ElementRef;
  @ViewChild('thumbnailInput', { static: false }) thumbnailInput: ElementRef;
  @ViewChild('menuThumbnailInput', { static: false })
  menuThumbnailInput: ElementRef;
  categoryForm: FormGroup;
  fileList = [];
  coverProgress: number;
  thumnailProgress: number;
  menuThumnailProgress: number;
  coverImageData: MediaObjects;
  thumnailImageData: MediaObjects;
  menuThumnailImageData: MediaObjects;
  coverImageLoading: boolean;
  thumnailImageLoading: boolean;
  menuThumnailImageLoading: boolean;
  uploadError;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public categoriesFacade: CategoriesFacade,
    private categoriesService: CategoriesService,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.initializeCategoryForm();
    this.listenFormReset();
    this.categoriesFacade.dispatch(
      fromActions.getCategories({ page: 'pagination=false' })
    );
  }

  initializeCategoryForm() {
    this.categoryForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      parent: [null, []],
      description: ['', []],
      displayed: [true, []],
      coverImage: [null, []],
      thumbnail: [null, []],
      menuThumbnail: [null, []],
      metaTitle: ['', []],
      metaDescription: ['', []]
    });
  }

  addImage(data: string) {
    let fileCatInput;
    if (data === 'cover') {
      fileCatInput = this.coverInput.nativeElement;
    }
    if (data === 'thumbnailImage') {
      fileCatInput = this.thumbnailInput.nativeElement;
    }
    if (data === 'menuthumbnailImage') {
      fileCatInput = this.menuThumbnailInput.nativeElement;
    }
    fileCatInput.onchange = () => {
      for (let index = 0; index < fileCatInput.files.length; index++) {
        const file = fileCatInput.files[index];
        this.fileList.push({ data: file, inProgress: false, progress: 0 });
      }
      this.selectFile(data);
    };
    fileCatInput.click();
  }

  selectFile(data: string): void {
    const formData: FormData = new FormData();
    formData.append('file', this.fileList[0].data, this.fileList[0].data.name);
    this.categoriesService
      .uploadFile(formData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        event => {
          if (event.type === HttpEventType.UploadProgress && data === 'cover') {
            this.coverProgress = Math.round((100 * event.loaded) / event.total);
            this.cd.detectChanges();
          }
          if (
            event.type === HttpEventType.UploadProgress &&
            data === 'thumbnailImage'
          ) {
            this.thumnailProgress = Math.round(
              (100 * event.loaded) / event.total
            );
            this.cd.detectChanges();
          }
          if (
            event.type === HttpEventType.UploadProgress &&
            data === 'menuthumbnailImage'
          ) {
            this.menuThumnailProgress = Math.round(
              (100 * event.loaded) / event.total
            );
            this.cd.detectChanges();
          }
          if (event.type === HttpEventType.Response) {
            if (data === 'cover') {
              this.coverImageData = event.body;
              this.coverImageLoading = false;
              const image = this.coverImageData['@id'].split('/');
              this.categoryForm.patchValue({
                coverImage: `/api/media_objects/${+image[image.length - 1]}`
              });
              this.cd.detectChanges();
            }
            if (data === 'thumbnailImage') {
              this.thumnailImageData = event.body;
              this.thumnailImageLoading = false;
              const image = this.thumnailImageData['@id'].split('/');
              this.categoryForm.patchValue({
                thumbnail: `/api/media_objects/${+image[image.length - 1]}`
              });
              this.cd.detectChanges();
            }
            if (data === 'menuthumbnailImage') {
              this.menuThumnailImageData = event.body;
              this.menuThumnailImageLoading = false;
              const image = this.menuThumnailImageData['@id'].split('/');
              this.categoryForm.patchValue({
                menuThumbnail: `/api/media_objects/${+image[image.length - 1]}`
              });
              this.cd.detectChanges();
            }
          }
        },
        error => {
          this.coverImageLoading = false;
          this.thumnailImageLoading = false;
          this.menuThumnailImageLoading = false;
          this.uploadError = error;
          this.cd.detectChanges();
        }
      );
  }

  listenFormReset() {
    this.categoriesService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetCategoryForm();
      });
  }

  resetCategoryForm() {
    this.formDirective.resetForm();
    this.categoryForm.reset({
      displayed: true
    });
  }

  addProductHasMenu() {
    this.subHasMenuControl.push(
      this.formBuilder.group({
        sub: ['', []]
      })
    );
  }

  get subHasMenuControl() {
    return this.categoryForm.get('subHasMenu') as FormArray;
  }

  deleteProductHasMenu(index) {
    this.subHasMenuControl.removeAt(index);
  }

  onCategory() {
    this.categoryForm.markAllAsTouched();
    if (this.categoryForm.invalid) {
      return;
    }
    this.categoriesFacade.dispatch(
      fromActions.createCategory({ request: this.categoryForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

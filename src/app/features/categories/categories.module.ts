import { NgModule } from '@angular/core';

import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesComponent } from './categories.component';
import { CategoriesListComponent } from './categories-list/categories-list.component';
import { CreateCategoryComponent } from './create-category/create-category.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { EffectsModule } from '@ngrx/effects';
import { CategoriesEffects } from './+store/categories.effects';
import { EditCategoryComponent } from './edit-category/edit-category.component';

@NgModule({
  declarations: [
    CategoriesComponent,
    CategoriesListComponent,
    CreateCategoryComponent,
    EditCategoryComponent
  ],
  imports: [
    SharedModule,
    CategoriesRoutingModule,
    EffectsModule.forFeature([CategoriesEffects])
  ]
})
export class CategoriesModule {}

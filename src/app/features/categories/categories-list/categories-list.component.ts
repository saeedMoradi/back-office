import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { PaginationData } from 'src/app/shared/interfaces';
import { CategoriesFacade } from '../+store/categories.facade';
import {
  getCategories,
  deleteCategory,
  putCategory
} from '../+store/categories.actions';
import { EditCategoryComponent } from '../edit-category/edit-category.component';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoriesListComponent implements OnInit {
  categories: any;
  originalCategories: any;
  uniqueKey: any;
  destroy$ = new Subject();

  constructor(
    public categoriesFacade: CategoriesFacade,
    private cd: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.categoriesFacade.dispatch(getCategories({ page: 'page=1' }));
    this.categoriesFacade.categories$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        if (data) {
          this.categories = data;
          this.originalCategories = data;
          data.forEach(element => {
            this.uniqueKey = {
              ...this.uniqueKey,
              [element.id]: false
            };
          });
          this.cd.detectChanges();
        }
      });
  }

  search(term = '') {
    this.categories = this.originalCategories.filter((element: any) => {
      return (
        element.title.toLowerCase().includes(term) ||
        element.description.toLowerCase().includes(term)
      );
    });
  }

  onRemove(data) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.categoriesFacade.dispatch(deleteCategory({ id: data.id }));
      }
    });
  }

  onEdit(data) {
    const dialogRef = this.dialog.open(EditCategoryComponent, {
      autoFocus: false,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.categoriesFacade.dispatch(
          putCategory({ id: data.id, request: result })
        );
      }
    });
  }

  onExpand(data) {
    this.uniqueKey = {
      ...this.uniqueKey,
      [data.id]: !this.uniqueKey[data.id]
    };
  }

  onChangePage(event: PaginationData) {
    this.categoriesFacade.dispatch(
      getCategories({ page: `page=${event.pageIndex + 1}` })
    );
  }

  trackByFn(index: number, item: any) {
    return index;
  }

  trackByFnDetails(index: number, item: any) {
    return index;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

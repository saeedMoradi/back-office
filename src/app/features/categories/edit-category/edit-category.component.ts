import { HttpEventType } from '@angular/common/http';
import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject,
  ElementRef,
  ViewChild,
  ChangeDetectorRef
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormGroupDirective
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MediaObjects } from 'src/app/shared/interfaces';
import { CategoriesFacade } from '../+store/categories.facade';
import { CategoriesService } from '../services/categories.service';
import * as fromActions from '../+store/categories.actions';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditCategoryComponent implements OnInit {
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  @ViewChild('coverInput', { static: false }) coverInput: ElementRef;
  @ViewChild('thumbnailInput', { static: false }) thumbnailInput: ElementRef;
  @ViewChild('menuThumbnailInput', { static: false })
  menuThumbnailInput: ElementRef;
  categoryForm: FormGroup;
  fileList = [];
  coverProgress: number;
  thumnailProgress: number;
  menuThumnailProgress: number;
  coverImageData: MediaObjects;
  thumnailImageData: MediaObjects;
  menuThumnailImageData: MediaObjects;
  coverImageLoading: boolean;
  thumnailImageLoading: boolean;
  menuThumnailImageLoading: boolean;
  uploadError;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditCategoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private cd: ChangeDetectorRef,
    public categoriesFacade: CategoriesFacade,
    private categoriesService: CategoriesService
  ) {}

  ngOnInit() {
    this.initializeCategoryForm();
    this.categoriesFacade.dispatch(
      fromActions.getCategories({ page: 'pagination=false' })
    );
  }

  initializeCategoryForm() {
    this.categoryForm = this.formBuilder.group({
      title: [this.data.title, [Validators.required]],
      parent: [this.data.parent, []],
      description: [this.data.description, []],
      displayed: [this.data.displayed, []],
      coverImage: [this.data.title, []],
      thumbnail: [this.data.title, []],
      menuThumbnail: [this.data.title, []],
      metaTitle: [this.data.metaTitle, []],
      metaDescription: [this.data.metaDescription, []]
    });
  }

  addImage(data: string) {
    let fileCatInput;
    if (data === 'cover') {
      fileCatInput = this.coverInput.nativeElement;
    }
    if (data === 'thumbnailImage') {
      fileCatInput = this.thumbnailInput.nativeElement;
    }
    if (data === 'menuthumbnailImage') {
      fileCatInput = this.menuThumbnailInput.nativeElement;
    }
    fileCatInput.onchange = () => {
      for (let index = 0; index < fileCatInput.files.length; index++) {
        const file = fileCatInput.files[index];
        this.fileList.push({ data: file, inProgress: false, progress: 0 });
      }
      this.selectFile(data);
    };
    fileCatInput.click();
  }

  selectFile(data: string): void {
    const formData: FormData = new FormData();
    formData.append('file', this.fileList[0].data, this.fileList[0].data.name);
    this.categoriesService
      .uploadFile(formData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        event => {
          if (event.type === HttpEventType.UploadProgress && data === 'cover') {
            this.coverProgress = Math.round((100 * event.loaded) / event.total);
            this.cd.detectChanges();
          }
          if (
            event.type === HttpEventType.UploadProgress &&
            data === 'thumbnailImage'
          ) {
            this.thumnailProgress = Math.round(
              (100 * event.loaded) / event.total
            );
            this.cd.detectChanges();
          }
          if (
            event.type === HttpEventType.UploadProgress &&
            data === 'menuthumbnailImage'
          ) {
            this.menuThumnailProgress = Math.round(
              (100 * event.loaded) / event.total
            );
            this.cd.detectChanges();
          }
          if (event.type === HttpEventType.Response) {
            if (data === 'cover') {
              this.coverImageData = event.body;
              this.coverImageLoading = false;
              const image = this.coverImageData['@id'].split('/');
              this.categoryForm.patchValue({
                coverImage: `/api/media_objects/${+image[image.length - 1]}`
              });
              this.cd.detectChanges();
            }
            if (data === 'thumbnailImage') {
              this.thumnailImageData = event.body;
              this.thumnailImageLoading = false;
              const image = this.thumnailImageData['@id'].split('/');
              this.categoryForm.patchValue({
                thumbnail: `/api/media_objects/${+image[image.length - 1]}`
              });
              this.cd.detectChanges();
            }
            if (data === 'menuthumbnailImage') {
              this.menuThumnailImageData = event.body;
              this.menuThumnailImageLoading = false;
              const image = this.menuThumnailImageData['@id'].split('/');
              this.categoryForm.patchValue({
                menuThumbnail: `/api/media_objects/${+image[image.length - 1]}`
              });
              this.cd.detectChanges();
            }
          }
        },
        error => {
          this.coverImageLoading = false;
          this.thumnailImageLoading = false;
          this.menuThumnailImageLoading = false;
          this.uploadError = error;
          this.cd.detectChanges();
        }
      );
  }

  onCategory() {
    this.categoryForm.markAllAsTouched();
    if (this.categoryForm.invalid) {
      return;
    }
    this.dialogRef.close(this.categoryForm.value);
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

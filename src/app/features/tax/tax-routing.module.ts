import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateTaxComponent } from './create-tax/create-tax.component';
import { TaxListComponent } from './tax-list/tax-list.component';

import { TaxComponent } from './tax.component';

const routes: Routes = [
  {
    path: '',
    component: TaxComponent,
    children: [
      { path: '', redirectTo: 'tax-list' },
      { path: 'tax-list', component: TaxListComponent },
      { path: 'create-tax', component: CreateTaxComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaxRoutingModule {}

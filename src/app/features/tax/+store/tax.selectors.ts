import { createSelector, createFeatureSelector } from '@ngrx/store';
import { TaxesState } from './tax.interface';
import { featureKey } from './tax.reducer';

export const selectFeature = createFeatureSelector<TaxesState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: TaxesState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: TaxesState) => state.isLoading
);
export const selectTaxes = createSelector(selectFeature, (state: TaxesState) =>
  state.taxes ? state.taxes['hydra:member'] : null
);
export const selectTaxesTotalLength = createSelector(
  selectFeature,
  (state: TaxesState) => (state.taxes ? state.taxes['hydra:totalItems'] : null)
);

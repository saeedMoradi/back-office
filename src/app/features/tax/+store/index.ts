import * as taxReducer from './tax.reducer';
import { TaxesState } from './tax.interface';
import * as taxSelectors from './tax.selectors';

export { taxReducer, taxSelectors, TaxesState };

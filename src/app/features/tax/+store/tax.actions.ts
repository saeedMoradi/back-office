import { createAction, props } from '@ngrx/store';

export const getTaxes = createAction(
  '[Taxes] Get Taxes',
  props<{ page: any }>()
);
export const getTaxesSuccess = createAction(
  '[Taxes] Get Taxes Success',
  props<{ response: any }>()
);
export const getTaxesFail = createAction(
  '[Taxes] Get Taxes Fail',
  props<{ error: any }>()
);

export const createTax = createAction(
  '[Taxes] Create Tax',
  props<{ request: any }>()
);
export const createTaxSuccess = createAction(
  '[Taxes] Create Tax Success',
  props<{ response: any }>()
);
export const createTaxFail = createAction(
  '[Taxes] Create Tax Fail',
  props<{ error: any }>()
);

export const getTax = createAction('[Taxes] Get Tax', props<{ id: any }>());
export const getTaxSuccess = createAction(
  '[Taxes] Get Tax Success',
  props<{ response: any }>()
);
export const getTaxFail = createAction(
  '[Taxes] Get Tax Fail',
  props<{ error: any }>()
);

export const deleteTax = createAction(
  '[Taxes] Delete Tax',
  props<{ id: any }>()
);
export const deleteTaxesuccess = createAction(
  '[Taxes] Delete Tax Success',
  props<{ id: any }>()
);
export const deleteTaxFail = createAction(
  '[Taxes] Delete Tax Fail',
  props<{ error: any }>()
);

export const putTax = createAction(
  '[Taxes] Put Tax',
  props<{ id: any; request: any }>()
);
export const putTaxSuccess = createAction(
  '[Taxes] Put Tax Success',
  props<{ response: any }>()
);
export const putTaxFail = createAction(
  '[Taxes] Put Tax Fail',
  props<{ error: any }>()
);

export const patchTax = createAction(
  '[Taxes] Patch Tax',
  props<{ id: any; request: any }>()
);
export const patchTaxSuccess = createAction(
  '[Taxes] Patch Tax Success',
  props<{ response: any }>()
);
export const patchTaxFail = createAction(
  '[Taxes] Patch Tax Fail',
  props<{ error: any }>()
);

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './tax.actions';
import { TaxesService } from '../services/taxes.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Injectable()
export class TaxEffects {
  constructor(
    private actions$: Actions,
    private TaxesService: TaxesService,
    private matSnackBar: MatSnackBar,
    private router: Router
  ) {}

  getTaxes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getTaxes),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.TaxesService.getTaxes(page).pipe(
          map((data: any) => {
            return fromActions.getTaxesSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getTaxesFail({ error })))
        );
      })
    )
  );

  CreateTax$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createTax),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.TaxesService.createTax(request).pipe(
          map((data: any) => {
            this.TaxesService.clearForm();
            this.matSnackBar.open('Tax Created');
            this.router.navigate(['/home/taxes/tax-list']);
            return fromActions.createTaxSuccess({ response: data });
          }),
          catchError(error => of(fromActions.createTaxFail({ error })))
        );
      })
    )
  );

  getTax$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getTax),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.TaxesService.getTax(id).pipe(
          map((data: any) => {
            return fromActions.getTaxSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getTaxFail({ error })))
        );
      })
    )
  );

  deleteTax$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteTax),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.TaxesService.deleteTax(id).pipe(
          map((data: any) => {
            return fromActions.deleteTaxesuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteTaxFail({ error }));
          })
        );
      })
    )
  );

  putTax$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putTax),
      switchMap(({ id, request }) => {
        return this.TaxesService.putTax(id, request).pipe(
          map((data: any) => {
            return fromActions.putTaxSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putTaxFail({ error }));
          })
        );
      })
    )
  );

  patchTax$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchTax),
      switchMap(({ id, request }) => {
        return this.TaxesService.patchTax(id, request).pipe(
          map((data: any) => {
            return fromActions.patchTaxSuccess({ response: data });
          }),
          catchError(error => of(fromActions.patchTaxFail({ error })))
        );
      })
    )
  );
}

import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './tax.selectors';

@Injectable({
  providedIn: 'root'
})
export class TaxesFacade implements Facade {
  isloading$: Observable<boolean>;
  error$: Observable<any>;
  taxes$: Observable<any>;
  taxesTotalLength$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.taxes$ = store.pipe(select(fromSelector.selectTaxes));
    this.taxesTotalLength$ = store.pipe(
      select(fromSelector.selectTaxesTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

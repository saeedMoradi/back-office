import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './tax.actions';
import { TaxesState } from './tax.interface';

export const initialState: TaxesState = {
  error: null,
  isLoading: false,
  taxes: null
};

export const featureKey = 'taxes';

const taxesReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): TaxesState => ({ ...state, error: false })
  ),
  on(
    fromActions.getTaxesSuccess,
    (state, action): TaxesState => ({
      ...state,
      error: null,
      taxes: action.response
    })
  ),
  on(
    fromActions.getTaxesFail,
    (state, action): TaxesState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createTax,
    (state, action): TaxesState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createTaxSuccess,
    (state, action): TaxesState => {
      if (!state.taxes) {
        return {
          ...state,
          taxes: {
            'hydra:member': [action.response],
            'hydra:totalItems': 1
          }
        };
      }
      return {
        ...state,
        error: null,
        isLoading: false,
        taxes: {
          ...state.taxes,
          'hydra:member': [action.response, ...state.taxes['hydra:member']],
          'hydra:totalItems': state.taxes['hydra:totalItems'] + 1
        }
      };
    }
  ),
  on(
    fromActions.createTaxFail,
    (state, action): TaxesState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteTaxesuccess,
    (state, action): TaxesState => {
      const newtaxes = state.taxes['hydra:member'].filter((item: any) => {
        return item.id !== action.id;
      });
      return {
        ...state,
        error: null,
        taxes: {
          ...state.taxes,
          'hydra:member': newtaxes,
          'hydra:totalItems': state.taxes['hydra:totalItems'] - 1
        }
      };
    }
  ),
  on(
    fromActions.putTaxSuccess,
    (state, action): TaxesState => {
      const newtaxes = state.taxes['hydra:member'].map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        taxes: { ...state.taxes, 'hydra:member': newtaxes }
      };
    }
  )
);

export function reducer(state: TaxesState, action: Action) {
  return taxesReducer(state, action);
}

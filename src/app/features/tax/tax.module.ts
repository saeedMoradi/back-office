import { NgModule } from '@angular/core';

import { TaxRoutingModule } from './tax-routing.module';
import { SharedModule } from '../../shared/shared.module';
// Components
import { TaxComponent } from './tax.component';
import { CreateTaxComponent } from './create-tax/create-tax.component';
import { TaxListComponent } from './tax-list/tax-list.component';
import { EditTaxComponent } from './edit-tax/edit-tax.component';
// NGRX
// import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { TaxEffects } from './+store/tax.effects';
// import * as fromReducer from './+store/tax.reducer';

@NgModule({
  declarations: [
    TaxComponent,
    CreateTaxComponent,
    TaxListComponent,
    EditTaxComponent
  ],
  imports: [
    SharedModule,
    TaxRoutingModule,
    // StoreModule.forFeature(fromReducer.featureKey, fromReducer.reducer),
    EffectsModule.forFeature([TaxEffects])
  ]
})
export class TaxModule {}

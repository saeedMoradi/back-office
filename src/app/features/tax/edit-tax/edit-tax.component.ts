import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-edit-tax',
  templateUrl: './edit-tax.component.html',
  styleUrls: ['./edit-tax.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditTaxComponent implements OnInit {
  taxForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditTaxComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.initializeTaxForm();
  }
  initializeTaxForm() {
    this.taxForm = this.formBuilder.group({
      name: [this.data.name, [Validators.required]],
      description: [this.data.description, [Validators.required]],
      percentage: [this.data.percentage, [Validators.required]],
      status: [this.data.status, []],
    });
  }

  onTax() {
    this.taxForm.markAllAsTouched();
    if (this.taxForm.invalid) {
      return;
    }
    this.dialogRef.close(this.taxForm.value);
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

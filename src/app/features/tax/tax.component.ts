import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-tax',
  templateUrl: './tax.component.html',
  styleUrls: ['./tax.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaxComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}

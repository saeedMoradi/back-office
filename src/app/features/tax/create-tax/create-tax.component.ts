import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormGroupDirective
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TaxesFacade } from '../+store/tax.facade';
import { TaxesService } from '../services/taxes.service';
import * as fromActions from '../+store/tax.actions';

@Component({
  selector: 'app-create-tax',
  templateUrl: './create-tax.component.html',
  styleUrls: ['./create-tax.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateTaxComponent implements OnInit {
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  taxForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public taxesFacade: TaxesFacade,
    private taxesService: TaxesService
  ) {}

  ngOnInit() {
    this.initializeTaxForm();
    this.listenFormReset();
  }

  initializeTaxForm() {
    this.taxForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      percentage: ['', [Validators.required]],
      status: [true , []],
    });
  }

  listenFormReset() {
    this.taxesService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetTaxForm();
      });
  }

  resetTaxForm() {
    this.formDirective.resetForm();
    this.taxForm.reset({
      displayed: true
    });
  }

  onTax() {
    this.taxForm.markAllAsTouched();
    if (this.taxForm.invalid) {
      return;
    }
    this.taxesFacade.dispatch(
      fromActions.createTax({ request: this.taxForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

import * as mealdealsReducer from './mealdeals.reducer';
import { MealdealsState } from './mealdeals.interface';
import * as mealdealsSelectors from './mealdeals.selectors';

export { mealdealsReducer, mealdealsSelectors, MealdealsState };

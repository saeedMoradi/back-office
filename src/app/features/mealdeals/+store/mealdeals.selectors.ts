import { createSelector, createFeatureSelector } from '@ngrx/store';
import { MealdealsState } from './mealdeals.interface';
import { featureKey } from './mealdeals.reducer';

export const selectFeature = createFeatureSelector<MealdealsState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: MealdealsState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: MealdealsState) => state.isLoading
);
export const selectSets = createSelector(
  selectFeature,
  (state: MealdealsState) => (state.sets ? state.sets['hydra:member'] : null)
);
export const selectSetsTotalLength = createSelector(
  selectFeature,
  (state: MealdealsState) =>
    state.sets ? state.sets['hydra:totalItems'] : null
);
export const selectSteps = createSelector(
  selectFeature,
  (state: MealdealsState) => (state.steps ? state.steps['hydra:member'] : null)
);
export const selectStepsTotalLength = createSelector(
  selectFeature,
  (state: MealdealsState) =>
    state.steps ? state.steps['hydra:totalItems'] : null
);

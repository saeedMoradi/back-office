import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './mealdeals.selectors';

@Injectable({
  providedIn: 'root'
})
export class MealDealsFacade implements Facade {
  isloading$: Observable<boolean>;
  error$: Observable<any>;
  sets$: Observable<any>;
  steps$: Observable<any>;
  setsTotalLength$: Observable<any>;
  stepsTotalLength$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.sets$ = store.pipe(select(fromSelector.selectSets));
    this.steps$ = store.pipe(select(fromSelector.selectSteps));
    this.setsTotalLength$ = store.pipe(
      select(fromSelector.selectSetsTotalLength)
    );
    this.stepsTotalLength$ = store.pipe(
      select(fromSelector.selectStepsTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './mealdeals.actions';
import { MealdealsState } from './mealdeals.interface';

export const initialState: MealdealsState = {
  error: null,
  isLoading: false,
  sets: null,
  steps: null
};

export const featureKey = 'mealdeals';

const mealdealsReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): MealdealsState => ({ ...state, error: null })
  ),
  on(
    fromActions.getSetsSuccess,
    (state, action): MealdealsState => ({
      ...state,
      error: null,
      sets: action.response
    })
  ),
  on(
    fromActions.getSetsFail,
    (state, action): MealdealsState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createSet,
    (state, action): MealdealsState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createStep,
    (state, action): MealdealsState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createSetSuccess,
    (state, action): MealdealsState => {
      if (!state.sets) {
        return {
          ...state,
          sets: {
            'hydra:member': [action.response],
            'hydra:totalItems': 1
          }
        };
      }
      return {
        ...state,
        error: null,
        isLoading: false,
        sets: {
          ...state.sets,
          'hydra:member': [action.response, ...state.sets['hydra:member']],
          'hydra:totalItems': state.sets['hydra:totalItems'] + 1
        }
      };
    }
  ),
  on(
    fromActions.createSetFail,
    (state, action): MealdealsState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteSetSuccess,
    (state, action): MealdealsState => {
      const newsets = state.sets['hydra:member'].filter((item: any) => {
        return item.id !== action.id;
      });
      return {
        ...state,
        error: null,
        sets: {
          ...state.sets,
          'hydra:member': newsets,
          'hydra:totalItems': state.sets['hydra:totalItems'] - 1
        }
      };
    }
  ),
  on(
    fromActions.putSetSuccess,
    (state, action): MealdealsState => {
      const newsets = state.sets['hydra:member'].map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        sets: { ...state.sets, 'hydra:member': newsets }
      };
    }
  ),
  on(
    fromActions.getStepsSuccess,
    (state, action): MealdealsState => ({
      ...state,
      error: null,
      steps: action.response
    })
  ),
  on(
    fromActions.getStepsFail,
    (state, action): MealdealsState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createStepSuccess,
    (state, action): MealdealsState => {
      if (!state.steps) {
        return {
          ...state,
          steps: {
            'hydra:member': [action.response],
            'hydra:totalItems': 1
          }
        };
      }
      return {
        ...state,
        error: null,
        isLoading: false,
        steps: {
          ...state.steps,
          'hydra:member': [action.response, ...state.steps['hydra:member']],
          'hydra:totalItems': state.steps['hydra:totalItems'] + 1
        }
      };
    }
  ),
  on(
    fromActions.createStepFail,
    (state, action): MealdealsState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteStep,
    (state, action): MealdealsState => {
      const newSteps = state.sets.filter((item: any) => {
        return item.id !== action.id;
      });
      return {
        ...state,
        error: null,
        steps: newSteps
      };
    }
  ),
  on(
    fromActions.putStepSuccess,
    (state, action): MealdealsState => {
      const newsteps = state.steps.map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        steps: newsteps
      };
    }
  )
);

export function reducer(state: MealdealsState, action: Action) {
  return mealdealsReducer(state, action);
}

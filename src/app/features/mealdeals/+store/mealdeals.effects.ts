import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './mealdeals.actions';
import { MealDealsService } from 'src/app/features/mealdeals/services/mealdeals.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Injectable()
export class MealDealsEffects {
  constructor(
    private actions$: Actions,
    private mealDealsService: MealDealsService,
    private matSnackBar: MatSnackBar,
    private router: Router
  ) {}

  getSets$ = createEffect((): any =>
    this.actions$.pipe(
      ofType(fromActions.getSets),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.mealDealsService.getSets(page).pipe(
          map((data: any) => {
            return fromActions.getSetsSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.getSetsFail({ error }));
          })
        );
      })
    )
  );

  getSteps$ = createEffect((): any =>
    this.actions$.pipe(
      ofType(fromActions.getSets),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.mealDealsService.getSteps(page).pipe(
          map((data: any) => {
            return fromActions.getStepsSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.getStepsFail({ error }));
          })
        );
      })
    )
  );

  createSet$ = createEffect((): any =>
    this.actions$.pipe(
      ofType(fromActions.createSet),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.mealDealsService.createSet(request).pipe(
          map((data: any) => {
            this.mealDealsService.clearForm();
            this.matSnackBar.open('Set Created');
            this.router.navigate(['/home/mealdeals/mealdeals-list']);
            return fromActions.createSetSuccess({ response: data });
          }),
          catchError(error => of(fromActions.createSetFail({ error })))
        );
      })
    )
  );

  createStep$ = createEffect((): any =>
    this.actions$.pipe(
      ofType(fromActions.createStep),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.mealDealsService.createStep(request).pipe(
          map((data: any) => {
            this.mealDealsService.clearForm();
            this.matSnackBar.open('Step Created');
            return fromActions.createStepSuccess({ response: data });
          }),
          catchError(error => of(fromActions.createStepFail({ error })))
        );
      })
    )
  );

  deleteSet$ = createEffect((): any =>
    this.actions$.pipe(
      ofType(fromActions.deleteSet),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.mealDealsService.deleteSet(id).pipe(
          map((data: any) => {
            return fromActions.deleteSetSuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteSetFail({ error }));
          })
        );
      })
    )
  );

  deleteStep$ = createEffect((): any =>
    this.actions$.pipe(
      ofType(fromActions.deleteStep),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.mealDealsService.deleteStep(id).pipe(
          map((data: any) => {
            return fromActions.deleteStepSuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteStepFail({ error }));
          })
        );
      })
    )
  );

  putSet$ = createEffect((): any =>
    this.actions$.pipe(
      ofType(fromActions.putSet),
      switchMap(({ id, request }) => {
        return this.mealDealsService.putSet(id, request).pipe(
          map((data: any) => {
            return fromActions.putSetSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putSetFail({ error }));
          })
        );
      })
    )
  );

  putStep$ = createEffect((): any =>
    this.actions$.pipe(
      ofType(fromActions.putStep),
      switchMap(({ id, request }) => {
        return this.mealDealsService.putStep(id, request).pipe(
          map((data: any) => {
            return fromActions.putStepSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putStepFail({ error }));
          })
        );
      })
    )
  );
}

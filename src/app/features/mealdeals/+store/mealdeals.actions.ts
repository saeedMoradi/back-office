import { createAction, props } from '@ngrx/store';

export const getSets = createAction('[Sets] Get Sets', props<{ page: any }>());
export const getSetsSuccess = createAction(
  '[Sets] Get Sets Success',
  props<{ response: any }>()
);
export const getSetsFail = createAction(
  '[Sets] Get Sets Fail',
  props<{ error: any }>()
);
export const getSteps = createAction(
  '[Steps] Get Steps',
  props<{ page: any }>()
);
export const getStepsSuccess = createAction(
  '[Steps] Get Steps Success',
  props<{ response: any }>()
);
export const getStepsFail = createAction(
  '[Steps] Get Steps Fail',
  props<{ error: any }>()
);
export const createSet = createAction(
  '[Sets] Create Set',
  props<{ request: any }>()
);
export const createSetSuccess = createAction(
  '[Sets] Create Set Success',
  props<{ response: any }>()
);
export const createSetFail = createAction(
  '[Sets] Create Set Fail',
  props<{ error: any }>()
);
export const createStep = createAction(
  '[Steps] Create Step',
  props<{ request: any }>()
);
export const createStepSuccess = createAction(
  '[Steps] Create Step Success',
  props<{ response: any }>()
);
export const createStepFail = createAction(
  '[Steps] Create Step Fail',
  props<{ error: any }>()
);
export const deleteSet = createAction(
  '[Sets] Delete Set',
  props<{ id: any }>()
);
export const deleteSetSuccess = createAction(
  '[Sets] Delete Set Success',
  props<{ id: any }>()
);
export const deleteSetFail = createAction(
  '[Sets] Delete Set Fail',
  props<{ error: any }>()
);
export const deleteStep = createAction(
  '[Steps] Delete Step',
  props<{ id: any }>()
);
export const deleteStepSuccess = createAction(
  '[Steps] Delete Step Success',
  props<{ id: any }>()
);
export const deleteStepFail = createAction(
  '[Steps] Delete Step Fail',
  props<{ error: any }>()
);
export const putSet = createAction(
  '[Sets] Put Set',
  props<{ id: any; request: any }>()
);
export const putSetSuccess = createAction(
  '[Sets] Put Set Success',
  props<{ response: any }>()
);
export const putSetFail = createAction(
  '[Sets] Put Set Fail',
  props<{ error: any }>()
);
export const putStep = createAction(
  '[Steps] Put Step',
  props<{ id: any; request: any }>()
);
export const putStepSuccess = createAction(
  '[Steps] Put Step Success',
  props<{ response: any }>()
);
export const putStepFail = createAction(
  '[Steps] Put Step Fail',
  props<{ error: any }>()
);

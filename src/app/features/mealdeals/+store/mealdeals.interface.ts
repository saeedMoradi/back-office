export interface MealdealsState {
  error: any;
  isLoading: boolean;
  sets: any;
  steps: any;
}

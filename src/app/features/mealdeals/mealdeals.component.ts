import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-mealdeals',
  templateUrl: './mealdeals.component.html',
  styleUrls: ['./mealdeals.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MealdealsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

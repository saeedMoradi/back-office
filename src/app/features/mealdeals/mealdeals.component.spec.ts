import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MealdealsComponent } from './mealdeals.component';

describe('MealdealsComponent', () => {
  let component: MealdealsComponent;
  let fixture: ComponentFixture<MealdealsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MealdealsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MealdealsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

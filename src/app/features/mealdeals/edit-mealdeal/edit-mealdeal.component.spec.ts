import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMealdealComponent } from './edit-mealdeal.component';

describe('EditMealdealComponent', () => {
  let component: EditMealdealComponent;
  let fixture: ComponentFixture<EditMealdealComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditMealdealComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMealdealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

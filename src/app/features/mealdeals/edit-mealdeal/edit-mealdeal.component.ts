import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { MealDealsFacade } from '../+store/mealdeals.facade';
import * as fromActions from '../+store/mealdeals.actions';
// import { ProductsFacade } from '../../products/+store/products.facade';
// import { getProducts } from '../../products/+store/products.actions';
import { ShopsFacade } from '../../shops/+store/shops.facade';
import { getShops } from '../../shops/+store/shops.actions';

@Component({
  selector: 'app-edit-mealdeal',
  templateUrl: './edit-mealdeal.component.html',
  styleUrls: ['./edit-mealdeal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditMealdealComponent implements OnInit {
  mealDealForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditMealdealComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public mealDealsFacade: MealDealsFacade,
    public shopsFacade: ShopsFacade
  ) {}

  ngOnInit() {
    this.initializeMealDealForm();
    this.mealDealsFacade.dispatch(
      fromActions.getSets({ page: 'pagination=false' })
    );
    // this.mealDealsFacade.dispatch(fromActions.getSteps());
    this.shopsFacade.dispatch(getShops({ page: 'pagination=false' }));
  }

  initializeMealDealForm() {
    const startTime = `${new Date(this.data.startTime).getHours()}:${new Date(
      this.data.startTime
    ).getMinutes()}`;
    const endTime = `${new Date(this.data.endTime).getHours()}:${new Date(
      this.data.endTime
    ).getMinutes()}`;
    this.mealDealForm = this.formBuilder.group({
      title: [this.data.title, [Validators.required]],
      description: [this.data.description, []],
      minStep: [this.data.minStep, []],
      maxStep: [this.data.maxStep, []],
      price: [this.data.price, []],
      startingDate: [this.data.startingDate, [Validators.required]],
      endDate: [this.data.endDate, [Validators.required]],
      startTime: [startTime, [Validators.required]],
      endTime: [endTime, [Validators.required]],
      status: [this.data.status, []],
      image: [this.data.image || null, []],
      shop: [this.data.shop, [Validators.required]],
      // currency: [this.data., []],
      steps: [this.data.steps, [Validators.required]] // []
    });
  }

  onMealDeal() {
    this.mealDealForm.markAllAsTouched();
    if (this.mealDealForm.invalid) {
      return;
    }
    this.dialogRef.close(this.mealDealForm.value);
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

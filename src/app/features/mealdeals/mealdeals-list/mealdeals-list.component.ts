import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { PaginationData } from 'src/app/shared/interfaces';
import { MealDealsFacade } from '../+store/mealdeals.facade';
import { deleteSet, getSets, putSet } from '../+store/mealdeals.actions';
import { EditMealdealComponent } from '../edit-mealdeal/edit-mealdeal.component';

@Component({
  selector: 'app-mealdeals-list',
  templateUrl: './mealdeals-list.component.html',
  styleUrls: ['./mealdeals-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MealdealsListComponent implements OnInit {
  sets: any;
  originalSets: any;
  uniqueKey: any;
  destroy$ = new Subject();

  constructor(
    public mealDealsFacade: MealDealsFacade,
    private cd: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.mealDealsFacade.dispatch(getSets({ page: 'page=1' }));
    this.mealDealsFacade.sets$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        if (data) {
          this.sets = data;
          this.originalSets = data;
          data.forEach(element => {
            this.uniqueKey = {
              ...this.uniqueKey,
              [element.id]: false
            };
          });
          this.cd.detectChanges();
        }
      });
  }

  search(term = '') {
    this.sets = this.originalSets.filter((element: any) => {
      return (
        element.title.toLowerCase().includes(term) ||
        element.description.toLowerCase().includes(term)
      );
    });
  }

  onRemove(data) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.mealDealsFacade.dispatch(deleteSet({ id: data.id }));
      }
    });
  }

  onEdit(data) {
    const dialogRef = this.dialog.open(EditMealdealComponent, {
      autoFocus: false,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.mealDealsFacade.dispatch(putSet({ id: data.id, request: result }));
      }
    });
  }

  onExpand(data) {
    this.uniqueKey = {
      ...this.uniqueKey,
      [data.id]: !this.uniqueKey[data.id]
    };
  }

  onChangePage(event: PaginationData) {
    this.mealDealsFacade.dispatch(
      getSets({ page: `page=${event.pageIndex + 1}` })
    );
  }

  trackByFn(index: number, item: any) {
    return index;
  }

  trackByFnDetails(index: number, item: any) {
    return index;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MealdealsListComponent } from './mealdeals-list.component';

describe('MealdealsListComponent', () => {
  let component: MealdealsListComponent;
  let fixture: ComponentFixture<MealdealsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MealdealsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MealdealsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';

import { MealdealsRoutingModule } from './mealdeals-routing.module';
import { MealdealsComponent } from './mealdeals.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CreateMealdealComponent } from './create-mealdeal/create-mealdeal.component';
import { MealdealsListComponent } from './mealdeals-list/mealdeals-list.component';
// import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MealDealsEffects } from './+store/mealdeals.effects';
import { EditMealdealComponent } from './edit-mealdeal/edit-mealdeal.component';
import { ProductsEffects } from '../products/+store/products.effects';
import { ShopsEffects } from '../shops/+store/shops.effects';

@NgModule({
  declarations: [
    MealdealsComponent,
    CreateMealdealComponent,
    MealdealsListComponent,
    EditMealdealComponent
  ],
  imports: [
    SharedModule,
    MealdealsRoutingModule,
    // StoreModule.forFeature(fromReducer.featureKey, fromReducer.reducer),
    EffectsModule.forFeature([MealDealsEffects, ProductsEffects, ShopsEffects])
  ]
})
export class MealdealsModule {}

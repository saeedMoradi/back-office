import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MealdealsComponent } from './mealdeals.component';
import { CreateMealdealComponent } from './create-mealdeal/create-mealdeal.component';
import { MealdealsListComponent } from './mealdeals-list/mealdeals-list.component';

const routes: Routes = [
  {
    path: '',
    component: MealdealsComponent,
    children: [
      { path: '', redirectTo: 'mealdeals-list' },
      { path: 'mealdeals-list', component: MealdealsListComponent },
      { path: 'create-mealdeal', component: CreateMealdealComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MealdealsRoutingModule {}

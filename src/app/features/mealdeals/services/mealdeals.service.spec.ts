import { TestBed } from '@angular/core/testing';

import { MealDealsService } from './mealdeals.service';

describe('MealDealsService', () => {
  let service: MealDealsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MealDealsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

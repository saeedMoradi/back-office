import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PersistanceService } from 'src/app/core/services/persistance.service';
import { LoginResponse } from 'src/app/shared/interfaces';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MealDealsService {
  private readonly apiUrl = environment.apiUrl;
  private clearFormSource = new Subject<boolean>();
  clearForm$ = this.clearFormSource.asObservable();

  constructor(
    private http: HttpClient,
    private persistanceService: PersistanceService
  ) {}

  clearForm() {
    this.clearFormSource.next(true);
  }

  getSets(page = ''): Observable<any> {
    const profile: LoginResponse = this.persistanceService.get('profile');
    return this.http.get<any>(
      `${this.apiUrl}/api/sets?owner.id=${profile.id}&order[id]=desc&${page}`
    );
  }
  getSteps(page = ''): Observable<any> {
    const profile: LoginResponse = this.persistanceService.get('profile');
    return this.http.get<any>(
      `${this.apiUrl}/api/steps?owner.id=${profile.id}&order[id]=desc&${page}`
    );
  }
  createSet(data): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/sets`, data);
  }
  createStep(data): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/steps`, data);
  }
  deleteSet(id): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/api/sets/${id}`);
  }
  deleteStep(id): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/api/steps/${id}`);
  }
  putSet(id: any, data: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/api/sets/${id}`, data);
  }
  putStep(id: any, data: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/api/steps/${id}`, data);
  }
  uploadFile(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/media_objects`, data, {
      headers: { Anonymous: 'undefined' },
      reportProgress: true,
      observe: 'events'
    });
  }
}

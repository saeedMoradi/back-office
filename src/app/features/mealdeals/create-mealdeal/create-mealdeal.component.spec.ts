import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMealdealComponent } from './create-mealdeal.component';

describe('CreateMealdealComponent', () => {
  let component: CreateMealdealComponent;
  let fixture: ComponentFixture<CreateMealdealComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateMealdealComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMealdealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

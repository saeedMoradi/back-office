import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ChangeDetectorRef,
  ElementRef
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormGroupDirective,
  Validators
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MealDealsFacade } from '../+store/mealdeals.facade';
import * as fromActions from '../+store/mealdeals.actions';
import { ProductsFacade } from '../../products/+store/products.facade';
import { getProducts } from '../../products/+store/products.actions';
import { ShopsFacade } from '../../shops/+store/shops.facade';
import { getShops } from '../../shops/+store/shops.actions';
import { HttpEventType } from '@angular/common/http';
import { MediaObjects } from 'src/app/shared/interfaces';
import { MealDealsService } from '../services/mealdeals.service';

@Component({
  selector: 'app-create-mealdeal',
  templateUrl: './create-mealdeal.component.html',
  styleUrls: ['./create-mealdeal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateMealdealComponent implements OnInit {
  @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  @ViewChild('setDirective') setDirective: FormGroupDirective;
  mealDealForm: FormGroup;
  setForm: FormGroup;
  fileList = [];
  currentSelectedFile;
  imageSrc;
  uploadFileLoading;
  progress;
  productImageData: MediaObjects;
  uploadError;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public mealDealsFacade: MealDealsFacade,
    public productsFacade: ProductsFacade,
    private mealDealsService: MealDealsService,
    public shopsFacade: ShopsFacade,
    public cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.initializeMealDealForm();
    this.initializeSetForm();
    this.listenFormReset();
    this.productsFacade.dispatch(getProducts({ page: 'pagination=false' }));
    this.mealDealsFacade.dispatch(
      fromActions.getSets({ page: 'pagination=false' })
    );
    this.mealDealsFacade.dispatch(
      fromActions.getSteps({ page: 'pagination=false' })
    );
    this.shopsFacade.dispatch(getShops({ page: 'pagination=false' }));
  }

  initializeMealDealForm() {
    this.mealDealForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', []],
      foods: ['', [Validators.required]], // []
      minimumPick: [0, []],
      maximumPick: [0, []],
      status: [true, []]
      // icon: ['', []]
      // sets: ['', []]
    });
  }
  initializeSetForm() {
    this.setForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', []],
      minStep: [0, []],
      maxStep: [0, []],
      price: [0, []],
      startingDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]],
      startTime: ['', [Validators.required]],
      endTime: ['', [Validators.required]],
      status: [true, []],
      image: [null, []],
      shop: ['', [Validators.required]],
      // currency: ['', []],
      steps: ['', [Validators.required]] // []
    });
  }

  listenFormReset() {
    this.mealDealsService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetMealDealForm();
      });
  }

  resetMealDealForm() {
    this.formDirective.resetForm();
    this.setDirective.resetForm();
    this.progress = null;
    this.productImageData = null;
    this.mealDealForm.reset({
      minimumPick: 0,
      maximumPick: 0,
      status: true
    });
    this.setForm.reset({
      minStep: 0,
      maxStep: 0,
      price: 0,
      status: true
    });
  }

  addImage() {
    const fileInput = this.fileInput.nativeElement;
    fileInput.onchange = () => {
      for (let index = 0; index < fileInput.files.length; index++) {
        const file = fileInput.files[index];
        this.fileList.push({ data: file, inProgress: false, progress: 0 });
      }
      this.selectFile();
    };
    fileInput.click();
  }

  selectFile(): void {
    const formData: FormData = new FormData();
    formData.append('file', this.fileList[0].data, this.fileList[0].data.name);
    this.mealDealsService
      .uploadFile(formData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progress = Math.round((100 * event.loaded) / event.total);
            this.cd.detectChanges();
          }
          if (event.type === HttpEventType.Response) {
            this.productImageData = event.body;
            this.uploadFileLoading = false;
            // const image = this.productImageData['@id'].split('/');
            this.setForm.patchValue({
              image: this.productImageData['@id']
            });
            this.cd.detectChanges();
          }
        },
        error => {
          this.uploadFileLoading = false;
          this.uploadError = error;
          this.cd.detectChanges();
        }
      );
  }

  onMealDeal() {
    this.mealDealForm.markAllAsTouched();
    if (this.mealDealForm.invalid) {
      return;
    }
    this.mealDealsFacade.dispatch(
      fromActions.createStep({ request: this.mealDealForm.value })
    );
  }

  onSet() {
    this.setForm.markAllAsTouched();
    if (this.setForm.invalid) {
      return;
    }
    this.mealDealsFacade.dispatch(
      fromActions.createSet({ request: this.setForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-time-dialog',
  templateUrl: './time-dialog.component.html',
  styleUrls: ['./time-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimeDialogComponent implements OnInit {
  weekDay = [
    { day: 'Saturday', time: [] },
    { day: 'Sunday', time: [] },
    { day: 'Monday', time: [] },
    { day: 'Tuesday', time: [] },
    { day: 'Wednesday', time: [] },
    { day: 'Thursday', time: [] },
    { day: 'Friday', time: [] }
  ];
  constructor(
    public dialogRef: MatDialogRef<TimeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {}

  selectDay(e, day) {
    if (e.checked === false) {
      const index = this.weekDay.findIndex(
        (el: { day: string; time: any[] }) => {
          return el.day === day;
        }
      );
      this.weekDay.splice(index, 1);
    } else {
      this.weekDay = [
        ...this.weekDay,
        {
          day,
          time: []
        }
      ];
    }
  }

  cancel(): void {
    this.dialogRef.close();
  }
  done(open, close, maxCustomer): void {
    if (open && close) {
      this.weekDay.forEach((el: any) => {
        el.time = [...el.time, { open, close, maxCustomer }];
      });
      this.dialogRef.close(this.weekDay);
    } else {
      return;
    }
  }
}

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './pickups.actions';
import { PickupsService } from '../services/pickups.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PickupsFacade } from './pickups.facade';
import { Router } from '@angular/router';

@Injectable()
export class PickupsEffects {
  constructor(
    private actions$: Actions,
    private pickupsService: PickupsService,
    private matSnackBar: MatSnackBar,
    private pickupsFacade: PickupsFacade,
    private router: Router
  ) {}

  getPickups$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getPickups),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.pickupsService.getPickups(page).pipe(
          map((data: any) => {
            return fromActions.getPickupsSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getPickupsFail({ error })))
        );
      })
    )
  );

  createPickup$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createPickup),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.pickupsService.createPickup(request).pipe(
          map((data: any) => {
            this.pickupsService.clearForm();
            this.matSnackBar.open('Pickup Created');
            this.router.navigate(['/home/pickups/pickups-list']);
            return fromActions.createPickupsuccess({ response: data });
          }),
          catchError(error => of(fromActions.createPickupFail({ error })))
        );
      })
    )
  );

  getPickup$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getPickup),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.pickupsService.getPickup(id).pipe(
          map((data: any) => {
            return fromActions.getPickupsuccess({ response: data });
          }),
          catchError(error => of(fromActions.getPickupFail({ error })))
        );
      })
    )
  );

  deletePickup$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deletePickup),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.pickupsService.deletePickup(id).pipe(
          map((data: any) => {
            return fromActions.deletePickupSuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deletePickupFail({ error }));
          })
        );
      })
    )
  );

  putPickup$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putPickup),
      switchMap(({ id, request }) => {
        return this.pickupsService.putPickup(id, request).pipe(
          map((data: any) => {
            this.pickupsFacade.dispatch(
              fromActions.getPickups({ page: 'page=1' })
            );
            return fromActions.putPickupSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putPickupFail({ error }));
          })
        );
      })
    )
  );

  patchPickup$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchPickup),
      switchMap(({ id, request }) => {
        return this.pickupsService.patchPickup(id, request).pipe(
          map((data: any) => {
            return fromActions.patchPickupSuccess({ response: data });
          }),
          catchError(error => of(fromActions.patchPickupFail({ error })))
        );
      })
    )
  );
}

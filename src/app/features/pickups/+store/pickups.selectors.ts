import { createSelector, createFeatureSelector } from '@ngrx/store';
import { PickupsState } from './pickups.interface';
import { featureKey } from './pickups.reducer';

export const selectFeature = createFeatureSelector<PickupsState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: PickupsState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: PickupsState) => state.isLoading
);
export const selectPickups = createSelector(
  selectFeature,
  (state: PickupsState) =>
    state.pickups ? state.pickups['hydra:member'] : null
);
export const selectPickupsTotalLength = createSelector(
  selectFeature,
  (state: PickupsState) =>
    state.pickups ? state.pickups['hydra:totalItems'] : null
);

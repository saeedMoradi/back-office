import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './pickups.selectors';

@Injectable({
  providedIn: 'root'
})
export class PickupsFacade implements Facade {
  isloading$: Observable<boolean>;
  error$: Observable<any>;
  pickups$: Observable<any>;
  pickupsTotalLength$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.pickups$ = store.pipe(select(fromSelector.selectPickups));
    this.pickupsTotalLength$ = store.pipe(
      select(fromSelector.selectPickupsTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

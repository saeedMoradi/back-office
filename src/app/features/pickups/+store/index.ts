import * as pickupsReducer from './pickups.reducer';
import { PickupsState } from './pickups.interface';
import * as pickupsSelectors from './pickups.selectors';

export { pickupsReducer, pickupsSelectors, PickupsState };

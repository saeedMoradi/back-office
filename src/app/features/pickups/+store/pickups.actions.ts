import { createAction, props } from '@ngrx/store';

export const getPickups = createAction(
  '[Pickups] Get Pickups',
  props<{ page: any }>()
);
export const getPickupsSuccess = createAction(
  '[Pickups] Get Pickups Success',
  props<{ response: any }>()
);
export const getPickupsFail = createAction(
  '[Pickups] Get Pickups Fail',
  props<{ error: any }>()
);

export const createPickup = createAction(
  '[Pickups] Create Pickup',
  props<{ request: any }>()
);
export const createPickupsuccess = createAction(
  '[Pickups] Create Pickup Success',
  props<{ response: any }>()
);
export const createPickupFail = createAction(
  '[Pickups] Create Pickup Fail',
  props<{ error: any }>()
);

export const getPickup = createAction(
  '[Pickups] Get Pickup',
  props<{ id: any }>()
);
export const getPickupsuccess = createAction(
  '[Pickups] Get Pickup Success',
  props<{ response: any }>()
);
export const getPickupFail = createAction(
  '[Pickups] Get Pickup Fail',
  props<{ error: any }>()
);

export const deletePickup = createAction(
  '[Pickups] Delete Pickup',
  props<{ id: any }>()
);
export const deletePickupSuccess = createAction(
  '[Pickups] Delete Pickup Success',
  props<{ id: any }>()
);
export const deletePickupFail = createAction(
  '[Pickups] Delete Pickup Fail',
  props<{ error: any }>()
);

export const putPickup = createAction(
  '[Pickups] Put Pickup',
  props<{ id: any; request: any }>()
);
export const putPickupSuccess = createAction(
  '[Pickups] Put Pickup Success',
  props<{ response: any }>()
);
export const putPickupFail = createAction(
  '[Pickups] Put Pickup Fail',
  props<{ error: any }>()
);

export const patchPickup = createAction(
  '[Pickups] Patch Pickup',
  props<{ id: any; request: any }>()
);
export const patchPickupSuccess = createAction(
  '[Pickups] Patch Pickup Success',
  props<{ response: any }>()
);
export const patchPickupFail = createAction(
  '[Pickups] Patch Pickup Fail',
  props<{ error: any }>()
);

import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './pickups.actions';
import { PickupsState } from './pickups.interface';

export const initialState: PickupsState = {
  error: null,
  isLoading: false,
  pickups: null
};

export const featureKey = 'pickups';

const pickupsReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): PickupsState => ({ ...state, error: false })
  ),
  on(
    fromActions.getPickupsSuccess,
    (state, action): PickupsState => ({
      ...state,
      error: null,
      pickups: action.response
    })
  ),
  on(
    fromActions.getPickupsFail,
    (state, action): PickupsState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createPickup,
    (state, action): PickupsState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createPickupsuccess,
    (state, action): PickupsState => {
      if (!state.pickups) {
        return {
          ...state,
          pickups: {
            'hydra:member': [action.response],
            'hydra:totalItems': 1
          }
        };
      }
      return {
        ...state,
        error: null,
        isLoading: false,
        pickups: {
          ...state.pickups,
          'hydra:member': [action.response, ...state.pickups['hydra:member']],
          'hydra:totalItems': state.pickups['hydra:totalItems'] + 1
        }
      };
    }
  ),
  on(
    fromActions.createPickupFail,
    (state, action): PickupsState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deletePickupSuccess,
    (state, action): PickupsState => {
      const newpickups = state.pickups['hydra:member'].filter((item: any) => {
        return item.id !== action.id;
      });
      return {
        ...state,
        error: null,
        pickups: {
          ...state.pickups,
          'hydra:member': newpickups,
          'hydra:totalItems': state.pickups['hydra:totalItems'] - 1
        }
      };
    }
  ),
  on(
    fromActions.putPickupSuccess,
    (state, action): PickupsState => {
      const newpickups = state.pickups['hydra:member'].map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        pickups: { ...state.pickups, 'hydra:member': newpickups }
      };
    }
  )
);

export function reducer(state: PickupsState, action: Action) {
  return pickupsReducer(state, action);
}

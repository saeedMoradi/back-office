import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreatePickupComponent } from './create-pickup/create-pickup.component';
import { PickupsListComponent } from './pickups-list/pickups-list.component';

import { PickupsComponent } from './pickups.component';

const routes: Routes = [
  {
    path: '',
    component: PickupsComponent,
    children: [
      { path: '', redirectTo: 'pickups-list' },
      { path: 'pickups-list', component: PickupsListComponent },
      { path: 'create-pickup', component: CreatePickupComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PickupsRoutingModule {}

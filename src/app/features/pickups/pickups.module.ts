import { NgModule } from '@angular/core';

import { PickupsRoutingModule } from './pickups-routing.module';
import { SharedModule } from '../../shared/shared.module';
// Components
import { PickupsComponent } from './pickups.component';
import { CreatePickupComponent } from './create-pickup/create-pickup.component';
import { PickupsListComponent } from './pickups-list/pickups-list.component';
import { EditPickupComponent } from './edit-pickup/edit-pickup.component';
// NGRX
// import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { PickupsEffects } from './+store/pickups.effects';
import { ShopsEffects } from '../shops/+store/shops.effects';
import { TimeDialogComponent } from './time-dialog/time-dialog.component';

@NgModule({
  declarations: [
    PickupsComponent,
    CreatePickupComponent,
    PickupsListComponent,
    EditPickupComponent,
    TimeDialogComponent
  ],
  imports: [
    SharedModule,
    PickupsRoutingModule,
    EffectsModule.forFeature([PickupsEffects, ShopsEffects])
  ]
})
export class PickupsModule {}

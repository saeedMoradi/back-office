import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { PickupsFacade } from '../+store/pickups.facade';
import { getPickups, deletePickup, putPickup } from '../+store/pickups.actions';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PaginationData } from '../../../shared/interfaces';
import { ConfirmDialogComponent } from '../../../shared/components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { EditPickupComponent } from '../edit-pickup/edit-pickup.component';

@Component({
  selector: 'app-pickups-list',
  templateUrl: './pickups-list.component.html',
  styleUrls: ['./pickups-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PickupsListComponent implements OnInit {
  pickups: any;
  originalPickups: any;
  uniqueKey: any;
  destroy$ = new Subject();

  constructor(
    public pickupsFacade: PickupsFacade,
    private cd: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.pickupsFacade.dispatch(getPickups({ page: 'page=1' }));
    this.pickupsFacade.pickups$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        if (data) {
          this.pickups = data;
          this.originalPickups = data;
          data.forEach(element => {
            this.uniqueKey = {
              ...this.uniqueKey,
              [element.id]: false
            };
          });
          this.cd.detectChanges();
        }
      });
  }

  search(term = '') {
    this.pickups = this.originalPickups.filter((element: any) => {
      return element.time.toLowerCase().includes(term);
    });
  }

  onRemove(data) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.pickupsFacade.dispatch(deletePickup({ id: data.id }));
      }
    });
  }

  onEdit(data) {
    const dialogRef = this.dialog.open(EditPickupComponent, {
      autoFocus: false,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.pickupsFacade.dispatch(
          putPickup({ id: data.id, request: result })
        );
      }
    });
  }

  onExpand(data) {
    this.uniqueKey = {
      ...this.uniqueKey,
      [data.id]: !this.uniqueKey[data.id]
    };
  }

  onChangePage(event: PaginationData) {
    this.pickupsFacade.dispatch(
      getPickups({ page: `page=${event.pageIndex + 1}` })
    );
  }

  trackByFn(index: number, item: any) {
    return index;
  }

  trackByFnDetails(index: number, item: any) {
    return index;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

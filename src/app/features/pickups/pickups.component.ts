import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-pickups',
  templateUrl: './pickups.component.html',
  styleUrls: ['./pickups.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PickupsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}

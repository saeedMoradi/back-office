import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ChangeDetectorRef
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormGroupDirective,
  FormArray
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PickupsFacade } from '../+store/pickups.facade';
import { PickupsService } from '../services/pickups.service';
import * as fromActions from '../+store/pickups.actions';
import { ShopsFacade } from '../../shops/+store/shops.facade';
import { getShops } from '../../shops/+store/shops.actions';
import { MatDialog } from '@angular/material/dialog';
import { TimeDialogComponent } from '../time-dialog/time-dialog.component';

@Component({
  selector: 'app-create-pickup',
  templateUrl: './create-pickup.component.html',
  styleUrls: ['./create-pickup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreatePickupComponent implements OnInit {
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  pickupForm: FormGroup;
  workTime: any[] = [];
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public pickupsFacade: PickupsFacade,
    private pickupsService: PickupsService,
    public shopsFacade: ShopsFacade,
    public dialog: MatDialog,
    public cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.initializePickupForm();
    this.listenFormReset();
    this.shopsFacade.dispatch(getShops({ page: 'pagination=false' }));
  }

  initializePickupForm() {
    this.pickupForm = this.formBuilder.group({
      shop: ['', [Validators.required]],
      time: [15, [Validators.required]],
      // fromDate: ['', [Validators.required]],
      // toDate: ['', [Validators.required]],
      status: [true, [Validators.required]],
      availableTimes: [[], [Validators.required]]
      // availableTimes: this.formBuilder.array([])
    });
  }

  listenFormReset() {
    this.pickupsService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetPickupForm();
      });
  }

  resetPickupForm() {
    this.workTime = [];
    this.formDirective.resetForm();
    this.pickupForm.reset({
      time: 15,
      status: true
    });
    // this.pickupForm.setControl('availableTimes', this.formBuilder.array([]));
  }

  get availableTimesControl() {
    return this.pickupForm.get('availableTimes') as FormArray;
  }

  getTime(form) {
    return form.controls.time.controls;
  }

  addAvailableTimes() {
    this.availableTimesControl.push(
      this.formBuilder.group({
        day: ['', []],
        time: this.formBuilder.array([])
      })
    );
  }

  deleteAvailableTimes(index) {
    this.availableTimesControl.removeAt(index);
  }

  addTime() {
    const dialogRef = this.dialog.open(TimeDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe((result: any[]) => {
      if (result) {
        result.forEach((element: any) => {
          const find = this.workTime.find((el: any) => {
            return element.day === el.day;
          });
          if (find) {
            find.time = [...find.time, ...element.time];
          } else {
            this.workTime = [...this.workTime, element];
          }
        });
        this.pickupForm.patchValue({
          availableTimes: this.workTime
        });
        this.cd.detectChanges();
      }
    });
  }

  deleteDay(index) {
    this.workTime.splice(index, 1);
    this.pickupForm.patchValue({
      workTime: this.workTime
    });
  }

  deleteTime(i, j) {
    const control = this.availableTimesControl.controls[i].get(
      'time'
    ) as FormArray;
    control.removeAt(j);
  }

  onPickup() {
    this.pickupForm.markAllAsTouched();
    if (this.pickupForm.invalid) {
      return;
    }
    this.pickupsFacade.dispatch(
      fromActions.createPickup({ request: this.pickupForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

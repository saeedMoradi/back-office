import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject,
  ChangeDetectorRef
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { TimeDialogComponent } from '../time-dialog/time-dialog.component';

@Component({
  selector: 'app-edit-pickup',
  templateUrl: './edit-pickup.component.html',
  styleUrls: ['./edit-pickup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditPickupComponent implements OnInit {
  pickupForm: FormGroup;
  workTime = [];
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditPickupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialog,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.initializePickupForm();
  }

  initializePickupForm() {
    this.pickupForm = this.formBuilder.group({
      // shop: [this.data.shop, [Validators.required]],
      time: [this.data.time, [Validators.required]],
      status: [this.data.status, [Validators.required]],
      availableTimes: [[], [Validators.required]]
    });
    this.data.availableTimes.forEach((element: any, index) => {
      const arr = [];
      element.times.forEach(el => {
        const open =
          new Date(el.open.date).getHours() +
          ':' +
          this.correctMin(el.open.date);
        const close =
          new Date(el.close.date).getHours() +
          ':' +
          this.correctMin(el.close.date);
        arr.push({ open, close, maxCustomer: el.maxCustomer });
      });
      this.workTime.push({ day: element.day, time: arr });
      this.pickupForm.patchValue({
        availableTimes: this.workTime
      });
    });
  }

  correctMin(data) {
    let min: any = new Date(data).getMinutes();
    if (min < 10) {
      min = `0${min}`;
    }
    return min;
  }

  addTime() {
    const dialogRef = this.dialog.open(TimeDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe((result: any[]) => {
      if (result) {
        result.forEach((element: any) => {
          const find = this.workTime.find((el: any) => {
            return element.day === el.day;
          });
          if (find) {
            find.time = [...find.time, ...element.time];
          } else {
            this.workTime = [...this.workTime, element];
          }
        });
        this.pickupForm.patchValue({
          availableTimes: this.workTime
        });
        this.cd.detectChanges();
      }
    });
  }

  deleteDay(index) {
    this.workTime.splice(index, 1);
    this.pickupForm.patchValue({
      workTime: this.workTime
    });
  }

  onPickup() {
    this.pickupForm.markAllAsTouched();
    if (this.pickupForm.invalid) {
      return;
    }
    this.dialogRef.close(this.pickupForm.value);
  }

  cancel(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

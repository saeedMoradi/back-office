import { NgModule } from '@angular/core';

import { GeneralRoutingModule } from './general-routing.module';
import { GeneralComponent } from './general.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ColorPickerModule } from 'ngx-color-picker';
import { EffectsModule } from '@ngrx/effects';
import { GeneralEffect } from './+store/general.effects';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule({
  declarations: [GeneralComponent],
  imports: [
    SharedModule,
    GeneralRoutingModule,
    ColorPickerModule,
    EffectsModule.forFeature([GeneralEffect]),
    CKEditorModule
  ]
})
export class GeneralModule {}

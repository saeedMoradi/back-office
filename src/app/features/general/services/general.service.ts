import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PersistanceService } from 'src/app/core/services/persistance.service';
import { LoginResponse } from 'src/app/shared/interfaces';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  private readonly apiUrl = environment.apiUrl;
  private clearFormSource = new Subject<boolean>();
  clearForm$ = this.clearFormSource.asObservable();

  constructor(
    private http: HttpClient,
    private persistanceService: PersistanceService
  ) {}

  clearForm() {
    this.clearFormSource.next(true);
  }

  getSettings(page = ''): Observable<any> {
    const profile: LoginResponse = this.persistanceService.get('profile');
    return this.http.get<any>(
      `${this.apiUrl}/api/settings?owner.id=${profile.id}&order[id]=desc&${page}`
    );
  }
  createSetting(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/settings`, data);
  }
  getSetting(id: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/settings/${id}`);
  }
  deleteSetting(id: any): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/api/settings/${id}`);
  }
  putSetting(id: any, data: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/api/settings/${id}`, data);
  }
  patchSetting(id: any, data: any): Observable<any> {
    return this.http.patch<any>(`${this.apiUrl}/api/settings/${id}`, data);
  }
  getCurrencies(page = ''): Observable<any> {
    const profile: LoginResponse = this.persistanceService.get('profile');
    return this.http.get<any>(
      `${this.apiUrl}/api/currencies?owner.id=${profile.id}&order[id]=desc&${page}`
    );
  }
  createCurrency(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/currencies`, data);
  }
  getCurrency(id: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/currencies/${id}`);
  }
  deleteCurrency(id: any): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/api/currencies/${id}`);
  }
  putCurrency(id: any, data: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/api/currencies/${id}`, data);
  }
  patchCurrency(id: any, data: any): Observable<any> {
    return this.http.patch<any>(`${this.apiUrl}/api/currencies/${id}`, data);
  }
  getCurrencyList(): Observable<any[]> {
    return this.http.get<any[]>(`assets/json/currency.json`);
  }
}

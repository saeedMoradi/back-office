export interface SettingsState {
  error: any;
  isLoading: boolean;
  settings: any;
  currencies: any;
}

import { createSelector, createFeatureSelector } from '@ngrx/store';
import { SettingsState } from './general.interface';
import { featureKey } from './general.reducer';

export const selectFeature = createFeatureSelector<SettingsState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: SettingsState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: SettingsState) => state.isLoading
);
export const selectSettings = createSelector(
  selectFeature,
  (state: SettingsState) =>
    state.settings ? state.settings['hydra:member'] : null
);
export const selectSettingsTotalLength = createSelector(
  selectFeature,
  (state: SettingsState) =>
    state.settings ? state.settings['hydra:totalItems'] : null
);
export const selectCurrencies = createSelector(
  selectFeature,
  (state: SettingsState) =>
    state.currencies ? state.currencies['hydra:member'] : null
);
export const selectCurrenciesTotalLength = createSelector(
  selectFeature,
  (state: SettingsState) =>
    state.currencies ? state.currencies['hydra:totalItems'] : null
);

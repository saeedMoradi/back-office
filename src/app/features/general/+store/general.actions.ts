import { createAction, props } from '@ngrx/store';

export const getSettings = createAction(
  '[Settings] Get Settings',
  props<{ page: any }>()
);
export const getSettingsSuccess = createAction(
  '[Settings] Get Settings Success',
  props<{ response: any }>()
);
export const getSettingsFail = createAction(
  '[Settings] Get Settings Fail',
  props<{ error: any }>()
);

export const createSetting = createAction(
  '[Settings] Create Setting',
  props<{ request: any }>()
);
export const createSettingsuccess = createAction(
  '[Settings] Create Setting Success',
  props<{ response: any }>()
);
export const createSettingFail = createAction(
  '[Settings] Create Setting Fail',
  props<{ error: any }>()
);

export const getSetting = createAction(
  '[Settings] Get Setting',
  props<{ id: any }>()
);
export const getSettingsuccess = createAction(
  '[Settings] Get Setting Success',
  props<{ response: any }>()
);
export const getSettingFail = createAction(
  '[Settings] Get Setting Fail',
  props<{ error: any }>()
);

export const deleteSetting = createAction(
  '[Settings] Delete Setting',
  props<{ id: any }>()
);
export const deleteSettingsuccess = createAction(
  '[Settings] Delete Setting Success',
  props<{ id: any }>()
);
export const deleteSettingFail = createAction(
  '[Settings] Delete Setting Fail',
  props<{ error: any }>()
);

export const putSetting = createAction(
  '[Settings] Put Setting',
  props<{ id: any; request: any }>()
);
export const putSettingsuccess = createAction(
  '[Settings] Put Setting Success',
  props<{ response: any }>()
);
export const putSettingFail = createAction(
  '[Settings] Put Setting Fail',
  props<{ error: any }>()
);

export const patchSetting = createAction(
  '[Settings] Patch Setting',
  props<{ id: any; request: any }>()
);
export const patchSettingsuccess = createAction(
  '[Settings] Patch Setting Success',
  props<{ response: any }>()
);
export const patchSettingFail = createAction(
  '[Settings] Patch Setting Fail',
  props<{ error: any }>()
);

export const getCurrencies = createAction(
  '[Currencies] Get Currencies',
  props<{ page: any }>()
);
export const getCurrenciesSuccess = createAction(
  '[Currencies] Get Currencies Success',
  props<{ response: any }>()
);
export const getCurrenciesFail = createAction(
  '[Currencies] Get Currencies Fail',
  props<{ error: any }>()
);

export const createCurrency = createAction(
  '[Currencies] Create Currency',
  props<{ request: any }>()
);
export const createCurrencySuccess = createAction(
  '[Currencies] Create Currency Success',
  props<{ response: any }>()
);
export const createCurrencyFail = createAction(
  '[Currencies] Create Currency Fail',
  props<{ error: any }>()
);

export const getCurrency = createAction(
  '[Currencies] Get Currency',
  props<{ id: any }>()
);
export const getCurrencySuccess = createAction(
  '[Currencies] Get Currency Success',
  props<{ response: any }>()
);
export const getCurrencyFail = createAction(
  '[Currencies] Get Currency Fail',
  props<{ error: any }>()
);

export const deleteCurrency = createAction(
  '[Currencies] Delete Currency',
  props<{ id: any }>()
);
export const deleteCurrencySuccess = createAction(
  '[Currencies] Delete Currency Success',
  props<{ id: any }>()
);
export const deleteCurrencyFail = createAction(
  '[Currencies] Delete Currency Fail',
  props<{ error: any }>()
);

export const putCurrency = createAction(
  '[Currencies] Put Currency',
  props<{ id: any; request: any }>()
);
export const putCurrencySuccess = createAction(
  '[Currencies] Put Currency Success',
  props<{ response: any }>()
);
export const putCurrencyFail = createAction(
  '[Currencies] Put Currency Fail',
  props<{ error: any }>()
);

export const patchCurrency = createAction(
  '[Currencies] Patch Currency',
  props<{ id: any; request: any }>()
);
export const patchCurrencySuccess = createAction(
  '[Currencies] Patch Currency Success',
  props<{ response: any }>()
);
export const patchCurrencyFail = createAction(
  '[Currencies] Patch Currency Fail',
  props<{ error: any }>()
);

import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './general.selectors';

@Injectable({
  providedIn: 'root'
})
export class GeneralFacade implements Facade {
  isloading$: Observable<boolean>;
  error$: Observable<any>;
  settings$: Observable<any>;
  currencies$: Observable<any>;
  settingsTotalLength$: Observable<any>;
  currenciesTotalLength$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.settings$ = store.pipe(select(fromSelector.selectSettings));
    this.currencies$ = store.pipe(select(fromSelector.selectCurrencies));
    this.settingsTotalLength$ = store.pipe(
      select(fromSelector.selectSettingsTotalLength)
    );
    this.currenciesTotalLength$ = store.pipe(
      select(fromSelector.selectCurrenciesTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

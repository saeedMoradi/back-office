import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './general.actions';
import { SettingsState } from './general.interface';

export const initialState: SettingsState = {
  error: null,
  isLoading: false,
  settings: null,
  currencies: null
};

export const featureKey = 'settings';

const settingsReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): SettingsState => ({ ...state, error: false })
  ),
  on(
    fromActions.getSettingsSuccess,
    (state, action): SettingsState => ({
      ...state,
      error: null,
      settings: action.response
    })
  ),
  on(
    fromActions.getSettingsFail,
    (state, action): SettingsState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createSetting,
    (state, action): SettingsState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createSettingsuccess,
    (state, action): SettingsState => ({
      ...state,
      error: null,
      isLoading: false
    })
  ),
  on(
    fromActions.createSettingFail,
    (state, action): SettingsState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteSettingsuccess,
    (state, action): SettingsState => {
      const newsettings = state.settings.filter((item: any) => {
        return item.id !== action.id;
      });
      return {
        ...state,
        error: null,
        settings: newsettings
      };
    }
  ),
  on(
    fromActions.putSettingsuccess,
    (state, action): SettingsState => {
      const newsettings = state.settings.map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        settings: newsettings
      };
    }
  ),
  on(
    fromActions.getCurrenciesSuccess,
    (state, action): SettingsState => ({
      ...state,
      error: null,
      currencies: action.response
    })
  ),
  on(
    fromActions.getCurrenciesFail,
    (state, action): SettingsState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createCurrency,
    (state, action): SettingsState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createCurrencySuccess,
    (state, action): SettingsState => ({
      ...state,
      error: null,
      isLoading: false,
      currencies: {
        ...state.currencies,
        'hydra:member': [action.response, ...state.currencies['hydra:member']],
        'hydra:totalItems': state.currencies['hydra:totalItems'] + 1
      }
    })
  ),
  on(
    fromActions.createCurrencyFail,
    (state, action): SettingsState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteCurrencySuccess,
    (state, action): SettingsState => {
      const newCurrencies = state.settings.filter((item: any) => {
        return item.id !== action.id;
      });
      return {
        ...state,
        error: null,
        currencies: newCurrencies
      };
    }
  ),
  on(
    fromActions.putCurrencySuccess,
    (state, action): SettingsState => {
      const newCurrencies = state.settings.map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        currencies: newCurrencies
      };
    }
  )
);

export function reducer(state: SettingsState, action: Action) {
  return settingsReducer(state, action);
}

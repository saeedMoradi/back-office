import * as generalReducer from './general.reducer';
import { SettingsState } from './general.interface';
import * as generalSelectors from './general.selectors';

export { generalReducer, generalSelectors, SettingsState };

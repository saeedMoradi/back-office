import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './general.actions';
import { GeneralService } from '../services/general.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { GeneralFacade } from './general.facade';

@Injectable()
export class GeneralEffect {
  constructor(
    private actions$: Actions,
    private generalService: GeneralService,
    private matSnackBar: MatSnackBar,
    private generalFacade: GeneralFacade
  ) {}

  getSettings$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getSettings),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.generalService.getSettings(page).pipe(
          map((data: any) => {
            return fromActions.getSettingsSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getSettingsFail({ error })))
        );
      })
    )
  );

  createSetting$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createSetting),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.generalService.createSetting(request).pipe(
          map((data: any) => {
            this.generalService.clearForm();
            this.matSnackBar.open('Setting Created');
            return fromActions.createSettingsuccess({ response: data });
          }),
          catchError(error => of(fromActions.createSettingFail({ error })))
        );
      })
    )
  );

  getSetting$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getSetting),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.generalService.getSetting(id).pipe(
          map((data: any) => {
            return fromActions.getSettingsuccess({ response: data });
          }),
          catchError(error => of(fromActions.getSettingFail({ error })))
        );
      })
    )
  );

  deleteSetting$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteSetting),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.generalService.deleteSetting(id).pipe(
          map((data: any) => {
            return fromActions.deleteSettingsuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteSettingFail({ error }));
          })
        );
      })
    )
  );

  putSetting$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putSetting),
      switchMap(({ id, request }) => {
        return this.generalService.putSetting(id, request).pipe(
          map((data: any) => {
            return fromActions.putSettingsuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putSettingFail({ error }));
          })
        );
      })
    )
  );

  patchSetting$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchSetting),
      switchMap(({ id, request }) => {
        return this.generalService.patchSetting(id, request).pipe(
          map((data: any) => {
            return fromActions.patchSettingsuccess({ response: data });
          }),
          catchError(error => of(fromActions.patchSettingFail({ error })))
        );
      })
    )
  );

  getCurrencies$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getCurrencies),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.generalService.getCurrencies(page).pipe(
          map((data: any) => {
            return fromActions.getCurrenciesSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getCurrenciesFail({ error })))
        );
      })
    )
  );

  createCurrency$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createCurrency),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.generalService.createCurrency(request).pipe(
          map((data: any) => {
            this.generalService.clearForm();
            this.matSnackBar.open('Currency Created');
            return fromActions.createCurrencySuccess({ response: data });
          }),
          catchError(error => of(fromActions.createCurrencyFail({ error })))
        );
      })
    )
  );

  getCurrency$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getCurrency),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.generalService.getCurrency(id).pipe(
          map((data: any) => {
            return fromActions.getCurrencySuccess({ response: data });
          }),
          catchError(error => of(fromActions.getCurrencyFail({ error })))
        );
      })
    )
  );

  deleteCurrency$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteCurrency),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.generalService.deleteCurrency(id).pipe(
          map((data: any) => {
            return fromActions.deleteCurrencySuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteCurrencyFail({ error }));
          })
        );
      })
    )
  );

  putCurrency$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putCurrency),
      switchMap(({ id, request }) => {
        return this.generalService.putCurrency(id, request).pipe(
          map((data: any) => {
            return fromActions.putCurrencySuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putCurrencyFail({ error }));
          })
        );
      })
    )
  );
}

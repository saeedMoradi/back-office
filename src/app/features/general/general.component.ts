import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild
} from '@angular/core';
import {
  FormGroupDirective,
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GeneralService } from './services/general.service';
// import { GeneralFacade } from './+store/general.facade';
// import * as fromActions from './+store/general.actions';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GeneralComponent implements OnInit {
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  currencyForm: FormGroup;
  themeColor = '#2883e9';
  buttonColor = '#23e294';
  // currencies$: Observable<any>;
  public Editor = ClassicEditor;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    // public generalFacade: GeneralFacade,
    private generalService: GeneralService
  ) {}

  ngOnInit() {
    this.initializeCurrencyForm();
    this.listenFormReset();
  }

  initializeCurrencyForm() {
    this.currencyForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', []],
      symbol: ['', [Validators.required]],
      sets: [[], []]
    });
  }

  colorPickerChange(e) {
    // console.log('data:', e);
  }

  listenFormReset() {
    this.generalService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetCurrencyForm();
      });
  }

  resetCurrencyForm() {
    this.formDirective.resetForm();
    this.currencyForm.reset();
  }

  public onReady(editor) {
    editor.ui
      .getEditableElement()
      .parentElement.insertBefore(
        editor.ui.view.toolbar.element,
        editor.ui.getEditableElement()
      );
  }

  // onCurrency() {
  //   this.currencyForm.markAllAsTouched();
  //   if (this.currencyForm.invalid) {
  //     return;
  //   }
  //   this.generalFacade.dispatch(
  //     fromActions.createCurrency({ request: this.currencyForm.value })
  //   );
  // }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

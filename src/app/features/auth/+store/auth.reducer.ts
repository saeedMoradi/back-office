import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as authActions from './auth.actions';
import { AuthState } from './auth.interface';

export const initialState: AuthState = {
  isAuthenticated: null,
  isLoading: false,
  profile: null,
  forgotCode: null,
  users: null,
  error: null
};

export const featureKey = 'auth';

const authReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): AuthState => ({
      ...state,
      error: null
    })
  ),
  on(
    authActions.checkAuthSuccess,
    (state, action): AuthState => ({
      ...state,
      isAuthenticated: true,
      profile: action.profile
    })
  ),
  on(
    authActions.checkAuthFail,
    (state): AuthState => ({
      ...state,
      isAuthenticated: false,
      profile: null
    })
  ),
  on(
    authActions.login,
    (state): AuthState => ({ ...state, isLoading: true, error: null })
  ),
  on(
    authActions.loginSuccess,
    (state, action): AuthState => ({
      ...state,
      isLoading: false,
      isAuthenticated: true,
      error: null,
      profile: action.profile
    })
  ),
  on(
    authActions.loginFail,
    (state, action): AuthState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    authActions.register,
    (state): AuthState => ({ ...state, isLoading: true, error: null })
  ),
  on(
    authActions.registerSuccess,
    (state): AuthState => ({ ...state, isLoading: false, error: null })
  ),
  on(
    authActions.registerFail,
    (state, action): AuthState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    authActions.forgotPasswordSuccess,
    (state, action): AuthState => ({
      ...state,
      isLoading: false,
      error: null,
      forgotCode: action.data
    })
  ),
  on(
    authActions.forgotPasswordFail,
    (state, action): AuthState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    authActions.verifyPasswordSuccess,
    (state, action): AuthState => ({
      ...state,
      isLoading: false,
      error: null,
      forgotCode: null
    })
  ),
  on(
    authActions.verifyPasswordFail,
    (state, action): AuthState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    authActions.logoutSuccess,
    (state, action): AuthState => ({
      ...state,
      isLoading: false,
      isAuthenticated: false,
      profile: null,
      error: null
    })
  ),
  on(
    authActions.getUsersSuccess,
    (state, action): AuthState => ({
      ...state,
      users: action.response
    })
  )
);

export function reducer(state: AuthState, action: Action) {
  return authReducer(state, action);
}

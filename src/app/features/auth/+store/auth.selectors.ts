import { createSelector, createFeatureSelector } from '@ngrx/store';
import { AuthState } from './auth.interface';
import { featureKey } from './auth.reducer';

export const selectFeature = createFeatureSelector<AuthState>(featureKey);

export const selectIsLoading = createSelector(
  selectFeature,
  (state: AuthState) => state.isLoading
);
export const selectIsAuthenticated = createSelector(
  selectFeature,
  (state: AuthState) => state.isAuthenticated
);
export const selectError = createSelector(
  selectFeature,
  (state: AuthState) => state.error
);
export const selectProfile = createSelector(
  selectFeature,
  (state: AuthState) => state.profile
);
export const selectForgotCode = createSelector(
  selectFeature,
  (state: AuthState) => state.forgotCode
);
export const selectUsers = createSelector(selectFeature, (state: AuthState) =>
  state.users ? state.users['hydra:member'] : null
);
export const selectUsersTotalLength = createSelector(
  selectFeature,
  (state: AuthState) => (state.users ? state.users['hydra:totalItems'] : null)
);

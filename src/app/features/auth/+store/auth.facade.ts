import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
  Facade,
  ForgotResponse,
  LoginResponse
} from '../../../shared/interfaces';
import * as fromSelector from './auth.selectors';
import { AuthState } from './auth.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthFacade implements Facade {
  isloading$: Observable<boolean>;
  isAuthenticated$: Observable<boolean>;
  error$: Observable<boolean>;
  profile$: Observable<LoginResponse>;
  forgotCode$: Observable<ForgotResponse>;
  users$: Observable<any>;
  usersTotalLength$: Observable<any>;

  constructor(private readonly store: Store<AuthState>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.isAuthenticated$ = store.pipe(
      select(fromSelector.selectIsAuthenticated)
    );
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.profile$ = store.pipe(select(fromSelector.selectProfile));
    this.forgotCode$ = store.pipe(select(fromSelector.selectForgotCode));
    this.users$ = store.pipe(select(fromSelector.selectUsers));
    this.usersTotalLength$ = store.pipe(
      select(fromSelector.selectUsersTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

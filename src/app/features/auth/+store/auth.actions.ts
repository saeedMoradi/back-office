import { createAction, props } from '@ngrx/store';
import {
  LoginRequest,
  RegisterRequest,
  LoginResponse,
  RegisterResponse,
  ForgotRequest,
  ForgotResponse,
  VerifyRequest,
  VerifyResponse
} from 'src/app/shared/interfaces';

export const checkAuth = createAction('[Auth] Check Auth');
export const checkAuthSuccess = createAction(
  '[Auth] Check Auth Success',
  props<{ profile: LoginResponse }>()
);
export const checkAuthFail = createAction('[Auth] Check Auth Fail');

export const login = createAction(
  '[Auth] Login',
  props<{ request: LoginRequest }>()
);
export const loginSuccess = createAction(
  '[Auth] Login Success',
  props<{ profile: LoginResponse }>()
);
export const loginFail = createAction(
  '[Auth] Login Fail',
  props<{ error: any }>()
);

export const register = createAction(
  '[Auth] Register',
  props<{ request: RegisterRequest }>()
);
export const registerSuccess = createAction(
  '[Auth] Register Success',
  props<{ profile: RegisterResponse }>()
);
export const registerFail = createAction(
  '[Auth] Register Fail',
  props<{ error: any }>()
);

export const forgotPassword = createAction(
  '[Auth] Forgot Password',
  props<{ request: ForgotRequest }>()
);
export const forgotPasswordSuccess = createAction(
  '[Auth] Forgot Password Success',
  props<{ data: ForgotResponse }>()
);
export const forgotPasswordFail = createAction(
  '[Auth] Forgot Password Fail',
  props<{ error: any }>()
);

export const verifyPassword = createAction(
  '[Auth] Verify Password',
  props<{ request: VerifyRequest }>()
);
export const verifyPasswordSuccess = createAction(
  '[Auth] Verify Password Success',
  props<{ data: VerifyResponse }>()
);
export const verifyPasswordFail = createAction(
  '[Auth] Verify Password Fail',
  props<{ error: any }>()
);

export const logout = createAction('[Auth] logout');
export const logoutSuccess = createAction('[Auth] logout Success');
export const logoutFail = createAction(
  '[Auth] logout Fail',
  props<{ error: any }>()
);

export const getUsers = createAction(
  '[Users] Get Users',
  props<{ page: any }>()
);
export const getUsersSuccess = createAction(
  '[Users] Get Users Success',
  props<{ response: any }>()
);
export const getUsersFail = createAction(
  '[Users] Get Users Fail',
  props<{ error: any }>()
);

export const putUser = createAction(
  '[Users] Put User',
  props<{ id: any; request: any }>()
);
export const putUsersuccess = createAction(
  '[Users] Put User Success',
  props<{ response: any }>()
);
export const putUserFail = createAction(
  '[Users] Put User Fail',
  props<{ error: any }>()
);

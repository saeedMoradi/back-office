import { ForgotResponse, LoginResponse } from 'src/app/shared/interfaces';

export interface AuthState {
  isLoading: boolean;
  isAuthenticated: boolean;
  profile: LoginResponse;
  forgotCode: ForgotResponse;
  error: any;
  users: any;
}

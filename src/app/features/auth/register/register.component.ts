import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthFacade } from '../+store/auth.facade';
import * as authActions from '../+store/auth.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'eposense-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  isloading$: Observable<boolean>;

  constructor(
    private formBuilder: FormBuilder,
    public authFacade: AuthFacade
  ) {}

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(3)]],
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      restaurantName: ['', [Validators.required, Validators.minLength(3)]],
      roles: [['ROLE_USER'], []]
      // roles: [[], [Validators.required]]
      // roles: [['ROLE_ADMIN', 'ROLE_MERCHANT', 'ROLE_SUPER_ADMIN', 'ROLE_USER']]
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onRegister() {
    this.registerForm.markAllAsTouched();
    if (this.registerForm.invalid) {
      return;
    }
    this.authFacade.dispatch(
      authActions.register({ request: this.registerForm.value })
    );
  }

  ngOnDestroy() {}
}

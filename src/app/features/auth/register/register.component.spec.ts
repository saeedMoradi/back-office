import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { RegisterComponent } from './register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { AuthFacade } from '../+store/auth.facade';

describe('Register Component', () => {
  let spectator: Spectator<RegisterComponent>;
  let store: MockStore;
  const createComponent = createComponentFactory({
    component: RegisterComponent,
    providers: [
      AuthFacade,
      provideMockStore()
      // other providers
    ],
    imports: [ReactiveFormsModule, RouterTestingModule, MatSnackBarModule]
  });

  beforeEach(() => (spectator = createComponent()));

  it('should create', () => {
    expect(spectator).toBeTruthy();
  });
});

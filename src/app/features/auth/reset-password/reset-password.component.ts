import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
  ValidationErrors
} from '@angular/forms';
import { Router } from '@angular/router';
import { ForgotResponse } from 'src/app/shared/interfaces';
import { verifyPassword } from '../+store/auth.actions';
import { AuthFacade } from '../+store/auth.facade';
// import { forgotPassword } from '../+store/auth.actions';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResetPasswordComponent implements OnInit {
  resetForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public authFacade: AuthFacade,
    private router: Router
  ) {}

  ngOnInit() {
    this.resetForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      confirmPassword: [
        '',
        [Validators.required, this.matchValues('password')]
      ],
      code: ['', [Validators.required]]
    });
    this.authFacade.forgotCode$.subscribe((data: ForgotResponse) => {
      if (data) {
        this.resetForm.patchValue({
          code: data.code
        });
      } else {
        this.router.navigate(['/login']);
      }
    });
  }

  matchValues(matchTo: string): (AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      return !!control.parent &&
        !!control.parent.value &&
        control.value === control.parent.controls[matchTo].value
        ? null
        : { isMatching: true };
    };
  }

  get f() {
    return this.resetForm.controls;
  }

  onReset() {
    this.resetForm.markAllAsTouched();
    if (this.resetForm.invalid) {
      return;
    }
    this.authFacade.dispatch(verifyPassword({ request: this.resetForm.value }));
  }

  ngOnDestroy() {}
}

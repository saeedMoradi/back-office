import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { ResetPasswordComponent } from './reset-password.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { AuthFacade } from '../+store/auth.facade';

describe('Reset Component', () => {
  let spectator: Spectator<ResetPasswordComponent>;
  let store: MockStore;
  const createComponent = createComponentFactory({
    component: ResetPasswordComponent,
    providers: [
      AuthFacade,
      provideMockStore()
      // other providers
    ],
    imports: [ReactiveFormsModule, RouterTestingModule, MatSnackBarModule]
  });

  beforeEach(() => (spectator = createComponent()));

  it('should create', () => {
    expect(spectator).toBeTruthy();
  });
});

import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { ForgotPasswordComponent } from './forgot-password.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { AuthFacade } from '../+store/auth.facade';

describe('Forgot Component', () => {
  let spectator: Spectator<ForgotPasswordComponent>;
  let store: MockStore;
  const createComponent = createComponentFactory({
    component: ForgotPasswordComponent,
    providers: [
      AuthFacade,
      provideMockStore()
      // other providers
    ],
    imports: [ReactiveFormsModule, RouterTestingModule, MatSnackBarModule]
  });

  beforeEach(() => (spectator = createComponent()));

  it('should create', () => {
    expect(spectator).toBeTruthy();
  });
});

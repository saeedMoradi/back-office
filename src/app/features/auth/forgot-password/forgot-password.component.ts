import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthFacade } from '../+store/auth.facade';
import { forgotPassword } from '../+store/auth.actions';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ForgotPasswordComponent implements OnInit {
  forgotForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public authFacade: AuthFacade
  ) {}

  ngOnInit() {
    this.forgotForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  get f() {
    return this.forgotForm.controls;
  }

  forgot() {
    this.router.navigateByUrl('/forgot');
  }

  onForgot() {
    this.forgotForm.markAllAsTouched();
    if (this.forgotForm.invalid) {
      return;
    }
    this.authFacade.dispatch(
      forgotPassword({ request: this.forgotForm.value })
    );
  }

  ngOnDestroy() {}
}

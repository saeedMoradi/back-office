import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { LoginComponent } from './login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { AuthFacade } from '../+store/auth.facade';

describe('Login Component', () => {
  let spectator: Spectator<LoginComponent>;
  let store: MockStore;
  const createComponent = createComponentFactory({
    component: LoginComponent,
    providers: [
      AuthFacade,
      provideMockStore()
      // other providers
    ],
    imports: [ReactiveFormsModule, RouterTestingModule, MatSnackBarModule]
  });

  beforeEach(() => (spectator = createComponent()));

  it('should create', () => {
    expect(spectator).toBeTruthy();
  });
});

import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { EMPTY, Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { AuthFacade } from '../+store/auth.facade';
import { logout } from '../+store/auth.actions';
import { Injectable } from '@angular/core';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private authFacade: AuthFacade) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      // retry(1),
      catchError((error: HttpErrorResponse) => {
        if (error && error.status === 401) {
          this.authFacade.dispatch(logout());
          // return EMPTY;
          return throwError(error.error.message);
        } else {
          if (error.error.message) {
            return throwError(error.error.message);
          } else {
            return throwError(error.error['hydra:description']); // detail
          }
        }
      })
    );
  }
}

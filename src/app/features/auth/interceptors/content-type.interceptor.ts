import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ContentTypeInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (request.headers.has('Anonymous')) {
      request = request.clone({
        headers: request.headers.delete('Anonymous', 'undefined'),
        setHeaders: {
          Accept: 'application/ld+json'
        }
      });
      request = request.clone({
        headers: request.headers.delete('Authorization')
      });

      return next.handle(request);
    } else {
      request.headers.delete('Anonymous');
      request = request.clone({
        setHeaders: {
          'Content-Type': 'application/json',
          Accept: 'application/ld+json'
        }
      });

      return next.handle(request);
    }
  }
}

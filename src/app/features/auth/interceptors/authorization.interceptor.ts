import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginResponse } from 'src/app/shared/interfaces';
import { Store } from '@ngrx/store';
import { first, flatMap } from 'rxjs/operators';
import { AuthState } from '../+store/auth.interface';
import { selectProfile } from '../+store/auth.selectors';

@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {
  constructor(private store: Store<AuthState>) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return this.store.select(selectProfile).pipe(
      first(),
      flatMap((data: LoginResponse) => {
        const authReq = !!data
          ? request.clone({
              setHeaders: { Authorization: 'Bearer ' + data.token }
            })
          : request;
        return next.handle(authReq);
      })
    );
  }
}

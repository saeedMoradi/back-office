import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ChangeDetectorRef
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormGroupDirective,
  FormArray
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DiscountsFacade } from '../+store/discounts.facade';
import { DiscountsService } from '../services/discounts.service';
import * as fromActions from '../+store/discounts.actions';
import { ShopsFacade } from '../../shops/+store/shops.facade';
import { getShops } from '../../shops/+store/shops.actions';
import { AuthFacade } from '../../auth/+store/auth.facade';
import { getUsers } from '../../auth/+store/auth.actions';
// import { MatSelect } from '@angular/material/select';
import { CategoriesFacade } from '../../categories/+store/categories.facade';
import { getCategoriesWithGraphql } from '../../categories/+store/categories.actions';

const enum DiscountType {
  product = 'product',
  cart = 'cart',
  mix = 'mix'
}
@Component({
  selector: 'app-create-discount',
  templateUrl: './create-discount.component.html',
  styleUrls: ['./create-discount.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateDiscountComponent implements OnInit {
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  users: any;
  usersIds: any;
  categories: any;
  discountedProductsList: any;
  freeProductsList: any;
  discountType: DiscountType;
  discountForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public discountsFacade: DiscountsFacade,
    public shopsFacade: ShopsFacade,
    public authFacade: AuthFacade,
    public categoriesFacade: CategoriesFacade,
    private discountsService: DiscountsService,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.initializeDiscountForm();
    this.listenFormReset();
    this.shopsFacade.dispatch(getShops({ page: 'pagination=false' }));
    this.authFacade.dispatch(getUsers({ page: 'pagination=false' }));
    this.categoriesFacade.dispatch(getCategoriesWithGraphql());
    this.authFacade.users$
      .pipe(takeUntil(this.destroy$))
      .subscribe((users: any) => {
        if (users) {
          this.users = users;
          this.usersIds = this.users.map(element => {
            return `/api/users/${element.id}`;
          });
          this.cd.detectChanges();
        }
      });
    this.categoriesFacade.categoriesWithGraphql$
      .pipe(takeUntil(this.destroy$))
      .subscribe((categories: any) => {
        if (categories) {
          this.categories = categories;
          this.cd.detectChanges();
        }
      });
  }

  initializeDiscountForm() {
    this.discountForm = this.formBuilder.group({
      name: ['', []],
      description: ['', []],
      discountType: ['', []],
      type: ['', []],
      isExclusive: [true, []],
      priority: ['', []],
      code: ['', []],
      discountedCategory: ['', []],
      freeCategory: ['', []],
      discountedProducts: ['', []],
      freeProducts: [[], []],
      discountedNumber: ['', []],
      freeNumber: ['', []],
      expirationDate: [null, []],
      userLimitation: [[], []], // []
      fromDate: ['', []],
      toDate: ['', []],
      fromTime: ['', []],
      toTime: ['', []],
      minCartPrice: [0, []],
      cartSpecificPrice: [0, []],
      numberOfusePerUser: [0, []],
      shop: [[], []], // []
      freeDelivery: [true, []],
      userType: ['', []],
      amount: [0, []],
      gifts: [[], []],
      perProductData: this.formBuilder.array([])
    });
  }

  selectDiscountedCategory(id: string) {
    const find = this.categories.find((data: any) => {
      return id === data.node.id;
    });
    this.discountedProductsList = find.node.products.edges;
  }

  selectFreeCategory(id: string) {
    const find = this.categories.find((data: any) => {
      return id === data.node.id;
    });
    this.freeProductsList = find.node.products.edges;
  }

  listenFormReset() {
    this.discountsService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetDiscountForm();
      });
  }

  get perProductDataControl() {
    return this.discountForm.get('perProductData') as FormArray;
  }

  addPerProductData() {
    this.perProductDataControl.push(
      this.formBuilder.group({
        category: ['', []],
        products: ['', []]
      })
    );
  }
  deletePerProductData(index) {
    this.perProductDataControl.removeAt(index);
  }

  resetDiscountForm() {
    this.discountType = null;
    this.formDirective.resetForm();
    this.discountForm.reset({
      freeDelivery: true,
      isExclusive: true
    });
    this.discountForm.setControl(
      'categoryAndproductdata',
      this.formBuilder.array([])
    );
  }

  changeDiscountType(data: DiscountType) {
    this.discountType = data;
  }

  calculateProductData(catId) {
    if (catId) {
      const find = this.categories.find(el => el.node.id === catId);
      return find.node.products.edges;
    }
    return [];
  }

  onDiscount() {
    this.discountForm.markAllAsTouched();
    if (this.discountForm.invalid) {
      return;
    }
    if (this.discountForm.value.discountType === 'mix') {
      const arr = [
        {
          id: this.discountForm.value.discountedProducts,
          catId: this.discountForm.value.discountedCategory,
          quantity: this.discountForm.value.discountedNumber,
          gift: this.discountForm.value.freeProducts.map(element => {
            return {
              id: element,
              quantity: this.discountForm.value.freeNumber,
              catId: this.discountForm.value.freeCategory
            };
          })
        }
      ];
      if (arr[0].gift.length === 1) {
        if (arr[0].id === arr[0].gift[0].id) {
          this.discountForm.patchValue({
            discountType: 'match',
            gifts: arr
          });
        }
      } else {
        this.discountForm.patchValue({
          gifts: arr
        });
      }
    }
    this.discountsFacade.dispatch(
      fromActions.createDiscount({ request: this.discountForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

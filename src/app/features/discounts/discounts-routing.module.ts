import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateDiscountComponent } from './create-discount/create-discount.component';
import { DiscountsListComponent } from './discounts-list/discounts-list.component';

import { DiscountsComponent } from './discounts.component';

const routes: Routes = [
  {
    path: '',
    component: DiscountsComponent,
    children: [
      { path: '', redirectTo: 'discounts-list' },
      { path: 'discounts-list', component: DiscountsListComponent },
      { path: 'create-discount', component: CreateDiscountComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiscountsRoutingModule {}

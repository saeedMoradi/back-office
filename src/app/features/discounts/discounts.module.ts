import { NgModule } from '@angular/core';

import { DiscountsRoutingModule } from './discounts-routing.module';
import { SharedModule } from '../../shared/shared.module';
// Components
import { DiscountsComponent } from './discounts.component';
import { CreateDiscountComponent } from './create-discount/create-discount.component';
import { DiscountsListComponent } from './discounts-list/discounts-list.component';
import { EditDiscountComponent } from './edit-discount/edit-discount.component';
// NGRX
// import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { DiscountsEffects } from './+store/discounts.effects';
import { ShopsEffects } from '../shops/+store/shops.effects';
import { CategoriesEffects } from '../categories/+store/categories.effects';

@NgModule({
  declarations: [
    DiscountsComponent,
    CreateDiscountComponent,
    DiscountsListComponent,
    EditDiscountComponent
  ],
  imports: [
    SharedModule,
    DiscountsRoutingModule,
    // StoreModule.forFeature(fromReducer.featureKey, fromReducer.reducer),
    EffectsModule.forFeature([
      DiscountsEffects,
      ShopsEffects,
      CategoriesEffects
    ])
  ]
})
export class DiscountsModule {}

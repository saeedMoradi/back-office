import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject,
  ChangeDetectorRef
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { getUsers } from '../../auth/+store/auth.actions';
import { AuthFacade } from '../../auth/+store/auth.facade';
import { getShops } from '../../shops/+store/shops.actions';
import { ShopsFacade } from '../../shops/+store/shops.facade';
import { CategoriesFacade } from '../../categories/+store/categories.facade';
import { takeUntil } from 'rxjs/operators';
import { getCategoriesWithGraphql } from '../../categories/+store/categories.actions';

const enum DiscountType {
  product = 'product',
  cart = 'cart',
  mix = 'mix'
}

@Component({
  selector: 'app-edit-discount',
  templateUrl: './edit-discount.component.html',
  styleUrls: ['./edit-discount.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditDiscountComponent implements OnInit {
  discountForm: FormGroup;
  users: any;
  usersIds: any;
  categories: any;
  discountedProductsList: any;
  freeProductsList: any;
  discountType: DiscountType;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditDiscountComponent>,
    public shopsFacade: ShopsFacade,
    public authFacade: AuthFacade,
    public categoriesFacade: CategoriesFacade,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.initializeDiscountForm();
    this.shopsFacade.dispatch(getShops({ page: 'pagination=false' }));
    this.authFacade.dispatch(getUsers({ page: 'pagination=false' }));
    this.categoriesFacade.dispatch(getCategoriesWithGraphql());
    this.authFacade.users$
      .pipe(takeUntil(this.destroy$))
      .subscribe((users: any) => {
        if (users) {
          this.users = users;
          this.usersIds = this.users.map(element => {
            return `/api/users/${element.id}`;
          });
          this.cd.detectChanges();
        }
      });
    this.categoriesFacade.categoriesWithGraphql$
      .pipe(takeUntil(this.destroy$))
      .subscribe((categories: any) => {
        if (categories) {
          this.categories = categories;
          if (this.data.discountType === 'product') {
            this.selectFreeCategory(this.data.category);
          }
          if (
            this.data.discountType === 'mix' ||
            this.data.discountType === 'match'
          ) {
            this.selectDiscountedCategory(this.data.gifts[0].catId);
            this.selectFreeCategory(this.data.gifts[0].gift[0].catId);
          }
          this.cd.detectChanges();
        }
      });
  }

  initializeDiscountForm() {
    let category = this.data.category ? this.data.category : null;
    let shop = [];
    let freeProducts = [];
    let discountedProducts = '';
    let freeCategory = '';
    let discountedCategory = '';
    let discountedNumber = '';
    let freeNumber = '';
    if (
      this.data.discountType === 'mix' ||
      this.data.discountType === 'match'
    ) {
      this.discountType = DiscountType.mix;
      discountedProducts = this.data.gifts[0].id;
      freeCategory = this.data.gifts[0].gift[0].catId;
      discountedCategory = this.data.gifts[0].catId;
      discountedNumber = this.data.gifts[0].quantity;
      freeNumber = this.data.gifts[0].gift[0].quantity;
      this.data.gifts[0].gift.forEach(element => {
        freeProducts.push(element.id);
      });
    } else {
      this.discountType = this.data.discountType;
    }
    this.data.shop.forEach(element => {
      shop.push(element);
    });
    this.discountForm = this.formBuilder.group({
      name: [this.data.name, []],
      description: [this.data.description, []],
      discountType: [this.discountType, []],
      type: [this.data.type, []],
      isExclusive: [this.data.isExclusive, []],
      priority: [this.data.priority, []],
      code: [this.data.code || '', []],
      category: [category, []],
      discountedCategory: [discountedCategory, []],
      freeCategory: [freeCategory, []],
      discountedProducts: [discountedProducts, []],
      freeProducts: [freeProducts, []],
      discountedNumber: [discountedNumber, []],
      freeNumber: [freeNumber, []],
      expirationDate: [null, []],
      userLimitation: [this.data.userLimitation, []], // []
      fromDate: [this.data.fromDate, []],
      toDate: [this.data.toDate, []],
      minCartPrice: [this.data.minCartPrice, []],
      cartSpecificPrice: [this.data.cartSpecificPrice, []],
      numberOfusePerUser: [this.data.numberOfusePerUser, []],
      shop: [shop, []], // []
      products: [this.data.products || [], []], // []
      freeDelivery: [this.data.freeDelivery, []],
      userType: [this.data.userType, []],
      amount: [this.data.amount, []],
      gifts: [[this.data.gifts || []], []]
    });
  }

  changeDiscountType(data: DiscountType) {
    this.discountType = data;
  }

  selectDiscountedCategory(id: string) {
    const find = this.categories.find((data: any) => {
      return id === data.node.id;
    });
    this.discountedProductsList = find.node.products.edges;
  }

  selectFreeCategory(id: string) {
    const find = this.categories.find((data: any) => {
      return id === data.node.id;
    });
    this.freeProductsList = find.node.products.edges;
  }

  onDiscount() {
    this.discountForm.markAllAsTouched();
    if (this.discountForm.invalid) {
      return;
    }
    if (this.discountForm.value.discountType === 'mix') {
      const arr = [
        {
          id: this.discountForm.value.discountedProducts,
          catId: this.discountForm.value.discountedCategory,
          quantity: this.discountForm.value.discountedNumber,
          gift: this.discountForm.value.freeProducts.map(element => {
            return {
              id: element,
              quantity: this.discountForm.value.freeNumber,
              catId: this.discountForm.value.freeCategory
            };
          })
        }
      ];
      if (arr[0].gift.length === 1) {
        if (arr[0].id === arr[0].gift[0].id) {
          this.discountForm.patchValue({
            discountType: 'match',
            gifts: arr
          });
        }
      } else {
        this.discountForm.patchValue({
          gifts: arr
        });
      }
    }
    this.dialogRef.close(this.discountForm.value);
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

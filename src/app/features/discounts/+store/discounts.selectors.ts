import { createSelector, createFeatureSelector } from '@ngrx/store';
import { DiscountsState } from './discounts.interface';
import { featureKey } from './discounts.reducer';

export const selectFeature = createFeatureSelector<DiscountsState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: DiscountsState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: DiscountsState) => state.isLoading
);
export const selectDiscounts = createSelector(
  selectFeature,
  (state: DiscountsState) =>
    state.discounts ? state.discounts['hydra:member'] : null
);
export const selectDiscountsTotalLength = createSelector(
  selectFeature,
  (state: DiscountsState) =>
    state.discounts ? state.discounts['hydra:totalItems'] : null
);

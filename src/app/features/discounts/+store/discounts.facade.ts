import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './discounts.selectors';

@Injectable({
  providedIn: 'root'
})
export class DiscountsFacade implements Facade {
  isloading$: Observable<boolean>;
  error$: Observable<any>;
  discounts$: Observable<any>;
  discountsTotalLength$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.discounts$ = store.pipe(select(fromSelector.selectDiscounts));
    this.discountsTotalLength$ = store.pipe(
      select(fromSelector.selectDiscountsTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

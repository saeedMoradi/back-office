import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './discounts.actions';
import { DiscountsService } from '../services/discounts.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Injectable()
export class DiscountsEffects {
  constructor(
    private actions$: Actions,
    private discountsService: DiscountsService,
    private matSnackBar: MatSnackBar,
    private router: Router
  ) {}

  getDiscounts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getDiscounts),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.discountsService.getDiscounts(page).pipe(
          map((data: any) => {
            return fromActions.getDiscountsSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getDiscountsFail({ error })))
        );
      })
    )
  );

  createDiscount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createDiscount),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.discountsService.createDiscount(request).pipe(
          map((data: any) => {
            this.discountsService.clearForm();
            this.matSnackBar.open('Discount Created');
            this.router.navigate(['/home/discounts/discounts-list']);
            return fromActions.createDiscountSuccess({ response: data });
          }),
          catchError(error => of(fromActions.createDiscountFail({ error })))
        );
      })
    )
  );

  getDiscount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getDiscount),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.discountsService.getDiscount(id).pipe(
          map((data: any) => {
            return fromActions.getDiscountSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getDiscountFail({ error })))
        );
      })
    )
  );

  deleteDiscount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteDiscount),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.discountsService.deleteDiscount(id).pipe(
          map((data: any) => {
            return fromActions.deleteDiscountSuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteDiscountFail({ error }));
          })
        );
      })
    )
  );

  putDiscount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putDiscount),
      switchMap(({ id, request }) => {
        return this.discountsService.putDiscount(id, request).pipe(
          map((data: any) => {
            return fromActions.putDiscountSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putDiscountFail({ error }));
          })
        );
      })
    )
  );

  patchDiscount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchDiscount),
      switchMap(({ id, request }) => {
        return this.discountsService.patchDiscount(id, request).pipe(
          map((data: any) => {
            return fromActions.patchDiscountSuccess({ response: data });
          }),
          catchError(error => of(fromActions.patchDiscountFail({ error })))
        );
      })
    )
  );
}

import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './discounts.actions';
import { DiscountsState } from './discounts.interface';

export const initialState: DiscountsState = {
  error: null,
  isLoading: false,
  discounts: null
};

export const featureKey = 'discounts';

const discountsReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): DiscountsState => ({ ...state, error: false })
  ),
  on(
    fromActions.getDiscountsSuccess,
    (state, action): DiscountsState => ({
      ...state,
      error: null,
      discounts: action.response
    })
  ),
  on(
    fromActions.getDiscountsFail,
    (state, action): DiscountsState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createDiscount,
    (state, action): DiscountsState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createDiscountSuccess,
    (state, action): DiscountsState => {
      if (!state.discounts) {
        return {
          ...state,
          discounts: {
            'hydra:member': [action.response],
            'hydra:totalItems': 1
          }
        };
      }
      return {
        ...state,
        error: null,
        isLoading: false,
        discounts: {
          ...state.discounts,
          'hydra:member': [action.response, ...state.discounts['hydra:member']],
          'hydra:totalItems': state.discounts['hydra:totalItems'] + 1
        }
      };
    }
  ),
  on(
    fromActions.createDiscountFail,
    (state, action): DiscountsState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteDiscountSuccess,
    (state, action): DiscountsState => {
      const newdiscounts = state.discounts['hydra:member'].filter(
        (item: any) => {
          return item.id !== action.id;
        }
      );
      return {
        ...state,
        error: null,
        discounts: {
          ...state.discounts,
          'hydra:member': newdiscounts,
          'hydra:totalItems': state.discounts['hydra:totalItems'] - 1
        }
      };
    }
  ),
  on(
    fromActions.putDiscountSuccess,
    (state, action): DiscountsState => {
      const newdiscounts = state.discounts['hydra:member'].map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        discounts: { ...state.discounts, 'hydra:member': newdiscounts }
      };
    }
  )
);

export function reducer(state: DiscountsState, action: Action) {
  return discountsReducer(state, action);
}

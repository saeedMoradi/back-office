import { createAction, props } from '@ngrx/store';

export const getDiscounts = createAction(
  '[Discounts] Get Discounts',
  props<{ page: any }>()
);
export const getDiscountsSuccess = createAction(
  '[Discounts] Get Discounts Success',
  props<{ response: any }>()
);
export const getDiscountsFail = createAction(
  '[Discounts] Get Discounts Fail',
  props<{ error: any }>()
);

export const createDiscount = createAction(
  '[Discounts] Create Discount',
  props<{ request: any }>()
);
export const createDiscountSuccess = createAction(
  '[Discounts] Create Discount Success',
  props<{ response: any }>()
);
export const createDiscountFail = createAction(
  '[Discounts] Create Discount Fail',
  props<{ error: any }>()
);

export const getDiscount = createAction(
  '[Discounts] Get Discount',
  props<{ id: any }>()
);
export const getDiscountSuccess = createAction(
  '[Discounts] Get Discount Success',
  props<{ response: any }>()
);
export const getDiscountFail = createAction(
  '[Discounts] Get Discount Fail',
  props<{ error: any }>()
);

export const deleteDiscount = createAction(
  '[Discounts] Delete Discount',
  props<{ id: any }>()
);
export const deleteDiscountSuccess = createAction(
  '[Discounts] Delete Discount Success',
  props<{ id: any }>()
);
export const deleteDiscountFail = createAction(
  '[Discounts] Delete Discount Fail',
  props<{ error: any }>()
);

export const putDiscount = createAction(
  '[Discounts] Put Discount',
  props<{ id: any; request: any }>()
);
export const putDiscountSuccess = createAction(
  '[Discounts] Put Discount Success',
  props<{ response: any }>()
);
export const putDiscountFail = createAction(
  '[Discounts] Put Discount Fail',
  props<{ error: any }>()
);

export const patchDiscount = createAction(
  '[Discounts] Patch Discount',
  props<{ id: any; request: any }>()
);
export const patchDiscountSuccess = createAction(
  '[Discounts] Patch Discount Success',
  props<{ response: any }>()
);
export const patchDiscountFail = createAction(
  '[Discounts] Patch Discount Fail',
  props<{ error: any }>()
);

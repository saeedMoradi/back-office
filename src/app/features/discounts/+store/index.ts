import * as discountsReducer from './discounts.reducer';
import { DiscountsState } from './discounts.interface';
import * as discountsSelectors from './discounts.selectors';

export { discountsReducer, discountsSelectors, DiscountsState };

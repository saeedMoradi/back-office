import { NgModule } from '@angular/core';

import { PreOrdersRoutingModule } from './preorders-routing.module';
import { SharedModule } from '../../shared/shared.module';
// Components
import { PreOrdersComponent } from './preorders.component';
import { CreatePreOrderComponent } from './create-preorder/create-preorder.component';
import { PreOrdersListComponent } from './preorders-list/preorders-list.component';
import { EditPreOrderComponent } from './edit-preorder/edit-preorder.component';
// NGRX
import { EffectsModule } from '@ngrx/effects';
import { PreOrdersEffects } from './+store/preorders.effects';
import { ShopsEffects } from '../shops/+store/shops.effects';

@NgModule({
  declarations: [
    PreOrdersComponent,
    CreatePreOrderComponent,
    PreOrdersListComponent,
    EditPreOrderComponent
  ],
  imports: [
    SharedModule,
    PreOrdersRoutingModule,
    EffectsModule.forFeature([PreOrdersEffects, ShopsEffects])
  ]
})
export class PreOrdersModule {}

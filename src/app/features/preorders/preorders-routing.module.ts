import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreatePreOrderComponent } from './create-preorder/create-preorder.component';
import { PreOrdersListComponent } from './preorders-list/preorders-list.component';

import { PreOrdersComponent } from './preorders.component';

const routes: Routes = [
  {
    path: '',
    component: PreOrdersComponent,
    children: [
      { path: '', redirectTo: 'preorders-list' },
      { path: 'preorders-list', component: PreOrdersListComponent },
      { path: 'create-preorder', component: CreatePreOrderComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PreOrdersRoutingModule {}

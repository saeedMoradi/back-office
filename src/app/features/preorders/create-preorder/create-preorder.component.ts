import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ChangeDetectorRef
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormGroupDirective,
  FormArray
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PreOrdersFacade } from '../+store/preorders.facade';
import { PreOrdersService } from '../services/preorders.service';
import * as fromActions from '../+store/preorders.actions';
import { ShopsFacade } from '../../shops/+store/shops.facade';
import { getShops } from '../../shops/+store/shops.actions';
import { MatDialog } from '@angular/material/dialog';
import { TimeDialogComponent } from 'src/app/shared/components/time-dialog/time-dialog.component';
import * as moment from 'moment';

@Component({
  selector: 'app-create-preorder',
  templateUrl: './create-preorder.component.html',
  styleUrls: ['./create-preorder.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreatePreOrderComponent implements OnInit {
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  preOrderForm: FormGroup;
  workTime: any[] = [];
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public preOrdersFacade: PreOrdersFacade,
    private preOrdersService: PreOrdersService,
    public shopsFacade: ShopsFacade,
    public dialog: MatDialog,
    public cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.initializePreOrderForm();
    this.listenFormReset();
    this.shopsFacade.dispatch(getShops({ page: 'pagination=false' }));
  }

  initializePreOrderForm() {
    this.preOrderForm = this.formBuilder.group({
      shop: ['', [Validators.required]],
      status: [true, [Validators.required]],
      fromDate: ['', [Validators.required]],
      toDate: ['', [Validators.required]],
      preOrderAvailableTimes: [[], [Validators.required]]
    });
  }

  listenFormReset() {
    this.preOrdersService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetPreOrderForm();
      });
  }

  resetPreOrderForm() {
    this.workTime = [];
    this.formDirective.resetForm();
    this.preOrderForm.reset({ status: true });
  }

  getDates(startDate, endDate) {
    let dates = [];
    const currDate = moment(startDate).startOf('day');
    const lastDate = moment(endDate).startOf('day');
    do {
      dates.push(moment(currDate.clone().toDate()).format('dddd'));
    } while (currDate.add(1, 'days').diff(lastDate) - 1 < 0);
    return dates;
  }

  addTime() {
    let daysOfWeek = [];
    const fromDate = this.preOrderForm.get('fromDate').value;
    const toDate = this.preOrderForm.get('toDate').value;
    if (fromDate && toDate) {
      const days = moment(toDate).diff(fromDate, 'days') + 1;
      if (days < 7) {
        daysOfWeek = this.getDates(fromDate, toDate);
      }
    }
    const dialogRef = this.dialog.open(TimeDialogComponent, {
      autoFocus: false,
      data: daysOfWeek
    });
    dialogRef.afterClosed().subscribe((result: any[]) => {
      if (result) {
        result.forEach((element: any) => {
          const find = this.workTime.find((el: any) => {
            return element.day === el.day;
          });
          if (find) {
            find.time = [...find.time, ...element.time];
          } else {
            this.workTime = [...this.workTime, element];
          }
        });
        this.preOrderForm.patchValue({
          preOrderAvailableTimes: this.workTime
        });
        this.cd.detectChanges();
      }
    });
  }

  deleteDay(index) {
    this.workTime.splice(index, 1);
    this.preOrderForm.patchValue({
      workTime: this.workTime
    });
  }

  onPreOrder() {
    this.preOrderForm.markAllAsTouched();
    if (this.preOrderForm.invalid) {
      return;
    }
    this.preOrdersFacade.dispatch(
      fromActions.createPreOrder({ request: this.preOrderForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

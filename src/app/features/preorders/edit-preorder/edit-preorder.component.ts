import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject
} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { TimeDialogComponent } from 'src/app/shared/components/time-dialog/time-dialog.component';

@Component({
  selector: 'app-edit-preorder',
  templateUrl: './edit-preorder.component.html',
  styleUrls: ['./edit-preorder.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditPreOrderComponent implements OnInit {
  preOrderForm: FormGroup;
  workTime = [];
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditPreOrderComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.initializePreOrderForm();
  }

  initializePreOrderForm() {
    this.preOrderForm = this.formBuilder.group({
      fromDate: [this.data.fromDate, [Validators.required]],
      toDate: [this.data.toDate, [Validators.required]],
      status: [this.data.status || true, [Validators.required]],
      preOrderAvailableTimes: [[]]
    });
    this.data.preOrderAvailableTimes.forEach((element: any, index) => {
      const arr = [];
      element.times.forEach(el => {
        const open =
          new Date(el.open.date).getHours() +
          ':' +
          this.correctMin(el.open.date);
        const close =
          new Date(el.close.date).getHours() +
          ':' +
          this.correctMin(el.close.date);
        arr.push({ open, close });
      });
      this.workTime.push({ day: element.day, time: arr });
      this.preOrderForm.patchValue({
        preOrderAvailableTimes: this.workTime
      });
    });
  }

  correctMin(data) {
    let min: any = new Date(data).getMinutes();
    if (min < 10) {
      min = `0${min}`;
    }
    return min;
  }

  addTime() {
    const dialogRef = this.dialog.open(TimeDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe((result: any[]) => {
      if (result) {
        result.forEach((element: any) => {
          const find = this.workTime.find((el: any) => {
            return element.day === el.day;
          });
          if (find) {
            find.time = [...find.time, ...element.time];
          } else {
            this.workTime = [...this.workTime, element];
          }
        });
        this.preOrderForm.patchValue({
          availableTimes: this.workTime
        });
      }
    });
  }

  deleteDay(index) {
    this.workTime.splice(index, 1);
    this.preOrderForm.patchValue({
      workTime: this.workTime
    });
  }

  onPreOrder() {
    this.preOrderForm.markAllAsTouched();
    if (this.preOrderForm.invalid) {
      return;
    }
    this.dialogRef.close(this.preOrderForm.value);
  }

  cancel(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

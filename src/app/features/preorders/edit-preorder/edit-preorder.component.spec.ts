import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPreOrderComponent } from './edit-preorder.component';

describe('EditPreOrderComponent', () => {
  let component: EditPreOrderComponent;
  let fixture: ComponentFixture<EditPreOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EditPreOrderComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPreOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

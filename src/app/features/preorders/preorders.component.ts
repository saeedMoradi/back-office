import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-preorders',
  templateUrl: './preorders.component.html',
  styleUrls: ['./preorders.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreOrdersComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}

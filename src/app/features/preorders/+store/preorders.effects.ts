import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './preorders.actions';
import { PreOrdersService } from '../services/preorders.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PreOrdersFacade } from './preorders.facade';
import { Router } from '@angular/router';

@Injectable()
export class PreOrdersEffects {
  constructor(
    private actions$: Actions,
    private preOrdersService: PreOrdersService,
    private preOrdersFacade: PreOrdersFacade,
    private matSnackBar: MatSnackBar,
    private router: Router
  ) {}

  getPreOrders$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getPreOrders),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.preOrdersService.getPreOrders(page).pipe(
          map((data: any) => {
            return fromActions.getPreOrdersSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getPreOrdersFail({ error })))
        );
      })
    )
  );

  createPreOrder$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createPreOrder),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.preOrdersService.createPreOrder(request).pipe(
          map((data: any) => {
            this.preOrdersService.clearForm();
            this.matSnackBar.open('Order For Later Created');
            this.router.navigate(['/home/preorders/preorders-list']);
            return fromActions.createPreOrderSuccess({ response: data });
          }),
          catchError(error => of(fromActions.createPreOrderFail({ error })))
        );
      })
    )
  );

  getPreOrder$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getPreOrder),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.preOrdersService.getPreOrder(id).pipe(
          map((data: any) => {
            return fromActions.getPreOrderSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getPreOrderFail({ error })))
        );
      })
    )
  );

  deletePreOrder$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deletePreOrder),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.preOrdersService.deletePreOrder(id).pipe(
          map((data: any) => {
            return fromActions.deletePreOrderSuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deletePreOrderFail({ error }));
          })
        );
      })
    )
  );

  putPreOrder$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putPreOrder),
      switchMap(({ id, request }) => {
        return this.preOrdersService.putPreOrder(id, request).pipe(
          map((data: any) => {
            this.preOrdersFacade.dispatch(
              fromActions.getPreOrders({ page: 'page=1' })
            );
            return fromActions.putPreOrderSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putPreOrderFail({ error }));
          })
        );
      })
    )
  );

  patchPreOrder$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchPreOrder),
      switchMap(({ id, request }) => {
        return this.preOrdersService.patchPreOrder(id, request).pipe(
          map((data: any) => {
            return fromActions.patchPreOrderSuccess({ response: data });
          }),
          catchError(error => of(fromActions.patchPreOrderFail({ error })))
        );
      })
    )
  );
}

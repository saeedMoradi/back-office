import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './preorders.selectors';

@Injectable({
  providedIn: 'root'
})
export class PreOrdersFacade implements Facade {
  isloading$: Observable<boolean>;
  error$: Observable<any>;
  preorders$: Observable<any>;
  preordersTotalLength$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.preorders$ = store.pipe(select(fromSelector.selectPreOrders));
    this.preordersTotalLength$ = store.pipe(
      select(fromSelector.selectPreordersTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

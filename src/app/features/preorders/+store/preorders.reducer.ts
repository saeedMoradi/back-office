import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './preorders.actions';
import { PreOrdersState } from './preorders.interface';

export const initialState: PreOrdersState = {
  error: null,
  isLoading: false,
  preorders: null
};

export const featureKey = 'preorders';

const preordersReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): PreOrdersState => ({ ...state, error: false })
  ),
  on(
    fromActions.getPreOrdersSuccess,
    (state, action): PreOrdersState => ({
      ...state,
      error: null,
      preorders: action.response
    })
  ),
  on(
    fromActions.getPreOrdersFail,
    (state, action): PreOrdersState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createPreOrder,
    (state, action): PreOrdersState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createPreOrderSuccess,
    (state, action): PreOrdersState => {
      if (!state.preorders) {
        return {
          ...state,
          preorders: {
            'hydra:member': [action.response],
            'hydra:totalItems': 1
          }
        };
      }
      return {
        ...state,
        error: null,
        isLoading: false,
        preorders: {
          ...state.preorders,
          'hydra:member': [action.response, ...state.preorders['hydra:member']],
          'hydra:totalItems': state.preorders['hydra:totalItems'] + 1
        }
      };
    }
  ),
  on(
    fromActions.createPreOrderFail,
    (state, action): PreOrdersState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deletePreOrderSuccess,
    (state, action): PreOrdersState => {
      const newpreorders = state.preorders['hydra:member'].filter(
        (item: any) => {
          return item.id !== action.id;
        }
      );
      return {
        ...state,
        error: null,
        preorders: {
          ...state.preorders,
          'hydra:member': newpreorders,
          'hydra:totalItems': state.preorders['hydra:totalItems'] - 1
        }
      };
    }
  ),
  on(
    fromActions.putPreOrderSuccess,
    (state, action): PreOrdersState => {
      const newpreorders = state.preorders['hydra:member'].map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        preorders: { ...state.preorders, 'hydra:member': newpreorders }
      };
    }
  )
);

export function reducer(state: PreOrdersState, action: Action) {
  return preordersReducer(state, action);
}

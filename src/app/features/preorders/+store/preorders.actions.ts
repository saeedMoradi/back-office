import { createAction, props } from '@ngrx/store';

export const getPreOrders = createAction(
  '[PreOrders] Get PreOrders',
  props<{ page: any }>()
);
export const getPreOrdersSuccess = createAction(
  '[PreOrders] Get PreOrders Success',
  props<{ response: any }>()
);
export const getPreOrdersFail = createAction(
  '[PreOrders] Get PreOrders Fail',
  props<{ error: any }>()
);

export const createPreOrder = createAction(
  '[PreOrders] Create PreOrder',
  props<{ request: any }>()
);
export const createPreOrderSuccess = createAction(
  '[PreOrders] Create PreOrder Success',
  props<{ response: any }>()
);
export const createPreOrderFail = createAction(
  '[PreOrders] Create PreOrder Fail',
  props<{ error: any }>()
);

export const getPreOrder = createAction(
  '[PreOrders] Get PreOrder',
  props<{ id: any }>()
);
export const getPreOrderSuccess = createAction(
  '[PreOrders] Get PreOrder Success',
  props<{ response: any }>()
);
export const getPreOrderFail = createAction(
  '[PreOrders] Get PreOrder Fail',
  props<{ error: any }>()
);

export const deletePreOrder = createAction(
  '[PreOrders] Delete PreOrder',
  props<{ id: any }>()
);
export const deletePreOrderSuccess = createAction(
  '[PreOrders] Delete PreOrder Success',
  props<{ id: any }>()
);
export const deletePreOrderFail = createAction(
  '[PreOrders] Delete PreOrder Fail',
  props<{ error: any }>()
);

export const putPreOrder = createAction(
  '[PreOrders] Put PreOrder',
  props<{ id: any; request: any }>()
);
export const putPreOrderSuccess = createAction(
  '[PreOrders] Put PreOrder Success',
  props<{ response: any }>()
);
export const putPreOrderFail = createAction(
  '[PreOrders] Put PreOrder Fail',
  props<{ error: any }>()
);

export const patchPreOrder = createAction(
  '[PreOrders] Patch PreOrder',
  props<{ id: any; request: any }>()
);
export const patchPreOrderSuccess = createAction(
  '[PreOrders] Patch PreOrder Success',
  props<{ response: any }>()
);
export const patchPreOrderFail = createAction(
  '[PreOrders] Patch PreOrder Fail',
  props<{ error: any }>()
);

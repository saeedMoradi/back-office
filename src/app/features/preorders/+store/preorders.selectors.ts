import { createSelector, createFeatureSelector } from '@ngrx/store';
import { PreOrdersState } from './preorders.interface';
import { featureKey } from './preorders.reducer';

export const selectFeature = createFeatureSelector<PreOrdersState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: PreOrdersState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: PreOrdersState) => state.isLoading
);
export const selectPreOrders = createSelector(
  selectFeature,
  (state: PreOrdersState) =>
    state.preorders ? state.preorders['hydra:member'] : null
);
export const selectPreordersTotalLength = createSelector(
  selectFeature,
  (state: PreOrdersState) =>
    state.preorders ? state.preorders['hydra:totalItems'] : null
);

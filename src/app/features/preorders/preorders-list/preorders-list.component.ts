import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { PreOrdersFacade } from '../+store/preorders.facade';
import {
  getPreOrders,
  deletePreOrder,
  putPreOrder
} from '../+store/preorders.actions';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PaginationData } from '../../../shared/interfaces';
import { ConfirmDialogComponent } from '../../../shared/components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { EditPreOrderComponent } from '../edit-preorder/edit-preorder.component';

@Component({
  selector: 'app-preorders-list',
  templateUrl: './preorders-list.component.html',
  styleUrls: ['./preorders-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreOrdersListComponent implements OnInit {
  preorders: any;
  originalPreorders: any;
  uniqueKey: any;
  destroy$ = new Subject();

  constructor(
    public preOrdersFacade: PreOrdersFacade,
    private cd: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.preOrdersFacade.dispatch(getPreOrders({ page: 'page=1' }));
    this.preOrdersFacade.preorders$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        if (data) {
          this.preorders = data;
          this.originalPreorders = data;
          data.forEach(element => {
            this.uniqueKey = {
              ...this.uniqueKey,
              [element.id]: false
            };
          });
          this.cd.detectChanges();
        }
      });
  }

  search(term = '') {
    this.preorders = this.originalPreorders.filter((element: any) => {
      return;
      // return (
      //   element.title.toLowerCase().includes(term) ||
      //   element.description.toLowerCase().includes(term)
      // );
    });
  }

  onRemove(data) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.preOrdersFacade.dispatch(deletePreOrder({ id: data.id }));
      }
    });
  }

  onEdit(data) {
    const dialogRef = this.dialog.open(EditPreOrderComponent, {
      autoFocus: false,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.preOrdersFacade.dispatch(
          putPreOrder({ id: data.id, request: result })
        );
      }
    });
  }

  onExpand(data) {
    this.uniqueKey = {
      ...this.uniqueKey,
      [data.id]: !this.uniqueKey[data.id]
    };
  }

  onChangePage(event: PaginationData) {
    this.preOrdersFacade.dispatch(
      getPreOrders({ page: `page=${event.pageIndex + 1}` })
    );
  }

  trackByFn(index: number, item: any) {
    return index;
  }

  trackByFnDetails(index: number, item: any) {
    return index;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

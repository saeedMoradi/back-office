import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { CreateProductComponent } from './create-product/create-product.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { ExpandbleGroupComponent } from './expandble-group/expandble-group.component';
import { EffectsModule } from '@ngrx/effects';
import { ProductsEffects } from './+store/products.effects';
import { EditProductComponent } from './edit-product/edit-product.component';
import { AddAccessoriesDialogComponent } from './add-accessories-dialog/add-accessories-dialog.component';
import { MultiplePriceDialogComponent } from './multiple-price-dialog/multiple-price-dialog.component';
import { CategoriesEffects } from '../categories/+store/categories.effects';
import { MerchantsEffects } from '../merchants/+store/merchants.effects';
import { VariationsEffects } from '../variations/+store/variations.effects';
import { AccessoriesEffects } from '../accessories/+store/accessories.effects';
import { DepartmentsEffects } from '../departments/+store/departments.effects';
import { AllergiesEffects } from '../allergies/+store/allergies.effects';
import { TypesEffects } from '../types/+store/types.effects';
import { TaxEffects } from '../tax/+store/tax.effects';

@NgModule({
  declarations: [
    ProductsComponent,
    CreateProductComponent,
    ProductsListComponent,
    EditProductComponent,
    ExpandbleGroupComponent,
    AddAccessoriesDialogComponent,
    MultiplePriceDialogComponent
  ],
  imports: [
    SharedModule,
    ProductsRoutingModule,
    EffectsModule.forFeature([
      ProductsEffects,
      CategoriesEffects,
      MerchantsEffects,
      VariationsEffects,
      AccessoriesEffects,
      DepartmentsEffects,
      AllergiesEffects,
      TypesEffects,
      TaxEffects
    ])
  ]
})
export class ProductsModule {}

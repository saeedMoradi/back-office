import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PaginationData } from 'src/app/shared/interfaces';
import { ProductsFacade } from '../+store/products.facade';
import {
  getProducts,
  putProduct,
  deleteProduct
} from '../+store/products.actions';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { EditProductComponent } from '../edit-product/edit-product.component';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductsListComponent implements OnInit {
  products: any;
  originalProducts: any;
  uniqueKey: any;
  destroy$ = new Subject();

  constructor(
    public productsFacade: ProductsFacade,
    private cd: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.productsFacade.dispatch(getProducts({ page: 'page=1' }));
    this.productsFacade.products$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        if (data) {
          this.products = data;
          this.originalProducts = data;
          data.forEach(element => {
            this.uniqueKey = {
              ...this.uniqueKey,
              [element.id]: false
            };
          });
          this.cd.detectChanges();
        }
      });
  }

  search(term = '') {
    this.products = this.originalProducts.filter((element: any) => {
      return (
        element.title.toLowerCase().includes(term) ||
        element.description.toLowerCase().includes(term)
      );
    });
  }

  onRemove(data) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.productsFacade.dispatch(deleteProduct({ id: data.id }));
      }
    });
  }

  onEdit(data) {
    const dialogRef = this.dialog.open(EditProductComponent, {
      autoFocus: false,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.productsFacade.dispatch(
          putProduct({ id: data.id, request: result })
        );
      }
    });
  }

  onExpand(data) {
    this.uniqueKey = {
      ...this.uniqueKey,
      [data.id]: !this.uniqueKey[data.id]
    };
  }

  onChangePage(event: PaginationData) {
    this.productsFacade.dispatch(
      getProducts({ page: `page=${event.pageIndex + 1}` })
    );
  }

  trackByFn(index: number, item: any) {
    return index;
  }

  trackByFnDetails(index: number, item: any) {
    return index;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

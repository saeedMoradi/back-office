import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './products.selectors';

@Injectable({
  providedIn: 'root'
})
export class ProductsFacade implements Facade {
  isloading$: Observable<boolean>;
  products$: Observable<any>;
  productsTotalLength$: Observable<any>;
  // productsForModify$: Observable<any>;
  error$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.products$ = store.pipe(select(fromSelector.selectProducts));
    this.productsTotalLength$ = store.pipe(
      select(fromSelector.selectProductsTotalLength)
    );
    // this.productsForModify$ = store.pipe(
    //   select(fromSelector.selectProductsForModify)
    // );
    this.error$ = store.pipe(select(fromSelector.selectError));
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

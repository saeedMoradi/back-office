import * as productsReducer from './products.reducer';
import { ProductsState } from './products.interface';
import * as productsSelectors from './products.selectors';

export { productsReducer, productsSelectors, ProductsState };

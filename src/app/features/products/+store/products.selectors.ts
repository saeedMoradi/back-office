import { createSelector, createFeatureSelector } from '@ngrx/store';
import { ProductsState } from './products.interface';
import { featureKey } from './products.reducer';

export const selectFeature = createFeatureSelector<ProductsState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: ProductsState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: ProductsState) => state.isLoading
);
// export const selectProductsForModify = createSelector(
//   selectFeature,
//   (state: ProductsState) => state.productsForModify
// );
export const selectProducts = createSelector(
  selectFeature,
  (state: ProductsState) =>
    state.products ? state.products['hydra:member'] : null
);
export const selectProductsTotalLength = createSelector(
  selectFeature,
  (state: ProductsState) =>
    state.products ? state.products['hydra:totalItems'] : null
);

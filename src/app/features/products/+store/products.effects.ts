import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './products.actions';
import { ProductsService } from '../services/products.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProductsFacade } from './products.facade';
import { Router } from '@angular/router';

@Injectable()
export class ProductsEffects {
  constructor(
    private actions$: Actions,
    private productsService: ProductsService,
    private matSnackBar: MatSnackBar,
    private productsFacade: ProductsFacade,
    private router: Router
  ) {}

  getProducts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getProducts),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.productsService.getProducts(page).pipe(
          map((data: any) => {
            return fromActions.getProductsSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.getProductsFail({ error }));
          })
        );
      })
    )
  );

  createProduct$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createProduct),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.productsService.createProduct(request).pipe(
          map((data: any) => {
            this.productsService.clearForm();
            this.matSnackBar.open('Product Created');
            this.router.navigate(['/home/products/products-list']);
            return fromActions.createProductSuccess({ response: data });
          }),
          catchError(error => of(fromActions.createProductFail({ error })))
        );
      })
    )
  );

  getProduct$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getProduct),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.productsService.getProduct(id).pipe(
          map((data: any) => {
            return fromActions.getProductSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getProductFail({ error })))
        );
      })
    )
  );

  deleteProduct$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteProduct),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.productsService.deleteProduct(id).pipe(
          map((data: any) => {
            return fromActions.deleteProductSuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteProductFail({ error }));
          })
        );
      })
    )
  );

  putProduct$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putProduct),
      switchMap(({ id, request }) => {
        return this.productsService.putProduct(id, request).pipe(
          map((data: any) => {
            this.productsFacade.dispatch(
              fromActions.getProducts({ page: 'page=1' })
            );
            return fromActions.putProductSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putProductFail({ error }));
          })
        );
      })
    )
  );

  putProductWithoutId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putProductWithoutId),
      switchMap(({ request }) => {
        return this.productsService.putProductWithoutId(request).pipe(
          map((data: any) => {
            this.productsService.clearForm();
            this.matSnackBar.open('Product Created');
            this.router.navigate(['/home/products/products-list']);
            return fromActions.putProductWithoutIdSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putProductWithoutIdFail({ error }));
          })
        );
      })
    )
  );
}

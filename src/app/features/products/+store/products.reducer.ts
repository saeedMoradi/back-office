import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './products.actions';
import { ProductsState } from './products.interface';

export const initialState: ProductsState = {
  error: null,
  isLoading: false,
  products: null
};

export const featureKey = 'products';

const productsReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): ProductsState => ({
      ...state,
      error: null
    })
  ),
  on(
    fromActions.getProductsSuccess,
    (state, action): ProductsState => ({
      ...state,
      error: null,
      products: action.response
    })
  ),
  on(
    fromActions.getProductsFail,
    (state, action): ProductsState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createProduct,
    (state, action): ProductsState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createProductSuccess,
    (state, action): ProductsState => {
      return {
        ...state,
        error: null,
        isLoading: false
      };
      // if (!action.response.combinations.length) {
      // } else {
      //   return {
      //     ...state,
      //     error: null,
      //     isLoading: false,
      //     productsForModify: action.response
      //   };
      // }
    }
  ),
  on(
    fromActions.createProductFail,
    (state, action): ProductsState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteProduct,
    (state, action): ProductsState => {
      const newproducts = state.products['hydra:member'].filter((item: any) => {
        return item.id !== action.id;
      });
      return {
        ...state,
        error: null,
        products: {
          ...state.products,
          'hydra:member': newproducts,
          'hydra:totalItems': state.products['hydra:totalItems'] - 1
        }
      };
    }
  ),
  on(
    fromActions.putProductWithoutId,
    (state, action): ProductsState => {
      return {
        ...state,
        isLoading: true,
        error: null
      };
    }
  ),
  on(
    fromActions.putProductWithoutIdSuccess,
    (state, action): ProductsState => {
      return {
        ...state,
        error: null,
        isLoading: false
        // productsForModify: null
      };
    }
  ),
  on(
    fromActions.putProductWithoutIdFail,
    (state, action): ProductsState => {
      return {
        ...state,
        error: action.error,
        isLoading: false
      };
    }
  ),
  on(
    fromActions.clearModifyProduct,
    (state, action): ProductsState => {
      return {
        ...state
        // productsForModify: null
      };
    }
  )
);

export function reducer(state: ProductsState, action: Action) {
  return productsReducer(state, action);
}

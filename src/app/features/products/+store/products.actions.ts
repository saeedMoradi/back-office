import { createAction, props } from '@ngrx/store';

export const getProducts = createAction(
  '[Products] Get Products',
  props<{ page: any }>()
);
export const getProductsSuccess = createAction(
  '[Products] Get Products Success',
  props<{ response: any }>()
);
export const getProductsFail = createAction(
  '[Products] Get Products Fail',
  props<{ error: any }>()
);

export const createProduct = createAction(
  '[Products] Create Product',
  props<{ request: any }>()
);
export const createProductSuccess = createAction(
  '[Products] Create Product Success',
  props<{ response: any }>()
);
export const createProductFail = createAction(
  '[Products] Create Product Fail',
  props<{ error: any }>()
);

export const getProduct = createAction(
  '[Products] Get Product',
  props<{ id: any }>()
);
export const getProductSuccess = createAction(
  '[Products] Get Product Success',
  props<{ response: any }>()
);
export const getProductFail = createAction(
  '[Products] Get Product Fail',
  props<{ error: any }>()
);

export const deleteProduct = createAction(
  '[Products] Delete Product',
  props<{ id: any }>()
);
export const deleteProductSuccess = createAction(
  '[Products] Delete Product Success',
  props<{ id: any }>()
);
export const deleteProductFail = createAction(
  '[Products] Delete Product Fail',
  props<{ error: any }>()
);

export const putProduct = createAction(
  '[Products] Put Product',
  props<{ id: any; request: any }>()
);
export const putProductSuccess = createAction(
  '[Products] Put Product Success',
  props<{ response: any }>()
);
export const putProductFail = createAction(
  '[Products] Put Product Fail',
  props<{ error: any }>()
);

export const clearModifyProduct = createAction(
  '[Products] Clear Modify Product'
);
export const putProductWithoutId = createAction(
  '[Products] Put Product Without Id',
  props<{ request: any }>()
);
export const putProductWithoutIdSuccess = createAction(
  '[Products] Put Product Without Id Success',
  props<{ response: any }>()
);
export const putProductWithoutIdFail = createAction(
  '[Products] Put Product Without Id Fail',
  props<{ error: any }>()
);

export const patchProduct = createAction(
  '[Products] Patch Product',
  props<{ id: any; request: any }>()
);
export const patchProductSuccess = createAction(
  '[Products] Patch Product Success',
  props<{ response: any }>()
);
export const patchProductFail = createAction(
  '[Products] Patch Product Fail',
  props<{ error: any }>()
);

import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ChangeDetectorRef,
  ElementRef
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormGroupDirective,
  Validators,
  FormArray
} from '@angular/forms';
import { ProductsFacade } from '../+store/products.facade';
import * as fromActions from '../+store/products.actions';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ProductsService } from '../services/products.service';
import { CategoriesFacade } from '../../categories/+store/categories.facade';
import { getCategories } from '../../categories/+store/categories.actions';
import { VariationsFacade } from '../../variations/+store/variations.facade';
import { getVariations } from '../../variations/+store/variations.actions';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { getAccessories } from '../../accessories/+store/accessories.actions';
import { AccessoriesFacade } from '../../accessories/+store/accessories.facade';
import { DepartmentsFacade } from '../../departments/+store/departments.facade';
import { getDepartments } from '../../departments/+store/departments.actions';
import { getAllergies } from '../../allergies/+store/allergies.actions';
import { AllergiesFacade } from '../../allergies/+store/allergies.facade';
import { AddAccessoriesDialogComponent } from '../add-accessories-dialog/add-accessories-dialog.component';
import { HttpEventType } from '@angular/common/http';
import { TypesFacade } from '../../types/+store/types.facade';
import { getTypes } from '../../types/+store/types.actions';
import { MultiplePriceDialogComponent } from '../multiple-price-dialog/multiple-price-dialog.component';
import { MediaObjects } from 'src/app/shared/interfaces';
import { TaxesFacade } from '../../tax/+store/tax.facade';
import { getTaxes } from '../../tax/+store/tax.actions';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateProductComponent implements OnInit {
  @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  @ViewChild('productWithAccessory') productWithAccessory: FormGroupDirective;
  productForm: FormGroup;
  productWithAccessoryForm: FormGroup;
  variations;
  accessories;
  fileList = [];
  currentSelectedFile;
  imageSrc;
  uploadFileLoading;
  progress;
  productImageData: MediaObjects;
  uploadError;
  selectedVar = [];
  combinations = [];
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private productsService: ProductsService,
    public productsFacade: ProductsFacade,
    public categoriesFacade: CategoriesFacade,
    public variationsFacade: VariationsFacade,
    public accessoriesFacade: AccessoriesFacade,
    public departmentsFacade: DepartmentsFacade,
    public allergiesFacade: AllergiesFacade,
    public typesFacade: TypesFacade,
    public taxFacade: TaxesFacade,
    private cd: ChangeDetectorRef,
    private snackbar: MatSnackBar,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.initializeProductForm();
    this.initializeProductWithAccessoryForm();
    this.listenFormReset();
    this.categoriesFacade.dispatch(getCategories({ page: 'pagination=false' }));
    this.variationsFacade.dispatch(getVariations({ page: 'pagination=false' }));
    this.taxFacade.dispatch(getTaxes({ page: 'pagination=false' }));
    this.accessoriesFacade.dispatch(
      getAccessories({ page: 'pagination=false' })
    );
    this.departmentsFacade.dispatch(
      getDepartments({ page: 'pagination=false' })
    );
    this.allergiesFacade.dispatch(getAllergies({ page: 'pagination=false' }));
    this.typesFacade.dispatch(getTypes({ page: 'pagination=false' }));
    this.accessoriesFacade.accessories$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        if (data) {
          this.accessories = data;
        }
      });
  }

  initializeProductForm() {
    this.productForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', []],
      tax: [null, []],
      department: [null, []],
      price: ['', [Validators.required]],
      categories: [[], [Validators.required]],
      status: [true, [Validators.required]],
      variants: [[], []],
      allergies: [[], []],
      images: [[], []]
    });
  }

  initializeProductWithAccessoryForm() {
    this.productWithAccessoryForm = this.formBuilder.group({
      productVariant: this.formBuilder.array([])
    });
  }

  get productsControl() {
    return this.productWithAccessoryForm.get('productVariant') as FormArray;
  }

  removeAccessory(productIndex, accessoryIndex) {
    const control = this.productsControl.controls[productIndex].get(
      'accessories'
    ) as FormArray;
    control.removeAt(accessoryIndex);
  }

  getAccesories(form) {
    return form.controls.accessories.controls;
  }

  multiplePrice(index) {
    const control = this.productsControl.controls[index].get(
      'prices'
    ) as FormArray;
    const dialogRef = this.dialog.open(MultiplePriceDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const control = this.productsControl.controls[index];
        let multiPrice = control.value.prices;
        result.forEach((element: any) => {
          const find = multiPrice.find((el: any) => {
            return element.day === el.day;
          });
          if (find) {
            find.time = [...find.time, ...element.time];
          } else {
            multiPrice = [...multiPrice, element];
          }
        });
        control.patchValue({
          prices: multiPrice
        });
        this.cd.detectChanges();
      }
    });
  }

  deleteBaseTimePrice(controlIndex, removeIndex) {
    const control = this.productsControl.controls[controlIndex];
    let multiPrice = control.value.multiPrice;
    multiPrice.splice(removeIndex, 1);
    control.patchValue({
      multiPrice
    });
  }

  addAccessories(index) {
    if (this.accessories && this.accessories.length) {
      const dialogRef = this.dialog.open(AddAccessoriesDialogComponent, {
        autoFocus: false,
        data: {
          accessories: this.accessories
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result && result.length) {
          const control = this.productsControl.controls[index].get(
            'accessories'
          ) as FormArray;
          result.forEach(element => {
            element.childs.forEach(el => {
              control.push(
                this.formBuilder.group({
                  accessory: [
                    `/api/accessories/${element.id}`,
                    [Validators.required]
                  ],
                  title: [
                    element.title + ' - ' + el.title,
                    [Validators.required]
                  ],
                  price: [el.price],
                  free: [el.free],
                  min: [el.min],
                  max: [el.max],
                  default: [true],
                  allergies: [[], []]
                })
              );
            });
          });
          this.cd.detectChanges();
        }
      });
    } else {
      this.snackbar.open('Not Accessories Found');
    }
  }

  listenFormReset() {
    this.productsService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetProductForm();
      });
  }

  resetProductForm(data = null) {
    if (data === 'clear-accessory') {
      this.productsFacade.dispatch(fromActions.clearModifyProduct());
    }
    this.selectedVar = [];
    this.combinations = [];
    this.formDirective.resetForm();
    this.productForm.reset({ status: true });
    this.progress = null;
    this.productImageData = null;
    if (this.productWithAccessory) {
      this.productWithAccessory.resetForm();
      this.productWithAccessoryForm.setControl(
        'products',
        this.formBuilder.array([])
      );
    }
  }

  addImage() {
    const fileInput = this.fileInput.nativeElement;
    fileInput.onchange = () => {
      for (let index = 0; index < fileInput.files.length; index++) {
        const file = fileInput.files[index];
        this.fileList.push({ data: file, inProgress: false, progress: 0 });
      }
      this.selectFile();
    };
    fileInput.click();
  }

  selectFile(): void {
    const formData: FormData = new FormData();
    formData.append('file', this.fileList[0].data, this.fileList[0].data.name);
    this.productsService
      .uploadFile(formData)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progress = Math.round((100 * event.loaded) / event.total);
            this.cd.detectChanges();
          }
          if (event.type === HttpEventType.Response) {
            this.productImageData = event.body;
            this.uploadFileLoading = false;
            const image = this.productImageData['@id'].split('/');
            this.productForm.patchValue({
              images: [+image[image.length - 1]]
            });
            this.cd.detectChanges();
          }
        },
        error => {
          this.uploadFileLoading = false;
          this.uploadError = error;
          this.cd.detectChanges();
        }
      );
  }

  selectVariant({ checked, items, item }) {
    const findParentIndex = this.selectedVar.findIndex((data: any) => {
      return data.id === items.id;
    });
    if (checked) {
      if (findParentIndex >= 0) {
        this.selectedVar[findParentIndex].childs.push({
          id: item.id,
          title: item.title
        });
      } else {
        this.selectedVar.push({
          id: items.id,
          title: items.title,
          childs: [{ id: item.id, title: item.title }]
        });
      }
    } else {
      if (findParentIndex >= 0) {
        if (this.selectedVar[findParentIndex].childs.length === 1) {
          this.selectedVar.splice(findParentIndex, 1);
        } else {
          const findChildIndex = this.selectedVar[
            findParentIndex
          ].childs.findIndex((el: any) => {
            return el.id === item.id;
          });
          this.selectedVar[findParentIndex].childs.splice(findChildIndex, 1);
        }
      }
    }
    this.productForm.patchValue({
      variants: this.selectedVar
    });
  }

  onProduct() {
    this.productForm.markAllAsTouched();
    if (this.productForm.invalid) {
      return;
    }
    if (!this.selectedVar.length) {
      this.productsFacade.dispatch(
        fromActions.createProduct({
          request: this.productForm.value
        })
      );
    } else {
      let arr = [];
      this.selectedVar.forEach((element: any) => {
        let nestedArr = [];
        element.childs.forEach((el: any) => {
          nestedArr.push({ id: el.id, title: el.title });
        });
        arr.push(nestedArr);
      });
      let result = arr[0].map(function (item) {
        return [item];
      });
      for (let k = 1; k < arr.length; k++) {
        const next = [];
        result.forEach(function (item) {
          arr[k].forEach(function (word) {
            const line = item.slice(0);
            line.push(word);
            next.push(line);
          });
        });
        result = next;
      }
      result.forEach(element => {
        let finaleNested = { ids: [], title: '' };
        element.forEach(el => {
          finaleNested = {
            ids: [...finaleNested.ids, `/api/variants/${el.id}`],
            title: `${finaleNested.title}${finaleNested.title ? '-' : ''}${
              el.title
            }`
          };
        });
        this.combinations.push(finaleNested);
      });
      this.combinations.forEach(element => {
        this.productsControl.push(
          this.formBuilder.group({
            title: [element.title, []],
            variants: [element.ids, []],
            price: [0, []],
            status: [false, []],
            accessories: this.formBuilder.array([]),
            productTypes: [[], []],
            allergies: [[], []],
            prices: [[], []]
          })
        );
      });
    }
  }

  onProductWithAccessory() {
    this.productWithAccessoryForm.markAllAsTouched();
    if (this.productWithAccessoryForm.invalid) {
      return;
    }
    let countDefault = 0;
    this.productsControl.controls.forEach((data: any) => {
      if (data.value.status) {
        countDefault++;
      }
    });
    if (countDefault === 0) {
      return this.snackbar.open('Please Select a Default Product');
    }
    if (countDefault > 1) {
      return this.snackbar.open('Please Select Only One Default Product');
    }
    // this.productsFacade.dispatch(
    //   fromActions.putProductWithoutId({
    //     request: this.productWithAccessoryForm.value.products
    //   })
    // );
    this.productsFacade.dispatch(
      fromActions.createProduct({
        request: {
          ...this.productForm.value,
          productVariant: this.productWithAccessoryForm.value.productVariant
        }
      })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

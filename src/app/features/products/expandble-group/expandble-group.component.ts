import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  EventEmitter,
  Output
} from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-expandble-group',
  templateUrl: './expandble-group.component.html',
  styleUrls: ['./expandble-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExpandbleGroupComponent implements OnInit {
  @Input('items') items;
  @Input('selected') selected;
  @Output() selectItem = new EventEmitter();
  expanded = false;
  destroy$ = new Subject();

  constructor() {}

  ngOnInit() {}

  onChange(checked, items, item) {
    this.selectItem.emit({ checked, items, item });
  }

  get childs(): [] {
    if (this.items.childern) {
      return this.items.childern;
    }
    if (this.items.children) {
      return this.items.children;
    }
    return [];
  }

  calcChecked(id) {
    if (!this.selected.length) {
      return false;
    } else {
      const find = this.selected.find(element => {
        return element.childs.find((el: any) => {
          return el.id === id;
        });
      });
      if (find) {
        return true;
      } else {
        return false;
      }
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

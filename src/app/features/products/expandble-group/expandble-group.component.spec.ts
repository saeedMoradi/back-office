import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpandbleGroupComponent } from './expandble-group.component';

describe('ExpandbleGroupComponent', () => {
  let component: ExpandbleGroupComponent;
  let fixture: ComponentFixture<ExpandbleGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExpandbleGroupComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpandbleGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

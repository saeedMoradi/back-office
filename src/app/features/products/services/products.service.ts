import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PersistanceService } from 'src/app/core/services/persistance.service';
import { LoginResponse } from 'src/app/shared/interfaces';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private readonly apiUrl = environment.apiUrl;
  private clearFormSource = new Subject<boolean>();
  clearForm$ = this.clearFormSource.asObservable();

  constructor(
    private http: HttpClient,
    private persistanceService: PersistanceService
  ) {}

  clearForm() {
    this.clearFormSource.next(true);
  }

  getProducts(page = ''): Observable<any> {
    const profile: LoginResponse = this.persistanceService.get('profile');
    return this.http.get<any>(
      `${this.apiUrl}/api/products?owner.id=${profile.id}&order[id]=desc&${page}`
    );
  }
  createProduct(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/products`, data);
  }
  getProduct(id: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/products/${id}`);
  }
  deleteProduct(id: any): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/api/products/${id}`);
  }
  putProduct(id: any, data: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/api/products/${id}`, data);
  }
  putProductWithoutId(data: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/api/products`, data);
  }
  patchProduct(id: any, data: any): Observable<any> {
    return this.http.patch<any>(`${this.apiUrl}/api/products/${id}`, data);
  }
  uploadFile(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/media_objects`, data, {
      headers: { Anonymous: 'undefined' },
      reportProgress: true,
      observe: 'events'
    });
  }
  getFiles(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/media_objects`);
  }
  getFile(id): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/media_objects/${id}`);
  }
}

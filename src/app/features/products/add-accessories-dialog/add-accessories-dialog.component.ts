import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-add-accessories-dialog',
  templateUrl: './add-accessories-dialog.component.html',
  styleUrls: ['./add-accessories-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddAccessoriesDialogComponent implements OnInit {
  destroy$ = new Subject();
  selected = [];

  constructor(
    public dialogRef: MatDialogRef<AddAccessoriesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  selectAccessory({ checked, items, item }) {
    const findParentIndex = this.selected.findIndex((data: any) => {
      return data.id === items.id;
    });
    if (checked) {
      if (findParentIndex >= 0) {
        this.selected[findParentIndex].childs.push({
          id: item.id,
          title: item.title,
          price: item.price || 0,
          free: item.free || 0,
          min: item.min || 0,
          max: item.max || 0
        });
      } else {
        this.selected.push({
          id: items.id,
          title: items.title,
          childs: [
            {
              id: item.id,
              title: item.title,
              price: item.price || 0,
              free: item.free || 0,
              min: item.min || 0,
              max: item.max || 0
            }
          ]
        });
      }
    } else {
      if (findParentIndex >= 0) {
        if (this.selected[findParentIndex].childs.length === 1) {
          this.selected.splice(findParentIndex, 1);
        } else {
          const findChildIndex = this.selected[
            findParentIndex
          ].childs.findIndex((el: any) => {
            return el.id === item.id;
          });
          this.selected[findParentIndex].childs.splice(findChildIndex, 1);
        }
      }
    }
  }

  // calcChecked(id) {
  //   if (!this.selected || !this.selected.length) {
  //     return false;
  //   } else {
  //     const find = this.selected.find(element => {
  //       return element.childs.find((el: any) => {
  //         return el.id === id;
  //       });
  //     });
  //     if (find) {
  //       return true;
  //     } else {
  //       return false;
  //     }
  //   }
  // }

  close(): void {
    this.dialogRef.close();
  }
  done(): void {
    this.dialogRef.close(this.selected);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

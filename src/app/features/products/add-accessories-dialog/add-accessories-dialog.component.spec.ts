import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAccessoriesDialogComponent } from './add-accessories-dialog.component';

describe('AddAccessoriesDialogComponent', () => {
  let component: AddAccessoriesDialogComponent;
  let fixture: ComponentFixture<AddAccessoriesDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddAccessoriesDialogComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAccessoriesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

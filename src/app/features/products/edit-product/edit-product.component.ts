import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditProductComponent implements OnInit {
  productForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditProductComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.initializeProductForm();
  }

  initializeProductForm() {
    let department = null;
    if (this.data.department) {
      const dep = this.data.department.split('/');
      department = +dep[dep.length - 1];
    }
    const shop = this.data.shops.map((el: any) => {
      const s = el.split('/');
      return +s[s.length - 1];
    });
    const category = this.data.categories.map((el: any) => {
      const s = el.split('/');
      return +s[s.length - 1];
    });
    this.productForm = this.formBuilder.group({
      title: [this.data.title, []],
      description: [this.data.description, []],
      price: [this.data.price, []],
      tax: [this.data.tax, []],
      department: [department, []],
      category: [category, []],
      shop: [shop, []],
      status: [this.data.status, []],
      variants: [[], []],
      limitations: [[], []],
      allergies: [[], []],
      images: [[], []]
    });
  }

  onProduct() {
    this.productForm.markAllAsTouched();
    if (this.productForm.invalid) {
      return;
    }
    this.dialogRef.close(this.productForm.value);
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { ProductsComponent } from './products.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('Products Component', () => {
  let spectator: Spectator<ProductsComponent>;
  const createComponent = createComponentFactory({
    component: ProductsComponent,
    imports: [RouterTestingModule]
  });

  beforeEach(() => (spectator = createComponent()));

  it('should create', () => {
    expect(spectator).toBeTruthy();
  });
});

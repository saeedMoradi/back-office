import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiplePriceDialogComponent } from './multiple-price-dialog.component';

describe('MultiplePriceDialogComponent', () => {
  let component: MultiplePriceDialogComponent;
  let fixture: ComponentFixture<MultiplePriceDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultiplePriceDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiplePriceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

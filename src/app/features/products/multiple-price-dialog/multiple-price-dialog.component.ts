import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-multiple-price-dialog',
  templateUrl: './multiple-price-dialog.component.html',
  styleUrls: ['./multiple-price-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiplePriceDialogComponent implements OnInit {
  weekDay = [
    { day: 'Saturday', time: [] },
    { day: 'Sunday', time: [] },
    { day: 'Monday', time: [] },
    { day: 'Tuesday', time: [] },
    { day: 'Wednesday', time: [] },
    { day: 'Thursday', time: [] },
    { day: 'Friday', time: [] }
  ];
  constructor(
    public dialogRef: MatDialogRef<MultiplePriceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {}

  selectDay(e, day) {
    if (e.checked === false) {
      const index = this.weekDay.findIndex(
        (el: { day: string; time: any[] }) => {
          return el.day === day;
        }
      );
      this.weekDay.splice(index, 1);
    } else {
      this.weekDay = [
        ...this.weekDay,
        {
          day,
          time: []
        }
      ];
    }
  }

  cancel(): void {
    this.dialogRef.close();
  }
  done(open, close, price): void {
    if (open && close && price) {
      this.weekDay.forEach((el: any) => {
        el.time = [...el.time, { open, close, price }];
      });
      this.dialogRef.close(this.weekDay);
    } else {
      return;
    }
  }
}

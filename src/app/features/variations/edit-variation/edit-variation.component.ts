import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-edit-variation',
  templateUrl: './edit-variation.component.html',
  styleUrls: ['./edit-variation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditVariationComponent implements OnInit {
  variationForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditVariationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.initializeVariationForm();
  }

  initializeVariationForm() {
    this.variationForm = this.formBuilder.group({
      title: [this.data.variant.title, [Validators.required]],
      description: [this.data.variant.description, [Validators.required]],
      parent: [this.data.variant.parent ? this.data.variant.parent : null]
    });
  }

  onVariation() {
    this.variationForm.markAllAsTouched();
    if (this.variationForm.invalid) {
      return;
    }
    this.dialogRef.close(this.variationForm.value);
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { RouterTestingModule } from '@angular/router/testing';
import { CreateVariationComponent } from './create-variation.component';

describe('Create Variations Component', () => {
  let spectator: Spectator<CreateVariationComponent>;
  const createComponent = createComponentFactory({
    component: CreateVariationComponent,
    imports: [RouterTestingModule]
  });

  beforeEach(() => (spectator = createComponent()));

  it('should create', () => {
    expect(spectator).toBeTruthy();
  });
});

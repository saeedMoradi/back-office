import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormGroupDirective,
  Validators
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { VariationsService } from '../services/variations.service';
import { VariationsFacade } from '../+store/variations.facade';
import * as fromActions from '../+store/variations.actions';

@Component({
  selector: 'app-create-variation',
  templateUrl: './create-variation.component.html',
  styleUrls: ['./create-variation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateVariationComponent implements OnInit {
  @ViewChild('formParentDirective') formParentDirective: FormGroupDirective;
  @ViewChild('formChildDirective') formChildDirective: FormGroupDirective;
  variationParentForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public variationsFacade: VariationsFacade,
    private variationsService: VariationsService
  ) {}

  ngOnInit() {
    this.initializeVariationParentForm();
    this.listenFormReset();
    this.variationsFacade.dispatch(
      fromActions.getVariations({ page: 'pagination=false' })
    );
  }

  initializeVariationParentForm() {
    this.variationParentForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', []],
      parent: [null, []]
    });
  }

  listenFormReset() {
    this.variationsService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetVariationForm();
      });
  }

  resetVariationForm() {
    this.formParentDirective.resetForm();
    this.variationParentForm.reset();
  }

  onVariationParent() {
    this.variationParentForm.markAllAsTouched();
    if (this.variationParentForm.invalid) {
      return;
    }
    this.variationsFacade.dispatch(
      fromActions.createVariation({ request: this.variationParentForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

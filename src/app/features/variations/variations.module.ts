import { NgModule } from '@angular/core';

import { VariationsRoutingModule } from './variations-routing.module';
import { VariationsComponent } from './variations.component';
import { VariationsListComponent } from './variations-list/variations-list.component';
import { CreateVariationComponent } from './create-variation/create-variation.component';
import { SharedModule } from 'src/app/shared/shared.module';
// import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { VariationsEffects } from './+store/variations.effects';
import { EditVariationComponent } from './edit-variation/edit-variation.component';
// import * as fromReducer from './+store/variations.reducer';

@NgModule({
  declarations: [
    VariationsComponent,
    VariationsListComponent,
    CreateVariationComponent,
    EditVariationComponent
  ],
  imports: [
    SharedModule,
    VariationsRoutingModule,
    // StoreModule.forFeature(fromReducer.featureKey, fromReducer.reducer),
    EffectsModule.forFeature([VariationsEffects])
  ]
})
export class VariationsModule {}

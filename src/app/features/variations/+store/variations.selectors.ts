import { createSelector, createFeatureSelector } from '@ngrx/store';
import { VariationsState } from './variations.interface';
import { featureKey } from './variations.reducer';

export const selectFeature = createFeatureSelector<VariationsState>(featureKey);

export const selectError = createSelector(
  selectFeature,
  (state: VariationsState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: VariationsState) => state.isLoading
);
export const selectVariations = createSelector(
  selectFeature,
  (state: VariationsState) =>
    state.variations ? state.variations['hydra:member'] : null
);
export const selectVariationsTotalLength = createSelector(
  selectFeature,
  (state: VariationsState) =>
    state.variations ? state.variations['hydra:totalItems'] : null
);

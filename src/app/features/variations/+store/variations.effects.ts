import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './variations.actions';
import { VariationsService } from '../services/variations.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { VariationsFacade } from './variations.facade';
import { Router } from '@angular/router';

@Injectable()
export class VariationsEffects {
  constructor(
    private actions$: Actions,
    private variationsService: VariationsService,
    private matSnackBar: MatSnackBar,
    private variationsFacade: VariationsFacade,
    private router: Router
  ) {}

  getVariations$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getVariations),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.variationsService.getVariants(page).pipe(
          map((data: any) => {
            return fromActions.getVariationsSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.getVariationsFail({ error }));
          })
        );
      })
    )
  );

  createVariation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createVariation),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.variationsService.createVariant(request).pipe(
          map((data: any) => {
            this.variationsService.clearForm();
            this.matSnackBar.open('Variation Created');
            this.router.navigate(['/home/variations/variations-list']);
            return fromActions.createVariationSuccess({ response: data });
          }),
          catchError(error => of(fromActions.createVariationFail({ error })))
        );
      })
    )
  );

  getVariation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getVariation),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.variationsService.getVariant(id).pipe(
          map((data: any) => {
            return fromActions.getVariationSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getVariationFail({ error })))
        );
      })
    )
  );

  deleteVariation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteVariation),
      switchMap((action: any) => {
        const id = action.childId ? action.childId : action.parentId;
        return this.variationsService.deleteVariant(id).pipe(
          map((data: any) => {
            return fromActions.deleteVariationSuccess({
              parentId: action.parentId,
              childId: action.childId
            });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteVariationFail({ error }));
          })
        );
      })
    )
  );

  putVariation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putVariation),
      switchMap(({ id, request }) => {
        return this.variationsService.putVariant(id, request).pipe(
          map((data: any) => {
            this.variationsFacade.dispatch(
              fromActions.getVariations({ page: 'page=1' })
            );
            return fromActions.putVariationSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putVariationFail({ error }));
          })
        );
      })
    )
  );

  patchVariation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchVariation),
      switchMap(({ id, request }) => {
        return this.variationsService.patchVariant(id, request).pipe(
          map((data: any) => {
            return fromActions.patchVariationSuccess({ response: data });
          }),
          catchError(error => of(fromActions.patchVariationFail({ error })))
        );
      })
    )
  );
}

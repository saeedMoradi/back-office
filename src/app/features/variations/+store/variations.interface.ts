export interface VariationsState {
  error: any;
  isLoading: boolean;
  variations: any;
}

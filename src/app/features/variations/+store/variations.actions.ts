import { createAction, props } from '@ngrx/store';

export const getVariations = createAction(
  '[Variations] Get Variations',
  props<{ page: any }>()
);
export const getVariationsSuccess = createAction(
  '[Variations] Get Variations Success',
  props<{ response: any }>()
);
export const getVariationsFail = createAction(
  '[Variations] Get Variations Fail',
  props<{ error: any }>()
);

export const createVariation = createAction(
  '[Variations] Create Variation',
  props<{ request: any }>()
);
export const createVariationSuccess = createAction(
  '[Variations] Create Variation Success',
  props<{ response: any }>()
);
export const createVariationFail = createAction(
  '[Variations] Create Variation Fail',
  props<{ error: any }>()
);

export const getVariation = createAction(
  '[Variations] Get Variation',
  props<{ id: any }>()
);
export const getVariationSuccess = createAction(
  '[Variations] Get Variation Success',
  props<{ response: any }>()
);
export const getVariationFail = createAction(
  '[Variations] Get Variation Fail',
  props<{ error: any }>()
);

export const deleteVariation = createAction(
  '[Variations] Delete Variation',
  props<{ parentId: any; childId: any }>()
);
export const deleteVariationSuccess = createAction(
  '[Variations] Delete Variation Success',
  props<{ parentId: any; childId: any }>()
);
export const deleteVariationFail = createAction(
  '[Variations] Delete Variation Fail',
  props<{ error: any }>()
);

export const putVariation = createAction(
  '[Variations] Put Variation',
  props<{ id: any; request: any }>()
);
export const putVariationSuccess = createAction(
  '[Variations] Put Variation Success',
  props<{ response: any }>()
);
export const putVariationFail = createAction(
  '[Variations] Put Variation Fail',
  props<{ error: any }>()
);

export const patchVariation = createAction(
  '[Variations] Patch Variation',
  props<{ id: any; request: any }>()
);
export const patchVariationSuccess = createAction(
  '[Variations] Patch Variation Success',
  props<{ response: any }>()
);
export const patchVariationFail = createAction(
  '[Variations] Patch Variation Fail',
  props<{ error: any }>()
);

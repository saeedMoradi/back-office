import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './variations.selectors';

@Injectable({
  providedIn: 'root'
})
export class VariationsFacade implements Facade {
  isloading$: Observable<boolean>;
  error$: Observable<any>;
  variations$: Observable<any>;
  variationsTotalLength$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.variations$ = store.pipe(select(fromSelector.selectVariations));
    this.variationsTotalLength$ = store.pipe(
      select(fromSelector.selectVariationsTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

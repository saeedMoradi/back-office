import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './variations.actions';
import { VariationsState } from './variations.interface';

export const initialState: VariationsState = {
  error: null,
  isLoading: false,
  variations: null
};

export const featureKey = 'variations';

const variationsReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): VariationsState => ({ ...state, error: null })
  ),
  on(
    fromActions.getVariationsSuccess,
    (state, action): VariationsState => ({
      ...state,
      error: null,
      variations: action.response
    })
  ),
  on(
    fromActions.getVariationsFail,
    (state, action): VariationsState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createVariation,
    (state, action): VariationsState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createVariationFail,
    (state, action): VariationsState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteVariationSuccess,
    (state, action): VariationsState => {
      let newVariation;
      if (action.childId) {
        const index = state.variations['hydra:member'].findIndex(
          element => element.id === action.parentId
        );
        const childIndex = state.variations['hydra:member'][
          index
        ].childern.findIndex(element => element.id === action.childId);
        const newChildAccessory = removeItem(
          state.variations['hydra:member'][index].childern,
          childIndex
        );
        newVariation = state.variations['hydra:member'].map((data: any) => {
          if (state.variations['hydra:member'][index].id === action.parentId) {
            return { ...data, childern: newChildAccessory };
          }
          return data;
        });
      } else {
        newVariation = state.variations['hydra:member'].filter((item: any) => {
          return item.id !== action.parentId;
        });
      }
      return {
        ...state,
        error: null,
        variations: {
          ...state.variations,
          'hydra:member': newVariation,
          'hydra:totalItems': action.childId
            ? state.variations['hydra:totalItems']
            : state.variations['hydra:totalItems'] - 1
        }
      };
    }
  )
  // on(
  //   fromActions.putVariationSuccess,
  //   (state, action): VariationsState => {
  //     const newVariations = state.variations.map(item => {
  //       if (item.id !== action.response.id) {
  //         return item;
  //       }
  //       return action.response;
  //     });
  //     return {
  //       ...state,
  //       variations: newVariations
  //     };
  //   }
  // )
);

export function reducer(state: VariationsState, action: Action) {
  return variationsReducer(state, action);
}

function removeItem(array, index) {
  let newArray = array.slice();
  newArray.splice(index, 1);
  return newArray;
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateVariationComponent } from './create-variation/create-variation.component';
import { VariationsListComponent } from './variations-list/variations-list.component';

import { VariationsComponent } from './variations.component';

const routes: Routes = [
  {
    path: '',
    component: VariationsComponent,
    children: [
      { path: '', redirectTo: 'variations-list' },
      { path: 'variations-list', component: VariationsListComponent },
      { path: 'create-variation', component: CreateVariationComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VariationsRoutingModule {}

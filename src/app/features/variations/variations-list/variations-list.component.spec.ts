import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { RouterTestingModule } from '@angular/router/testing';
import { VariationsListComponent } from './variations-list.component';

describe('Variations List Component', () => {
  let spectator: Spectator<VariationsListComponent>;
  const createComponent = createComponentFactory({
    component: VariationsListComponent,
    imports: [RouterTestingModule]
  });

  beforeEach(() => (spectator = createComponent()));

  it('should create', () => {
    expect(spectator).toBeTruthy();
  });
});

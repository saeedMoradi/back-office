import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { PaginationData } from 'src/app/shared/interfaces';
import { EditVariationComponent } from '../edit-variation/edit-variation.component';
import { VariationsFacade } from '../+store/variations.facade';
import {
  getVariations,
  deleteVariation,
  putVariation
} from '../+store/variations.actions';

@Component({
  selector: 'app-variations-list',
  templateUrl: './variations-list.component.html',
  styleUrls: ['./variations-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VariationsListComponent implements OnInit {
  variations: any;
  originalVariations: any;
  uniqueKey: any;
  destroy$ = new Subject();

  constructor(
    public variationsFacade: VariationsFacade,
    private cd: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.variationsFacade.dispatch(getVariations({ page: 'page=1' }));
    this.variationsFacade.variations$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        if (data) {
          this.variations = data;
          this.originalVariations = data;
          if (!this.uniqueKey) {
            data.forEach(element => {
              this.uniqueKey = {
                ...this.uniqueKey,
                [element.id]: false
              };
            });
          }
          this.cd.detectChanges();
        }
      });
  }

  search(term = '') {
    this.variations = this.originalVariations.filter((element: any) => {
      return (
        element.title.toLowerCase().includes(term) ||
        element.description.toLowerCase().includes(term)
      );
    });
  }

  onRemove(parent, child = null) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.variationsFacade.dispatch(
          deleteVariation({ parentId: parent, childId: child })
        );
      }
    });
  }

  onEdit(data) {
    const dialogRef = this.dialog.open(EditVariationComponent, {
      autoFocus: false,
      data: {
        variants: this.variations,
        variant: data
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.variationsFacade.dispatch(
          putVariation({ id: data.id, request: result })
        );
      }
    });
  }

  onExpand(data) {
    this.uniqueKey = {
      ...this.uniqueKey,
      [data.id]: !this.uniqueKey[data.id]
    };
  }

  onChangePage(event: PaginationData) {
    this.variationsFacade.dispatch(
      getVariations({ page: `page=${event.pageIndex + 1}` })
    );
  }
  trackByFn(index: number, item: any) {
    return index;
  }

  trackByFnDetails(index: number, item: any) {
    return index;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

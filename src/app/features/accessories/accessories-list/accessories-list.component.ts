import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PaginationData } from 'src/app/shared/interfaces';
import { AccessoriesFacade } from '../+store/accessories.facade';
import {
  getAccessories,
  deleteAccessory,
  putAccessory
} from '../+store/accessories.actions';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { EditAccessoryComponent } from '../edit-accessory/edit-accessory.component';

@Component({
  selector: 'app-accessories-list',
  templateUrl: './accessories-list.component.html',
  styleUrls: ['./accessories-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccessoriesListComponent implements OnInit {
  accessories: any;
  originalAccessories: any;
  uniqueKey: any;
  destroy$ = new Subject();

  constructor(
    public accessoriesFacade: AccessoriesFacade,
    private cd: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.accessoriesFacade.dispatch(getAccessories({ page: 'page=1' }));
    this.accessoriesFacade.accessories$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: any) => {
        if (data) {
          this.accessories = data;
          this.originalAccessories = data;
          if (!this.uniqueKey) {
            data.forEach(element => {
              this.uniqueKey = {
                ...this.uniqueKey,
                [element.id]: false
              };
            });
          }
          this.cd.detectChanges();
        }
      });
  }

  search(term = '') {
    this.accessories = this.originalAccessories.filter((element: any) => {
      return element.title.toLowerCase().includes(term);
    });
  }

  onRemove(parent, child = null) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.accessoriesFacade.dispatch(
          deleteAccessory({ parentId: parent, childId: child })
        );
      }
    });
  }

  onEdit(data, identifier: string) {
    const dialogRef = this.dialog.open(EditAccessoryComponent, {
      autoFocus: false,
      data: {
        accessories: this.accessories,
        accessory: data,
        identifier
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.accessoriesFacade.dispatch(
          putAccessory({ id: data.id, request: result })
        );
      }
    });
  }

  onExpand(data) {
    this.uniqueKey = {
      ...this.uniqueKey,
      [data.id]: !this.uniqueKey[data.id]
    };
  }

  onChangePage(event: PaginationData) {
    this.accessoriesFacade.dispatch(
      getAccessories({ page: `page=${event.pageIndex + 1}` })
    );
  }

  trackByFn(index: number, item: any) {
    return index;
  }

  trackByFnDetails(index: number, item: any) {
    return index;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormGroupDirective,
  Validators
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AccessoriesFacade } from '../+store/accessories.facade';
import * as fromActions from '../+store/accessories.actions';
import { AccessoriesService } from '../services/accessories.service';
import { CategoriesFacade } from '../../categories/+store/categories.facade';
import { getCategories } from '../../categories/+store/categories.actions';

@Component({
  selector: 'app-create-accessory',
  templateUrl: './create-accessory.component.html',
  styleUrls: ['./create-accessory.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateAccessoryComponent implements OnInit {
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  accessoryForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public accessoriesFacade: AccessoriesFacade,
    public categoriesFacade: CategoriesFacade,
    private accessoriesService: AccessoriesService
  ) {}

  ngOnInit() {
    this.initializeAccessoryForm();
    this.listenFormReset();
    this.accessoriesFacade.dispatch(
      fromActions.getAccessories({ page: 'pagination=false' })
    );
    this.categoriesFacade.dispatch(getCategories({ page: 'pagination=false' }));
  }

  initializeAccessoryForm() {
    this.accessoryForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      parent: [null, []],
      price: [0, []],
      min: [0, []],
      max: [0, []],
      free: [0, []]
    });
  }

  listenFormReset() {
    this.accessoriesService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetAccessoryForm();
      });
  }

  resetAccessoryForm() {
    this.formDirective.resetForm();
    this.accessoryForm.reset();
  }

  onAccessory() {
    this.accessoryForm.markAllAsTouched();
    if (this.accessoryForm.invalid) {
      return;
    }
    this.accessoriesFacade.dispatch(
      fromActions.createAccessory({ request: this.accessoryForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccessoriesComponent } from './accessories.component';
import { AccessoriesListComponent } from './accessories-list/accessories-list.component';
import { CreateAccessoryComponent } from './create-accessory/create-accessory.component';

const routes: Routes = [
  {
    path: '',
    component: AccessoriesComponent,
    children: [
      { path: '', redirectTo: 'accessories-list' },
      { path: 'accessories-list', component: AccessoriesListComponent },
      { path: 'create-accessory', component: CreateAccessoryComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccessoriesRoutingModule {}

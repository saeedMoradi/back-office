import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-accessories',
  templateUrl: './accessories.component.html',
  styleUrls: ['./accessories.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccessoriesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { AccessoriesRoutingModule } from './accessories-routing.module';
import { AccessoriesComponent } from './accessories.component';
import { AccessoriesListComponent } from './accessories-list/accessories-list.component';
import { CreateAccessoryComponent } from './create-accessory/create-accessory.component';
import { EffectsModule } from '@ngrx/effects';
// import { StoreModule } from '@ngrx/store';
import { AccessoriesEffects } from './+store/accessories.effects';
import { EditAccessoryComponent } from './edit-accessory/edit-accessory.component';
import { CategoriesEffects } from '../categories/+store/categories.effects';
// import * as fromReducer from './+store/accessories.reducer';

@NgModule({
  declarations: [
    AccessoriesComponent,
    AccessoriesListComponent,
    CreateAccessoryComponent,
    EditAccessoryComponent
  ],
  imports: [
    SharedModule,
    AccessoriesRoutingModule,
    // StoreModule.forFeature(fromReducer.featureKey, fromReducer.reducer),
    EffectsModule.forFeature([AccessoriesEffects, CategoriesEffects])
  ]
})
export class AccessoriesModule {}

import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './accessories.selectors';

@Injectable({
  providedIn: 'root'
})
export class AccessoriesFacade implements Facade {
  isloading$: Observable<boolean>;
  error$: Observable<any>;
  accessories$: Observable<any>;
  accessoriesTotalLength$: Observable<any>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.accessories$ = store.pipe(select(fromSelector.selectAccessories));
    this.accessoriesTotalLength$ = store.pipe(
      select(fromSelector.selectAccessoriesTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

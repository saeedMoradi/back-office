import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './accessories.actions';
import { AccessoriesService } from '../services/accessories.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AccessoriesFacade } from './accessories.facade';
import { Router } from '@angular/router';

@Injectable()
export class AccessoriesEffects {
  constructor(
    private actions$: Actions,
    private accessoriesService: AccessoriesService,
    private matSnackBar: MatSnackBar,
    private accessoriesFacade: AccessoriesFacade,
    private router: Router
  ) {}

  getAccessories$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getAccessories),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.accessoriesService.getAccessories(page).pipe(
          map((data: any) => {
            return fromActions.getAccessoriesSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.getAccessoriesFail({ error }));
          })
        );
      })
    )
  );

  createAccessory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createAccessory),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.accessoriesService.createAccessory(request).pipe(
          map((data: any) => {
            this.accessoriesService.clearForm();
            this.matSnackBar.open('Accessory Created');
            // this.accessoriesFacade.dispatch(fromActions.getAccessories({page: 'page=1'}));
            this.router.navigate(['/home/accessories/accessories-list']);
            return fromActions.createAccessorySuccess({ response: data });
          }),
          catchError(error => of(fromActions.createAccessoryFail({ error })))
        );
      })
    )
  );

  getAccessory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getAccessory),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.accessoriesService.getAccessory(id).pipe(
          map((data: any) => {
            return fromActions.getAccessorySuccess({ response: data });
          }),
          catchError(error => of(fromActions.getAccessoryFail({ error })))
        );
      })
    )
  );

  deleteAccessory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteAccessory),
      switchMap((action: any) => {
        const id = action.childId ? action.childId : action.parentId;
        return this.accessoriesService.deleteAccessory(id).pipe(
          map((data: any) => {
            return fromActions.deleteAccessorySuccess({
              parentId: action.parentId,
              childId: action.childId
            });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteAccessoryFail({ error }));
          })
        );
      })
    )
  );

  putAccessory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putAccessory),
      switchMap(({ id, request }) => {
        return this.accessoriesService.putAccessory(id, request).pipe(
          map((data: any) => {
            this.accessoriesFacade.dispatch(
              fromActions.getAccessories({ page: 'page=1' })
            );
            return fromActions.putAccessorySuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putAccessoryFail({ error }));
          })
        );
      })
    )
  );

  patchAccessory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchAccessory),
      switchMap(({ id, request }) => {
        return this.accessoriesService.patchAccessory(id, request).pipe(
          map((data: any) => {
            return fromActions.patchAccessorySuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.patchAccessoryFail({ error }));
          })
        );
      })
    )
  );
}

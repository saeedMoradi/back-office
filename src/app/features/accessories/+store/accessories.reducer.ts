import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './accessories.actions';
import { AccessoriesState } from './accessories.interface';

export const initialState: AccessoriesState = {
  isLoading: false,
  accessories: null,
  error: null
};

export const featureKey = 'accessories';

const accessoriesReducer = createReducer(
  initialState,
  on(routerNavigationAction, (state): AccessoriesState => initialState),
  on(
    fromActions.getAccessoriesSuccess,
    (state, action): AccessoriesState => ({
      ...state,
      error: null,
      accessories: action.response
    })
  ),
  on(
    fromActions.getAccessoryFail,
    (state, action): AccessoriesState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createAccessory,
    (state, action): AccessoriesState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createAccessoryFail,
    (state, action): AccessoriesState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteAccessorySuccess,
    (state, action): AccessoriesState => {
      let newAccessories;
      if (action.childId) {
        const index = state.accessories['hydra:member'].findIndex(
          element => element.id === action.parentId
        );
        const childIndex = state.accessories['hydra:member'][
          index
        ].childern.findIndex(element => element.id === action.childId);
        const newChildAccessory = removeItem(
          state.accessories['hydra:member'][index].childern,
          childIndex
        );
        // const newChildAccessory = state.accessories['hydra:member'][
        //   index
        // ].childern.filter((item: any) => {
        //   return item.id !== action.childId;
        // });
        newAccessories = state.accessories['hydra:member'].map((data: any) => {
          if (state.accessories['hydra:member'][index].id === action.parentId) {
            return { ...data, childern: newChildAccessory };
          }
          return data;
        });
      } else {
        newAccessories = state.accessories['hydra:member'].filter(
          (item: any) => {
            return item.id !== action.parentId;
          }
        );
      }
      return {
        ...state,
        error: null,
        accessories: {
          ...state.accessories,
          'hydra:member': newAccessories,
          'hydra:totalItems': action.childId
            ? state.accessories['hydra:totalItems']
            : state.accessories['hydra:totalItems'] - 1
        }
      };
    }
  )
  // on(
  //   fromActions.putAccessorySuccess,
  //   (state, action): AccessoriesState => {
  //     const newAccessories = state.accessories.map(item => {
  //       if (item.id !== action.response.id) {
  //         return item;
  //       }
  //       return action.response;
  //     });
  //     return {
  //       ...state,
  //       accessories: newAccessories
  //     };
  //   }
  // )
);

export function reducer(state: AccessoriesState, action: Action) {
  return accessoriesReducer(state, action);
}

function removeItem(array, index) {
  let newArray = array.slice();
  newArray.splice(index, 1);
  return newArray;
}

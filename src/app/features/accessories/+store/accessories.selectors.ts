import { createSelector, createFeatureSelector } from '@ngrx/store';
import { AccessoriesState } from './accessories.interface';
import { featureKey } from './accessories.reducer';

export const selectFeature = createFeatureSelector<AccessoriesState>(
  featureKey
);

export const selectError = createSelector(
  selectFeature,
  (state: AccessoriesState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: AccessoriesState) => state.isLoading
);
export const selectAccessories = createSelector(
  selectFeature,
  (state: AccessoriesState) =>
    state.accessories ? state.accessories['hydra:member'] : null
);
export const selectAccessoriesTotalLength = createSelector(
  selectFeature,
  (state: AccessoriesState) =>
    state.accessories ? state.accessories['hydra:totalItems'] : null
);

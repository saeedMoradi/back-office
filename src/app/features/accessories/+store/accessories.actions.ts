import { createAction, props } from '@ngrx/store';

export const getAccessories = createAction(
  '[Accessories] Get Accessories',
  props<{ page: any }>()
);
export const getAccessoriesSuccess = createAction(
  '[Accessories] Get Accessories Success',
  props<{ response: any }>()
);
export const getAccessoriesFail = createAction(
  '[Accessories] Get Accessories Fail',
  props<{ error: any }>()
);

export const createAccessory = createAction(
  '[Accessories] Create Accessory',
  props<{ request: any }>()
);
export const createAccessorySuccess = createAction(
  '[Accessories] Create Accessory Success',
  props<{ response: any }>()
);
export const createAccessoryFail = createAction(
  '[Accessories] Create Accessory Fail',
  props<{ error: any }>()
);

export const getAccessory = createAction(
  '[Accessories] Get Accessory',
  props<{ id: any }>()
);
export const getAccessorySuccess = createAction(
  '[Accessories] Get Accessory Success',
  props<{ response: any }>()
);
export const getAccessoryFail = createAction(
  '[Accessories] Get Accessory Fail',
  props<{ error: any }>()
);

export const deleteAccessory = createAction(
  '[Accessories] Delete Accessory',
  props<{ parentId: any; childId: any }>()
);
export const deleteAccessorySuccess = createAction(
  '[Accessories] Delete Accessory Success',
  props<{ parentId: any; childId: any }>()
);
export const deleteAccessoryFail = createAction(
  '[Accessories] Delete Accessory Fail',
  props<{ error: any }>()
);

export const putAccessory = createAction(
  '[Accessories] Put Accessory',
  props<{ id: any; request: any }>()
);
export const putAccessorySuccess = createAction(
  '[Accessories] Put Accessory Success',
  props<{ response: any }>()
);
export const putAccessoryFail = createAction(
  '[Accessories] Put Accessory Fail',
  props<{ error: any }>()
);

export const patchAccessory = createAction(
  '[Accessories] Patch Accessory',
  props<{ id: any; request: any }>()
);
export const patchAccessorySuccess = createAction(
  '[Accessories] Patch Accessory Success',
  props<{ response: any }>()
);
export const patchAccessoryFail = createAction(
  '[Accessories] Patch Accessory Fail',
  props<{ error: any }>()
);

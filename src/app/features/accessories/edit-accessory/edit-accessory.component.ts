import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-edit-accessory',
  templateUrl: './edit-accessory.component.html',
  styleUrls: ['./edit-accessory.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditAccessoryComponent implements OnInit {
  accessoryForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditAccessoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.initializeAccessoryForm();
  }

  initializeAccessoryForm() {
    this.accessoryForm = this.formBuilder.group({
      title: [this.data.accessory.title, [Validators.required]],
      parent: [this.data.accessory.parent ? this.data.accessory.parent : null],
      // price: [this.data.accessory.price, []],
      min: [this.data.accessory.min, []],
      max: [this.data.accessory.max, []],
      free: [this.data.accessory.free, []]
    });
  }

  onAccessory() {
    this.accessoryForm.markAllAsTouched();
    if (this.accessoryForm.invalid) {
      return;
    }
    this.dialogRef.close(this.accessoryForm.value);
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

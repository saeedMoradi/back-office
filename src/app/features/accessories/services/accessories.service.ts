import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PersistanceService } from 'src/app/core/services/persistance.service';
import { LoginResponse } from 'src/app/shared/interfaces';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccessoriesService {
  private readonly apiUrl = environment.apiUrl;
  private clearFormSource = new Subject<boolean>();
  clearForm$ = this.clearFormSource.asObservable();

  constructor(
    private http: HttpClient,
    private persistanceService: PersistanceService
  ) {}

  clearForm() {
    this.clearFormSource.next(true);
  }

  getAccessories(page = ''): Observable<any> {
    const profile: LoginResponse = this.persistanceService.get('profile');
    return this.http.get<any>(
      `${this.apiUrl}/api/accessories?owner.id=${profile.id}&order[id]=desc&${page}`
    );
  }
  createAccessory(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/accessories`, data);
  }
  getAccessory(id: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/accessories/${id}`);
  }
  deleteAccessory(id: any): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/api/accessories/${id}`);
  }
  putAccessory(id: any, data: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/api/accessories/${id}`, data);
  }
  patchAccessory(id: any, data: any): Observable<any> {
    return this.http.patch<any>(`${this.apiUrl}/api/accessories/${id}`, data);
  }
}

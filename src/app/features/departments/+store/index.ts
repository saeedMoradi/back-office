import * as departmentsReducer from './departments.reducer';
import { DepartmentsState } from './departments.interface';
import * as departmentsSelectors from './departments.selectors';

export { departmentsReducer, departmentsSelectors, DepartmentsState };

import { Department } from 'src/app/shared/interfaces/department.interface';

export interface DepartmentsState {
  error: any;
  isLoading: boolean;
  departments: Department;
}

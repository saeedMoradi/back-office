import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as fromActions from './departments.actions';
import { DepartmentsService } from '../services/departments.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Department } from 'src/app/shared/interfaces/department.interface';

@Injectable()
export class DepartmentsEffects {
  constructor(
    private actions$: Actions,
    private departmentsService: DepartmentsService,
    private matSnackBar: MatSnackBar,
    private router: Router
  ) {}

  getDepartments$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getDepartments),
      map(action => {
        return action.page;
      }),
      switchMap(page => {
        return this.departmentsService.getDepartments(page).pipe(
          map((data: Department) => {
            return fromActions.getDepartmentsSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getDepartmentsFail({ error })))
        );
      })
    )
  );

  createDepartment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createDepartment),
      map(action => {
        return action.request;
      }),
      switchMap((request: any) => {
        return this.departmentsService.createDepartment(request).pipe(
          map((data: any) => {
            this.departmentsService.clearForm();
            this.matSnackBar.open('Department Created');
            this.router.navigate(['/home/departments/departments-list']);
            return fromActions.createDepartmentSuccess({ response: data });
          }),
          catchError(error => of(fromActions.createDepartmentFail({ error })))
        );
      })
    )
  );

  getDepartment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getDepartment),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.departmentsService.getDepartment(id).pipe(
          map((data: any) => {
            return fromActions.getDepartmentSuccess({ response: data });
          }),
          catchError(error => of(fromActions.getDepartmentFail({ error })))
        );
      })
    )
  );

  deleteDepartment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.deleteDepartment),
      map(action => {
        return action.id;
      }),
      switchMap((id: any) => {
        return this.departmentsService.deleteDepartment(id).pipe(
          map((data: any) => {
            return fromActions.deleteDepartmentSuccess({ id });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.deleteDepartmentFail({ error }));
          })
        );
      })
    )
  );

  putDepartment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.putDepartment),
      switchMap(({ id, request }) => {
        return this.departmentsService.putDepartment(id, request).pipe(
          map((data: any) => {
            return fromActions.putDepartmentSuccess({ response: data });
          }),
          catchError(error => {
            this.matSnackBar.open(error);
            return of(fromActions.putDepartmentFail({ error }));
          })
        );
      })
    )
  );

  patchDepartment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.patchDepartment),
      switchMap(({ id, request }) => {
        return this.departmentsService.patchDepartment(id, request).pipe(
          map((data: any) => {
            return fromActions.patchDepartmentSuccess({ response: data });
          }),
          catchError(error => of(fromActions.patchDepartmentFail({ error })))
        );
      })
    )
  );
}

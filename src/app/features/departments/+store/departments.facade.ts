import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Facade } from '../../../shared/interfaces';
import * as fromSelector from './departments.selectors';
import { Hydramember } from 'src/app/shared/interfaces/department.interface';

@Injectable({
  providedIn: 'root'
})
export class DepartmentsFacade implements Facade {
  isloading$: Observable<boolean>;
  error$: Observable<string>;
  departments$: Observable<Hydramember[]>;
  departmentsTotalLength$: Observable<number>;

  constructor(private readonly store: Store<any>) {
    this.isloading$ = store.pipe(select(fromSelector.selectIsLoading));
    this.error$ = store.pipe(select(fromSelector.selectError));
    this.departments$ = store.pipe(select(fromSelector.selectDepartments));
    this.departmentsTotalLength$ = store.pipe(
      select(fromSelector.selectDepartmentsTotalLength)
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

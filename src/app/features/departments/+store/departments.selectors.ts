import { createSelector, createFeatureSelector } from '@ngrx/store';
import { DepartmentsState } from './departments.interface';
import { featureKey } from './departments.reducer';

export const selectFeature = createFeatureSelector<DepartmentsState>(
  featureKey
);

export const selectError = createSelector(
  selectFeature,
  (state: DepartmentsState) => state.error
);
export const selectIsLoading = createSelector(
  selectFeature,
  (state: DepartmentsState) => state.isLoading
);
export const selectDepartments = createSelector(
  selectFeature,
  (state: DepartmentsState) =>
    state.departments ? state.departments['hydra:member'] : null
);
export const selectDepartmentsTotalLength = createSelector(
  selectFeature,
  (state: DepartmentsState) =>
    state.departments ? state.departments['hydra:totalItems'] : null
);

import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './departments.actions';
import { DepartmentsState } from './departments.interface';

export const initialState: DepartmentsState = {
  error: null,
  isLoading: false,
  departments: null
};

export const featureKey = 'departments';

const departmentsReducer = createReducer(
  initialState,
  on(
    routerNavigationAction,
    (state): DepartmentsState => ({ ...state, error: false })
  ),
  on(
    fromActions.getDepartmentsSuccess,
    (state, action): DepartmentsState => ({
      ...state,
      error: null,
      departments: action.response
    })
  ),
  on(
    fromActions.getDepartmentsFail,
    (state, action): DepartmentsState => ({
      ...state,
      error: action.error
    })
  ),
  on(
    fromActions.createDepartment,
    (state, action): DepartmentsState => ({
      ...state,
      isLoading: true,
      error: null
    })
  ),
  on(
    fromActions.createDepartmentSuccess,
    (state, action): DepartmentsState => {
      if (!state.departments) {
        return {
          ...state,
          departments: {
            'hydra:member': [action.response],
            'hydra:totalItems': 1
          }
        };
      }
      return {
        ...state,
        error: null,
        isLoading: false,
        departments: {
          ...state.departments,
          'hydra:member': [
            action.response,
            ...state.departments['hydra:member']
          ],
          'hydra:totalItems': state.departments['hydra:totalItems'] + 1
        }
      };
    }
  ),
  on(
    fromActions.createDepartmentFail,
    (state, action): DepartmentsState => ({
      ...state,
      isLoading: false,
      error: action.error
    })
  ),
  on(
    fromActions.deleteDepartmentSuccess,
    (state, action): DepartmentsState => {
      const newdepartments = state.departments['hydra:member'].filter(
        (item: any) => {
          return item.id !== action.id;
        }
      );
      return {
        ...state,
        error: null,
        departments: {
          ...state.departments,
          'hydra:member': newdepartments,
          'hydra:totalItems': state.departments['hydra:totalItems'] - 1
        }
      };
    }
  ),
  on(
    fromActions.putDepartmentSuccess,
    (state, action): DepartmentsState => {
      const newdepartments = state.departments['hydra:member'].map(item => {
        if (item.id !== action.response.id) {
          return item;
        }
        return action.response;
      });
      return {
        ...state,
        departments: { ...state.departments, 'hydra:member': newdepartments }
      };
    }
  )
);

export function reducer(state: DepartmentsState, action: Action) {
  return departmentsReducer(state, action);
}

import { createAction, props } from '@ngrx/store';
import { Department } from 'src/app/shared/interfaces/department.interface';

export const getDepartments = createAction(
  '[Departments] Get Departments',
  props<{ page: string }>()
);
export const getDepartmentsSuccess = createAction(
  '[Departments] Get Departments Success',
  props<{ response: Department }>()
);
export const getDepartmentsFail = createAction(
  '[Departments] Get Departments Fail',
  props<{ error: any }>()
);

export const createDepartment = createAction(
  '[Departments] Create Department',
  props<{ request: any }>()
);
export const createDepartmentSuccess = createAction(
  '[Departments] Create Department Success',
  props<{ response: any }>()
);
export const createDepartmentFail = createAction(
  '[Departments] Create Department Fail',
  props<{ error: any }>()
);

export const getDepartment = createAction(
  '[Departments] Get Department',
  props<{ id: any }>()
);
export const getDepartmentSuccess = createAction(
  '[Departments] Get Department Success',
  props<{ response: any }>()
);
export const getDepartmentFail = createAction(
  '[Departments] Get Department Fail',
  props<{ error: any }>()
);

export const deleteDepartment = createAction(
  '[Departments] Delete Department',
  props<{ id: any }>()
);
export const deleteDepartmentSuccess = createAction(
  '[Departments] Delete Department Success',
  props<{ id: any }>()
);
export const deleteDepartmentFail = createAction(
  '[Departments] Delete Department Fail',
  props<{ error: any }>()
);

export const putDepartment = createAction(
  '[Departments] Put Department',
  props<{ id: any; request: any }>()
);
export const putDepartmentSuccess = createAction(
  '[Departments] Put Department Success',
  props<{ response: any }>()
);
export const putDepartmentFail = createAction(
  '[Departments] Put Department Fail',
  props<{ error: any }>()
);

export const patchDepartment = createAction(
  '[Departments] Patch Department',
  props<{ id: any; request: any }>()
);
export const patchDepartmentSuccess = createAction(
  '[Departments] Patch Department Success',
  props<{ response: any }>()
);
export const patchDepartmentFail = createAction(
  '[Departments] Patch Department Fail',
  props<{ error: any }>()
);

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateDepartmentComponent } from './create-department/create-department.component';
import { DepartmentsListComponent } from './departments-list/departments-list.component';

import { DepartmentsComponent } from './departments.component';

const routes: Routes = [
  {
    path: '',
    component: DepartmentsComponent,
    children: [
      { path: '', redirectTo: 'departments-list' },
      { path: 'departments-list', component: DepartmentsListComponent },
      { path: 'create-department', component: CreateDepartmentComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepartmentsRoutingModule {}

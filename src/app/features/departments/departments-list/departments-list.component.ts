import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { DepartmentsFacade } from '../+store/departments.facade';
import {
  getDepartments,
  deleteDepartment,
  putDepartment
} from '../+store/departments.actions';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PaginationData } from 'src/app/shared/interfaces';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { EditDepartmentComponent } from '../edit-department/edit-department.component';
import { Hydramember } from 'src/app/shared/interfaces/department.interface';

@Component({
  selector: 'app-departments-list',
  templateUrl: './departments-list.component.html',
  styleUrls: ['./departments-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DepartmentsListComponent implements OnInit {
  departments: Hydramember[];
  originalDepartments: any;
  uniqueKey: any;
  destroy$ = new Subject();

  constructor(
    public departmentsFacade: DepartmentsFacade,
    private cd: ChangeDetectorRef,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.departmentsFacade.dispatch(getDepartments({ page: 'page=1' }));
    this.departmentsFacade.departments$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: Hydramember[]) => {
        if (data) {
          this.departments = data;
          this.originalDepartments = data;
          data.forEach(element => {
            this.uniqueKey = {
              ...this.uniqueKey,
              [element.id]: false
            };
          });
          this.cd.detectChanges();
        }
      });
  }

  search(term = '') {
    this.departments = this.originalDepartments.filter((element: any) => {
      return (
        element.title.toLowerCase().includes(term) ||
        element.description.toLowerCase().includes(term)
      );
    });
  }

  onRemove(data: Hydramember) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.departmentsFacade.dispatch(deleteDepartment({ id: data.id }));
      }
    });
  }

  onEdit(data: Hydramember) {
    const dialogRef = this.dialog.open(EditDepartmentComponent, {
      autoFocus: false,
      data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.departmentsFacade.dispatch(
          putDepartment({ id: data.id, request: result })
        );
      }
    });
  }

  onChangePage(event: PaginationData) {
    this.departmentsFacade.dispatch(
      getDepartments({ page: `page=${event.pageIndex + 1}` })
    );
  }

  trackByFn(index: number, item: any) {
    return index;
  }

  trackByFnDetails(index: number, item: any) {
    return index;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

import { NgModule } from '@angular/core';

import { DepartmentsRoutingModule } from './departments-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
// Components
import { DepartmentsComponent } from './departments.component';
import { CreateDepartmentComponent } from './create-department/create-department.component';
import { DepartmentsListComponent } from './departments-list/departments-list.component';
import { EditDepartmentComponent } from './edit-department/edit-department.component';
// NGRX
// import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { DepartmentsEffects } from './+store/departments.effects';
// import * as fromReducer from './+store/departments.reducer';

@NgModule({
  declarations: [
    DepartmentsComponent,
    CreateDepartmentComponent,
    DepartmentsListComponent,
    EditDepartmentComponent
  ],
  imports: [
    SharedModule,
    DepartmentsRoutingModule,
    // StoreModule.forFeature(fromReducer.featureKey, fromReducer.reducer),
    EffectsModule.forFeature([DepartmentsEffects])
  ]
})
export class DepartmentsModule {}

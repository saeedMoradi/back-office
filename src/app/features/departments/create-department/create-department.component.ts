import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormGroupDirective
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DepartmentsFacade } from '../+store/departments.facade';
import { DepartmentsService } from '../services/departments.service';
import * as fromActions from '../+store/departments.actions';

@Component({
  selector: 'app-create-department',
  templateUrl: './create-department.component.html',
  styleUrls: ['./create-department.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateDepartmentComponent implements OnInit {
  @ViewChild('formDirective') formDirective: FormGroupDirective;
  departmentForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    public departmentsFacade: DepartmentsFacade,
    private departmentsService: DepartmentsService
  ) {}

  ngOnInit() {
    this.initializeDepartmentForm();
    this.listenFormReset();
  }

  initializeDepartmentForm() {
    this.departmentForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', []]
    });
  }

  listenFormReset() {
    this.departmentsService.clearForm$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.resetDepartmentForm();
      });
  }

  resetDepartmentForm() {
    this.formDirective.resetForm();
    this.departmentForm.reset();
  }

  onDepartment() {
    this.departmentForm.markAllAsTouched();
    if (this.departmentForm.invalid) {
      return;
    }
    this.departmentsFacade.dispatch(
      fromActions.createDepartment({ request: this.departmentForm.value })
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Department } from 'src/app/shared/interfaces/department.interface';
import { PersistanceService } from 'src/app/core/services/persistance.service';
import { LoginResponse } from 'src/app/shared/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DepartmentsService {
  private readonly apiUrl = environment.apiUrl;
  private clearFormSource = new Subject<boolean>();
  clearForm$ = this.clearFormSource.asObservable();

  constructor(
    private http: HttpClient,
    private persistanceService: PersistanceService
  ) {}

  clearForm() {
    this.clearFormSource.next(true);
  }

  getDepartments(page = ''): Observable<Department> {
    const profile: LoginResponse = this.persistanceService.get('profile');
    return this.http.get<Department>(
      `${this.apiUrl}/api/departments?owner.id=${profile.id}&order[id]=desc&${page}`
    );
  }
  createDepartment(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/departments`, data);
  }
  getDepartment(id: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/departments/${id}`);
  }
  deleteDepartment(id: any): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/api/departments/${id}`);
  }
  putDepartment(id: any, data: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/api/departments/${id}`, data);
  }
  patchDepartment(id: any, data: any): Observable<any> {
    return this.http.patch<any>(`${this.apiUrl}/api/departments/${id}`, data);
  }
}

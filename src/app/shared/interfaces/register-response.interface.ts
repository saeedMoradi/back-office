export interface RegisterResponse {
  id: number;
  email: string;
  roles: string[];
  restaurantName?: any;
  firstName?: any;
  lastName?: any;
}

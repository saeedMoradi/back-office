export interface LoginResponse {
  token: string;
  id: number;
  firstName?: any;
  lastName?: any;
  username: string;
  email: string;
  restaurantName?: any;
  roles: string[];
}

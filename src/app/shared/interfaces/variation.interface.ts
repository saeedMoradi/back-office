export interface Variation {
  '@context': string;
  '@id': string;
  '@type': string;
  'hydra:member': Hydramember[];
  'hydra:totalItems': number;
  'hydra:view': Hydraview;
  'hydra:search': Hydrasearch;
}

export interface Hydrasearch {
  '@type': string;
  'hydra:template': string;
  'hydra:variableRepresentation': string;
  'hydra:mapping': Hydramapping[];
}

export interface Hydramapping {
  '@type': string;
  variable: string;
  property: string;
  required: boolean;
}

export interface Hydraview {
  '@id': string;
  '@type': string;
}

export interface Hydramember {
  '@id': string;
  '@type': string;
  id: number;
  title: string;
  childern: (Childern | Childern2 | Childern3)[];
  description: string;
  variants: string[];
  dependentVariant: any[];
  accessories: any[];
}

export interface Childern3 {
  '@id': string;
  '@type': string;
  id: number;
  title: string;
  childern: any[];
  description: string;
  parent: string;
  variants: any[];
  dependentVariant: string[];
  accessories: any[];
}

export interface Childern2 {
  '@id': string;
  '@type': string;
  id: number;
  title: string;
  childern: any[];
  description: string;
  parent: string;
  variants: any[];
  dependentVariant: string[];
  accessories: any[];
}

export interface Childern {
  '@id': string;
  '@type': string;
  id: number;
  title: string;
  childern: any[];
  description: string;
  parent: string;
  variants: any[];
  dependentVariant: any[];
  accessories: any[];
}

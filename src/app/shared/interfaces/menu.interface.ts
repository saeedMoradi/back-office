export interface Menu {
  '@context': string;
  '@id': string;
  '@type': string;
  'hydra:member': Hydramember[];
  'hydra:totalItems': number;
  'hydra:view': Hydraview;
  'hydra:search': Hydrasearch;
}

export interface Hydrasearch {
  '@type': string;
  'hydra:template': string;
  'hydra:variableRepresentation': string;
  'hydra:mapping': Hydramapping[];
}

export interface Hydramapping {
  '@type': string;
  variable: string;
  property: string;
  required: boolean;
}

export interface Hydraview {
  '@id': string;
  '@type': string;
}

export interface Hydramember {
  '@id': string;
  '@type': string;
  id: number;
  shop: string;
  title: string;
  description: string;
  productHasMenu: ProductHasMenu[];
  menuAvailableTimes: MenuAvailableTime[];
}

export interface MenuAvailableTime {
  id: number;
  days: Day[];
  menu: string;
  fromDate: string;
  toDate: string;
}

export interface Day {
  day: string;
  times: Time[];
}

export interface Time {
  open: boolean;
  close: boolean;
}

export interface ProductHasMenu {
  id: number;
  menu: string;
  price: number;
  product: Product;
}

export interface Product {
  '@id': string;
  '@type': string;
  id: number;
  title: string;
  description: string;
  tax: number;
  price: number;
  category: string;
  images: any[];
  multiPrice: any[];
  allergies: any[];
  productTypes: any[];
  variantsDependencies: any[];
  basePrice: number;
  productVariant: ProductVariant[];
  productAccessories: any[];
  shops: string[];
  department: string;
  status: boolean;
  step?: string;
  isDefault: boolean;
  discounts: (string | string)[];
}

export interface ProductVariant {
  id: number;
  Variant: Variant;
  Product: string;
}

export interface Variant {
  '@id': string;
  '@type': string;
  id: number;
  title: string;
  childern: any[];
  description: string;
  parent: string;
  variants: any[];
  dependentVariant: any[];
  accessories: any[];
}

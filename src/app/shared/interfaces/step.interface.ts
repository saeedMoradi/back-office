export interface Step {
  '@context': string;
  '@id': string;
  '@type': string;
  'hydra:member': Hydramember[];
  'hydra:totalItems': number;
  'hydra:view': Hydraview;
}

export interface Hydraview {
  '@id': string;
  '@type': string;
}

export interface Hydramember {
  '@id': string;
  '@type': string;
  id: number;
  title: string;
  foods: string[];
  minimumPick: number;
  maximumPick: number;
  description: string;
  status: boolean;
  sets: string;
}

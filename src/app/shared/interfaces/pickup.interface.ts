export interface Pickup {
  '@context': string;
  '@id': string;
  '@type': string;
  'hydra:member': Hydramember[];
  'hydra:totalItems': number;
  'hydra:view': Hydraview;
  'hydra:search': Hydrasearch;
}

export interface Hydrasearch {
  '@type': string;
  'hydra:template': string;
  'hydra:variableRepresentation': string;
  'hydra:mapping': Hydramapping[];
}

export interface Hydramapping {
  '@type': string;
  variable: string;
  property: string;
  required: boolean;
}

export interface Hydraview {
  '@id': string;
  '@type': string;
}

export interface Hydramember {
  '@id': string;
  '@type': string;
  id: number;
  time: number;
  availableTimes: AvailableTime[];
  status: boolean;
  fromDate?: string;
  toDate?: string;
}

export interface AvailableTime {
  id: number;
  times: Time[];
  pickups: string;
  day: string;
}

export interface Time {
  open: Open;
  close: Open;
  maxCustomer: number | string;
}

export interface Open {
  date: string;
  timezone: string;
  timezone_type: number;
}

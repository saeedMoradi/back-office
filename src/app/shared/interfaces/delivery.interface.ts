export interface Delivery {
  '@context': string;
  '@id': string;
  '@type': string;
  'hydra:member': Hydramember[];
  'hydra:totalItems': number;
  'hydra:view': Hydraview;
  'hydra:search': Hydrasearch;
}

export interface Hydrasearch {
  '@type': string;
  'hydra:template': string;
  'hydra:variableRepresentation': string;
  'hydra:mapping': Hydramapping[];
}

export interface Hydramapping {
  '@type': string;
  variable: string;
  property: string;
  required: boolean;
}

export interface Hydraview {
  '@id': string;
  '@type': string;
}

export interface Hydramember {
  '@id': string;
  '@type': string;
  id: number;
  area: Area;
  deliveryFee: number;
  min: string;
  max: string;
  deliveryAvailableTimes: DeliveryAvailableTime[];
  deliveryTime: number;
  shop: string;
  name: string;
  toDate: string;
  fromDate: string;
}

export interface DeliveryAvailableTime {
  id: number;
  times: Time[];
  deliveryZone: string;
  day: string;
}

export interface Time {
  open: Open;
  close: Open;
}

export interface Open {
  date: string;
  timezone: string;
  timezone_type: number;
}

export interface Area {
  type: string;
  coordinates: number[][][];
  srid?: any;
}

export interface Accessory {
  '@context': string;
  '@id': string;
  '@type': string;
  'hydra:member': Hydramember[];
  'hydra:totalItems': number;
  'hydra:view': Hydraview;
  'hydra:search': Hydrasearch;
}

export interface Hydrasearch {
  '@type': string;
  'hydra:template': string;
  'hydra:variableRepresentation': string;
  'hydra:mapping': Hydramapping[];
}

export interface Hydramapping {
  '@type': string;
  variable: string;
  property: string;
  required: boolean;
}

export interface Hydraview {
  '@id': string;
  '@type': string;
}

export interface Hydramember {
  '@id': string;
  '@type': string;
  id: number;
  title: string;
  parent?: any;
  childern: Childern[];
  price?: any;
  min: number;
  max: number;
  free?: any;
  category: string;
  variantHasAccessories: any[];
  productHasAccessories: any[];
  owner: any[];
}

export interface Childern {
  '@id': string;
  '@type': string;
  id: number;
  title: string;
  parent: string;
  childern: any[];
  price?: any;
  min: number;
  max: number;
  free: number;
  category: string;
  variantHasAccessories: any[];
  productHasAccessories: any[];
  owner: any[];
}

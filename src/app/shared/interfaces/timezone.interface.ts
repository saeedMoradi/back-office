export interface TimeZone {
  timezones: string[];
  latlng: number[];
  name: string;
  country_code: string;
  capital: string;
}

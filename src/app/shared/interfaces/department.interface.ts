export interface Department {
  '@context'?: string;
  '@id'?: string;
  '@type'?: string;
  'hydra:member': Hydramember[];
  'hydra:totalItems': number;
  'hydra:view'?: Hydraview;
  'hydra:search'?: Hydrasearch;
}

export interface Hydrasearch {
  '@type': string;
  'hydra:template': string;
  'hydra:variableRepresentation': string;
  'hydra:mapping': Hydramapping[];
}

export interface Hydramapping {
  '@type': string;
  variable: string;
  property: string;
  required: boolean;
}

export interface Hydraview {
  '@id': string;
  '@type': string;
  'hydra:first': string;
  'hydra:last': string;
  'hydra:next': string;
}

export interface Hydramember {
  '@id': string;
  '@type': string;
  id: number;
  title: string;
  description: string;
  categories: any[];
}

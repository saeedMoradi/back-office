export interface VerifyRequest {
  code: string;
  email: string;
  password: string;
  confirmPassword: string;
}

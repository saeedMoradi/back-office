export interface Discount {
  '@context': string;
  '@id': string;
  '@type': string;
  'hydra:member': Hydramember[];
  'hydra:totalItems': number;
  'hydra:view': Hydraview;
  'hydra:search': Hydrasearch;
}

export interface Hydrasearch {
  '@type': string;
  'hydra:template': string;
  'hydra:variableRepresentation': string;
  'hydra:mapping': Hydramapping[];
}

export interface Hydramapping {
  '@type': string;
  variable: string;
  property: string;
  required: boolean;
}

export interface Hydraview {
  '@id': string;
  '@type': string;
  'hydra:first': string;
  'hydra:last': string;
  'hydra:next': string;
}

export interface Hydramember {
  '@id': string;
  '@type': string;
  totalGiftPrice: number;
  id: number;
  name: string;
  description: string;
  type: string;
  isExclusive: boolean;
  priority: number;
  userLimitation: string[];
  fromDate: string;
  toDate: string;
  minCartPrice: number;
  cartSpecificPrice: number;
  numberOfusePerUser: number;
  shop: string[];
  freeDelivery: boolean;
  userType: string;
  amount: number;
  category?: string;
  gifts?: (any[] | Gifts2 | Gifts3)[];
  discountType?: string;
  products: string[];
  code?: string;
  expirationDate?: string;
}

export interface Gifts3 {
  id: string;
  gift: Gift2[];
  quantity: number;
}

export interface Gift2 {
  id: string;
  quantity: number;
}

export interface Gifts2 {
  id: string;
  gift: Gift[];
  catId: string;
  quantity: number;
}

export interface Gift {
  id: string;
  catId: string;
  quantity: number;
}

export interface ShopForm {
  title: any;
  zipCode: any;
  location: any;
  merchant: any;
  timeZone: any;
  city: any;
  country: any;
  streetNameNumber: any;
  openingTime: any;
  closingTime: any;
  currency: any;
  url: any;
}

export interface Shop {
  '@context'?: string;
  '@id'?: string;
  '@type'?: string;
  'hydra:member': Hydramember[];
  'hydra:totalItems': number;
  'hydra:view'?: Hydraview;
  'hydra:search'?: Hydrasearch;
}

export interface Hydrasearch {
  '@type': string;
  'hydra:template': string;
  'hydra:variableRepresentation': string;
  'hydra:mapping': Hydramapping[];
}

export interface Hydramapping {
  '@type': string;
  variable: string;
  property: string;
  required: boolean;
}

export interface Hydraview {
  '@id': string;
  '@type': string;
}

export interface Hydramember {
  '@id': string;
  '@type': string;
  id: number;
  createdAt: string;
  updatedAt: string;
  title: string;
  zipCode: string;
  location: Location;
  merchant: string;
  timeZone: string;
  city: string;
  country: string;
  streetNameNumber: string;
  currency: string;
  url?: any;
  managers: string[];
  pickups: any[];
  preOrders: any[];
  worktimes: Worktime[];
  buildingNumber: string;
  address: string;
  state: string;
  email: string;
  phone: string;
  products: string[];
  discounts: string[];
  deliveryZones: string[];
  menus: string[];
  sets: string[];
  mealDeal?: any;
  setting?: any;
  status: any;
}

export interface Worktime {
  id: number;
  day: string;
  Shop: string;
  times: Time[];
}

export interface Time {
  open: Open;
  close: Open;
}

export interface Open {
  date: string;
  timezone: string;
  timezone_type: number;
}

export interface Location {
  type: string;
  coordinates: number[];
  srid?: any;
}

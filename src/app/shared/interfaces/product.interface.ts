export interface Product {
  '@context': string;
  '@id': string;
  '@type': string;
  'hydra:member': Hydramember[];
  'hydra:totalItems': number;
  'hydra:view': Hydraview;
  'hydra:search': Hydrasearch;
}

export interface Hydrasearch {
  '@type': string;
  'hydra:template': string;
  'hydra:variableRepresentation': string;
  'hydra:mapping': Hydramapping[];
}

export interface Hydramapping {
  '@type': string;
  variable: string;
  property: string;
  required: boolean;
}

export interface Hydraview {
  '@id': string;
  '@type': string;
}

export interface Hydramember {
  '@id': string;
  '@type': string;
  id: number;
  title: string;
  description: string;
  tax: number;
  price: number;
  category: string;
  images: any[];
  multiPrice?: any[];
  allergies: any[];
  productTypes: any[];
  variantsDependencies: string[];
  basePrice: number;
  productVariant: (ProductVariant | ProductVariant2)[];
  productAccessories: any[];
  merchant?: any;
  manager?: any;
  shops: string[];
  department: string;
  owner?: any;
  status: boolean;
  step?: string;
  isDefault: boolean;
  discounts: string[];
}

export interface ProductVariant2 {
  id: number;
  Variant: Variant2;
  Product: string;
}

export interface Variant2 {
  '@id': string;
  '@type': string;
  id: number;
  title: string;
  childern: any[];
  description: string;
  parent: string;
  variants: any[];
  dependentVariant: string[];
  accessories: any[];
}

export interface ProductVariant {
  id: number;
  Variant: Variant;
  Product: string;
}

export interface Variant {
  '@id': string;
  '@type': string;
  id: number;
  title: string;
  childern: any[];
  description: string;
  parent: string;
  variants: any[];
  dependentVariant: any[];
  accessories: any[];
}

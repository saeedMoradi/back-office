export interface PreOrder {
  '@context': string;
  '@id': string;
  '@type': string;
  'hydra:member': Hydramember[];
  'hydra:totalItems': number;
  'hydra:view': Hydraview;
  'hydra:search': Hydrasearch;
}

export interface Hydrasearch {
  '@type': string;
  'hydra:template': string;
  'hydra:variableRepresentation': string;
  'hydra:mapping': Hydramapping[];
}

export interface Hydramapping {
  '@type': string;
  variable: string;
  property: string;
  required: boolean;
}

export interface Hydraview {
  '@id': string;
  '@type': string;
}

export interface Hydramember {
  '@id': string;
  '@type': string;
  id: number;
  preOrderAvailableTimes: PreOrderAvailableTime[];
}

export interface PreOrderAvailableTime {
  id: number;
  times: Time[];
  day: string;
  preorder: string;
}

export interface Time {
  open: Open | Open | boolean | boolean | boolean;
  close: Open | Open | boolean | boolean;
}

export interface Open {
  date: string;
  timezone: string;
  timezone_type: number;
}

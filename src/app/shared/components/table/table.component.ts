import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  EventEmitter,
  Output
} from '@angular/core';
import { PaginationData } from '../../interfaces';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableComponent implements OnInit {
  _source: any;
  uniqueKey: any;
  @Input() set source(value) {
    if (!value) {
      this._source = null;
    } else {
      this._source = value;
      value.forEach(element => {
        this.uniqueKey = {
          ...this.uniqueKey,
          [element[this.sourceKey]]: false
        };
      });
    }
  }
  @Input('sourceKey') sourceKey: string;
  @Input('detailsKey') detailsKey: string;
  @Input('childsKey') childsKey: string;
  @Input('sourceCaptions') sourceCaptions: any[];
  @Input('detailsCaptions') detailsCaptions: any[];
  @Input('expand') expand = true;
  @Input('remove') remove = true;
  @Input('edit') edit = true;
  @Output() onRemoveItem = new EventEmitter();
  @Output() onEditElement = new EventEmitter();
  @Output() onPageData = new EventEmitter<PaginationData>();

  constructor() {}

  ngOnInit(): void {}

  onRemove(data) {
    this.onRemoveItem.emit(data);
  }

  onEdit(data) {
    this.onEditElement.emit(data);
  }

  onExpand(data) {
    this.uniqueKey = {
      ...this.uniqueKey,
      [data[this.sourceKey]]: !this.uniqueKey[data[this.sourceKey]]
    };
  }

  onChangePage(event: PaginationData) {
    this.onPageData.emit(event);
  }

  trackByFn(index: number, item: any) {
    return item[this.sourceKey];
  }
  trackByFnDetails(index: number, item: any) {
    return item[this.detailsKey];
  }
}

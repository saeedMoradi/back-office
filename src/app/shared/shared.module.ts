import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
// Pipes
import { TruncatePipe } from './pipes/truncate.pipe';
import { StatusPipe } from './pipes/status.pipe';
import { CurrencySymbolPipe } from './pipes/custom-currenct.pipe';
// Material Modules
import {
  MatSnackBarModule,
  MAT_SNACK_BAR_DEFAULT_OPTIONS
} from '@angular/material/snack-bar';
import { TableComponent } from './components/table/table.component';
import { PrefetchDirective } from './directives/prefetch.directive';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import {
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  MatMomentDateModule
} from '@angular/material-moment-adapter';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
// Compinents
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { TimeDialogComponent } from './components/time-dialog/time-dialog.component';

const MATERIAL_MODULES = [
  MatSnackBarModule,
  MatIconModule,
  MatButtonModule,
  MatPaginatorModule,
  MatInputModule,
  MatSelectModule,
  MatDialogModule,
  MatCheckboxModule,
  MatDividerModule,
  MatSlideToggleModule,
  MatExpansionModule,
  MatProgressBarModule,
  MatDatepickerModule,
  MatMomentDateModule
];

@NgModule({
  declarations: [
    TruncatePipe,
    CurrencySymbolPipe,
    StatusPipe,
    TableComponent,
    PrefetchDirective,
    ConfirmDialogComponent,
    TimeDialogComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    NgxMaterialTimepickerModule,
    ...MATERIAL_MODULES
  ],
  providers: [
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: {
        duration: 3000,
        panelClass: 'snack',
        verticalPosition: 'bottom',
        horizontalPosition: 'right'
      }
    }
  ],
  exports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    TableComponent,
    PrefetchDirective,
    ...MATERIAL_MODULES,
    NgxMaterialTimepickerModule,
    ConfirmDialogComponent,
    TimeDialogComponent,
    TruncatePipe,
    CurrencySymbolPipe,
    StatusPipe
  ]
})
export class SharedModule {}

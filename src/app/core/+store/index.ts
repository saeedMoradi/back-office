import { Action, ActionReducerMap } from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import { InjectionToken } from '@angular/core';
import {
  merchantsReducer,
  MerchantsState
} from '../../features/merchants/+store';
import {
  accessoriesReducer,
  AccessoriesState
} from '../../features/accessories/+store';
import {
  allergiesReducer,
  AllergiesState
} from '../../features/allergies/+store';
import {
  categoriesReducer,
  CategoriesState
} from '../../features/categories/+store';
import {
  departmentsReducer,
  DepartmentsState
} from '../../features/departments/+store';
import { managersReducer, ManagersState } from '../../features/managers/+store';
import {
  mealdealsReducer,
  MealdealsState
} from '../../features/mealdeals/+store';
import { productsReducer, ProductsState } from '../../features/products/+store';
import { shopsReducer, ShopsState } from '../../features/shops/+store';
import {
  variationsReducer,
  VariationsState
} from '../../features/variations/+store';
import { pickupsReducer, PickupsState } from '../../features/pickups/+store';
import { typesReducer, TypesState } from '../../features/types/+store';
import {
  deliveriesReducer,
  DeliveriesState
} from '../../features/deliveries/+store';
import {
  preordersReducer,
  PreOrdersState
} from '../../features/preorders/+store';
import {
  discountsReducer,
  DiscountsState
} from '../../features/discounts/+store';
import { menusReducer, MenusState } from '../../features/menus/+store';
import { generalReducer, SettingsState } from '../../features/general/+store';
import { taxReducer, TaxesState } from '../../features/tax/+store';

export interface State {
  router: fromRouter.RouterReducerState<any>;
  [merchantsReducer.featureKey]: MerchantsState;
  [accessoriesReducer.featureKey]: AccessoriesState;
  [allergiesReducer.featureKey]: AllergiesState;
  [categoriesReducer.featureKey]: CategoriesState;
  [departmentsReducer.featureKey]: DepartmentsState;
  [managersReducer.featureKey]: ManagersState;
  [mealdealsReducer.featureKey]: MealdealsState;
  [productsReducer.featureKey]: ProductsState;
  [shopsReducer.featureKey]: ShopsState;
  [variationsReducer.featureKey]: VariationsState;
  [pickupsReducer.featureKey]: PickupsState;
  [typesReducer.featureKey]: TypesState;
  [preordersReducer.featureKey]: PreOrdersState;
  [deliveriesReducer.featureKey]: DeliveriesState;
  [discountsReducer.featureKey]: DiscountsState;
  [menusReducer.featureKey]: MenusState;
  [generalReducer.featureKey]: SettingsState;
  [taxReducer.featureKey]: TaxesState;
}

export const ROOT_REDUCERS = new InjectionToken<
  ActionReducerMap<State, Action>
>('Root reducers token', {
  factory: () => ({
    router: fromRouter.routerReducer,
    [merchantsReducer.featureKey]: merchantsReducer.reducer,
    [accessoriesReducer.featureKey]: accessoriesReducer.reducer,
    [allergiesReducer.featureKey]: allergiesReducer.reducer,
    [categoriesReducer.featureKey]: categoriesReducer.reducer,
    [departmentsReducer.featureKey]: departmentsReducer.reducer,
    [managersReducer.featureKey]: managersReducer.reducer,
    [mealdealsReducer.featureKey]: mealdealsReducer.reducer,
    [productsReducer.featureKey]: productsReducer.reducer,
    [shopsReducer.featureKey]: shopsReducer.reducer,
    [variationsReducer.featureKey]: variationsReducer.reducer,
    [pickupsReducer.featureKey]: pickupsReducer.reducer,
    [typesReducer.featureKey]: typesReducer.reducer,
    [preordersReducer.featureKey]: preordersReducer.reducer,
    [deliveriesReducer.featureKey]: deliveriesReducer.reducer,
    [discountsReducer.featureKey]: discountsReducer.reducer,
    [menusReducer.featureKey]: menusReducer.reducer,
    [generalReducer.featureKey]: generalReducer.reducer,
    [taxReducer.featureKey]: taxReducer.reducer
  })
});

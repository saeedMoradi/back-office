import { Injectable } from '@angular/core';
import { Store, Action, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromReducer from './router.reducer';
import { Facade } from '../../shared/interfaces';

@Injectable({
  providedIn: 'root'
})
export class RouterFacade implements Facade {
  selectCurrentRoute$: Observable<any>;
  selectFragment$: Observable<any>;
  selectQueryParams$: Observable<any>;
  selectQueryParam$: Observable<any>;
  selectRouteParams$: Observable<any>;
  selectRouteParam$: Observable<any>;
  selectRouteData$: Observable<any>;
  selectUrl$: Observable<any>;

  constructor(private readonly store: Store<fromReducer.RouterState>) {
    this.selectCurrentRoute$ = store.pipe(
      select(fromReducer.selectCurrentRoute)
    );
    this.selectFragment$ = store.pipe(select(fromReducer.selectFragment));
    this.selectQueryParams$ = store.pipe(select(fromReducer.selectQueryParams));
    this.selectRouteParams$ = store.pipe(select(fromReducer.selectRouteParams));
    this.selectRouteData$ = store.pipe(select(fromReducer.selectRouteData));
    this.selectUrl$ = store.pipe(select(fromReducer.selectUrl));
  }

  selectQueryParam(param) {
    this.selectRouteParam$ = this.store.pipe(
      select(fromReducer.selectQueryParam(param))
    );
  }

  selectRouteParam(param) {
    this.selectRouteParam$ = this.store.pipe(
      select(fromReducer.selectRouteParam(param))
    );
  }

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}

import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { fadeInAnimation } from 'src/app/core/animations/fade-in.animation';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
  animations: [fadeInAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainLayoutComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit() {}

  getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
}

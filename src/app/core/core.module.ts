import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AuthModule } from 'src/app/features/auth/auth.module';
import { environment } from 'src/environments/environment';
// NGRX
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { ROOT_REDUCERS } from './+store';
// Components
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
// 3rd Modules
// import * as WebFont from 'webfontloader';

// WebFont.load({
//   custom: { families: ['Material Icons', 'Material Icons Outline'] }
// });

@NgModule({
  declarations: [MainLayoutComponent],
  imports: [
    BrowserAnimationsModule,
    RouterModule,
    HttpClientModule,
    AuthModule,
    StoreModule.forRoot(ROOT_REDUCERS, {}),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    }),
    EffectsModule.forRoot([]),
    StoreRouterConnectingModule.forRoot()
  ],
  exports: [MainLayoutComponent]
})
export class CoreModule {}
